<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Iblock\InheritedProperty;

class CNovostiDetail extends CBitrixComponent
{
    public $el;
    public $elementCode;
    public $arMonthsNormal = array(
        '01' => 'january',
        '02' => 'february',
        '03' => 'march',
        '04' => 'april',
        '05' => 'may',
        '06' => 'june',
        '07' => 'july',
        '08' => 'august',
        '09' => 'september',
        '10' => 'october',
        '11' => 'november',
        '12' => 'december',
    );

    public function onPrepareComponentParams($arParams)
    {
        if (!CModule::IncludeModule("iblock")) {
            die('Error including module iblock');
        }

        $this->el = new CIBlockElement;
        $this->elementCode = $_REQUEST['ELEMENT_CODE'];
        return $arParams;
    }

    public function executeComponent()
    {
        $this->getMonths();
        $this->getYears();
        $this->arResult['ITEM'] = $this->getNewsDetail();
        $this->setSEO();
        $this->arResult['BACK_URL'] = $this->getBackUrl($this->arResult['ITEM']['TIMESTAMP']);
        $this->includeComponentTemplate();
        return $this->arResult;
    }


    /**
     * @param $date - timestemp дата создания новости
     * @return string ссылка на раздел назад
     */
    public function getBackUrl($date){
        if(intval($date) > 0){
            $year = date('Y',$date);
            $montheNumber = date('m',$date);
            $monthe = $this->arMonthsNormal[$montheNumber];

            if(intval($year) > 0 && strlen($monthe) > 0){
                return '/news/'.$year."/".$monthe."/";
            } else {
                return $this->arParams['BACK_URL'];
            }
        } else {
            return $this->arParams['BACK_URL'];
        }
    }


    public function getNewsDetail()
    {
        $dbRes = $this->el->GetList(
            array(),
            array(
                'IBLOCK_ID' => '24',
                'CODE' => $this->elementCode,
                'ACTIVE' => 'Y',
            ),
            false,
            array(),
            array(
                'ID',
                'NAME',
                'DETAIL_TEXT',
                'ACTIVE_FROM'
            )
        );

        if ($item = $dbRes->Fetch()) {

            // Получить активный год и месяц

            $this->arResult['ACTIVE_DATE'] = $this->getActiveDate($item['ACTIVE_FROM']);
            $timesTamp = strtotime($item['ACTIVE_FROM']);
            // Получить отформатированную дату
            $arDate = ParseDateTime($item['ACTIVE_FROM'], FORMAT_DATETIME);
            $item['ACTIVE_FROM'] = $arDate["DD"]." ".ToLower(GetMessage("MONTH_".intval($arDate["MM"])."_S"))." ".$arDate["YYYY"];

            return array(
                'NAME' => $item['NAME'],
                'DETAIL_TEXT' => $item['DETAIL_TEXT'],
                'DATE' => $item['ACTIVE_FROM'],
                'TIMESTAMP' => $timesTamp
            );
        } else {
            $newsName = $this->el->GetList(
                array(),
                array(
                    'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE'],
                    'ID' => $this->elementCode,
                    'ACTIVE' => 'Y',
                ),
                false,
                array(),
                array(
                    'ID',
                    'IBLOCK_ID',
                    'CODE'
                )
            )->Fetch();

            header("HTTP/1.1 301 Moved Permanently");
            header("Location: http://" . $_SERVER["HTTP_HOST"] . "/news/detail/" . $newsName["CODE"]);
            exit();
        }
    }

    public function getYears()
    {
        $dbRes = CIBlockSection::GetList(
            array("NAME"=>"DESC"),
            array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE']),
            false,
            array('ID', 'NAME', 'CODE'),
            false
        );
        while($arSection = $dbRes->Fetch()){
            $this->arResult['YEARS'][] = $arSection;
        }


    }
    public function getMonths()
    {
        $this->arResult['MONTHS'] = array(
            '01' => array('RU' => 'Январь', 'EN' => 'january'),
            '02' => array('RU' => 'Февраль','EN' => 'february'),
            '03' => array('RU' => 'Март','EN' => 'march'),
            '04' => array('RU' => 'Апрель','EN' => 'april'),
            '05' => array('RU' => 'Май','EN' => 'may'),
            '06' => array('RU' => 'Июнь','EN' => 'june'),
            '07' => array('RU' => 'Июль','EN' => 'july'),
            '08' => array('RU' => 'Август','EN' => 'august'),
            '09' => array('RU' => 'Сентябрь','EN' => 'september'),
            '10' => array('RU' => 'Октябрь','EN' => 'october'),
            '11' => array('RU' => 'Ноябрь','EN' => 'november'),
            '12' => array('RU' => 'Декабрь','EN' => 'december'),
        );

    }
    public function getActiveDate($date)
    {
        return array('YEAR'=> date('Y', strtotime($date)), 'MONTH' => date('m', strtotime($date)));
    }

    public function setSEO()
    {
        $ipropValues = new InheritedProperty\ElementValues(
            \metrium\helpers\IBlockHelper::getIblockIdByCode('object_news'),
            \metrium\helpers\IBlockHelper::getElementIdByElementCode($this->elementCode)
        );
        $values = $ipropValues->getValues();

        global $APPLICATION;
        $APPLICATION->SetPageProperty('title', $values['ELEMENT_META_TITLE']);
        $APPLICATION->SetPageProperty('keywords', $values['ELEMENT_META_KEYWORDS']);
        $APPLICATION->SetPageProperty('description', $values['ELEMENT_META_DESCRIPTION']);
        $APPLICATION->SetPageProperty('page_title', $values['ELEMENT_PAGE_TITLE']);
    }

}