<?
/** @var $this CBitrixComponentTemplate */
/** @global $APPLICATION */
/** @var array $arResult */
use \metrium\EstateObject;

$this->SetViewTarget("manager");
$APPLICATION->IncludeComponent(
    "metrium:manager.single",
    "novostroyki",
    array(
        "USER_ID" => 8,
        "COMPONENT_TEMPLATE" => "novostroyki",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "1"
    ),
    false,
    array("HIDE_ICONS" => "Y")
);
$this->EndViewTarget();?>
