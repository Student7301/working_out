<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="main-part right">
    <div class="content-full">
        <div class="news-detail" id="news_top">
            <div class="news__date">
                <div class="middle-text"><?=$arResult['ITEM']['DATE']?></div>
            </div>
            <div class="main-title">
                <?=$arResult['ITEM']['NAME']?>
            </div>
            <div class="editable-text">
                <?=$arResult['ITEM']['DETAIL_TEXT']?>
            </div>
            <div class="back-reff">
                <div class="back-reff__icon"><i class="sprite-icon right-arrow"></i></div><a href="<?=$arResult['BACK_URL']?>" title="" class="back-reff__link">К списку новостей</a>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>