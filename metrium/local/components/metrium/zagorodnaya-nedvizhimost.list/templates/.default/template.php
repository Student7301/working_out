<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript">
    window.basePath = '<?=SITE_TEMPLATE_PATH?>';
    window.defaultFilterInputOptions = [];
    defaultFilterInputOptions['name'] = '';
    defaultFilterInputOptions['id'] = '';
    defaultFilterInputOptions['currentValue'] = 'USD';
    defaultFilterInputOptions['priceFrom'] = 0;
    defaultFilterInputOptions['priceTo'] = 50000000;
    defaultFilterInputOptions['remotnessMKADFrom'] = 0;
    defaultFilterInputOptions['remotnessMKADTo'] = 50;
    defaultFilterInputOptions['groundAreaFrom'] = 0;
    defaultFilterInputOptions['groundAreaTo'] = 300;
    defaultFilterInputOptions['houseAreaFrom'] = 0;
    defaultFilterInputOptions['houseAreaTo'] = 2500;
</script>

<div class="content for-xs"><!-- BEGIN header -->
    <div class="header-panel header-panel--alternate c-top-panel-alternate"></div>
    <header class="header clearfix c-top-panel">
        <div class="container">
            <div class="header-panel">
                <a href="#" title="" class="header-panel__menu c-popup-open">
                    <span class="header-panel__icon">
                        <div class="icon-menu for-icon-menu"></div>
                    </span><span class="header-panel__text">меню</span><!-- popup -->
                </a>
                <div class="c-popup-menu popup clearfix">
                    <div class="popup__overlay"></div>
                    <div class="popup__inner">
                        <div class="popup__content-menu c-popup-content">
                            <a href="#" title="" class="popup__close-menu c-popup-close-menu">
                                <span class="icon-close"></span>
                                <div class="popup__close-text">Меню</div>
                            </a>
                            <div class="popup__elit-menu">
                                <a href="/zagorodnaya-nedvizhimost/" title="" class="popup__big-title">Элитная загородная недвижимость</a>
                                <a href="/zagorodnaya-nedvizhimost/" title="" class="width_for_a popup__big-btn btn-form"  > Более <?= \metrium\helpers\IBlockHelper::getCountObjectForFilter(''); ?> объектов </a>
                                <a href="/elitnaya-gorodskaya-nedvijimost/" title="" class="popup__big-title">Элитная городская недвижимость</a>
                                <a href="/elitnaya-gorodskaya-nedvijimost/" title="" class="width_for_a popup__big-btn btn-form"  > Более <?= \metrium\helpers\IBlockHelper::getCountEliteApartments(); ?> объектов </a>
                            </div>
                            <hr>
                            <div class="popup__new-menu">
                                <ul class="popup__new-menu-list">
                                    <li><a href="/novostroyki/novostroyki-moskvy/" title="" class="popup__new-menu-item">Новостройки Москвы</a></li>
                                    <li><a href="/novostroyki/novostroyki-moskovskoy-oblasti/" title="" class="popup__new-menu-item">Новостройки Подмосковья</a></li>
                                    <li><a href="/novostroyki/novostroyki-novaya-moskva/" title="" class="popup__new-menu-item">Новостройки новой москвы</a></li>
                                </ul>
                            </div>
                            <hr>
                            <div class="popup__main-menu">
                                <ul class="popup__main-menu-list">
                                    <li><a href="/funds/" title="" class="popup__main-menu-item">девелоперам</a></li>
                                    <li><a href="/o-kompanii/" title="" class="popup__main-menu-item">о компании</a></li>
                                    <li><a href="/news/<?= \metrium\helpers\IBlockHelper::getDataLastNews(); ?>" title="" class="popup__main-menu-item">пресс-служба</a></li>
                                    <li><a href="/ipoteka/programmy_bankov/" title="" class="popup__main-menu-item">ипотека</a></li>
                                    <li><a href="/trade-in/" title="" class="popup__main-menu-item">Trade-In</a></li>
                                    <li><a href="/analitika-i-konsalting-dev/" title="" class="popup__main-menu-item">Аналитика и консалтинг</a></li>
                                    <li><a href="/organizatsiya-prodazh/" title="" class="popup__main-menu-item">Организация продаж</a></li>
                                    <li><a href="/investitsii/" title="" class="popup__main-menu-item">Инвестиции</a></li>
                                </ul>
                            </div>
                            <hr>
                            <div class="popup__social-icon">
                                <a href="https://www.facebook.com/metrium" title="" class="popup__social-icon-item" target="_blank"><span class="icon-facebook"></span></a>
                                <a href="https://vk.com/metriumru" title="" class="popup__social-icon-item" target="_blank"><span class="icon-vk"></span></a>
                                <a href="https://www.instagram.com/metriumgroup/" title="" class="popup__social-icon-item" target="_blank"><span class="icon-instagram"></span></a>
                                <a href="https://twitter.com/Metrium2" title="" class="popup__social-icon-item" target="_blank"><span class="icon-twitter"></span></a>
                                <a href="https://ok.ru/group/53323706728662" title="" class="popup__social-icon-item" target="_blank"><span class="icon-odnoklassniki"></span></a>
                            </div>
                        </div>
                    </div>
                </div><a href="/" title="" class="header-panel__logo">
                    <div class="header-panel__logo-inner"><img src="/local/templates/zagorod-new/images/content/logo-elit-1920.png" alt=""></div></a>
                <div class="header-panel__info clearfix">
                    <div class="header-panel__info-title"><a href="/zagorodnaya-nedvizhimost/">Элитная загородная недвижимость</a></div>
                    <div class="header-panel__info-count"><span class="count-text">Всего</span><span>предложений:</span><span class="count"><?=\metrium\helpers\IBlockHelper::getCountAllObjects();?></span></div>
                </div>
                <div class="header-panel__left-info">
                    <div class="header-panel__phone">
                        <a href="tel:84997552580" id="phone_moscow">8 (499) 755-25-80</a>
                    </div>
                    <?if($USER->IsAuthorized()):?>
                        <a href="/kabinet/" title="" class="header-panel__personal">
                            <span class="header-panel__personal-text">Личный кабинет</span>
                        </a>
                    <?endif;?>
                    <a href="/kabinet/vybor/" title="" class="header-panel__choice choose right">
                        <span class="header-panel__personal-text">Мой выбор</span>
                        <span class="icon icon-heart"></span>
                        <span class="header-panel__personal-text choose__count" style="padding-top: 1px;"></span>
                    </a>
                </div>
            </div>
        </div>
    </header><!-- END header -->

    <!-- Вывод баннера если это главная -->
    <? if(!isset($arParams['SECTION_CODE'])) { ?>
        <div class="videos-main">
            <div class="videos-main__container">
                <? foreach ($arResult['BANNERS'] as $banner) { ?>
                    <div class="videos-main__item c-video-item">
                        <a href="/zagorodnaya-nedvizhimost/<?= $banner['NAME_IBLOCK'] ?>/" title="" class="big-link">
                        </a>
                        <video muted class="videos-main__video c-video">
                            <source src="<?= $banner['URL_VIDEO'] ?>" type="video/mp4">
                        </video>
                        <div style="background: url('<?= $banner['URL_PICTURE'] ?>') center no-repeat; background-size: cover;" class="videos-main__images c-hide-img"></div>
                        <div class="videos-main__hide-panel c-text-hide">
                            <div class="videos-main__panel-inner">
                                <div class="videos-main__title-item">
                                    <a href="/zagorodnaya-nedvizhimost/<?= $banner['NAME_IBLOCK'] ?>/" class="banner-link"><?= $banner['NAME'] ?></a>
                                </div>
                                <div class="videos-main__content">
                                    <div class="videos-main__play-btn c-play-video">
                                        <div class="durationBar">
                                            <div class="positionBar"></div>
                                            <span class="progress-percent c-progress-percent"></span>
                                        </div>
                                    </div>
                                    <div class="videos-main__descr">
                                        <a href="/zagorodnaya-nedvizhimost/<?= $banner['NAME_IBLOCK'] ?>/" class="banner-link"><?= $banner['DESCRIPTION'] ?></a>
                                    </div>
                                    <div class="videos-main__count-offers">
                                        Более
                                        <? if($banner['IBLOCKCODE'] != 'arenda') { ?>
                                            <?= \metrium\helpers\IBlockHelper::getCountObjectForFilter($banner['IBLOCKCODE']); ?>
                                        <? }else{ ?>
                                            <?= \metrium\helpers\IBlockHelper::getCountObjectArendaForFilter(); ?>
                                        <? } ?>
                                        предложений!
                                    </div>
                                    <a href="/zagorodnaya-nedvizhimost/<?= $banner['NAME_IBLOCK'] ?>/" title="" class="videos-main__more-btn">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
    <? }elseif($arParams['SECTION_CODE'] == 'kottedzhi_taunkhausy' || $arParams['SECTION_CODE'] == 'poselki' || $arParams['SECTION_CODE'] == 'uchastki' || $arParams['SECTION_CODE'] == 'arenda') { ?>
        <div class="filter">
            <div class="filter__header">
                <a href="/zagorodnaya-nedvizhimost/kottedzhi_taunkhausy/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'kottedzhi_taunkhausy'){?>active<?}?>">Коттеджи и таунхаусы</a>
                <a href="/zagorodnaya-nedvizhimost/poselki/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'poselki'){?>active<?}?>">Поселки</a>
                <a href="/zagorodnaya-nedvizhimost/uchastki/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'uchastki'){?>active<?}?>">Участки</a>
                <a href="/zagorodnaya-nedvizhimost/arenda/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'arenda'){?>active<?}?>">Аренда</a>
            </div>
            <form method="" action="?AJAX=Y&SECTION=<?= $arParams['SECTION_CODE'] ?>" class="c-filter-form filter__body clearfix">
                <? if($_SERVER['SITE_TEMPLATE'] != 'zagorod-new.mobile') { ?>
                <div class="video">
                    <video muted autoplay loop>
                        <? foreach ($arResult['BANNERS'] as $banner) { ?>
                            <? if($banner['NAME_IBLOCK'] == $arParams['SECTION_CODE']) { ?>
                                <source src="<?= $banner['URL_VIDEO'] ?>" type="video/mp4">
                            <? } ?>
                        <? } ?>
                    </video>
                </div>
                <? } ?>

                <input type="hidden" name="state-map" value="close" class="c-detected-map">
                <div class="filter__column">
                    <? if($arParams['SECTION_CODE'] != 'uchastki') { ?>
                        <div class="filter__block">
                            <div class="filter__name">Площадь дома</div>
                            <div class="filter__field">
                                <label for="houseAreaFrom" class="filter__field-label">От</label>
                                <input type="text" name="houseAreaFrom" id="houseAreaFrom" placeholder="<?= $component::FORM_MIN_VALUE ?>"
                                        class="input-filter c-input-filter js-only-number">
                            </div>
                            <div class="filter__field">
                                <label for="houseAreaTo" class="filter__field-label">До</label>
                                <input type="text" name="houseAreaTo" id="houseAreaTo" placeholder="<?= $arResult['FILTERS']['MAX_HOUSE_AREA'] ?>"
                                        class="input-filter c-input-filter js-only-number">
                            </div>
                            <div class="filter__dimension">м<span>2</span></div>
                        </div>
                    <? } ?>
                    <div class="filter__block">
                        <div class="filter__name">Расстояние от МКАД</div>
                        <div class="filter__field">
                            <label for="remotnessMKADFrom" class="filter__field-label">От</label>
                            <input type="text" name="remotnessMKADFrom" id="remotnessMKADFrom"
                                   placeholder="<?= $component::FORM_MIN_VALUE ?>" class="input-filter c-input-filter js-only-number">
                        </div>
                        <div class="filter__field">
                            <label for="remotnessMKADTo" class="filter__field-label">До</label>
                            <input type="text" name="remotnessMKADTo" id="remotnessMKADTo"
                                   placeholder="<?= $arResult['FILTERS']['MAX_REMOTNESS_MKAD'] ?>" class="input-filter c-input-filter js-only-number">
                        </div>
                        <div class="filter__dimension">км</div>
                    </div>
                </div>
                <div class="filter__column">
                    <div class="filter__block">
                        <div class="filter__name">Площадь участка</div>
                        <div class="filter__field">
                            <label for="groundAreaFrom" class="filter__field-label">От</label>
                            <input type="text" name="groundAreaFrom" id="groundAreaFrom"
                                   placeholder="<?= $component::FORM_MIN_VALUE ?>" class="input-filter c-input-filter js-only-number">
                        </div>
                        <div class="filter__field">
                            <label for="groundAreaTo" class="filter__field-label">До</label>
                            <input type="text" name="groundAreaTo" id="groundAreaTo"
                                   placeholder="<?= $arResult['FILTERS']['MAX_TERRITORY_AREA'] ?>" class="input-filter c-input-filter js-only-number">
                        </div>
                        <div class="filter__dimension">соток</div>
                    </div>

                    <? if($arParams['SECTION_CODE'] != 'uchastki' && $arParams['SECTION_CODE'] != 'poselki') { ?>
                        <div class="filter__block">
                            <div class="filter__name">Отделка</div>
                            <div class="filter__field">
                                <select name="facing" id="facing" class="input-filter input-filter--select c-custom-select">
                                    <option value="not_set">Не выбрано</option>
                                    <?foreach($arResult['FILTERS']['FACING'] as $facingKey=>$facing):?>
                                        <?if($facing['PROPERTY_FACING_VALUE']):?>
                                            <option value="<?=$facing['PROPERTY_FACING_VALUE']?>"><?=$facing['PROPERTY_FACING_VALUE'] . ' (' . $facing['CNT'] . ')'?></option>
                                        <?endif;?>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </div>
                    <? } ?>
                </div>
                <div class="filter__column">
                    <div class="filter__block">
                        <div class="filter__name">Цена</div>
                        <div class="filter__field">
                            <label for="priceFrom" class="filter__field-label">От</label>
                            <input type="text" name="priceFrom" id="priceFrom" autocomplete="off" class="input-filter js-only-number" value="<?= $component::FORM_MIN_VALUE ?>" data-value="<?= $component::FORM_MIN_VALUE ?>">
                        </div>
                        <div class="filter__field">
                            <label for="priceTo" class="filter__field-label">До</label>
                            <?
                                if($arParams['SECTION_CODE'] != 'arenda') {
                                    $price = ceil($arResult['FILTERS']['MAX_PRICE']['usd'] / $component::FORM_DEFAULT_PRICE_MULTIPLIER);
                                }else {
                                    $price = ceil($arResult['FILTERS']['MAX_PRICE']['usd'] / $component::FORM_DEFAULT_PRICE_MULTIPLIER_ARENDA);
                                }
                            ?>
                            <input type="text" name="priceTo" id="priceTo" autocomplete="off" class="input-filter js-only-number" value="<?= $price ?>" data-value="<?= $price ?>">
                        </div>
                        <div class="filter__dimension">
                            <? if($arParams['SECTION_CODE'] != 'arenda') { ?>
                                млн USD
                            <? }else { ?>
                                тыс USD
                            <? } ?>
                        </div>
                    </div>
                    <div class="filter__block filter__block--range">
                        <div id="price" class="filter__field"></div>
                    </div>
                </div>

                <div class="filter__column filter__column--direction">
                    <div class="filter__block">
                        <div class="filter__name">Направления</div>
                        <div class="filter__field"><a href="#" title="" style="width: 183px" class="input-filter input-filter--dropdown-link c-dropdown-link"><span class="c-input-filter-name">Выбор направления<span></span></span></a>
                            <div class="dropdown-content c-dropdown-content c-custom-scroll">
                                <div class="dropdown-inner">
                                    <?foreach($arResult['FILTERS']['DIRECTIONS'] as $directionKey=>$direction):?>
                                        <?if($direction['PROPERTY_DIRECTION_VALUE']):?>
                                            <div class="filter__field--dropdown">
                                                <input type="checkbox" name="direction[]" id="direction<?=$directionKey?>" value="<?=$direction['PROPERTY_DIRECTION_VALUE']?>" class="custom-checkbox c-custom-checkbox">
                                                <label for="direction<?=$directionKey?>" class="custom-label c-custom-label"><?=$direction['PROPERTY_DIRECTION_VALUE']?> (<?=$direction['CNT']?>)</label>
                                            </div>
                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="filter__block" style="margin-top: 0px">
                        <div class="filter__name" style="margin-bottom: -10px">Поиск по ID или названию</div>
                        <div class="filter__field filter__field_new">
                            <input type="text" name="findByIDorName" id="findByIDorName"
                                   placeholder="ID или название" class="input-filter input-filter-main c-input-filter id-name">
                        </div>
                    </div>
                </div>

                <div class="filter__column filter__column--buttons">
                    <div class="filter__name">
                        Найдено:<span class="count js-count-filter"><?=$arResult['TOTAL_OBJECTS_COUNT'];?></span>
                    </div>
                    <span class="filter__button c-sea-list"><span class="icon icon-list"></span>Показать списком</span>
                    <a href="#" title="" class="filter__button filter__button--map c-show-on-map"><span class="icon icon-location2"></span>Показать на карте</a>
                </div>
            </form>
            <div id="filter-map" class="filter__map c-filter-map"><a href="#" title="" class="filter__map-open c-show-on-map">Смотреть на карте</a></div>
        </div>
    <? } ?>
</div>
<div class="tile">
    <div class="tile__content">
        <div class="tile__sort">
            <div class="tile__sort-text">сортировать список</div><a href="#" title="" class="tile__sort-btn c-sort js-sort-type">дороже → дешевле</a>
        </div>
        <div class="tile__clear"></div>
        <div class="tile__line-content js-filter-content">
            <? foreach($arResult['OBJECTS'] as $object) { ?>
                <a href="<?=$object['URL']?>" class="tile__item">
                    <div class="tile__item-content">
                        <div style="background: url(<?=$object['PREVIEW_PICTURE']?>) no-repeat; background-size: cover" class="tile__item-img"></div>
                        <div class="tile__item-info">
                            <div class="tile__info-content">
                                <div class="tile__info-name"><?= $object['NAME'] ?></div>
                                <div class="tile__info-address">
                                    <?if($object['DIRECTION']):?>
                                        <?=$object['DIRECTION']?>
                                    <?endif;?>
                                    <?if($object['REMOTNESS_MKAD'] && $object['DIRECTION']):?>
                                        ,
                                    <?endif;?>
                                    <?if($object['REMOTNESS_MKAD']):?>
                                        <?=$object['REMOTNESS_MKAD']?> км. от МКАД
                                    <?endif;?>
                                </div>
                                <div class="tile__info-params">
                                    <? if($object['HOUSE_AREA']) { ?>
                                        <div class="tile__info-param"><?=$object['HOUSE_AREA']?>  кв.м.</div>
                                    <? } ?>
                                    <? if($object['TERRITORY_AREA']) { ?>
                                        <div class="tile__info-param"><?=$object['TERRITORY_AREA']?> соток</div>
                                    <? } ?>
                                    <? if($object['FACING']) { ?>
                                        <div class="tile__info-param"><?=$object['FACING']?></div>
                                    <? } ?>
                                </div>
                                <div class="tile__info-price">
                                    <div class="tile__info-price-one">
                                        <?if ($object["HIDE_PRICES"]):?>
                                            Цена по запросу
                                        <?else:?>
                                            <?if($object['PRICE_USD'] != 0):?>
                                                <? if($arParams['SECTION_CODE'] == 'poselki') { ?>
                                                    от
                                                <? } ?>
                                                <?=($arParams['SECTION_CODE'] == 'arenda')?$object['ARENDA_USD'] . ' $':$object['PRICE_USD'] . ' $'?>
                                            <?else:?>
                                                <?=($arParams['SECTION_CODE'] == 'arenda')?$object['ARENDA_EUR'] . ' €':$object['PRICE_EUR'] . ' €'?>
                                            <?endif;?>
                                        <?endif;?>
                                        <? if($object['PRICE_UPDATE_RESULT'] == 'up' || $object['PRICE_UPDATE_RESULT'] == 'down') { ?>
                                        <i class="fa fa-arrow-<?= $object['PRICE_UPDATE_RESULT'] ?> <?= ($object['PRICE_UPDATE_RESULT'] == 'up')? 'up-red':''; ?>" aria-hidden="true"></i>
                                        <? } ?>
                                    </div>
                                    <div class="tile__info-price-two">
                                        <?if ($object["HIDE_PRICES"]):?>

                                        <?else:?>
                                            <?if($object['PRICE_RUB'] != 0):?>
                                                <? if($arParams['SECTION_CODE'] == 'poselki') { ?>
                                                    от
                                                <? } ?>
                                                <?=($arParams['SECTION_CODE'] == 'arenda' && !$hideArendaRub)?$object['ARENDA_RUB'].' &#8381;':''?>
                                                <?=($arParams['SECTION_CODE'] != 'arenda' && !$hideRub)?$object['PRICE_RUB'].' &#8381;':''?>
                                            <?endif;?>
                                        <?endif;?>
                                    </div>
                                </div>
                                <div class="tile__info-id">ID объекта: <?=$object['CRM_ID']?></div>
                            </div>
                        </div>
                        <? if($object['NEW'] == 'Y') { ?>
                            <div class="tile__special-mark tile__special-mark--new-offer">
                                <span class="tile__special-text">Новое предложение</span>
                            </div>
                        <? } ?>
                        <div class="tile__special-mark c-like" data-obj_id="<?=$object['OBJ_ID']?>" data-ajax_url="/local/templates/.default/ajax/like.php">
                            <span class="tile__icon icon-heart"></span>
                        </div>
                    </div>
                </a>
                <div class="tile__item-clear"></div>
            <? } ?>

        </div>
    </div>
</div>
<div class="content">
    <div class="pagination clearfix">
        <div class="pagination__count-result">
            <a class="pagination__text pagination__text--count">Показывать по:</a>
            <a href="#" title="" data-content="20" class="active pagination__item c-pagination-item">20</a>
            <a href="#" title="" data-content="30" class="pagination__item c-pagination-item">30</a>
            <a href="#" title="" data-content="50" class="pagination__item c-pagination-item">50</a>
            <a href="#" title="" data-content="100" class="pagination__item c-pagination-item">100</a>
            <a href="#" title="" class="pagination__text pagination__text--clear">Сбросить все</a>
        </div>
        <?=$arResult['NAV_STRING']?>
    </div>
</div>
<div class="clear"></div><!-- BEGIN footer -->
<footer class="footer">
    <? if($arResult['IBLOCK_DESCRIPTION'] || $arResult['DIRECTION']['DETAIL_TEXT']) { ?>
        <div class="description">
            <div class="description__content">
                <?if (!empty($arResult['DIRECTION'])) { ?>
                    <?=$arResult['DIRECTION']['DETAIL_TEXT']?>
                <? }elseif (!empty($arResult['IBLOCK_DESCRIPTION'])) { ?>
                    <?=$arResult['IBLOCK_DESCRIPTION'];?>
                <? } ?>
                <div class="description__text clearfix">
                    <div class="arrow-top c-arrow-top"><span class="icon-arrow-up2"></span></div>
                </div>
            </div>
        </div>
    <? } ?>
