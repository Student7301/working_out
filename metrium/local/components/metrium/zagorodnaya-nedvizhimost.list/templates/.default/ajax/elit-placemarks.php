<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!CModule::IncludeModule('iblock')) {
    $response['status'] = 'fail';
    echo json_encode($response);
    return;
}
/******  Выборка объектов по фильтру ********/

$arFilter = array(
    'ACTIVE' => 'Y',
    'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'),
    '!PROPERTY_LATITUDE' => false,
    '!PROPERTY_LONGITUDE' => false,
);

if($_REQUEST['SECTION'] == 'arenda'){
    $arFilter['PROPERTY_ARENDA'] = '1';
}else{
    $arFilter['PROPERTY_OBJECT_TYPE'] = $_REQUEST['SECTION'];
}


foreach($_REQUEST['filter']['directions'] as $direction){
    $arDirectionFilter[] = array("PROPERTY_DIRECTION" => $direction);
}

if($_REQUEST['filter']['inputOptions']) {
    foreach ($_REQUEST['filter']['inputOptions'] as $inputOption) {
        if ($inputOption['name'] == 'remotnessMKADFrom') {
            $arMKADFilter[0][">=PROPERTY_REMOTNESS_MKAD"] = $inputOption['value'];
        } elseif ($inputOption['name'] == 'remotnessMKADTo') {
            $arMKADFilter[0]["<=PROPERTY_REMOTNESS_MKAD"] = $inputOption['value'];
        } elseif ($inputOption['name'] == 'houseArea') {
            $arFilter['>=PROPERTY_HOUSE_AREA'] = $inputOption['value'];
        } elseif ($inputOption['name'] == 'groundArea') {
            $arFilter['>=PROPERTY_TERRITORY_AREA'] = $inputOption['value'];
        } elseif ($inputOption['name'] == 'priceFrom') {
            $arPriceFilter[0][">=PROPERTY_PRICE_USD"] = $inputOption['value'];
        } elseif ($inputOption['name'] == 'priceTo') {
            $arPriceFilter[0]["<=PROPERTY_PRICE_USD"] = $inputOption['value'];
        }
    }
}

if($arDirectionFilter){
    $arDirectionFilter['LOGIC'] = "OR";
    $arFilter[] = $arDirectionFilter;
}

if($arMKADFilter){
    $arMKADFilter['LOGIC'] = "AND";
    $arFilter[] = $arMKADFilter;
}

if($arPriceFilter){
    $arPriceFilter['LOGIC'] = "AND";
    $arFilter[] = $arPriceFilter;
}

if($_REQUEST['filter']['checkOptions']){
    foreach($_REQUEST['filter']['checkOptions'] as $checkOption){
        if ($checkOption['name'] == 'facing') {
            $arFilter['PROPERTY_FACING'] = $checkOption['value'];
        } elseif ($checkOption['name'] == 'uchastok') {
            $arFilter['PROPERTY_UCHASTOK_TYPE'] = $checkOption['value'];
        }
    }
}

$dbRes = CIBlockElement::GetList(
    array(
        $sortOrder => 'ASC',
        'SORT' => 'ASC'
    ),
    $arFilter,
    false,
    false,
    array(
        'ID',
        'NAME',
        'CODE',
        'PREVIEW_PICTURE',
        'PROPERTY_DIRECTION',
        'PROPERTY_REMOTNESS_MKAD',
        'PROPERTY_HOUSE_AREA',
        'PROPERTY_TERRITORY_AREA',
        'PROPERTY_FACING',
        'PROPERTY_PRICE_EUR',
        'PROPERTY_PRICE_RUB',
        'PROPERTY_PRICE_USD',
        'PROPERTY_OBJECT_ID',
        'PROPERTY_OBJECT_TYPE',
        'PROPERTY_LATITUDE',
        'PROPERTY_LONGITUDE'
    )

);

while($object = $dbRes->Fetch()){
    $tempArr = array(

        'NAME' => $object['NAME'],
        'CODE' => $object['CODE'],
        'OBJECT_ID' => $object['PROPERTY_OBJECT_ID_VALUE'],
        'LATITUDE' => $object['PROPERTY_LATITUDE_VALUE'],
        'LONGITUDE' => $object['PROPERTY_LONGITUDE_VALUE'],
    );

    $resObjects[] = $tempArr;

}
$arFeatures = array();
foreach($resObjects as $arItem){
    $arFeatures[] = array(  'type' => 'Feature',
                            'geometry' => array('type' => 'Point', 'coordinates' => array($arItem['LATITUDE'], $arItem['LONGITUDE'])),
                            'properties' => array('hintContent' => $arItem['NAME'], 'iconContent' => 1)
                        );
}
$arFeatureCollection = array('type' => 'FeatureCollection',
                             'features' =>  $arFeatures );

echo json_encode($arFeatureCollection);

/********************************************/



?>

