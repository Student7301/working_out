<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript">
    $(document).ready(function () {
        var likes = <?=json_encode(\metrium\Likes::getLikes());?>;
        
//        $('#banners').insertAfter('div.fixed-header.c-fixed-header'); // Вставляем баннеры после хедера
        $('.header-panel__left-info .choose .choose__count').text(likes.length); // Подменяем кол-во лайков

        $('.tile__item-content .c-like').each(function (i, e) {
            var objectId = $(e).data('obj_id').toString();

            /* Добавляем / удаляем активность у объектов */
            if ((likes.indexOf(objectId) != -1 || likes.indexOf(parseInt(objectId)) != -1) && !$(e).hasClass('active')) {
                $(e).addClass('active');
                $(e).attr('data-action', 'dislike');
            } else {
                $(e).attr('data-action', 'like');
            }
        });
    });
</script>

<!--<div id="banners">-->
<!--    --><?//$APPLICATION->ShowViewContent("banners");?>
<!--</div>-->

