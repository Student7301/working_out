<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Entity;
use metrium\helpers\ZagorodHelper;

class CZagorodnayaNedvizhimost extends CBitrixComponent
{
    /** @var CIBlockElement  */
    public $el;
    /** @var CIBlock  */
    public $CIBlock;
    /** @var  CIBlockSection */
    public $CIBlockSection;
    /** @var CUserFieldEnum */
    public $CUserFieldEnum;
    /** @var  CFile */
    public $cFile;
    public $objectsPerPage;
    public $arNavStartParams;
    public $sortOrder;
    public $arSort;
    private $newObjectIds;
    private $direction;
    private $arJson;
    private $inputOptions;
    private $checkedOptions;
    private $isAjax = false;
    private $isMobile = false;

    const DIRECTION_BLOCK_CODE = 'directions';
    const POSELKI_CODE = "poselki";
    const KOTTEDZHI_CODE = "kottedzhi_taunkhausy";
    const UCHASTKI_CODE = "uchastki";
    const POSELKI_TEXT = "Поселки";
    const UCHASTKI_TEXT = "Участки";
    const KOTTEDZHI_TEXT = "Коттеджи и таунхаусы";
    const SETTLEMENTS_CODE = "settlements";
    const LOCALITIES_CODE = "localities";
    //const FORM_USD = 'dollar';
    const FORM_RUB = 'rub';

    const FORM_ZERO_VALUE = '0';
    const FORM_MIN_TERRITORY_VALUE = '10';
    const FORM_MAX_VALUE = '30';


    const FORM_STEP = '1';
    const FORM_TERRITORY_STEP = '10';
    const FORM_REMOTNESS_MKAD_STEP = '10';
    
    const FORM_MIN_VALUE = '0';
    const FORM_MAX_TERRITORY_AREA = '500+';
    const FORM_MAX_HOUSE_AREA = '2500+';
    const FORM_MAX_PRICE = '30';
    const FORM_MAX_REMOTNESS_MKAD_VALUE = '50+';
    const FORM_DEFAULT_PRICE_MULTIPLIER = 1000000;
    const FORM_DEFAULT_PRICE_MULTIPLIER_ARENDA = 1000;


    public function __construct($component)
    {
        parent::__construct($component);

        if(!CModule::IncludeModule("iblock")){
            die('Error including module iblock');
        }

        $this->CIBlock = new CIBlock();
        $this->el = new CIBlockElement();
        $this->CIBlockSection = new CIBlockSection();
    }


    public function onPrepareComponentParams($arParams)
    {
        $this->cFile = new CFile();
        $this->CUserFieldEnum = new CUserFieldEnum;

        if ($_REQUEST['AJAX'] == 'Y') {
            $this->isAjax = true;
        }

        if ($_SERVER['SITE_TEMPLATE'] == 'elite.mobile') {
            $this->isMobile = true;
        }

        if (isset($_REQUEST['SHOW']) && ($_REQUEST['SHOW'] > 0)) {
            $this->objectsPerPage = $_REQUEST['SHOW'];
        } elseif ($this->isAjax && $_REQUEST['showBy']) {
            $this->objectsPerPage = $_REQUEST['showBy'];
        } elseif ($arParams['SECTION_CODE'] == 'dom-novorizhskoe-shosse') {
            $this->objectsPerPage = 50;
        } else {
            $this->objectsPerPage = 20;
        }

        if(preg_match("/dom-?([\\w-].*?-shosse)/", $arParams['SECTION_CODE'], $match)) {
            $arParams['SECTION_CODE'] = '';
            $arParams['SEO_TYPE'] = 'dom';
            $arParams['CODE'] = $match[1];
        }

        $this->arNavStartParams['nPageSize'] = $this->objectsPerPage;
        if ($this->isAjax && $_REQUEST['currPage']){
            $this->arNavStartParams['iNumPage'] = $_REQUEST['currPage'];
        }elseif($_REQUEST['PAGEN_1']){
            $this->arNavStartParams['iNumPage'] = $_REQUEST['PAGEN_1'];
        }else{
            $this->arNavStartParams['iNumPage'] = 1;
        }
        $sortOrder = isset($_REQUEST['sort']['order']) ? $_REQUEST['sort']['order'] : 'DESC';
        switch($_REQUEST['SORT']){

            case 'price':
				$sortBy = ($arParams['SECTION_CODE']== 'arenda') ? 'PROPERTY_ARENDA_USD' : 'PROPERTY_PRICE_USD';
                $this->arSort = array(
                    $sortBy => $sortOrder,
                    'SORT' => 'ASC'
                );
                break;
            case 'direction':
                $this->arSort = array(
                    'PROPERTY_DIRECTION' => $sortOrder,
                    'SORT' => 'ASC'
                );
                break;
            case 'finish':
                $this->arSort = array(
                    'PROPERTY_FACING' => $sortOrder,
                    'SORT' => 'ASC'
                );
                break;
            case 'area':
                $this->arSort = array(
                    'PROPERTY_HOUSE_AREA' => $sortOrder,
                    'SORT' => 'ASC'
                );
                break;
            default:
                $sortBy = ($arParams['SECTION_CODE']== 'arenda') ? 'PROPERTY_ARENDA_USD' : 'PROPERTY_PRICE_USD';
                $this->arSort = array(
                    //'SORT' => 'ASC',
                    $sortBy => $sortOrder
                );
                break;
        }

        return $arParams;
    }

    public function executeComponent()
    {
        global $APPLICATION;
        $this->arResult['FILTERS'] = $this->getFilters($this->arParams['SECTION_CODE']);
        if ($this->isAjax) {
            $APPLICATION->RestartBuffer();
            while(ob_end_clean());
            $section = empty($_REQUEST['DIRECTION_NAME']) ? $this->arParams['SECTION_CODE'] : '';
            $this->getZagorodka($section);
            echo json_encode($this->arJson);
            die();
        }

        $arCacheId = array_merge($this->arParams, $_REQUEST);
        $arCacheId['ISMOBILE'] = $this->isMobile;
        $cache_id = serialize($arCacheId);
        $cacheTime = 3600;
        if ($this->startResultCache($cacheTime, $cache_id)) {
            $this->setSEOh1($this->arParams['SECTION_CODE']);
            if($this->arParams['SEO_TYPE'] == 'dom'){
                $this->direction = $this->getDirectionData();
                if(!$this->direction['NAME']){
                    define("ERROR_404", "Y");
                }
                $this->arParams['DIRECTION_NAME'] = $this->direction['META_H1'];
                $this->arResult['DIRECTION'] = $this->direction;
                $this->arResult['SETTLEMENTS'] = self::getSettlementsByDirection($this->direction['ID']);
            } elseif (!$this->isAjax && !$this->isMobile && $this->arParams['CUSTOM_LOCATION'] != 'special-page') {
                $this->arResult['NEW_OBJECTS'] = $this->getNewObjects($this->arParams['SECTION_CODE']);
            }


            $this->arResult['BANNERS'] = $this->getBanners();
            $this->arResult['OBJECTS'] = $this->getZagorodka($this->arParams['SECTION_CODE']);
            $this->arResult['SECTION'] = $this->arParams['SECTION_CODE'];

            $this->arResult['IBLOCK_DESCRIPTION'] = $this->getIBlockDescription($this->arParams['SECTION_CODE']);
            $this->includeComponentTemplate();
        }
        $this->setSEO($this->arParams['SECTION_CODE']);
        return $this->arResult;
    }

    /**
     * Получение информации для баннеров
     */
    public function getBanners()
    {
        CModule::IncludeModule('highloadblock');
        $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(HLB_VIDEO_BANNER_ZAGOROD)->fetch();
        $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);

        $q = new Entity\Query($hlentity);
        $q->setSelect(array('*'));
        $infrastuctures = new CDBResult($q->exec());
        while ($element = $infrastuctures->Fetch())
        {
            $result[] = array(
                'NAME_IBLOCK'     => $element['UF_IBLOCKCODE'],
                'NAME'            => $element['UF_NAME'],
                'DESCRIPTION'     => $element['UF_DESCRIPTION'],
                'URL_PICTURE'     => $this->cFile->GetPath($element['UF_PICTURE']),
                'URL_VIDEO'       => $this->cFile->GetPath($element['UF_BANNER']),
                'ASSIGNMENT_TEXT' => $element['UF_TEXT_ASSIGNMENT'],
                'IBLOCKCODE'      => $element['UF_IBLOCKCODE'],
            );
        }
        return $result;
    }

    /**Возвращает описание инфоблока по его символьному коду
     * @param $code
     * @return string
     */
    public function getIBlockDescription($code) {
        if (empty($code)) {
            $code = 'for_filter';
        }

        $cdbResult = $this->CIBlock->GetList(
            array(),
            array(
                'SITE_ID'=>SITE_ID,
                'ACTIVE'=>'Y',
                "CODE"=>$code
            )
        );

        if ($iblock = $cdbResult->GetNext()) {
            return $iblock['DESCRIPTION'];
        }
        return "";
    }

    public function getZagorodka($section)
    {
        $arFilter = $this->getArFilter($section);
        $arSort = $arFilter['SORT'];
        $arFilter = $arFilter['FILTER'];

        if ($this->arParams['CUSTOM_LOCATION'] == 'special-page') {
            $arFilter['CODE'] = array(6189, 3877, 3229, 1126, 3547, 1971, 5023, 5748, 5565, 6619, 3747);
        }

        $dbRes = $this->el->GetList(
            $arSort,
            $arFilter,
            false,
            $this->arNavStartParams,
            array(
                'ID',
                'IBLOCK_ID',
                'NAME',
                'CODE',
                'PROPERTY_PHOTO',
                'PROPERTY_DIRECTION',
                'PROPERTY_REMOTNESS_MKAD',
                'PROPERTY_HOUSE_AREA',
                'PROPERTY_TERRITORY_AREA',
                'PROPERTY_FACING',
                'PROPERTY_PRICE_EUR',
                'PROPERTY_PRICE_RUB',
                'PROPERTY_PRICE_USD',
                'PROPERTY_PRICE_USD_OLD',
                'PROPERTY_OBJECT_TYPE',
                'PROPERTY_ARENDA_USD',
                'PROPERTY_ARENDA_RUB',
                'PROPERTY_ARENDA_EUR',
                'PROPERTY_ARENDA',
                'PROPERTY_CURRENCY',
                'PROPERTY_HIDE_PRICES',
                'PROPERTY_LATITUDE',
                'PROPERTY_LONGITUDE',
                'PROPERTY_ELIT_CITY',
                'PROPERTY_ELIT_CITY.NAME',
                'PROPERTY_ELIT_CITY.PROPERTY_id_in_crm',
                'PROPERTY_ELIT_CITY.PROPERTY_flat_square_from',
                'PROPERTY_ELIT_CITY.PROPERTY_flat_square_to',
                'PROPERTY_ELIT_CITY.PROPERTY_flat_cost_from',
                'PROPERTY_ELIT_CITY.PROPERTY_flat_cost_to',
                'PROPERTY_ELIT_CITY.DETAIL_PAGE_URL',
                'PROPERTY_ELIT_CITY.PREVIEW_PICTURE',
                'PROPERTY_IS_NEW',
                'PROPERTY_OBJECT_ID',
                'PROPERTY_PRICE_UPDATE_RESULT',
                'PROPERTY_CRM_ID'
            )
        );

        $count = (array_key_exists('NEW', $_GET)) ? $dbRes->SelectedRowsCount() : $dbRes->SelectedRowsCount() + count($this->newObjectIds);
        $this->arResult['TOTAL_OBJECTS_COUNT'] = $count;

        while($object = $dbRes->Fetch()){

            $picture = (!empty($object['PROPERTY_ELIT_CITY_VALUE'])) ? $object['PROPERTY_ELIT_CITY_PREVIEW_PICTURE'] : $object['PROPERTY_PHOTO_VALUE'];

            $ar = \metrium\helpers\EliteHelper::ResizePhotoObjectList($picture);

            $tempArr = array(
                'ID' => $object['ID'],
                'CODE' => $object['CODE'],
                'CRM_ID' => $object['PROPERTY_CRM_ID_VALUE'],
                'PREVIEW_PICTURE' => $ar['src'],
                'DIRECTION' => $object['PROPERTY_DIRECTION_VALUE'],
                'REMOTNESS_MKAD' => $object['PROPERTY_REMOTNESS_MKAD_VALUE'],
                'TERRITORY_AREA' => $object['PROPERTY_TERRITORY_AREA_VALUE'],
                'FACING' => $object['PROPERTY_FACING_VALUE'],
                'SECTION' => $object['PROPERTY_OBJECT_TYPE_VALUE'],
                'HIDE_PRICES' => $object['PROPERTY_HIDE_PRICES_VALUE'],
                'NEW' => $object['PROPERTY_IS_NEW_VALUE'],
                'PRICE_UPDATE_RESULT' => $object['PROPERTY_PRICE_UPDATE_RESULT_VALUE']
            );
            if($_REQUEST['state-map'] == 'open') {
                $tempArr['YANDEX_MAP'] = $object['PROPERTY_LATITUDE_VALUE'] . ', ' . $object['PROPERTY_LONGITUDE_VALUE'];
            }

            $prices = array(
                "PRICE_RUB" => \metrium\Price::getFormattedPrice($object["PROPERTY_PRICE_RUB_VALUE"]),
                "PRICE_USD" => \metrium\Price::getFormattedPrice($object["PROPERTY_PRICE_USD_VALUE"]),
                "PRICE_EUR" => \metrium\Price::getFormattedPrice($object["PROPERTY_PRICE_EUR_VALUE"]),
                "ARENDA_RUB" => \metrium\Price::getFormattedPrice($object["PROPERTY_ARENDA_RUB_VALUE"]),
                "ARENDA_USD" => \metrium\Price::getFormattedPrice($object["PROPERTY_ARENDA_USD_VALUE"]),
                "ARENDA_EUR" => \metrium\Price::getFormattedPrice($object["PROPERTY_ARENDA_EUR_VALUE"])
            );
            $tempArr = array_merge($tempArr, $prices);

            if (!empty($object['PROPERTY_ELIT_CITY_VALUE'])) { // если ссылается на объект новостроек
                $tempArr['OBJ_ID'] = $object['PROPERTY_ELIT_CITY_ID_IN_CRM_VALUE'];
                $tempArr['NAME'] = $object['PROPERTY_ELIT_CITY_NAME'];
                $tempArr['EXT_URL'] = $object['PROPERTY_ELIT_CITY_DETAIL_PAGE_URL'];
                $tempArr['URL'] = $object['EXT_URL'];
                $tempArr['HOUSE_AREA'] = $object['PROPERTY_ELIT_CITY_PROPERTY_FLAT_SQUARE_FROM_VALUE'] . ' - ' . $object['PROPERTY_ELIT_CITY_PROPERTY_FLAT_SQUARE_TO_VALUE'];
                $tempArr['PRICE_USD'] = \metrium\Price::getFormattedPrice($object['PROPERTY_ELIT_CITY_PROPERTY_FLAT_COST_FROM_VALUE']) . ' - '
                    . \metrium\Price::getFormattedPrice($object['PROPERTY_ELIT_CITY_PROPERTY_FLAT_COST_TO_VALUE']);
                $tempArr['PRICE_RUB'] = \metrium\Price::getFormattedPrice(metrium\Price::getPriceRub($object['PROPERTY_ELIT_CITY_PROPERTY_FLAT_COST_FROM_VALUE'])) . ' -  '
                    . \metrium\Price::getFormattedPrice(metrium\Price::getPriceRub($object['PROPERTY_ELIT_CITY_PROPERTY_FLAT_COST_TO_VALUE']));
            } else {
                $type = ($this->arParams['SECTION_CODE'] == 'arenda') ? 'arenda' : $object['PROPERTY_OBJECT_TYPE_VALUE'];

                $tempArr['OBJ_ID'] = $object['PROPERTY_OBJECT_ID_VALUE'];
                $tempArr['NAME'] = $object['NAME'];
                $tempArr['HOUSE_AREA'] = $object['PROPERTY_HOUSE_AREA_VALUE'];
                $tempArr['URL'] = '/zagorodnaya-nedvizhimost/' . $type . '/' . $object['CODE'] . '/';
            }

            $resObjects[] = $tempArr;
        }

        $this->arResult['NAV_STRING'] = $dbRes->GetPageNavStringEx($this, 'Элитная загородная недвижимость', 'zagorod_new', 'Y');

        //Формирование вывода для ajax запроса
        if($this->isAjax){
            $this->OutputJSON($resObjects, $arFilter, $dbRes, $section);
        }
        return $resObjects;
    }

    //Формирование $arFilter для выборки элементов
    private function getArFilter($section){
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter')
        );

        if(!empty($_REQUEST['DIRECTION_NAME'])){
            $this->direction['NAME'] = $_REQUEST['DIRECTION_NAME'];
        }

        //Формирование фильтра для первой загрузки страницы
        if($this->direction['NAME'] && !$this->isAjax){
            $arDirectionFilter[] = array("=PROPERTY_DIRECTION" => $this->direction['NAME']);
        } else {
            if(is_array($_REQUEST['direction'])) {
                foreach ($_REQUEST['direction'] as $direction) {
                    $arDirectionFilter[] = array("=PROPERTY_DIRECTION" => $direction);
                }
            }
        }

        if($section == 'arenda'){
            $arFilter['=PROPERTY_ARENDA'] = '1';

            /* Убираем элементы только для аренды */
            $subFilter = array(
                'LOGIC' => 'OR',
                '>PROPERTY_ARENDA_USD' => 2,
                '>PROPERTY_ARENDA_EUR' => 2,
                '>PROPERTY_ARENDA_RUB' => 1000
            );

            $arFilter = array_merge($arFilter, array($subFilter));
        }elseif(!empty($section)){
            $arFilter['=PROPERTY_OBJECT_TYPE'] = $section;

            $subFilter = array(
                'LOGIC' => 'OR',
                '>PROPERTY_PRICE_USD' => 2,
                '>PROPERTY_PRICE_EUR' => 2,
                '>PROPERTY_PRICE_RUB' => 1000
            );

            $arFilter = array_merge($arFilter, array($subFilter));
        }else{
            $subFilter = array(
                'LOGIC' => 'OR',
                '>PROPERTY_PRICE_USD' => 2,
                '>PROPERTY_PRICE_EUR' => 2,
                '>PROPERTY_PRICE_RUB' => 1000
            );

            $arFilter = array_merge($arFilter, array($subFilter));
        }

        if($this->arSort){
            $arSort = $this->arSort;
        }

        //Формирование фильтра для ajax запрсов
        if($this->isAjax){
            if(isset($_REQUEST['sortDir'])){
                $sortOrder = $_REQUEST['sortDir'];
                $sortBy = ($_REQUEST['SECTION'] == 'arenda') ? 'PROPERTY_ARENDA_USD' : 'PROPERTY_PRICE_USD';
                $arSort = array(
                    'PROPERTY_PRICE_UPDATE_SORT' => 'DESC',
                    $sortBy => $sortOrder,
                    'SORT' => 'ASC'
                );
            }

            if($_REQUEST['findByIDorName']) {
                $arIDorNameFilter['LOGIC'] = 'OR';
                $arIDorNameFilter['PROPERTY_CRM_ID'] = $_REQUEST['findByIDorName'];
                $arIDorNameFilter['CODE'] = $_REQUEST['findByIDorName'];
                $arIDorNameFilter['NAME'] = '%' . $_REQUEST['findByIDorName'] . '%';
            }

            if(isset($_REQUEST['houseAreaFrom'])) {
                if($_REQUEST['houseAreaFrom'] != '0' && $_REQUEST['houseAreaFrom'] != '') {
                    $arHouseAreaFilter[0][">=PROPERTY_HOUSE_AREA"] = $_REQUEST['houseAreaFrom'];
                    $filter_select['houseAreaFrom'] = $_REQUEST['houseAreaFrom'];
                }
            }
            if(isset($_REQUEST['houseAreaTo'])) {
                if($_REQUEST['houseAreaTo'] != '2500+' && $_REQUEST['houseAreaTo'] != '') {
                    $arHouseAreaFilter[0]["<=PROPERTY_HOUSE_AREA"] = $_REQUEST['houseAreaTo'];
                }
            }

            if(isset($_REQUEST['remotnessMKADFrom']) && $_REQUEST['remotnessMKADFrom'] != '') {
                if($_REQUEST['remotnessMKADFrom'] != '0') {
                    $arMKADFilter[0][">=PROPERTY_REMOTNESS_MKAD"] = $_REQUEST['remotnessMKADFrom'];
                }
            }
            if(isset($_REQUEST['remotnessMKADTo']) && $_REQUEST['remotnessMKADTo'] != '') {
                if($_REQUEST['remotnessMKADTo'] != '50+') {
                    $arMKADFilter[0]["<=PROPERTY_REMOTNESS_MKAD"] = $_REQUEST['remotnessMKADTo'];
                }
            }

            if(isset($_REQUEST['groundAreaFrom']) && $_REQUEST['groundAreaFrom'] != '') {
                if($_REQUEST['groundAreaFrom'] != '0') {
                    $arGroundAreaFilter[0][">=PROPERTY_TERRITORY_AREA"] = $_REQUEST['groundAreaFrom'];
                }
            }
            if(isset($_REQUEST['groundAreaTo']) && $_REQUEST['groundAreaTo'] != '') {
                if($_REQUEST['groundAreaTo'] != '500+') {
                    $arGroundAreaFilter[0]["<=PROPERTY_TERRITORY_AREA"] = $_REQUEST['groundAreaTo'];
                }
            }

            if(isset($_REQUEST['facing'])) {
                if($_REQUEST['facing'] != 'not_set') {
                    $arFilter['PROPERTY_FACING'][] = $_REQUEST['facing'];
                }
            }

            $currentValue = 'USD';
            if(isset($_REQUEST['priceFrom'])) {
                if($_REQUEST['priceFrom'] != '0') {
                    if($this->arParams['SECTION_CODE'] != 'arenda') {
                        $arPriceFilter[0][">=PROPERTY_PRICE_" . $currentValue] = $_REQUEST['priceFrom'] * self::FORM_DEFAULT_PRICE_MULTIPLIER;
                    }else {
                        $arPriceFilter[0][">=PROPERTY_ARENDA_" . $currentValue] = $_REQUEST['priceFrom'] * self::FORM_DEFAULT_PRICE_MULTIPLIER_ARENDA;
                    }
                }
            }
            if(isset($_REQUEST['priceTo'])) {
                if($_REQUEST['priceTo'] != $this->arResult['FILTERS']['MAX_PRICE']['usd'] / self::FORM_DEFAULT_PRICE_MULTIPLIER) {
                    if($this->arParams['SECTION_CODE'] != 'arenda') {
                        $arPriceFilter[0]["<=PROPERTY_PRICE_" . $currentValue] = $_REQUEST['priceTo'] * self::FORM_DEFAULT_PRICE_MULTIPLIER;
                    }else {
                        $arPriceFilter[0]["<=PROPERTY_ARENDA_" . $currentValue] = $_REQUEST['priceTo'] * self::FORM_DEFAULT_PRICE_MULTIPLIER_ARENDA;
                    }
                }
            }

            if(isset($_REQUEST['direction'])) {
                foreach ($_REQUEST['direction'] as $direction) {
                    $arDirectionFilter[] = array("=PROPERTY_DIRECTION" => $direction);
                }
            }
        }

        //Формирование фильтра для мобильной версии
        if($this->isMobile){
            if (is_array($_REQUEST['district'])) {
                foreach($_REQUEST['district'] as $district) {
                    $arDirectionFilter[] = array("=PROPERTY_DIRECTION" => $district);
                }
            }

            if ($_REQUEST['pricefromNumb']) {
                $arPriceFilter[0]['>=PROPERTY_PRICE_USD'] = (int)$_REQUEST['pricefromNumb'];
            }

            if ($_REQUEST['pricetoNumb']) {
                if($_REQUEST['pricetoNumb'] != (int)self::ROLL_MAX_PRICE_USD) {
                    $arPriceFilter[0]['<=PROPERTY_PRICE_USD'] = (int)$_REQUEST['pricetoNumb'];
                }
            }

            if ($_REQUEST['groundareafrom'] && is_numeric($_REQUEST['groundareafrom']) && $_REQUEST['groundareafrom'] != self::FORM_MIN_TERRITORY_VALUE) {
                $arTerritoryAreaFilter[0][">=PROPERTY_TERRITORY_AREA"] = (int)$_REQUEST['groundareafrom'];
            }

            if ($_REQUEST['groundareato'] && is_numeric($_REQUEST['groundareato']) && $_REQUEST['groundareato'] < $this->getMaxValue('TERRITORY_AREA')) {
                if ($_REQUEST['groundareato'] != self::FORM_MAX_TERRITORY_VALUE) {
                    $arTerritoryAreaFilter[0]["<=PROPERTY_TERRITORY_AREA"] = (int)$_REQUEST['groundareato'];
                }
            }

            if ($_REQUEST['houseareafrom'] && is_numeric($_REQUEST['houseareafrom']) && $_REQUEST['houseareafrom'] != self::FORM_MIN_VALUE) {
                    $arHouseAreaFilter[0][">=PROPERTY_HOUSE_AREA"] = (int)$_REQUEST['houseareafrom'];
            }

            if ($_REQUEST['houseareato'] && is_numeric($_REQUEST['houseareato']) && $_REQUEST['houseareato'] < $this->getMaxValue('HOUSE_AREA')) {
                if ($_REQUEST['houseareato'] != self::FORM_MAX_VALUE) {
                    $arHouseAreaFilter[0]["<=PROPERTY_HOUSE_AREA"] = (int)$_REQUEST['houseareato'];
                }
            }

            if ($_REQUEST['mkadFrom'] && is_numeric($_REQUEST['mkadFrom']) && $_REQUEST['mkadFrom'] != self::FORM_MIN_VALUE) {
                $arMKADFilter[0][">=PROPERTY_REMOTNESS_MKAD"] = (int)$_REQUEST['mkadFrom'];
            }

            if ($_REQUEST['mkadTo'] && is_numeric($_REQUEST['mkadTo']) && $_REQUEST['mkadTo'] < $this->getMaxValue('REMOTNESS_MKAD')) {
                $arMKADFilter[0]["<=PROPERTY_REMOTNESS_MKAD"] = (int)$_REQUEST['mkadTo'];
            }
            if (is_array($_REQUEST['facing'])) {
                foreach($_REQUEST['facing'] as $facing) {
                    $arFacingFilter[] = array("=PROPERTY_FACING" => $facing);
                }
            }

            if (!empty($_REQUEST['type'])) {
                foreach($_REQUEST['type'] as $type) {
                    $arTypeFilter[] = array("=PROPERTY_OBJECT_TYPE" => $type);
                }
            }

            if ((int)$_REQUEST['id'] >0) {
                $arFilter['CODE'] = (int)$_REQUEST['id'];
            }

            if (strlen(trim($_REQUEST['name'])) >0) {
                $arFilter['NAME'] = '%'.trim($_REQUEST['name']).'%';
            }
        }

        if($arDirectionFilter){
            $arDirectionFilter['LOGIC'] = "OR";
            $arFilter[] = $arDirectionFilter;
        }

        if($arMKADFilter){
            $arMKADFilter['LOGIC'] = "OR";
            $arFilter[] = $arMKADFilter;
        }

        if($arPriceFilter){
            $arPriceFilter['LOGIC'] = "OR";
            $arFilter[] = $arPriceFilter;
        }

        if($arHouseAreaFilter){
            $arHouseAreaFilter['LOGIC'] = "OR";
            $arFilter[] = $arHouseAreaFilter;
        }

        if($arTerritoryAreaFilter){
            $arTerritoryAreaFilter['LOGIC'] = "OR";
            $arFilter[] = $arTerritoryAreaFilter;
        }

        if ($arFacingFilter) {
            $arFacingFilter['LOGIC'] = "OR";
            $arFilter[] = $arFacingFilter;
        }

        if ($arGroundAreaFilter) {
            $arGroundAreaFilter['LOGIC'] = "OR";
            $arFilter[] = $arGroundAreaFilter;
        }

        if ($arTypeFilter) {
            $arTypeFilter['LOGIC'] = "OR";
            $arFilter[] = $arTypeFilter;
        }

        if($arIDorNameFilter) {
            $arFilter[] = $arIDorNameFilter;
        }

        //var_dump($arFilter);
        return array('FILTER' => $arFilter, 'SORT' => $arSort);
    }

    //Вывод json ответа
    private function outputJSON($resObjects, $arFilter, $dbRes, $section){
        $this->arJson['items'] = array();
        foreach($resObjects as $object){
            $direction = '';
            if($object['DIRECTION']){
                $direction = $object['DIRECTION'];
            }
            if($object['REMOTNESS_MKAD'] && $object['DIRECTION']){
                $direction .= ', ';
            }
            if($object['REMOTNESS_MKAD']){
                $direction .= $object['REMOTNESS_MKAD'].' км от МКАД';
            }

            $object['PRICE_RUB'] = $object['PRICE_RUB'] . ' ₽';


            if ($object["HIDE_PRICES"] == "Y") {
                $object['PRICE_USD'] = 'Цена по запросу';
                $object['PRICE_RUB'] = '';
            } elseif (isset($object["PRICE_USD"])) {
                $object['PRICE_USD'] = (str_replace(" ", "", str_replace(" ", "", $object['PRICE_USD'])) > 1) ? $object['PRICE_USD'].' $' : 'Цена по запросу';
                $USD = str_replace(" ", "", $object['PRICE_USD']); // Приводим к нормальному виду
            } else { // Хак для евро
                $object['PRICE_USD'] = $object['PRICE_EUR'] . ' €'; // Приводим к нормальному виду
            }

            if (isset($object['ARENDA_USD'])) {
                $object['ARENDA_USD'] = (str_replace(" ", "", $object['ARENDA_USD']) > 1) ? $object['ARENDA_USD'].' $' : 'Цена по запросу';
                $object['ARENDA_RUB'] = (str_replace(" ", "", $object['ARENDA_USD']) > 1) ? $object['ARENDA_RUB'].' ₽' : '';
            } elseif (isset($object['ARENDA_EUR'])) { // Хак для евро
                $object['ARENDA_USD'] = (str_replace(" ", "", $object['ARENDA_EUR']) > 1) ? $object['ARENDA_EUR'].' €' : 'Цена по запросу';
                $object['ARENDA_RUB'] = (str_replace(" ", "", $object['ARENDA_USD']) > 1) ? $object['ARENDA_RUB'].' ₽' : '';
            } else {
				$object['ARENDA_USD'] = 'Цена по запросу';
                $object['ARENDA_RUB'] = '';
			}



            $object['SECTION'] = ($section == 'arenda') ? $section : $object['SECTION'];

            $this->arJson['items'][] = array(
                "id" => $object['CODE'],
                'crm_id' => ($this->arParams['SECTION_CODE'] == 'poselki') ? $object['CRM_ID'] : $object['CODE'],
                "objId"=>$object['OBJ_ID'],
                "image" => $object['PREVIEW_PICTURE'],
                "name" => $object['NAME'],
                "direction" => $direction,
                "status" => '',
                'new' => ($object['NEW'])?'Y':null,
                'yandex_map' => $object['YANDEX_MAP'],
                "price" => array(
                    "usd" => ($_REQUEST['SECTION'] == 'arenda') ? $object['ARENDA_USD'] : $object['PRICE_USD'],
                    "rub" => ($_REQUEST['SECTION'] == 'arenda') ? (($object['ARENDA_USD'] != 'Цена по запросу')?$object['ARENDA_RUB']:'') : (($object['PRICE_USD'] != 'Цена по запросу')?$object['PRICE_RUB']:''),
                ),
                'priceUpdate' => $object['PRICE_UPDATE_RESULT'],
                "propsList" => array(
                    ($object['HOUSE_AREA'])?$object['HOUSE_AREA']." кв.м.":"",
                    ($object['TERRITORY_AREA'])?$object['TERRITORY_AREA']." соток":"",
                    ($object['FACING'])?$object['FACING']:""
                ),
                "url" => !empty($object['EXT_URL']) ? $object['EXT_URL'] : '/zagorodnaya-nedvizhimost/'.$object['SECTION'].'/'.$object['CODE'].'/',
                "likeBlock" => array(
                    "action" => (\metrium\Likes::hasLike($object['OBJ_ID']))?"dislike":"like"
                ),
                'sectionCode' => $this->arParams['SECTION_CODE']
            );
        }

        $this->arJson['itemsCount'] = $dbRes->SelectedRowsCount();
        $arPager = $dbRes->GetPageNavStringEx($dbRes, 'Элитная загородная недвижимость', 'zagorod_new_json', 'Y');
        $this->arJson['pager'] = json_decode($arPager, true);
        $this->arJson['showBy'] = $this->objectsPerPage;
        $this->arJson['sortBy'] = $_REQUEST['sortDir'];
        $this->arJson['filters'] = $this->getJSONFilters($_REQUEST['SECTION'], $arFilter, $this->inputOptions, $this->checkedOptions);
    }

    //Формирование фильтров для json
    private function getJSONFilters($section, $arFilter, $inputOptions, $checkedOptions) {

        $arRes = array();
        $arRes['DEBUG']['arFilter'] = $checkedOptions;

        //Напрвления
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_DIRECTION'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_DIRECTION'
            )

        );

        $arFilters['DIRECTIONS']['items'] = array();
        while($direction = $dbRes->Fetch()){

            $isChecked = false;

            if(in_array($direction['PROPERTY_DIRECTION_VALUE'], $_REQUEST['direction'])){
                $isChecked = true;
            }

            $arFilters['DIRECTIONS']['items'][] = array(
                'name' => $direction['PROPERTY_DIRECTION_VALUE'],
                'count' => $direction['CNT'],
                'value' => $direction['PROPERTY_DIRECTION_VALUE'],
                'checked' => $isChecked,
				'hash' => md5('Направление'.$direction['PROPERTY_DIRECTION_VALUE']),
                'type' => 'check'
            );
        }

        $arRes[] = array(
            'name' => 'Направление',
            'attr' => array(
                'type' => 'check',
                'name' => 'direction'
            ),
            'items' => $arFilters['DIRECTIONS']['items']
        );

        //Расстояние от МКАД
        $arRes[] = array(
            'name' => 'Расстояние от МКАД, км',
            'attr' => array(
                'type' => 'text'
            ),
            'items' => array(
                array(
                    'name' => 'remotnessMKADFrom',
                    'value' => $_REQUEST['remotnessMKADFrom'],
                    'placeholder' => 'от',
                    'type' => 'text'
                ),
                array(
                    'name' => 'remotnessMKADTo',
                    'value' => $_REQUEST['remotnessMKADTo'],
                    'placeholder' => 'до',
                    'type' => 'text'
                )
            )
        );

        //Цена
        $arRes[] = array(
            'name' => 'Цена',
            'attr' => array(
                'type' => 'text'
            ),
            'items' => array(
                array(
                    'name' => 'currentValue',
                    'value' => 'USD',
                    'items' => array(
                        array(
                            'value' => 'USD',
                            'text' => 'Доллар'
                        ),
                        array(
                            'value' => 'RUB',
                            'text' => 'Рубль'
                        )
                    ),
                    'type' => 'select'
                ),
                array(
                    'name' => 'priceFrom',
                    'value' => $_REQUEST['priceFrom'],
                    'placeholder' => 'от',
                    'type' => 'text'
                ),
                array(
                    'name' => 'priceTo',
                    'value' => $_REQUEST['priceTo'],
                    'placeholder' => 'до',
                    'type' => 'text'
                )
            )
        );

        //Площадь участка
        $arRes[] = array(
            'name' => 'Площадь участка, соток',
            'attr' => array(
                'type' => 'text'
            ),
            'items' => array(
                array(
                    'name' => 'groundAreaFrom',
                    'value' => $_REQUEST['groundAreaFrom'],
                    'placeholder' => 'от',
                    'type' => 'text'
                ),
                array(
                    'name' => 'groundAreaTo',
                    'value' => $_REQUEST['groundAreaTo'],
                    'placeholder' => 'до',
                    'type' => 'text'
                )
            )
        );


        //Площадь дома
        if($section != 'uchastki') {
            $arRes[] = array(
                'name' => 'Площадь дома, кв.м.',
                'attr' => array(
                    'type' => 'text'
                ),
                'items' => array(
                    array(
                        'name' => 'houseAreaFrom',
                        'value' => $_REQUEST['houseAreaFrom'],
                        'placeholder' => 'от',
                        'type' => 'text'
                    ),
                    array(
                        'name' => 'houseAreaTo',
                        'value' => $_REQUEST['houseAreaTo'],
                        'placeholder' => 'до',
                        'type' => 'text'
                    )
                )
            );
        }

        //Отделка
        if($section != 'poselki' && $section != 'uchastki') {
            $dbRes = $this->el->GetList(
                array(),
                $arFilter,
                array('PROPERTY_FACING'),
                false,
                array(
                    'ID',
                    'NAME',
                    'PROPERTY_FACING'
                )

            );
            $arFilters['FACING']['items'] = array();
            while ($facing = $dbRes->Fetch()) {

                $isChecked = false;

                if (in_array($facing['PROPERTY_FACING_VALUE'], $_REQUEST['facing'])) {
                    $isChecked = true;
                }

				if($facing['PROPERTY_FACING_VALUE']){
					$arFilters['FACING']['items'][] = array(
						'name' => $facing['PROPERTY_FACING_VALUE'],
						'count' => $facing['CNT'],
						'value' => $facing['PROPERTY_FACING_VALUE'],
						'checked' => $isChecked,
						'hash' => md5('Отделка'.$facing['PROPERTY_FACING_VALUE']),
                        'type' => 'check'
					);
				}
			}

            $arRes[] = array(
                'name' => 'Отделка',
                'attr' => array(
                    'type' => 'check',
                    'name' => 'facing'
                ),
                'items' => $arFilters['FACING']['items']
            );
        }

        return $arRes;
    }

    public function getFilters($section){

        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'),
            array(
                'LOGIC' => 'OR',
                '>PROPERTY_PRICE_USD' => 2,
                '>PROPERTY_PRICE_EUR' => 2,
                '>PROPERTY_PRICE_RUB' => 1000
            )
        );

        if(array_key_exists('NEW', $_GET))
        {
            $arFilter['PROPERTY_IS_NEW'] = 'Y';
            //$arFilter['>=DATE_CREATE'] = date('d.m.Y H:i:s', strtotime('-30 days'));
        }

        if($section == 'arenda'){
            $arFilter['PROPERTY_ARENDA'] = '1';
        }else{
            $arFilter['PROPERTY_OBJECT_TYPE'] = $section;
        }

        if($this->direction['NAME']){
            $arFilter['!PROPERTY_DIRECTION'] = false;
        }

        //Тип объекта
        $typeNameByCode = array(
            self::POSELKI_CODE => self::POSELKI_TEXT,
            self::KOTTEDZHI_CODE => self::KOTTEDZHI_TEXT,
            self::UCHASTKI_CODE => self::UCHASTKI_TEXT
        );
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_OBJECT_TYPE'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_OBJECT_TYPE'
            )
        );

        while($type = $dbRes->Fetch()){
            if ($type['PROPERTY_OBJECT_TYPE_VALUE'] != null) {
                $type['NAME'] = $typeNameByCode[$type['PROPERTY_OBJECT_TYPE_VALUE']];
                $arFilters['TYPES'][] = $type;
            }
        }

        //Напрвления
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_DIRECTION'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_DIRECTION'
            )
        );

        while ($direction = $dbRes->Fetch()) {
            $subFilter = ($this->direction['NAME'] == $direction['PROPERTY_DIRECTION_VALUE']) ? array('CURRENT' => 'Y') : array('CURRENT' => 'N');
            $arFilters['DIRECTIONS'][] = $direction + $subFilter;
        }

        //Аренда
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_ARENDA'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_ARENDA'
            )

        );
        while($arenda = $dbRes->Fetch()){
            $arFilters['ARENDA'][] = $arenda;
        }

        //Охрана
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_GUARDED'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_GUARDED'
            )
        );

        while($guard = $dbRes->Fetch()){
            $arFilters['GUARDED'][] = $guard;
        }

        //Отделка
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_FACING'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_FACING'
            )
        );

        while($facing = $dbRes->Fetch()){
            if (!empty($facing['PROPERTY_FACING_VALUE'])) {
                $arFilters['FACING'][] = $facing;
            }
        }

        //Материал дома
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_MATERIAL'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_MATERIAL'
            )
        );

        while($material = $dbRes->Fetch())
        {
            $arFilters['MATERIAL'][] = $material;
        }

        //Тип дома
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_HOUSE_TYPE'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_HOUSE_TYPE'
            )
        );

        while($type = $dbRes->Fetch()){
            $arFilters['HOUSE_TYPE'][] = $type;
        }

        //Тип земельного участка
        $dbRes = $this->el->GetList(
            array(),
            $arFilter,
            array('PROPERTY_UCHASTOK_TYPE'),
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_UCHASTOK_TYPE'
            )
        );

        while($type = $dbRes->Fetch()){
            $arFilters['UCHASTOK_TYPE'][] = $type;
        }

        $arFilters['MAX_REMOTNESS_MKAD']    = ceil($this->getMaxValue('REMOTNESS_MKAD', $this->arParams['SECTION_CODE']));
        $arFilters['MAX_TERRITORY_AREA']    = ceil($this->getMaxValue('TERRITORY_AREA', $this->arParams['SECTION_CODE']));
        $arFilters['MAX_HOUSE_AREA']        = ceil($this->getMaxValue('HOUSE_AREA', $this->arParams['SECTION_CODE']));
        $arFilters['MAX_PRICE']             = $this->getMaxPrice($this->arParams['SECTION_CODE']);

        return $arFilters;
    }

    public function getNewObjects($section)
    {
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter')
        );

        if($section == 'arenda'){
            $arFilter['PROPERTY_ARENDA'] = '1';

            $subFilter = array( // Убираем элементы только для аренды
                'LOGIC' => 'OR',
                '>PROPERTY_ARENDA_USD' => 2,
                '>PROPERTY_ARENDA_EUR' => 2,
                '>PROPERTY_ARENDA_RUB' => 1000
            );

            $arFilter = array_merge($arFilter, array($subFilter));
        }else{
            $arFilter['PROPERTY_OBJECT_TYPE'] = $section;

            $subFilter = array( // Убираем элементы только для аренды
                'LOGIC' => 'OR',
                '>PROPERTY_PRICE_USD' => 2,
                '>PROPERTY_PRICE_EUR' => 2,
                '>PROPERTY_PRICE_RUB' => 1000
            );

            $arFilter = array_merge($arFilter, array($subFilter));
        }
        //$arFilter['>=DATE_CREATE'] = date('d.m.Y H:i:s', strtotime('-30 days'));
        $arFilter['PROPERTY_IS_NEW'] = 'Y';

        $dbRes = $this->el->GetList(
            array('SORT' => 'DESC'),
            $arFilter,
            false,
            array("nTopCount" => 4),
            array(
                'ID',
                'NAME',
                'CODE',
                'PREVIEW_PICTURE',
                'PROPERTY_PHOTO',
                'PROPERTY_DIRECTION',
                'PROPERTY_REMOTNESS_MKAD',
                'PROPERTY_HOUSE_AREA',
                'PROPERTY_TERRITORY_AREA',
                'PROPERTY_FACING',
                'PROPERTY_PRICE_EUR',
                'PROPERTY_PRICE_RUB',
                'PROPERTY_PRICE_USD',
                'PROPERTY_OBJECT_TYPE',
                'PROPERTY_ARENDA_USD',
                'PROPERTY_ARENDA'
            )
        );

        while($object = $dbRes->Fetch()){
            $this->newObjectIds[] = $object['ID'];
            $ar = \metrium\helpers\EliteHelper::ResizePhotoObjectList($object['PREVIEW_PICTURE']);

            $type = ($this->arParams['SECTION_CODE'] == 'arenda') ? 'arenda' : $object['PROPERTY_OBJECT_TYPE_VALUE'];

            $tempArr = array(
                'ID' => $object['ID'],
                'NAME' => $object['NAME'],
                'CODE' => $object['CODE'],
                'PREVIEW_PICTURE' => $ar['src'],
                'DIRECTION' => $object['PROPERTY_DIRECTION_VALUE'],
                'REMOTNESS_MKAD' => $object['PROPERTY_REMOTNESS_MKAD_VALUE'],
                'HOUSE_AREA' => $object['PROPERTY_HOUSE_AREA_VALUE'],
                'TERRITORY_AREA' => $object['PROPERTY_TERRITORY_AREA_VALUE'],
                'FACING' => $object['PROPERTY_FACING_VALUE'],
                'SECTION' => $object['PROPERTY_OBJECT_TYPE_VALUE'],
                'URL' => '/zagorodnaya-nedvizhimost/' . $type . '/' . $object['CODE'] . '/'
            );

            $currency = $this->el->GetList(
                array(),
                array("ACTIVE" => "Y", "IBLOCK_CODE" => $object["PROPERTY_OBJECT_TYPE_VALUE"], "PROPERTY_CRM_ID" => $object["CODE"]),
                false,
                false,
                array("ID", "IBLOCK_ID", "PROPERTY_CURRENCY", "PROPERTY_HIDE_PRICES", "PROPERTY_ARENDA_RUB", "PROPERTY_ARENDA_USD", "PROPERTY_ARENDA_EUR")
            )->Fetch();

            $tempArr["HIDE_PRICES"] = $currency["PROPERTY_HIDE_PRICES_VALUE"];

            $prices = array(
                "RUB" => $object["PROPERTY_PRICE_RUB_VALUE"],
                "USD" => $object["PROPERTY_PRICE_USD_VALUE"],
                "EUR" => $object["PROPERTY_PRICE_EUR_VALUE"],
                "RENT_RUB" => $currency["PROPERTY_ARENDA_RUB_VALUE"],
                "RENT_USD" => $currency["PROPERTY_ARENDA_USD_VALUE"],
                "RENT_EUR" => $currency["PROPERTY_ARENDA_EUR_VALUE"],
            );

            $tempArr = array_merge($tempArr, metrium\Price::getFormatPrices($currency["PROPERTY_CURRENCY_VALUE"], $prices));
            $resObjects[] = $tempArr;
        }
        return $resObjects;
    }

    private function getDirectionData() {
        $result = array();

        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "CODE",
            "DETAIL_PICTURE",
            "DETAIL_TEXT"
        );

        $seo_type = "";
        if (!empty($this->arParams['SEO_TYPE'])) {
            $seo_type = strtoupper($this->arParams['SEO_TYPE']);
            $arSelectAddition = array(
                "PROPERTY_".$seo_type."_META_DESCRIPTION",
                "PROPERTY_".$seo_type."_META_H1",
                "PROPERTY_".$seo_type."_META_KEYWORDS",
                "PROPERTY_".$seo_type."_META_TITLE",
                "PROPERTY_".$seo_type."_DESCRIPTION",
            );
            $arSelect = array_merge($arSelect, $arSelectAddition);
        }
        $arFilter = array(
            "IBLOCK_ID"=>\metrium\helpers\IBlockHelper::getIblockIdByCode(self::DIRECTION_BLOCK_CODE),
            "CODE" => $this->arParams['CODE'],
            "ACTIVE"=>"Y"
        );

        $cdbrResult = $this->el->GetList(array(), $arFilter, false, false, $arSelect);
        if ($element = $cdbrResult->GetNext()) {
            $arItem = array(
                "ID" => $element["ID"],
                "NAME" => $element['NAME'],
                "DETAIL_PICTURE" => $element['DETAIL_PICTURE'],
                "DETAIL_TEXT" => $element['DETAIL_TEXT'],
            );
            if (!empty($seo_type)) {
                if (!empty($element["PROPERTY_".$seo_type."_DESCRIPTION_VALUE"])) {
                    $arItem['DETAIL_TEXT'] = htmlspecialcharsBack($element["PROPERTY_".$seo_type."_DESCRIPTION_VALUE"]['TEXT']);
                } elseif ($this->arParams['IBLOCK_CODE'] == self::POSELKI_CODE) {
                    $arItem['DETAIL_TEXT'] = '';
                }
                $arItemProps = array(
                    "META_DESCRIPTION" => $element["PROPERTY_".$seo_type."_META_DESCRIPTION_VALUE"],
                    "META_H1" => $element["PROPERTY_".$seo_type."_META_H1_VALUE"],
                    "META_KEYWORDS" => $element["PROPERTY_".$seo_type."_META_KEYWORDS_VALUE"],
                    "META_TITLE" => $element["PROPERTY_".$seo_type."_META_TITLE_VALUE"],
                );
                $arItem = array_merge($arItem, $arItemProps);
            }
            $result = $arItem;
        }

        return $result;
    }

    public function getSettlementsByDirection($ID) {
        $sectionID = false;
        $result = array();
        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "CODE"
        );

        $arFilter = array(
            "IBLOCK_ID" => \metrium\helpers\IBlockHelper::getIblockIdByCode(self::LOCALITIES_CODE),
            "ACTIVE" => 'Y',
            "UF_LINKROAD" => $ID
        );

        $cdbResult = $this->CIBlockSection->GetList(array(), $arFilter, false, $arSelect);
        while ($element = $cdbResult->GetNext()) {
            $sectionID = $element['ID'];
        }

        if (!$sectionID) {
            return $result;
        }

        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "CODE",
            "DETAIL_PAGE_URL"
        );

        $arFilter = array(
            "IBLOCK_ID" => \metrium\helpers\IBlockHelper::getIblockIdByCode(self::LOCALITIES_CODE),
            "ACTIVE" => 'Y',
            "SECTION_ID" => $sectionID
        );

        $cdbResult = $this->el->GetList(array(), $arFilter, false, false, $arSelect);
        while ($element = $cdbResult->GetNext()) {
            $result[] = $element;
        }
        return $result;
    }

    /** Возвращает максимальное значения поля $field
     *
     * @param $field string
     * @return string
     */
    private function getMaxValue($field, $section_code = '')
    {
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter')
        );

        if($section_code == 'arenda') {
            $arFilter['PROPERTY_ARENDA'] = 1;
        }else {
            $arFilter['PROPERTY_OBJECT_TYPE'] = $section_code;
        }

        $dbRes = $this->el->GetList(
            array('PROPERTY_' . $field => 'DESC'),
            $arFilter,
            array('PROPERTY_' . $field),
            false
        );

        $res = $dbRes->Fetch();

        return $res['PROPERTY_' . $field . '_VALUE'];
    }

    /** Возвращает максимальное значения цен
     *
     * @return string
     */
    private function getMaxPrice($section_code = '')
    {
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter')
        );

        if($section_code == 'arenda') {
            $arFilter['PROPERTY_ARENDA'] = 1;
            $sortArray = array('PROPERTY_ARENDA_USD' => 'DESC');
            $arrayTo = array('PROPERTY_ARENDA_USD', 'PROPERTY_ARENDA_RUB');
        }else {
            $arFilter['PROPERTY_OBJECT_TYPE'] = $section_code;
            $sortArray = array('PROPERTY_PRICE_USD' => 'DESC');
            $arrayTo = array('PROPERTY_PRICE_USD', 'PROPERTY_PRICE_RUB');
        }



        $dbResUsd = $this->el->GetList(
            $sortArray,
            $arFilter,
            $arrayTo,
            false
        );

        $price = $dbResUsd->Fetch();

        if($section_code == 'arenda') {
            return array(
                'rub' => ceil($price['PROPERTY_ARENDA_RUB_VALUE']),
                'usd' => ceil($price['PROPERTY_ARENDA_USD_VALUE'])
            );
        }else {
            return array(
                'rub' => ceil($price['PROPERTY_PRICE_RUB_VALUE']),
                'usd' => ceil($price['PROPERTY_PRICE_USD_VALUE'])
            );
        }


    }

    public function setSEOh1($section)
    {
        switch($section){
            case'kottedzhi_taunkhausy':
                $this->arResult['H1'] = 'Элитные загородные коттеджи и таунхаусы';
                break;
            case'poselki':
                $this->arResult['H1'] = 'Элитные загородные посёлки';
                break;
            case'uchastki':
                $this->arResult['H1'] = 'Элитные загородные участки';
                break;
            case'arenda':
                $this->arResult['H1'] = 'Аренда элитной загородной недвижимости';
                break;
            case'':
                $this->arResult['H1'] = 'Элитная загородная недвижимость';
                break;
        }
    }
    private function setSEO($section)
    {
        global $APPLICATION;
        switch($section){
            case'kottedzhi_taunkhausy':
                $this->arResult['H1'] = 'Элитные загородные коттеджи и таунхаусы';
                $APPLICATION->SetPageProperty('h1', 'Элитные коттеджи и загородные дома в Москве и Подмосковье');
                $APPLICATION->SetPageProperty('title', 'Купить элитные загородные дома и коттеджи в Москве и поселках Подмосковья | Продажа, покупка, строительство недвижимости под ключ ');
                $APPLICATION->SetPageProperty('keywords', 'дома элитные в подмосковье загородные купить продажа недвижимость продажа коттеджи москве поселки строительство куплю недвижимость фото под ключ покупка');
                $APPLICATION->SetPageProperty('description', 'Компания «Метриум Групп» предлагает купить элитные загородные дома и коттеджи в Москве и Подмосковье на выгодных условиях. В продаже есть большое количество недвижимости различной площади в элитных поселках. На сайте вы можете посмотреть предложения, цены, фото. Обращайтесь по телефону +7 (499) 270-20-20.');
                break;
            case'poselki':
                $this->arResult['H1'] = 'Элитные загородные посёлки';
                $APPLICATION->SetPageProperty('h1', 'Элитные загородные посёлки');
                $APPLICATION->SetPageProperty('title', 'Загородные коттеджные поселки | Компания «Метриум Групп»');
                $APPLICATION->SetPageProperty('keywords', 'загородные коттеджные поселки');
                $APPLICATION->SetPageProperty('description', 'Компания «Метриум Групп» предлагает купить элитную недвижимость в загородных коттеджных поселках Подмосковья. Дополнительная информация по телефону: +7 (499) 270-20-20.');
                break;
            case'uchastki':
                $this->arResult['H1'] = 'Элитные загородные участки';
                $APPLICATION->SetPageProperty('h1', 'Элитные загородные участки');
                $APPLICATION->SetPageProperty('title', 'Участки на Рублевке по выгодным ценам от компании «Метриум Групп» | Купить земельные участки на Рублевке');
                $APPLICATION->SetPageProperty('keywords', 'земля на рублевке цена купить земельный участок стоимость');
                $APPLICATION->SetPageProperty('description', 'Компания «Метриум Групп» предлагает купить земельные участки на Рублевке по выгодным ценам. Для получения более подробной информации по стоимости предложений и другим вопросам обращайтесь по телефону 8 (495) 104-87-44.');
                break;
            case'arenda':
                $this->arResult['H1'] = 'Аренда элитной загородной недвижимости';
                $APPLICATION->SetPageProperty('h1', 'Аренда элитной загородной недвижимости');
                $APPLICATION->SetPageProperty('title', 'Аренда загородной недвижимости от компании «Метриум Групп» (Москва)');
                $APPLICATION->SetPageProperty('keywords', 'загородная недвижимость аренда');
                $APPLICATION->SetPageProperty('description', 'Компания «Метриум Групп» предлагает аренду загородной недвижимости. На нашем сайте вы можете посмотреть предложения, узнать цены. Дополнительная информация по телефону в Москве 8 (499) 270-20-20.');
                break;
            case'':
                $this->arResult['H1'] = 'Элитная загородная недвижимость';
                $APPLICATION->SetPageProperty('h1', "Элитная загородная недвижимость");
                if($this->arResult['DIRECTION']) {
                    $APPLICATION->SetPageProperty('h1', !empty($this->arResult['DIRECTION']['META_TITLE']) ? $this->arResult['DIRECTION']['META_TITLE'] : "Дома по направлению ".$this->arResult['DIRECTION']['NAME'].' шоссе');
                    $APPLICATION->SetPageProperty('title', !empty($this->arResult['DIRECTION']['META_TITLE']) ? $this->arResult['DIRECTION']['META_TITLE'] : "Дома по направлению ".$this->arResult['DIRECTION']['NAME'].' шоссе');
                    $APPLICATION->SetPageProperty('keywords', !empty($this->arResult['DIRECTION']['META_KEYWORDS']) ? $this->arResult['DIRECTION']['META_KEYWORDS'] : "Дома по направлению ".$this->arResult['DIRECTION']['NAME'].' шоссе');
                    $APPLICATION->SetPageProperty('description', !empty($this->arResult['DIRECTION']['META_DESCRIPTION']) ? $this->arResult['DIRECTION']['META_DESCRIPTION'] : "Дома по направлению ".$this->arResult['DIRECTION']['NAME'].' шоссе');
                }
                break;
        }
    }

}