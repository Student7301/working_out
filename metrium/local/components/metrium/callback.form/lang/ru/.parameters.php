<?
$MESS ['SUBSCRIPTION_GROUP_NAME'] = "Подписки";
$MESS ['SUBSCRIPTION_RUBRIC_NAME'] = "Рубрики для подписки";
$MESS ['POST_GROUP_NAME'] = "Настройки почтового события";
$MESS ['POST_EVENT_ID_NAME'] = "ID почтового события";
$MESS ['MESSAGE_ID_NAME'] = "ID почтового шаблона";
$MESS ['SITE_ID_NAME'] = "ID сайта";
$MESS ['SUCCESS_MESSAGE_NAME'] = "Текст при успешной отправке сообщения";
?>