<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Форма обратной связи",
	"DESCRIPTION" => "Форма отправки сообщений",
	"ICON" => "/images/search_title.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "utility",
		"CHILD" => array(
			"ID" => "contacts"
		)
	),
);

?>