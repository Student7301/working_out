<?
$MESS['FORM_NAME'] = 'Feedback';
$MESS['LAST_FIRST_NAME'] = 'First Name, Last Name';
$MESS['PHONE'] = 'Phone';
$MESS['EMAIL'] = 'E-Mail';
$MESS['NEW_BUILDING'] = 'New buildings';
$MESS['ELITE_BUILDING'] = 'Residential real estate';
$MESS['COMMERCIAL_BUILDING'] = 'Commercial real estate';
$MESS['MESSAGE'] = 'Message';
$MESS['BUTTON_SEND'] = 'Send';
?>