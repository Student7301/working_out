<?
$MESS['FORM_NAME'] = 'обратная связь';
$MESS['LAST_FIRST_NAME'] = 'Имя, Фамилия';
$MESS['PHONE'] = 'Телефон';
$MESS['EMAIL'] = 'Адрес электронной почты';
$MESS['NEW_BUILDING'] = 'Новостройки';
$MESS['ELITE_BUILDING'] = 'Элитная жилая недвижимость';
$MESS['COMMERCIAL_BUILDING'] = 'Коммерческая недвижимость';
$MESS['MESSAGE'] = 'Текст заявки';
$MESS['BUTTON_SEND'] = 'Отправить';
?>