<?
$response = array();
if (empty($_REQUEST['ACTION']) || (empty($_REQUEST['OBJ_ID']) && empty($_REQUEST['LIKE_ID']))) {
    $response['status']= 'fail';
    echo json_encode($response);
    return;
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule('iblock')) {
    $response['status']= 'fail';
    echo json_encode($response);
    return;
}
global $USER;
if($_REQUEST['ACTION'] == 'like') {
    $el = new CIBlockElement();
    $PROP = array();
    $PROP['USER_ID'] = $USER->GetID();  // свойству с кодом 12 присваиваем значение "Белый"
    $PROP['OBJECT_ID'] = $_REQUEST['OBJ_ID'];        // свойству с кодом 3 присваиваем значение 38

    $arLoadProductArray = Array(
        "IBLOCK_ID"      => \metrium\EstateObject::getIblockIdByCode('LIKES'),
        "PROPERTY_VALUES"=> $PROP,
        "NAME"           => "Лайк",
        "ACTIVE"         => "Y",
        "DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
    );

    if($likeId = $el->Add($arLoadProductArray)) {
        $response['status']= 'success';
        $response['likeId'] =$likeId;
    }
    else {
        $response['status'] = 'fail';
        $response['errMessage'] = $el->LAST_ERROR;
    }
}elseif($_REQUEST['ACTION'] == 'dislike') {
    $res = CIBlockElement::GetByID($_REQUEST['LIKE_ID']);
    if($ar_res = $res->GetNext())
        $iblockId = $ar_res['IBLOCK_ID'];
    //проверка, что ID принадлежит инфоблоку лайков
    if($iblockId != \metrium\EstateObject::getIblockIdByCode('LIKES')) {
        $response['status'] = 'fail';
        echo json_encode($response);
        return;
    }
    if(!CIBlockElement::Delete($_REQUEST['LIKE_ID']))
        $response['status'] = 'fail';
    else
        $response['status'] = 'success';
}else {
    $response['status'] = 'fail';
}
echo json_encode($response);
?>