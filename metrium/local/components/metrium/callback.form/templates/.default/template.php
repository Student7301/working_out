<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);?>

<form action="/ajax.forms.php?FORM=callback" method="post" class="c-ajaxForm">
    <div class="ajax-form-mess c-ajax-form-mess"></div>
    <div class="form-title"><?=Loc::getMessage('FORM_NAME')?></div>
    <div class="adap_from">
		<div class="form-line form-line--adap">
			<input type="text" placeholder="<?=Loc::getMessage('LAST_FIRST_NAME')?>" name="name" data-validation="required" class="form-input">
		</div>
		<div class="form-line form-line--adap">
			<input type="text" placeholder="<?=Loc::getMessage('PHONE')?>" name="phone" data-validation="number" class="form-input">
		</div>
	</div>
	<div class="adap_from">
		<div class="form-line form-line--adap">
			<input type="email" placeholder="<?=Loc::getMessage('EMAIL')?>" name="email" class="form-input">
		</div>
		<div class="form-line form-line--adap">
			<select class="custom-select" name="buildings_type">
				<option selected value="0"><?=Loc::getMessage('NEW_BUILDING')?></option>
				<option value="1"><?=Loc::getMessage('ELITE_BUILDING')?></option>
				<option value="2"><?=Loc::getMessage('COMMERCIAL_BUILDING')?></option>
			</select>
		</div>
	</div>
	<div class="adap_from">
		<div class="form-line form-line--adap">
			<textarea placeholder="<?=Loc::getMessage('MESSAGE')?>" name="message" data-validation="required" class="form-input form-input--textarea"></textarea>
		</div>
		<input type="hidden" name="manager_email" value="<?/*$APPLICATION->ShowViewContent("manager_email");*/?>christinar@php73.ru">
		<div class="form-submit bottom-shadow bottom-shadow--small right form-line--adap">
			<input value="<?=Loc::getMessage('BUTTON_SEND')?>" type="submit" name="callback_form" class="btn btn--gray btn--large">
		</div>

	</div>
	<div class="adap_from" style="margin-left: 2%; font-size: 11px; padding-top: 10px;">
		<input type="checkbox" onchange="checkAgreeSendInformation(this);" data-validation="required">
		Согласен на обработку персональных данных. Ставя отметку, я даю свое <a target="_blank" href="/real_estate/declaration/03.07.2017 Согласие на обработку персональных данных.pdf" style="color:#66a3e4;">согласие на обработку моих персональных данных</a> в соответствии с законом
		№152-Ф3 «О персональных данных» от 27.07.2006 г.
	</div>
</form>
