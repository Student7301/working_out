<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$rsSites = CSite::GetList($by="sort", $order="desc", Array());
while ($arSite = $rsSites->Fetch())
{
	$siteID[$arSite["LID"]] = $arSite["LID"];
}

$arComponentParameters = array(
	"GROUPS" => array(
		"POST_PARAMS" => array(
			"NAME" => GetMessage("POST_GROUP_NAME"),
			"SORT" => "200",
		),
	),
	"PARAMETERS" => array(
		"POST_EVENT_ID" => array(
			"PARENT" => "POST_PARAMS",
			"NAME" => GetMessage("POST_EVENT_ID_NAME"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N"
		),

		"MESSAGE_ID" => array(
			"PARENT" => "POST_PARAMS",
			"NAME" => GetMessage("MESSAGE_ID_NAME"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N"
		),

		"SITE_ID" => array(
			"PARENT" => "POST_PARAMS",
			"NAME" => GetMessage("SITE_ID_NAME"),
			"TYPE" => "LIST",
			"MULTIPLE" => "N",
			"VALUES" => $siteID,
		),

		"SUCCESS_MESSAGE" => array(
			"PARENT" => "POST_PARAMS",
			"NAME" => GetMessage("SUCCESS_MESSAGE_NAME"),
			"TYPE" => "TEXT",
			"MULTIPLE" => "N",
			"DEFAULT" => "Сообщение отправлено!",
		),
	),
);

?>
