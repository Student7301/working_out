<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CCallbackForm extends CBitrixComponent
{
    protected   $request;
    public      $arResult;
    protected   $action;
    protected   $params;
    protected   $fields;

    public function onPrepareComponentParams($arParams)
    {
        $this->action = isset($arParams["ACTION"]) ? $arParams["ACTION"] : "render";

        if ($this->action == "send")
            $this->params = $arParams;

        return $arParams;
    }

    public function executeComponent()
    {
        if ($this->action == "send")
            $this->send();

        $this->includeComponentTemplate();
        return $this->arResult;
    }

    private function send()
    {
        $this->request = Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest()->toArray();

        foreach($this->request as $type) {
            foreach ($type as $field) {
                $this->fields[$field['name']] = $field['value'];
            }
        }

        $arFields = array(
            "NAME" => $this->fields["name"],
            "PHONE" => $this->fields["phone"],
            "B_TYPE" => $this->fields["buildings_type"],
            "MESSAGE" => htmlspecialcharsEx($this->fields["message"]),
            "MANAGER_EMAIL" => $this->fields["manager_email"],
            "USER_EMAIL" => $this->fields["email"]
        );

        if($this->fields["buildings_type"] == '1') { //Элитная жилая недвижимость
            $arFields['SEND_TO_MAIL'] = 'ilya.menzhunov@metrium.ru';
        }else {
            $arFields['SEND_TO_MAIL'] = 'Vera.Korotkova@metrium.ru, Darya.Potenko@metrium.ru';
        }

        $errors = array();

        /*if (check_email($this->fields["email"]))
        {
            $arFields["USER_EMAIL"] = $this->fields["email"];
        }
        else
        {
            $error[] = "Некорректный email!";
        }*/

        if ( ! empty($errors))
            $this->response("error", $errors);

        if (CEvent::Send($this->params["POST_EVENT_ID"], $this->params["SITE_ID"], $arFields, "Y", $this->params["MESSAGE_ID"]))
            $this->response("success");
        else
            $this->response("error", "Не удалось отправить сообщение");
    }

    private function response($status, $mess = "Сообщение успешно отправлено!")
    {
        die(json_encode(array("status" => $status, "mess" => $mess)));
    }

}