<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use metrium\Months;
class CNovosti extends CBitrixComponent
{
    public $el;
    public $objectsPerPage;
    public $dateFrom;
    public $dateTo;
    public $arMonthsNormal;
    public $arMonthsReverse;


    public function onPrepareComponentParams($arParams)
    {
        if(!CModule::IncludeModule("iblock")){
            die('Error including module iblock');
        }

        $this->el = new CIBlockElement;
        $this->objectsPerPage = $arParams['NEWS_PER_PAGE']; // Количество новостей на странице
        $this->arMonthsNormal = metrium\Months::getMonthsNormal();
        $this->arMonthsReverse = metrium\Months::getMonthsReverse();
        if((strlen($_REQUEST['month']) > 0) && ($_REQUEST['year'] > 0 )){ // Если переданы значения года и месяца, то установить соответствующую переменную
            $strDateFrom = $_REQUEST['year'] . '-' . $this->arMonthsReverse[$_REQUEST['month']] . '-' . '01' . ' 00:00:00';
            $this->dateFrom = new \Bitrix\Main\Type\DateTime($strDateFrom, 'Y-m-d H:i:s');

            $strDateTo = date('Y-m-d', mktime(0, 0, 0, $this->arMonthsReverse[$_REQUEST['month']] + 1, 0, $_REQUEST['year'])) . ' 23:59:59';
            $this->dateTo = new \Bitrix\Main\Type\DateTime($strDateTo, 'Y-m-d H:i:s');

            global $APPLICATION;
            $APPLICATION->SetPageProperty('title', 'Новости за '.Months::getMonthRusByEng($_REQUEST['month']).' '.$_REQUEST['year'].' года');
            $APPLICATION->SetPageProperty('keywords', 'Новости за '.Months::getMonthRusByEng($_REQUEST['month']).' '.$_REQUEST['year'].' года');
            $APPLICATION->SetPageProperty('description', 'Новости за '.Months::getMonthRusByEng($_REQUEST['month']).' '.$_REQUEST['year'].' года');
            $APPLICATION->SetPageProperty('page_title', 'Новости за '.Months::getMonthRusByEng($_REQUEST['month']).' '.$_REQUEST['year'].' года');
        }
        else{
            LocalRedirect('/news/' . date('Y') . '/'. $this->arMonthsNormal[date('m')] . '/', false, '301 Moved Permanently');
        }

       return $arParams;
    }

    public function executeComponent()
    {
        $this->getNews();
        $this->includeComponentTemplate();
        return $this->arResult;
    }

    public function getNews()
    {
        $dbRes = $this->el->GetList(
            array('ACTIVE_FROM' => 'DESC'),
            array(
                'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE'],
                'ACTIVE' => 'Y',
                '>=DATE_ACTIVE_FROM' => $this->dateFrom,
                '<=DATE_ACTIVE_FROM' => $this->dateTo,
                'PROPERTY_SHOW_IN_MAIN_NEWS_VALUE' => 'Y'
            ),
            false,
            array("nPageSize"=>$this->objectsPerPage),
            array('ID', 'NAME', 'DATE_ACTIVE_FROM', 'CODE', 'PREVIEW_TEXT')

        );

        while($newsItem = $dbRes->Fetch()){

            $arDate = ParseDateTime($newsItem['DATE_ACTIVE_FROM'], FORMAT_DATETIME);
            $newsItem['DATE_ACTIVE_FROM'] = $arDate["DD"]." ".ToLower(GetMessage("MONTH_".intval($arDate["MM"])."_S"))." ".$arDate["YYYY"];

            $this->arResult['NEWS'][] = array(
                'ID' => $newsItem['ID'],
                'NAME' => $newsItem['NAME'],
                'DATE_ACTIVE_FROM' => $newsItem['DATE_ACTIVE_FROM'],
                'PREVIEW_TEXT' => $newsItem['PREVIEW_TEXT'],
                'CODE' => $newsItem['CODE']

                );
            }
        $this->arResult['NAV_STRING'] = $dbRes->GetPageNavStringEx($this, 'Новости', 'news', 'Y');


    }

}