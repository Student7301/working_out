<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Class Calc extends \CBitrixComponent
{
	public $el;

    public function onPrepareComponentParams($arParams)
    {
        if(!CModule::IncludeModule("iblock")){
            die('Error including module iblock');
        }

        $this->el = new CIBlockElement;

        return $arParams;
    }

    public function executeComponent()
    {
        $this->getParams();
        $this->IncludeComponentTemplate();
    }

    public function getParams()
    {
        $dbRes = $this->el->GetList(
            array(),
            array(
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                'ID' => $this->arParams['ELEMENT_ID']
            ),
            false,
            false,
            array(
                'ID',
                'NAME',
                'PROPERTY_price_min',
                'PROPERTY_price_max',
                'PROPERTY_price_step',
                'PROPERTY_period_min',
                'PROPERTY_period_max',
                'PROPERTY_period_step'
            )
        );
        while($result = $dbRes->Fetch())
        {
            $this->arResult['MIN_PRICE'] = $result['PROPERTY_PRICE_MIN_VALUE'];
            $this->arResult['MAX_PRICE'] = $result['PROPERTY_PRICE_MAX_VALUE'];
            $this->arResult['PRICE_STEP'] = $result['PROPERTY_PRICE_STEP_VALUE'];
            $this->arResult['MIN_PERIOD'] = $result['PROPERTY_PERIOD_MIN_VALUE'];
            $this->arResult['MAX_PERIOD'] = $result['PROPERTY_PERIOD_MAX_VALUE'];
            $this->arResult['PERIOD_STEP'] = $result['PROPERTY_PERIOD_STEP_VALUE'];
        }
    }
}