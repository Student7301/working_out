<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$articleType = $_REQUEST['type'];
$cost = $_REQUEST['summ'];
$firstPay = $_REQUEST['firstSumm'];
$age = $_REQUEST['age'];
$sex = $_REQUEST['sex'];
$readiness = $_REQUEST['ready'];
$profitProof = $_REQUEST['certificate'];
$experience = $_REQUEST['experience'];
$employmentType = $_REQUEST['workType'];
$period = $_REQUEST['period'];
$monthProfit = $_REQUEST['monthSumm'];
$firstPayPercent = (int)$_REQUEST['firstSummPercent'];
$page = $_REQUEST['tableStep'];

$APPLICATION->IncludeComponent(
    'metrium:calc.programs',
    '',
    array(
        'IBLOCK_ID'       => \metrium\EstateObject::getIblockIdByCode('bank_offers'),
        'ARTICLE_TYPE'    => $articleType,
        'MIN_PAY'         => $firstPayPercent,
        'CREDIT_PERIOD'   => $period,
        'AGE'             => $age,
        'SEX'             => $sex,
        'READINESS'       => $readiness,
        'PROOF'           => $profitProof,
        'EXPERIENCE'      => $experience,
        'EMPLOYMENT_TYPE' => $employmentType,
        'MONTHLY_PROFIT'  => $monthProfit,
        'COST'            => $cost,
        'FIRST_PAY'       => $firstPay,
        'PAGE'            => $page
    ),
    false
);