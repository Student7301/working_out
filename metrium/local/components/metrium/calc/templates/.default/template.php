<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="main-part right">
    <div class="content-full">
        <div class="main-title">Ипотечный калькулятор</div>
        <div class="filter filter--no-padding filter--button-out">
            <form id="credit-form" action="" method="" data-ajax-url="<?=$templateFolder?>/ajax/filter-result.php">
                <div class="filter-line">
                    <label class="filter-label">Тип недвижимости</label>
                    <div class="filter-full-width small-bottom-shadow clearfix">
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="type" id="type1"<?=($_GET['type']=='Квартира'|| !$_GET)?' checked ':' '?>value="Квартира" class="checkbox">
                            <label for="type1" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Квартира</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="type" id="type2"<?=($_GET['type']=='Апартаменты')?' checked ':' '?>value="Апартаменты" class="checkbox">
                            <label for="type2" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Апартаменты</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="type" id="type3"<?=($_GET['type']=='Коттедж')?' checked ':' '?>value="Коттедж" class="checkbox">
                            <label for="type3" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Коттедж</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="type" id="type4"<?=($_GET['type']=='Земельный участок')?' checked ':' '?>value="Земельный участок" class="checkbox">
                            <label for="type4" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Земельный участок</span></label>
                        </div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Укажите предполагаемую стоимость</label>
                    <div class="range-slider small-bottom-shadow c-range-slider">
<!--                        <div class="val-text left c-min-val"><?/*= $arResult['MIN_PRICE'] */?> руб.</div>
                        <div class="val-text right range-input--max c-max-val"><?/*=$arResult['MAX_PRICE']*/?> руб.</div>
                        <div id="summ" data-min="<?/*=$arResult['MIN_PRICE']*/?>" data-max="<?/*=$arResult['MAX_PRICE']*/?>" data-step="<?/*=$arResult['PRICE_STEP']*/?>" data-val-min="<?/*=(!empty($_GET['price']) ? intval($_GET['price']) : $arResult['MIN_PRICE'])*/?>" data-min-input="price" class="c-range range-block"></div>
                   -->
                        <div class="input-wrap">
                            <input type="text" name="summ" data-validation="required" class="form-input form-input--small c-first-summ c-format-price" autocomplete="off"><span class="input-wrap__text">рублей</span>
                        </div>
                        </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Первоначальный взнос</label>
                    <div class="filter-input filter-full-width small-bottom-shadow">
                        <div class="input-wrap">
                            <input type="text" name="firstSumm" data-validation="required" class="form-input form-input--small c-first-summ c-format-price" autocomplete="off"><span class="input-wrap__text">рублей</span>
                        </div>
                        <div class="filter-describe hidden c-summ-describe">- что составляет 50.00% от полной стоимости</div>
                        <input type="hidden" name="firstSummPercent" class="c-first-summ-percent">
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Укажите ваш возраст</label>
                    <div class="filter-input filter-full-width small-bottom-shadow">
                        <div class="input-wrap">
                            <input type="text" name="age" data-validation="required, number" class="form-input form-input--small" autocomplete="off"><span class="input-wrap__text">лет</span>
                        </div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Укажите ваш пол</label>
                    <div class="filter-full-width small-bottom-shadow clearfix">
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="sex" id="sex-man" checked value="male" class="checkbox">
                            <label for="sex-man" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Мужчина</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="sex" id="sex-feman" value="female" class="checkbox">
                            <label for="sex-feman" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Женщина</span></label>
                        </div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Готовность жилья</label>
                    <div class="filter-full-width small-bottom-shadow clearfix">
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="ready" id="ready-yes" <?=($_GET['ready']=='Готовое'|| !$_GET)?' checked ':' '?> value="Готовое" class="checkbox">
                            <label for="ready-yes" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Готовое</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="ready" id="ready-no" <?=($_GET['ready']=='Строящееся'|| !$_GET)?' checked ':' '?> value="Строящееся" class="checkbox">
                            <label for="ready-no" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Строящееся</span></label>
                        </div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Срок кредита</label>
                    <div class="range-slider small-bottom-shadow c-range-slider">
                        <div class="val-text left c-min-val"><?=$arResult['MIN_PERIOD']?> года</div>
                        <div class="val-text right range-input--max c-max-val"><?=$arResult['MAX_PERIOD']?> лет</div>
                        <div id="period" data-min="<?=$arResult['MIN_PERIOD']?>" data-max="<?=$arResult['MAX_PERIOD']?>" data-step="<?=$arResult['PERIOD_STEP']?>" data-val-min="<?=$arResult['MIN_PERIOD']?>" data-min-input="years" class="c-range range-block"></div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Ваш ежемесячный доход</label>
                    <div class="filter-input filter-full-width small-bottom-shadow">
                        <div class="input-wrap">
                            <input type="text" name="monthSumm" data-validation="required" class="form-input form-input--small c-format-price" autocomplete="off"><span class="input-wrap__text">рублей</span>
                        </div>
                        <div class="filter-describe">- после вычета налогов</div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Подтверждение дохода</label>
                    <div class="filter-full-width small-bottom-shadow clearfix">
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="certificate" id="certificate1" checked value="2-НДФЛ" class="checkbox">
                            <label for="certificate1" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">2-НДФЛ</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="certificate" id="certificate2" value="2-НДФЛ+справка по форме банка" class="checkbox">
                            <label for="certificate2" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">2-НДФЛ + справка по форме банка</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="certificate" id="certificate3" value="Справка по форме банка" class="checkbox">
                            <label for="certificate3" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Справка по форме банка</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-m bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="certificate" id="certificate4" value="Без подтверждения дохода" class="checkbox">
                            <label for="certificate4" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Без подтверждения доходов</span></label>
                        </div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Стаж на последнем месте работы</label>
                    <div class="filter-full-width small-bottom-shadow clearfix">
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience1" checked value="От 6х до 24х месяцев" class="checkbox">
                            <label for="experience1" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">от 6 до 24 мес.</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience2" value="От 3х до 24х месяцев" class="checkbox">
                            <label for="experience2" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">от 3 до 24 мес.</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience3" value="Более 12 месяцев" class="checkbox">
                            <label for="experience3" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">более 12 мес.</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience4" value="От 4х до 12 месяцев" class="checkbox">
                            <label for="experience4" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">от 4 до 12 мес.</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience5" value="От 3х до 12 месяцев" class="checkbox">
                            <label for="experience5" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">от 3 до 12 мес.</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience6" value="От 6 до 12 месяцев" class="checkbox">
                            <label for="experience6" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">от 6 до 12 мес.</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience7" value="От 3х до 6 месяцев" class="checkbox">
                            <label for="experience7" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">от 3 до 6 мес.</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-s bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="experience" id="experience8" value="Менее 3х месяцев" class="checkbox">
                            <label for="experience8" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">менее 3 мес.</span></label>
                        </div>
                    </div>
                </div>
                <div class="filter-line">
                    <label class="filter-label">Тип занятости</label>
                    <div class="filter-full-width small-bottom-shadow clearfix">
                        <div class="checkbox-wrap checkbox-wrap--buttons size-l bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="work-type" id="work-type1" value="Работа по найму" class="checkbox">
                            <label for="work-type1" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Работа по найму</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-l bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="work-type" id="work-type2" value="Индивидуальный предприниматель" class="checkbox">
                            <label for="work-type2" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Индивидуальный предприниматель</span></label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--buttons size-l bottom-shadow bottom-shadow--ex-small">
                            <input type="radio" name="work-type" id="work-type3" value="Собственный бизнес" class="checkbox">
                            <label for="work-type3" class="checkbox-label"><span class="checkbox-icon"></span><span class="checkbox-text">Собственный бизнес</span></label>
                        </div>
                    </div>
                </div>
                <div class="btn-wrap bottom-shadow bottom-shadow--ex-small right">
                    <input type="submit" value="Расcчитать" class="btn btn--full btn--big">
                </div>
            </form>
        </div>
        <div class="consult-order consult-order--after-filter c-accordion">
            <div class="consult-link c-accordion-link">
                <div class="consult-link__text bottom-shadow bottom-shadow--small">Заполнить заявку на консультацию</div>
                <div class="consult-link__icon bottom-shadow bottom-shadow--small"><i class="sprite-icon bottom-arrow"></i></div>
            </div>
            <div class="consult-drop contact-form bottom-shadow c-accordion-drop">
                <?$APPLICATION->IncludeComponent(
	"metrium:calc.form", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"POST_EVENT_ID" => "3441",
		"MESSAGE_ID" => "86",
		"SITE_ID" => "s1",
		"SUCCESS_MESSAGE" => "Сообщение отправлено!"
	),
	false
);?>
            </div>
        </div>
        <div class="c-filter-result filter-result"></div>
    </div>
</div>
<div class="clearfix"></div>