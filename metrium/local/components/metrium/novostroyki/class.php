<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\InheritedProperty;

Loc::loadLanguageFile(__FILE__);

/**
 * Раздел Новостройки
 * Class NovostroykiComponent
 */
class NovostroykiComponent extends \CBitrixComponent
{
    private $similarObjects;
    private $arLikes;
    private $accObjectIds;

    const PARKING_PLACES = 'mashinomesta';
    const MENU_COMPONENT_NAME = 'metrium:novostroyki.right_menu';
    const ABOUT = 'o-proekte';
    const HOW_TO = 'kak-dobratsya';
    const PLAN = 'plan-zastroyki';
    const PANORAMA = 'panorama';
    const LAYOUT = 'planirovki';
    const INFRASTRUCTURE = 'infrastruktura';
    const MORTGAGE = 'ipoteka';
    const PAY_TERMS = 'usloviya-oplaty';
    const PARTICIPANTS = 'uchastniki-proekta';
    const SPEC_OFFER = 'akcija';
    const DOCUMENTS = 'documents';
    const NEWS = 'novosti-proekta';
    const STAGES = 'etapy-stroitelstva';
    const APARTS_WITH_FINISH = 'kvartiry-s-otdelkoj';
    const ADDT_PAGES = 'dop-page';
    const FOR_SALE = 'v-prodazhe';
    const FOR_SALE_SBER = 'v-prodazhe-sberbank';

    /**
     * @inheritdoc
     * @param $arParams
     * @return mixed
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['SEF_MODE'] = trim($arParams['SEF_MODE']);
        if (empty($arParams['SEF_MODE'])) {
            $arParams['SEF_MODE'] = 'Y';
        }

        return $arParams;
    }

    /**
     * @inheritdoc
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        if ($this->arParams["LANG"] == "EN") {
            $this->arParams["SEF_FOLDER"] = "/en/novostroyki/";
        } else {
            $this->arParams["SEF_FOLDER"] = "/novostroyki/";
        }

        // запрещаем работать компоненту без ЧПУ
        if ($this->arParams['SEF_MODE'] != 'Y') {
            ShowError('Компонент не может работать в режиме отличном от ЧПУ.');
            return;
        }

        $arDefaultUrlTemplates404 = array(
            'gotovye' => '#SECTION_CODE#/gotovye/',
            'ryadom-s-metro' => '#SECTION_CODE#/ryadom-s-metro/',
            'metro_domodedovskaya' => '#SECTION_CODE#/metro_domodedovskaya/',
            'krasnogvardejskaya' => '#SECTION_CODE#/krasnogvardejskaya/',
            'ryadom-s-parkom' => '#SECTION_CODE#/ryadom-s-parkom/',
            's-otdelkoj' => '#SECTION_CODE#/s-otdelkoj/',
            'kashirskoe_shosse' => '#SECTION_CODE#/kashirskoe_shosse/',
            'shosse_entuziastov' => '#SECTION_CODE#/shosse_entuziastov/',
            // Административные округа
            'sao' => '#SECTION_CODE#/sao/',
            'szao' => '#SECTION_CODE#/szao/',
            'svao' => '#SECTION_CODE#/svao/',
            'vao' => '#SECTION_CODE#/vao/',
            'yuzao' => '#SECTION_CODE#/yuzao/',
            'zao' => '#SECTION_CODE#/zao/',
            'moskovskij' => '#SECTION_CODE#/moskovskij/',
            // Районы
            'ramenki' => '#SECTION_CODE#/ramenki/',
            'perovo' => '#SECTION_CODE#/perovo/',

            // Улицы, проезды
            'novogireevo' => '#SECTION_CODE#/novogireevo/',
            'biznes-klassa' => '#SECTION_CODE#/biznes-klassa/',
            'leninskij_prospekt' => '#SECTION_CODE#/leninskij_prospekt/',
            'beregovoj_proezd' => '#SECTION_CODE#/beregovoj_proezd/',
            'terleckij_proezd' => '#SECTION_CODE#/terleckij_proezd/',
            'golovinskoe_shosse' => '#SECTION_CODE#/golovinskoe_shosse/',
            'metallurgov' => '#SECTION_CODE#/metallurgov/',
            // Метро
            'dinamo' => '#SECTION_CODE#/dinamo/',

            'angliskiy-kvartal-v-prodazhe' => '#SECTION_CODE#/angliskiy-kvartal/v-prodazhe/',

            'section' => '#SECTION_CODE#/',
            'o-proekte' => '#SECTION_CODE#/#ELEMENT_CODE#/o-proekte/',
            'penthaus' => '#SECTION_CODE#/#ELEMENT_CODE#/penthaus/',
            'kak-dobratsya' => '#SECTION_CODE#/#ELEMENT_CODE#/kak-dobratsya/',
            'plan-zastroyki' => '#SECTION_CODE#/#ELEMENT_CODE#/plan-zastroyki/',
            'etapy-stroitelstva' => '#SECTION_CODE#/#ELEMENT_CODE#/etapy-stroitelstva/',
            'infrastruktura' => '#SECTION_CODE#/#ELEMENT_CODE#/infrastruktura/',
            'v-prodazhe' => '#SECTION_CODE#/#ELEMENT_CODE#/v-prodazhe/',
            'v-prodazhe-s-otdelkoy' => '#SECTION_CODE#/#ELEMENT_CODE#/v-prodazhe/s-otdelkoj/',
            'v-prodazhe-sberbank' => '#SECTION_CODE#/#ELEMENT_CODE#/v-prodazhe-sberbank/',
            'planirovki' => '#SECTION_CODE#/#ELEMENT_CODE#/planirovki/',
            'ipoteka' => '#SECTION_CODE#/#ELEMENT_CODE#/ipoteka/',
            'usloviya-oplaty' => '#SECTION_CODE#/#ELEMENT_CODE#/usloviya-oplaty/',
            'novosti-proekta' => '#SECTION_CODE#/#ELEMENT_CODE#/novosti-proekta/',
            'uchastniki-proekta' => '#SECTION_CODE#/#ELEMENT_CODE#/uchastniki-proekta/',
            'documents' => '#SECTION_CODE#/#ELEMENT_CODE#/documents/',
            'akcija' => '#SECTION_CODE#/#ELEMENT_CODE#/akcija/',
            'mashinomesta' => '#SECTION_CODE#/#ELEMENT_CODE#/mashinomesta/',
            'kvartiry-s-otdelkoj' => '#SECTION_CODE#/#ELEMENT_CODE#/kvartiry-s-otdelkoj/',
            'index' => '#SECTION_CODE#/#ELEMENT_CODE#/.*',
        );

        $arDefaultVariableAliases404 = array();
        $arVariables = array();

        $arComponentVariables = array(
            'SECTION_CODE',
            'ELEMENT_CODE'
        );

        $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);
        $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $this->arParams["VARIABLE_ALIASES"]);

        $componentPage = CComponentEngine::ParseComponentPath(
            $this->arParams["SEF_FOLDER"],
            $arUrlTemplates,
            $arVariables
        );

        /* Редирект с Новостроек.Бизнес-Класса на Новостройки.Москва*/
        if ($arVariables['SECTION_CODE'] == 'novostroyki-moskvy-business-class') {
            LocalRedirect('/novostroyki/novostroyki-moskvy/', false, '301 Moved Permanently');
        }


        /* Страница результатов поиска по фильтру, вывод новостроек сгруппированных по разделам */
        if ($arVariables['SECTION_CODE'] == 'filtered') {
            $componentPage = "filtered";
        }

        $b404 = false;
        if (!$componentPage) {
            $componentPage = "index";
            $b404 = true;
        }


        if ($b404) {
            global $APPLICATION;
            $folder404 = "/404.php";
            if ($folder404 != $APPLICATION->GetCurPage(true)) {
                \CHTTP::SetStatus("404 Not Found");
                //LocalRedirect($folder404);
            }
        }

        \CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

        $this->arResult = array(
            "FOLDER" => $this->arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $arVariables,
            "ALIASES" => $arVariableAliases,
        );

        if ($componentPage == 'index') {
            $this->arResult['INDEX'] = 'Y';
        }

        foreach ($this->arResult['URL_TEMPLATES'] as $code => $template) {
            $this->arResult['~URL_TEMPLATES'][$code] = $this->arParams['SEF_FOLDER'] . \CComponentEngine::makePathFromTemplate($template, $arVariables);
        }
        //добавляем объект в список просмотренных НЕ ЗАНОСИТЬ В ОБЛАСТЬ КЕША!
        if (!empty($arVariables['ELEMENT_CODE']))
            $this->addToReviewList($arVariables['ELEMENT_CODE']);

        return $this->includeComponentTemplate($componentPage);
    }

    private function addToReviewList($code)
    {
        $iblockId = \metrium\EstateObject::getIblockIdByCode('novostroyki');
        $objId = metrium\EstateObject::getObjectIdByCode($code);
        if (!$objId)
            return;
        \metrium\Reviews::addToReviewList($iblockId, $objId);
    }

    /**
     * Получение мета сео
     *
     * @param $id - ID новостройки
     * @param string $dopName - Дополнительная приписка
     * @return array
     */
    public static function getSEOProperty($id, $dopName = '')
    {
        if(LANGUAGE_ID == LANG_EN) {
            $idIBlockNovostroyki = \metrium\helpers\IBlockHelper::getIblockIdByCode('novostroyki_en');
        }else {
            $idIBlockNovostroyki = \metrium\helpers\IBlockHelper::getIblockIdByCode('novostroyki');
        }
        $ipropValues = new InheritedProperty\ElementValues(
            $idIBlockNovostroyki,
            $id
        );
        $meta = $ipropValues->getValues();
        foreach ($meta as &$item) {
            $item .= $dopName;
        }
        return $meta;
    }

    /**
     * Установка мета сео
     *
     * @param $dopName - Дополнительная приписка
     * @param $data
     */
    public static function setSEOProperty($dopName, $data)
    {
        if (is_numeric($data)) {
            $data = self::getSEOProperty($data, $dopName);
        }
        global $APPLICATION;
        $APPLICATION->SetPageProperty('title', $data['ELEMENT_META_TITLE']);
        $APPLICATION->SetPageProperty('keywords', $data['ELEMENT_META_KEYWORDS']);
        $APPLICATION->SetPageProperty('description', $data['ELEMENT_META_DESCRIPTION']);

    }

    public static function setSEOPropertySection($section_code, $arParamsLang, $dopText = '')
    {
        if($arParamsLang != 'EN') {
            $idIBlockNovostroyki = \metrium\helpers\IBlockHelper::getIblockIdByCode('novostroyki');
            $idNovostroyki = \metrium\helpers\IBlockHelper::getSectionIdBySectionCode($section_code);
        }else {
            $idIBlockNovostroyki = \metrium\helpers\IBlockHelper::getIblockIdByCode('novostroyki_en');
            $idNovostroyki = \metrium\helpers\IBlockHelper::getNovostroykiEnIDbySectionCode($section_code);
        }
        $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($idIBlockNovostroyki,$idNovostroyki);
        $IPROPERTY  = $ipropValues->getValues();

        global $APPLICATION;
        $APPLICATION->SetPageProperty('h1', $IPROPERTY['SECTION_PAGE_TITLE'] . ' ' . $dopText);
        $APPLICATION->SetPageProperty('title', $IPROPERTY['SECTION_META_TITLE'] . ' ' . $dopText);
        $APPLICATION->SetPageProperty("keywords", $IPROPERTY['SECTION_META_KEYWORDS'] . ' ' . $dopText);
        $APPLICATION->SetPageProperty("description", $IPROPERTY['SECTION_META_DESCRIPTION'] . ' ' . $dopText);
    }

    /**
     * Получаем 4 объекта близких по цене за кв.м.
     */
    public function getSimilarObjectsNovostroyki($middlePrice)
    {
        /* Получение информации по акциям */
        $this->accObjectIds = NovostroykiComponent::getAcciiObjectsIds();

        /* Получаем Лайки */
        $this->arLikes = \metrium\Likes::getLikes();

        // Получаем 2 объекта с ценой меньше
        $IBlockElement = new CIBlockElement();
        $dbRes = $IBlockElement->GetList(
            array('PROPERTY_middle_price' => 'DESC'),
            array(
                '!SECTION_CODE' => array(false, 'new-building'),
                'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki'),
                '<PROPERTY_middle_price' => $middlePrice,
                'ACTIVE' => 'Y'
            ),
            false,
            array(
                'nTopCount' => 2
            ),
            array(
                'ID',
                'NAME',
                'ACTIVE',
                'PROPERTY_middle_price',
                'DETAIL_PAGE_URL',
                'PREVIEW_PICTURE',
                'PROPERTY_ADDRESS',
                'CODE',
                'PROPERTY_PRICE',
                'PROPERTY_DEADLINE',
                'PROPERTY_photo_info',
                'PROPERTY_district',
                'PROPERTY_metro',
                'PROPERTY_articletypeCalc',
                'PROPERTY_MIN_AP_AREA',
                'PROPERTY_MAX_AP_AREA',
                'PROPERTY_EXTERNAL_OBJECT',
                'PROPERTY_EXTERNAL_OBJECT_URL',
                'PROPERTY_MIN_AP_PRICE',
                'PROPERTY_MAX_AP_PRICE'
            )
        );
        NovostroykiComponent::getResult($dbRes);


        // Получаем 2 объекта с ценой больше
        $IBlockElement = new CIBlockElement();
        $dbRes = $IBlockElement->GetList(
            array('PROPERTY_middle_price' => 'ASC'),
            array(
                '!SECTION_CODE' => array(false, 'new-building'),
                'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki'),
                '>PROPERTY_middle_price' => $middlePrice,
                'ACTIVE' => 'Y'
            ),
            false,
            array(
                'nTopCount' => 2
            ),
            array(
                'ID',
                'NAME',
                'ACTIVE',
                'PROPERTY_middle_price',
                'DETAIL_PAGE_URL',
                'PREVIEW_PICTURE',
                'PROPERTY_ADDRESS',
                'CODE',
                'PROPERTY_PRICE',
                'PROPERTY_DEADLINE',
                'PROPERTY_photo_info',
                'PROPERTY_district',
                'PROPERTY_metro',
                'PROPERTY_articletypeCalc',
                'PROPERTY_MIN_AP_AREA',
                'PROPERTY_MAX_AP_AREA',
                'PROPERTY_EXTERNAL_OBJECT',
                'PROPERTY_EXTERNAL_OBJECT_URL',
                'PROPERTY_MIN_AP_PRICE',
                'PROPERTY_MAX_AP_PRICE'
            )
        );
        NovostroykiComponent::getResult($dbRes);

        if ($this->arParams['LANG'] == 'EN') {
            foreach ($this->similarObjects as $item) {
                $id[] = $item['ID'];
            }
            NovostroykiComponent::getEnObject($id);
        }

        return $this->similarObjects;
    }


    public function getResult($dbRes)
    {
        while ($object = $dbRes->GetNext()) {
            $needBlank = false;
            // Сборка строки метро
            $stMetro = '';
            $metroCounter = 0;
            foreach ($object['PROPERTY_METRO_VALUE'] as $metro) {
                if ($metroCounter == 0) {
                    $stMetro .= $metro;
                } else {
                    $stMetro .= ', ' . $metro;
                }
                $metroCounter++;
            }
            if ($object['PREVIEW_PICTURE']) {
                $ar = CFile::ResizeImageGet(
                    $object['PREVIEW_PICTURE'],
                    array('width' => 400, 'height' => 260),
                    BX_RESIZE_IMAGE_EXACT,
                    false,
                    \metrium\EstateObject::getWaterMarkParams()
                );
            } else {
                $ar = CFile::ResizeImageGet(
                    array_shift($object['PROPERTY_PHOTO_INFO_VALUE']),
                    array('width' => 400, 'height' => 260),
                    BX_RESIZE_IMAGE_EXACT,
                    false,
                    \metrium\EstateObject::getWaterMarkParams()
                );
            }

            if (!empty($object['PROPERTY_EXTERNAL_OBJECT_VALUE']) && !empty($object['PROPERTY_EXTERNAL_OBJECT_URL_VALUE'])) {
                $object['DETAIL_PAGE_URL'] = $object['PROPERTY_EXTERNAL_OBJECT_URL_VALUE'];
                $needBlank = true;
            }

            $this->similarObjects[$object['ID']] = array(
                'ID' => $object['ID'],
                'NAME' => $object['NAME'],
                'PREVIEW_PICTURE_SRC' => $ar['src'],
                'PRICE' => $object['PROPERTY_PRICE_VALUE'],
                'ADDRESS' => $object['PROPERTY_ADDRESS_VALUE'],
                'CODE' => $object['CODE'],
                'DEADLINE' => $object['PROPERTY_DEADLINE_VALUE'],
                'PHOTOS_COUNT' => count($object['PROPERTY_PHOTO_INFO_VALUE']),
                'FLATS_TYPE' => $object['PROPERTY_ARTICLETYPECALC_VALUE'],
                'MIDDLE_PRICE_FOR_M2' => \metrium\Price::getFormattedPrice($object['PROPERTY_MIDDLE_PRICE_VALUE']),
                'MIN_AP_AREA' => $object['PROPERTY_MIN_AP_AREA_VALUE'],
                'MAX_AP_AREA' => $object['PROPERTY_MAX_AP_AREA_VALUE'],
                'EXTERNAL_OBJECT' => $object['PROPERTY_EXTERNAL_OBJECT_VALUE'],
                'EXTERNAL_OBJECT_URL' => $object['PROPERTY_EXTERNAL_OBJECT_URL_VALUE'],
                'ACCIJA' => ($this->accObjectIds[$object['CODE']]) ? 'Y' : 'N',
                'ACCIJA_PREVIEW' => $this->accObjectIds[$object['CODE']],
                'METRO' => $stMetro,
                'DETAIL_PAGE_URL' => $object['DETAIL_PAGE_URL'],
                'MIN_AP_PRICE' => \metrium\Price::getFormattedPrice($object['PROPERTY_MIN_AP_PRICE_VALUE']),
                'MAX_AP_PRICE' => \metrium\Price::getFormattedPrice($object['PROPERTY_MAX_AP_PRICE_VALUE']),
                'NEED_BLANK' => $needBlank,
                //'APARTS_COUNT' => $this->search->getObjectApartsCount($object['ID'])
            );

            if (in_array($object['ID'], $this->arLikes))
                $this->similarObjects[$object['ID']]['LIKE'] = true;
            else
                $this->similarObjects[$object['ID']]['LIKE'] = false;
        }
    }


    public function getEnObject($id)
    {
        $dbRes = CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki_en'),
                'PROPERTY_RU_OBJECT' => $id
            ),
            false,
            array(),
            array(
                'ID',
                'ACTIVE',
                'CODE',
                'IBLOCK_ID',
                'PROPERTY_NAME_EN',
                'PROPERTY_RU_OBJECT',
                'PROPERTY_RU_OBJECT.DETAIL_PAGE_URL',

                'PROPERTY_ADDRESS_EN',
                'PROPERTY_ST_METRO_EN',
                'PROPERTY_READY_EN',
                'PROPERTY_TYPE_EN',
                'PROPERTY_RU_OBJECT.CODE',
                'PROPERTY_RU_OBJECT.IBLOCK_SECTION_ID',
                'PROPERTY_RU_OBJECT.PROPERTY_middle_price',
                'PROPERTY_RU_OBJECT.PROPERTY_MIN_AP_AREA',
                'PROPERTY_RU_OBJECT.PROPERTY_MAX_AP_AREA',
                'PROPERTY_RU_OBJECT.PROPERTY_EXTERNAL_OBJECT',
                'PROPERTY_RU_OBJECT.PROPERTY_EXTERNAL_OBJECT_URL',
                'PROPERTY_AKCIYA_STATUS_EN',
                'PROPERTY_AKCIYA_ANONS_EN',
                'PROPERTY_RU_OBJECT.PROPERTY_MIN_AP_PRICE',
                'PROPERTY_RU_OBJECT.PROPERTY_MAX_AP_PRICE'
            )
        );
        while ($object = $dbRes->GetNext()) {
            $stMetro = '';
            $metroCounter = 0;
            $needBlank = false;
            foreach ($object['PROPERTY_ST_METRO_EN_VALUE'] as $metro) {
                if ($metroCounter == 0) {
                    $stMetro .= $metro;
                } else {
                    $stMetro .= ', ' . $metro;
                }
                $metroCounter++;
            }

            if (!empty($object['PROPERTY_RU_OBJECT_PROPERTY_EXTERNAL_OBJECT_VALUE']) && !empty($object['PROPERTY_RU_OBJECT_PROPERTY_EXTERNAL_OBJECT_URL_VALUE'])) {
                $object['DETAIL_PAGE_URL'] = $object['PROPERTY_RU_OBJECT_PROPERTY_EXTERNAL_OBJECT_URL_VALUE'];
                $needBlank = true;
            }else {
                $object['DETAIL_PAGE_URL'] = "/en".$object['PROPERTY_RU_OBJECT_DETAIL_PAGE_URL'];
            }

            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['NAME'] = $object['PROPERTY_NAME_EN_VALUE'];
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['ADDRESS'] = $object['PROPERTY_ADDRESS_EN_VALUE'];
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['DEADLINE'] = $object['PROPERTY_READY_EN_VALUE'];
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['METRO'] = $stMetro;
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['FLATS_TYPE'] = $object["PROPERTY_TYPE_EN_VALUE"];
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['NEED_BLANK'] = $needBlank;
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['DETAIL_PAGE_URL'] = $object['DETAIL_PAGE_URL'];
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['ACCIJA'] = $object['PROPERTY_AKCIYA_STATUS_EN_VALUE'];
            $this->similarObjects[$object['PROPERTY_RU_OBJECT_VALUE']]['ACCIJA_PREVIEW'] = $object['PROPERTY_AKCIYA_ANONS_EN_VALUE'];
        }

    }

    public function getAcciiObjectsIds()
    {
        $arAcObjIds = array();
        $dbRes = CIBlockElement::GetList(
            array(
                'SORT' => 'ASC'
            ),
            array(
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('akcija')
            ),
            false,
            false,
            array(
                'ID',
                'IBLOCK_ID',
                'CODE',
                'PROPERTY_OBJECT_ID',
                'PROPERTY_PREVIEW',
            )
        );

        while ($arAcObj = $dbRes->Fetch()) {
            $arAcObjIds[$arAcObj['CODE']] = $arAcObj['PROPERTY_PREVIEW_VALUE'];
        }
        return $arAcObjIds;
    }
}