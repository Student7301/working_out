<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/* @var NovostroykiComponent $component */
$APPLICATION->SetPageProperty("title", "Metrium - риелтор нового поколения");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Новости проекта");
?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.novosti-proekta",
    ".default",
    Array(
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'NEWS_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('object_news'),
        'OBJECT_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki'),
        'LANG' => $arParams['LANG'],
        'PAGE' => $component::NEWS
    ),
    $component
);
?>