<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/* @var NovostroykiComponent $component */
$APPLICATION->SetPageProperty("title", "Новостройки. В продаже");
$APPLICATION->SetTitle("Новостройки. В продаже");
?>
<?
$APPLICATION->IncludeComponent(
    "metrium:novostroyki.v-prodazhe",
    ".default",
    Array(
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'ELEMENT_CODE' => 'angliskiy-kvartal',
        'CORPUSES_IBLOCK_CODE' => 'corpuses',
        'SECTIONS_IBLOCK_CODE' => 'sections',
        'APARTMENTS_IBLOCK_CODE' => 'apartments',
        'PLAN_ZASTROYKI_IBLOCK_CODE' => 'plan-zastroyki',
        'LANG' => $arParams['LANG'],
        'PAGE' => $component::FOR_SALE
    ),
    $component
);
?>