<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult['VARIABLES']['SECTION_CODE'] == 'novostroyki-moskvy') {
    global $APPLICATION;
    $APPLICATION->SetPageProperty('h1', 'Новостройки в САО');
    $APPLICATION->SetPageProperty('title', 'Новостройки в САО Москвы от компании «Метриум Групп»');
    $APPLICATION->SetPageProperty("keywords", 'новостройка сао москва');
    $APPLICATION->SetPageProperty("description", 'Компания «Метриум Групп» предлагает купить квартиры в новостройках САО Москвы на выгодных условиях. Для получения более подробной информации обращайтесь по телефону +7 (495) 104-87-44.');
    $params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);
    $params['SEO_TEXT'] = '<p style="text-align: justify;" class="">
	 Риелторская компания «Метриум Групп» представляет вашему вниманию квартиры в новостройках САО Москвы. На этой странице сайта размещен каталог. Наши предложения ежедневно обновляются!
</p>
<p style="text-align: justify;" class="">
	 Северный административный округ включает 16 районов: Дмитровский, Головинский, Войковский, Беговой, Тимирязевский, Молжаниновский, Хорошёвский и другие. Здесь расположены три линии метрополитена.
</p>
<p class="" style="text-align: justify;">
	 Квартиры в новостройках САО пользуются огромным покупательским спросом. Назовем лишь некоторые причины такой популярности:
</p>
<p style="text-align: justify;">
</p>
<ul>
	<li>разнообразие предложений. На выбор покупателя представлены квартиры комфорт-класса, премиум-класса, бизнес-класса, эконом-класса различной площади и планировки;</li>
	<li>отличная транспортная доступность районов округа;<br>
 </li>
	<li>развитая инфраструктура. На территории округа расположены объекты торговли и сервиса, образовательные и медицинские учреждения, спортивные и развлекательные комплексы.</li>
</ul>
 <h2>Как приобрести квартиру в новостройке САО Москвы?</h2><br>
<p class="" style="text-align: justify;">
	 В электронном каталоге представлена актуальная информация о лучших ЖК Северного административного округа. Описание каждого объекта включает в себя его наименование, местоположение, сведения об инфраструктуре и сроках ввода в эксплуатацию. Также здесь вы сможете ознакомиться с характеристиками квартир в новостройках САО.
</p>
<p>
 <p style="font-size: 24px; font-weight: bold">Заинтересовали наши предложения?</p>
</p>
<p class="" style="text-align: justify;">
	 Риелторы «Метриум Групп» предлагают комплекс профессиональных услуг для покупателей:
</p>
<ul>
	<li>консультации;</li>
	<li>подбор объектов по запросу клиента;<br>
 </li>
	<li>организацию просмотров;<br>
 </li>
	<li>проверку документов на квартиру;<br>
 </li>
	<li>помощь в оформлении документов на ипотеку;<br>
 </li>
	<li>правовое сопровождение сделок.</li>
</ul>
<ul>
</ul>
<p class="" style="text-align: justify;">
	Клиентам гарантируются ответственный подход, надежность и выгодные условия сотрудничества.
</p>
<p style="text-align: justify;" class="">
	Заявку на просмотр квартиры в новостройке САО можно сделать на нашем сайте или по телефону.<br>
</p>';
}else {
    ($arParams['LANG'] == 'EN') ? $dopText = 'Building SAO' : $dopText = 'Новостройки САО';
    NovostroykiComponent::setSEOPropertySection($arResult['VARIABLES']['SECTION_CODE'], $arParams['LANG'], $dopText);
}

if($arParams["LANG"] == "EN"){$params["LANG"] = "EN";} ?>
<? $params['SAO'] = 'Y' ?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    ".default",
    $params,
    $component
);

$APPLICATION->IncludeComponent(
    "metrium:banners.rotator",
    "novostroyki",
    array("BANNERS_COUNT" => 50, "CATEGORY" => "BUILDINGS", "BANNER_IBLOCK_CODE" => "banner_rotator")
);
?>