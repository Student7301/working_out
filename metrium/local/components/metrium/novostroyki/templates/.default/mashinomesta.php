<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/* @var NovostroykiComponent $component */
$APPLICATION->SetPageProperty("title", "Metrium - риелтор нового поколения");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Metrium - риелтор нового поколения");
?>
<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.parking",
    ".default",
    Array(
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'LANG' => $arParams['LANG'],
        'PAGE' => $component::PARKING_PLACES
    ),
    $component
);
?>