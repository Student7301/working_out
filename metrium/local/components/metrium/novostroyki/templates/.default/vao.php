<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);

if ($params['SECTION_CODE'] == 'novostroyki-moskvy') {
    global $APPLICATION;
    $APPLICATION->SetPageProperty('h1', 'Новостройки ВАО');
    $APPLICATION->SetPageProperty('title', 'Новостройки в ВАО от компании «Метриум Групп»');
    $APPLICATION->SetPageProperty("keywords", 'новостройки вао');
    $APPLICATION->SetPageProperty("description", 'Компания «Метриум Групп» предлагает купить квартиры в новостройках ВАО на выгодных условиях. В каталоге имеется большое количество предложений с подробным описанием. Обращайтесь по телефону в Москве: +7 (495) 104-87-44.');
    $params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);
    $params['SEO_TEXT'] = '<p style="font-size: 29px; font-weight: bold">
Новостройки ВАО</p>
<p class="" style="text-align: justify;">
	 Хотите жить в чистом районе, с хорошей инфраструктурой и высоким уровнем безопасности? Новостройки ВАО Москвы идеально подойдут для проживания. Район отличается множеством зеленых насаждений, &nbsp;в том числе здесь расположены Измайловский и Сокольнический парки. Через него проходят три «ветки» метрополитена: Сокольническая, Арбатско-Покровская и Калининско-Солнцевская. Плотность населения в районе (по результатам исследования 2016 года) — самая низкая по столице. Именно поэтому квартиры в новостройках ВАО пользуются большой популярностью!
</p>
<p class="" style="text-align: justify;">
	 Компания «Метриум Групп» предлагает вам помощь в подборе и покупке недвижимости в Москве. Если вам нужен надежный риелтор, который обеспечит выгодную и безопасную сделку — обращайтесь к нам.
</p>
<h2>
	 Преимущества покупки квартиры в новостройках ВАО через «Метриум Групп»
</h2>
<p style="text-align: justify;">
</p>
<ul>
	<li>Благодаря налаженному сотрудничеству с застройщиками, мы предлагаем выгодные цены.<br>
 </li>
	<li>Мы подбираем квартиру под индивидуальный запрос заказчика. Вы сообщаете нам свои требования и пожелания, а мы находим идеальный вариант среди новостроек ВАО.<br>
 </li>
	<li>Мы даем гарантии. Вы можете ознакомиться с отзывами наших клиентов, документацией, которая разрешает нашу деятельность, и убедиться самостоятельно в безопасности сделки.</li>
</ul>
<p style="text-align: justify;">
	Свяжитесь с нашим менеджером, позвонив по указанным контактным телефонам. Мы расскажем об условиях сотрудничества и особенностях выбора жилья в том или ином районе. Также вы можете посетить наш офис, чтобы узнать все о приобретении квартиры вашей мечты в одной из новостроек ВАО.
</p>';
}else {
    ($arParams['LANG'] == 'EN') ? $dopText = 'Building VAO' : $dopText = 'Новостройки ВАО';
    NovostroykiComponent::setSEOPropertySection($arResult['VARIABLES']['SECTION_CODE'], $arParams['LANG'], $dopText);
}
?>

<?
if($arParams["LANG"] == "EN"){$params["LANG"] = "EN";} ?>
<? $params['VAO'] = 'Y' ?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    ".default",
    $params,
    $component
);

$APPLICATION->IncludeComponent(
    "metrium:banners.rotator",
    "novostroyki",
    array("BANNERS_COUNT" => 50, "CATEGORY" => "BUILDINGS", "BANNER_IBLOCK_CODE" => "banner_rotator")
);
?>