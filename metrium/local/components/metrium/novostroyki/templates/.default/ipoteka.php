<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/* @var NovostroykiComponent $component */
$APPLICATION->SetPageProperty("title", "Metrium - риелтор нового поколения");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Программы банков");
?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.ipoteka",
    ".default",
    Array(
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'BANKS_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('bank'),
        'BANK_OFFERS_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('bank_offers'),
        'OBJECT_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki'),
        'LANG' => $arParams['LANG'],
        'PAGE' => $component::MORTGAGE
    ),
    $component
);
?>