<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetPageProperty('title', 'Новостройки');
$APPLICATION->SetPageProperty('page_title', 'Новостройки. Результаты поиска');

$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    "all",
    array(
        'LANG' => $arParams['LANG'],
        'FILTERED' => 'Y',
        'SEF_FOLDER' => $arParams['SEF_FOLDER']
    ),
    $component
);


?>