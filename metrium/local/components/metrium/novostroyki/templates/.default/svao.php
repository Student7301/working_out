<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);

if ($params['SECTION_CODE'] == 'novostroyki-moskvy') {
    global $APPLICATION;
    $APPLICATION->SetPageProperty('h1', 'Новостройки СВАО');
    $APPLICATION->SetPageProperty('title', 'Новостройки в СВАО от компании «Метриум Групп»');
    $APPLICATION->SetPageProperty("keywords", 'новостройки свао');
    $APPLICATION->SetPageProperty("description", 'Компания «Метриум Групп» предлагает купить квартиры в новостройках СВАО на выгодных условиях. В каталоге имеется большое количество предложений с подробным описанием. Обращайтесь по телефону в Москве: +7 (495) 104-87-44.');
    $params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);
    $params['SEO_TEXT'] = '<p style="font-size: 29px; font-weight: bold">
Новостройки СВАО </p>
<p style="text-align: justify;">
	 Риелторская компания «Метриум Групп» предлагает квартиры в новостройках Северо-Восточного административного округа (СВАО) столицы. Это район с развитой торгово-социальной инфраструктурой, удобными транспортными развязками. Через него проходят три «ветки» метрополитена: Калужско-Рижская, Люблинско-Дмитровская и Серпуховско-Тимирязевская, что позволяет быстро добраться в любую точку города.
</p>
<p class="" style="text-align: justify;">
	 Планируете сэкономить при покупке жилья в Москве? Мы предлагаем купить квартиру в новостройке СВАО по цене от застройщика. Эти объекты обеспечивают современный уровень комфорта и безопасности, поддерживают высокий статус своего владельца.
</p>
<h2>
	 Преимущества покупки квартиры в новостройках СВАО через «Метриум Групп»
</h2>
<p class="" style="text-align: justify;">
	 Компания «Метриум Групп» предлагает выгодные и надежные условия сотрудничества. Подбираете квартиру в новостройках СВАО? Мы поможем!
</p>
<p class="" style="text-align: justify;">
	 Наши конкурентные преимущества:
</p>
<ul>
	<li>Гарантии. Мы работаем с вами не только при выборе жилья, но и после выполнения условий договора. То есть в течение гарантийного срока мы несем ответственность за выбранный вариант.<br>
 </li>
	<li>Новаторский подход. Мы подбираем то, что нужно нашим клиентам, в самые короткие сроки, опираясь на свою интуицию и многолетний опыт работы.<br>
 </li>
	<li>Использование современных технологий для оперативной обработки информации.<br>
 </li>
	<li>Мы всегда на связи и держим своих клиентов в курсе работы. Специалисты компании дают подробные консультации относительно выбора жилья в том или ином районе.</li>
</ul>
<p style="text-align: justify;">
	Позвоните в любое время, чтобы получить подробную консультацию о недвижимости в новостройках СВАО!
</p>';
}else {
    ($arParams['LANG'] == 'EN') ? $dopText = 'Building SVAO' : $dopText = 'Новостройки СВАО';
    NovostroykiComponent::setSEOPropertySection($arResult['VARIABLES']['SECTION_CODE'], $arParams['LANG'], $dopText);
}
?>

<?
if($arParams["LANG"] == "EN"){$params["LANG"] = "EN";} ?>
<? $params['SVAO'] = 'Y' ?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    ".default",
    $params,
    $component
);

$APPLICATION->IncludeComponent(
    "metrium:banners.rotator",
    "novostroyki",
    array("BANNERS_COUNT" => 50, "CATEGORY" => "BUILDINGS", "BANNER_IBLOCK_CODE" => "banner_rotator")
);
?>