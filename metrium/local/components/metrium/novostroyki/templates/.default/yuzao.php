<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);

if ($params['SECTION_CODE'] == 'novostroyki-moskvy') {
    global $APPLICATION;
    $APPLICATION->SetPageProperty('h1', 'Новостройки ЮЗАО');
    $APPLICATION->SetPageProperty('title', 'Новостройки в ЮЗАО от компании «Метриум Групп»');
    $APPLICATION->SetPageProperty("keywords", 'новостройки юзао');
    $APPLICATION->SetPageProperty("description", 'Компания «Метриум Групп» предлагает купить квартиры в новостройках ЮЗАО на выгодных условиях. В каталоге имеется большое количество предложений с подробным описанием. Обращайтесь по телефону в Москве: +7 (495) 104-87-44.');
    $params['SEO_TEXT'] = '<p style="font-size: 29px; font-weight: bold">
Новостройки ЮЗАО</p>
<p class="" style="text-align: justify;">
	 Компания «Метриум Групп» предлагает помощь в покупке жилья в новостройках ЮЗАО Москвы. Мы работаем с надежными застройщиками столицы, подтвердившими свою репутацию многочисленными сданными проектами.
</p>
<p class="" style="text-align: justify;">
	 ЮЗАО — один из наиболее развитых районов, имеет сложившуюся торговую, бытовую и социальную инфраструктуру. Благодаря близости природоохранных зон и господствующей розе ветров, он считается самой «чистой» частью столицы. Значительным &nbsp;преимуществом проживания здесь является удачная планировка транспортной сети.
</p>
<p class="" style="text-align: justify;">
	 Новостройки ЮЗАО способны удовлетворить практически любые запросы покупателей. В основном, это объекты бизнес-класса, в меньшей степени представлено жилье категорий «комфорт» и «элит».
</p>
<h2>
	 Преимущества покупки квартиры в новостройках ЮЗАО через «Метриум Групп»
</h2>
<p class="" style="text-align: justify;">
	 Наши специалисты обеспечат предоставление полного комплекса услуг — от поиска нужного варианта среди новостроек ЮЗАО до &nbsp;передачи ключей и последующего послепродажного сервиса. Особенности работы «Метриум Групп»:
</p>
<ul>
	<li>Гарантированная безопасность и юридическая чистота сделки.<br>
 </li>
	<li>Европейский уровень сервиса, индивидуальный подход к каждому клиенту.<br>
 </li>
	<li>Подбор жилья среди новостроек ЮЗАО в действительно короткие сроки.</li>
</ul>
<p>
	 Позвоните по указанным контактным телефонам и получите более подробную информацию об условиях сотрудничества.<br>
</p>
 <br>';
}else {
    ($arParams['LANG'] == 'EN') ? $dopText = 'Building YUZAO' : $dopText = 'Новостройки ЮЗАО';
    NovostroykiComponent::setSEOPropertySection($arResult['VARIABLES']['SECTION_CODE'], $arParams['LANG'], $dopText);
}
?>

<?
if($arParams["LANG"] == "EN"){$params["LANG"] = "EN";} ?>
<? $params['YUZAO'] = 'Y' ?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    ".default",
    $params,
    $component
);

$APPLICATION->IncludeComponent(
    "metrium:banners.rotator",
    "novostroyki",
    array("BANNERS_COUNT" => 50, "CATEGORY" => "BUILDINGS", "BANNER_IBLOCK_CODE" => "banner_rotator")
);
?>