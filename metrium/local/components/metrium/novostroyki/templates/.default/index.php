<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/* @var NovostroykiComponent $component */

if($arResult['VARIABLES']['SECTION_CODE'] && $arResult['VARIABLES']['ELEMENT_CODE']){
    $APPLICATION->IncludeComponent(
        "metrium:landing.router",
        "",
        array(
            'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
            'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
            'OBJECT_TYPE' => 'NEW_BUILDINGS',
            'SEF_FOLDER' => $arParams['SEF_FOLDER'],
            'INDEX' => 'Y',
            'LANG' => $arParams['LANG'],
            'PAGE' => $component::ABOUT
        ),
        $component
    );
}elseif(!$arResult['VARIABLES']['SECTION_CODE'] && !$arResult['VARIABLES']['ELEMENT_CODE']){
    LocalRedirect($arResult['FOLDER'].'novostroyki-moskvy/', false, '301 Moved Permanently');
}

?>