<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);

if ($params['SECTION_CODE'] == 'novostroyki-moskvy') {
    global $APPLICATION;
    $APPLICATION->SetPageProperty('h1', 'Новостройки ЗАО');
    $APPLICATION->SetPageProperty('title', 'Новостройки в ЗАО Москвы от компании «Метриум Групп»');
    $APPLICATION->SetPageProperty("keywords", 'новостройки зао новостройки зао москва');
    $APPLICATION->SetPageProperty("description", 'Компания «Метриум Групп» предлагает купить квартиры в новостройках ЗАО Москвы на выгодных условиях. В каталоге имеется большое количество предложений с подробным описанием. Обращайтесь по телефону +7 (495) 104-87-44.');
    $params['SEO_TEXT'] = '<p style="font-size: 29px; font-weight: bold">
Новостройки ЗАО</p>
<p class="" style="text-align: justify;">
	 Компания «Метриум Групп» реализует квартиры в новостройках ЗАО Москвы. Мы предлагаем разнообразные варианты, учитывающие финансовые возможности и индивидуальные запросы заказчика. Здесь представлены квартиры эконом- и бизнес-класса, а также элитное жилье.<br>
</p>
<h2>
	 Особенности новостроек Москвы
</h2>
<p class="" style="text-align: justify;">
	 В ЗАО столицы возводятся жилые комплексы с использованием технологий монолитного, панельного и монолитно-кирпичного строительства. Объекты полностью соответствуют современным требованиям и обеспечивают высокий комфорт проживания. Новые квартиры могут быть сданы как «под ключ», так и «без отделки». Они отличаются следующими характеристиками:
</p>
<p style="text-align: justify;">
</p>
<ul>
	<li>Лоджии оформляются качественными стеклопакетами.<br>
 </li>
	<li>В новостройках ЗАО предусматриваются подземные парковки.<br>
 </li>
	<li>В стилобатах возводимых зданий могут быть размещены магазины, детские сады, иные объекты инфраструктуры.</li>
</ul>
<p class="" style="text-align: justify;">
	 От «вторички» новостройки ЗАО отличаются современной планировкой, новыми коммуникациями, исправными лифтами, бесперебойной подачей тепла, воды и электричества, продуманной системой парковки. Также следует отметить, что оформление сделки с застройщиком безопаснее, чем с частным лицом. А компания «Метриум Групп» гарантирует надежность строительной компании и легитимность вашего договора.
</p>
<p style="text-align: justify;">
	Позвоните нам по указанным контактным телефонам, чтобы узнать подробные сведения о новостройках ЗАО и условиях сотрудничества.
</p>';
}else {
    ($arParams['LANG'] == 'EN') ? $dopText = 'Building ZAO' : $dopText = 'Новостройки ЗАО';
    NovostroykiComponent::setSEOPropertySection($arResult['VARIABLES']['SECTION_CODE'], $arParams['LANG'], $dopText);
}
?>

<?
if($arParams["LANG"] == "EN"){$params["LANG"] = "EN";} ?>
<? $params['ZAO'] = 'Y' ?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    ".default",
    $params,
    $component
);

$APPLICATION->IncludeComponent(
    "metrium:banners.rotator",
    "novostroyki",
    array("BANNERS_COUNT" => 50, "CATEGORY" => "BUILDINGS", "BANNER_IBLOCK_CODE" => "banner_rotator")
);
?>