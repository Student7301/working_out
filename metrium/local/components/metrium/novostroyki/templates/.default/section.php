<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

NovostroykiComponent::setSEOPropertySection($arResult['VARIABLES']['SECTION_CODE'], $arParams['LANG']);

?>

<? $params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);
if($arParams["LANG"] == "EN"){$params["LANG"] = "EN";} ?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    ".default",
    $params,
    $component
);

$APPLICATION->IncludeComponent(
    "metrium:banners.rotator",
    "novostroyki",
    array(
        "BANNERS_COUNT" => 50,
        "CATEGORY" => "BUILDINGS",
        "BANNER_IBLOCK_CODE" => "banner_rotator"
    )
);
?>