<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/* @var NovostroykiComponent $component */
?>
<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.kvartiry-s-otdelkoj",
    ".default",
    Array(
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'LANG' => $arParams['LANG'],
        'PAGE' => $component::APARTS_WITH_FINISH
    ),
    $component
);
?>