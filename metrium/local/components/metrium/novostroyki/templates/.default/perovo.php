<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$params = array('SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']);

if ($params['SECTION_CODE'] == 'novostroyki-moskvy') {
    global $APPLICATION;
    $APPLICATION->SetPageProperty('h1', 'Квартиры в новостройках в Раменках');
    $APPLICATION->SetPageProperty('title', 'Новостройки в Перово от компании «Метриум Групп»');
    $APPLICATION->SetPageProperty("keywords", 'новостройка перово');
    $APPLICATION->SetPageProperty("description", 'Компания «Метриум Групп» предлагает приобрести новостройки в Перово по выгодным ценам. В каталоге имеется большое количество предложений различной площади. Обращайтесь по телефону в Москве: +7 (495) 104-87-44.');
    $params['SEO_TEXT'] = '<p style="font-size: 29px; font-weight: bold">Новостройки в Перово</p>
<p style="text-align: justify;">
	 Компания «Метриум Групп» осуществляет продажу комфортабельных квартир в новостройках различных районов Москвы. Все дома возведены из прочных и экологически чистых материалов.
</p>
<p style="text-align: justify;">
	 В данном разделе сайта представлены актуальные новостройки Перово. В электронном каталоге вы найдете квартиры различной площади и планировки. Количество комнат варьируется от 1 до 4, также предлагаются квартиры-студии. Цена недвижимости стартует от 120 тыс. руб. за 1 м<sup>2</sup>.
</p>
<p class="" style="text-align: justify;">
	 Для удобства поиска подходящих вариантов в новостройках Перово предусмотрены удобные и простые фильтры. Используя их, вы можете отсортировать все предложения по стоимости, количеству комнат и типу отделки. Чтобы не тратить время и деньги на ремонт, отдавайте предпочтение объектам, которые сдаются под ключ. Многие квартиры продаются вместе с мебелью.
</p>
<p class="" style="text-align: justify;">
	 Главными достоинствами новостроек в Перово являются:
</p>
<p style="text-align: justify;">
</p>
<ul>
	<li>выгодное местоположение;<br>
 </li>
	<li>шаговая доступность от метро;<br>
 </li>
	<li>квартиры разной площади;<br>
 </li>
	<li>высокий уровень комфортабельности;<br>
 </li>
	<li>современные строительные материалы;<br>
 </li>
	<li>соответствие европейским требованиям;<br>
 </li>
	<li>надежность коммуникационных систем;<br>
 </li>
	<li>близость парковой зоны;<br>
 </li>
	<li>развитая инфраструктура района.</li>
</ul>
<h2>Почему жилье в новостройках Перово выгодно покупать у нас?</h2>
<p style="text-align: justify;">
	Мы предлагаем высокий уровень клиентского обслуживания и стараемся учесть все требования покупателей. Огромный ассортимент недвижимости позволяет в каждой ситуации найти оптимальный вариант.&nbsp;
</p>
<p class="" style="text-align: justify;">
	 Компания «Метриум Групп» сотрудничает с такими крупными коммерческими организациями, как «Сбербанк Управление Активами», что говорит о надежности и безграничном доверии. Для того чтобы купить квартиру в новостройке в Перово, достаточно позвонить по указанному контактному телефону.
</p>
 &nbsp;&nbsp;<br>';
}else {
    ($arParams['LANG'] == 'EN') ? $dopText = 'Perovo area' : $dopText = 'Район Перово';
    NovostroykiComponent::setSEOPropertySection($arResult['VARIABLES']['SECTION_CODE'], $arParams['LANG'], $dopText);
}
?>

<?
if($arParams["LANG"] == "EN"){$params["LANG"] = "EN";} ?>
<? $params['PEROVO'] = 'Y' ?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.list",
    ".default",
    $params,
    $component
);

$APPLICATION->IncludeComponent(
    "metrium:banners.rotator",
    "novostroyki",
    array("BANNERS_COUNT" => 50, "CATEGORY" => "BUILDINGS", "BANNER_IBLOCK_CODE" => "banner_rotator")
);
?>