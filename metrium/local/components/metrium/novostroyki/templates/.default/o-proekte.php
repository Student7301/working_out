<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/* @var NovostroykiComponent $component */

use Bitrix\Main\Page\Asset;

$host = explode(':',$_SERVER['HTTP_HOST']);
$host = $host[0];
Asset::getInstance()->addString('<link rel="canonical" href="http://'.$host.str_replace('o-proekte/', '', $_SERVER['REQUEST_URI']).'" /> ');?>

<?$APPLICATION->IncludeComponent(
    "metrium:landing.router",
    "",
    array(
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'OBJECT_TYPE' => 'NEW_BUILDINGS',
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
        'LANG' => $arParams['LANG'],
        'PAGE' => $component::ABOUT
    ),
    $component
);
?>
