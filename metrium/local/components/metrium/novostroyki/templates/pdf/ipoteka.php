<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.ipoteka",
    "pdf",
    Array(
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'BANKS_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('bank'),
        'BANK_OFFERS_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('bank_offers'),
        'OBJECT_IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki'),
    ),
    $component
);
?>