<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$APPLICATION->IncludeComponent(
    "metrium:novostroyki.v-prodazhe-sberbank",
    "pdf",
    Array(
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'CORPUSES_IBLOCK_CODE' => 'corpuses',
        'SECTIONS_IBLOCK_CODE' => 'sections',
        'APARTMENTS_IBLOCK_CODE' => 'apartments',
        'PLAN_ZASTROYKI_IBLOCK_CODE' => 'plan-zastroyki'
    ),
    $component
);
?>