<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Application;
use Bitrix\Iblock\InheritedProperty;

class CCZagorodnayaNedvizhimostDetail extends CBitrixComponent
{
    const STATUS_404 = 'ERROR_404';
    const STATUS_DISABLED = 'DISABLED';

    /** @var  CIBlockElement */
    public $el;
    public $isArenda;
    private $forFilterElemId = 0;

    public function onPrepareComponentParams($arParams)
    {
        if(!CModule::IncludeModule("iblock")){
            die('Error including module iblock');
        }

        $this->el = new CIBlockElement();

        self::includeComponentClass('metrium:zagorodnaya-nedvizhimost');
        if (!isset($arParams['SEF_FOLDER']) || empty($arParams['SEF_FOLDER'])) {
            $arParams['SEF_FOLDER'] = ZagorodnayaNedvizhimostComponent::DEF_SEF_FOLDER;
        }

        $request = Application::getInstance()->getContext()->getRequest()->toArray();
        $arParams['SEND'] = ($request['send'] == 'Y') ? 'Y' : 'N';
        $arParams['VERSION'] = ($request['version'] == 'advanced') ? 'advanced' : 'simple';

        return $arParams;
    }

    public function executeComponent()
    {
        $this->setMeta();

        $arCacheId = array_merge($this->arParams, $_REQUEST);
        $cache_id = serialize($arCacheId);
        $cacheTime = 3600;

        $obCache = new CPHPCache;

        if ($this->startResultCache($cacheTime, $cache_id)) {
            $templateName = $this->getTemplateName();

            if ($templateName === 'pdf_new_adv' || $templateName === 'pdf_new') {
                $this->abortResultCache();
            }

            $this->getObjectInfo();
            $this->arResult['RELATED_OBJECTS'] = $this->getRelatedObjects($this->arResult['OBJECT']['OBJECT_TYPE'],
                                                                        $this->arResult['OBJECT']['PRICE_USD_UNFORMATTED'],
                                                                        $this->arResult['OBJECT']['IS_ARENDA'],
                                                                        $this->arResult['OBJECT']['ID']); // Передаем id инфоблока для выборки похожих объектов

            if($this->arResult['OBJECT']['OBJECT_TYPE'] == 'poselki'){
                $this->arResult['SALES'] = $this->getSalesObjects($this->arResult['OBJECT']['NAME']);
            }

            $this->includeComponentTemplate();
        }

        switch ($this->arResult['OBJECT']['STATUS']) {
            case self::STATUS_404:
                define("ERROR_404", "Y");
                break;

            case self::STATUS_DISABLED:
                LocalRedirect("../", false, "301 Moved permanently");
                break;
        }

        //не включать этот метод в кеш
        if(!empty($this->forFilterElemId)) {
            \metrium\Reviews::addToReviewList(\metrium\EstateObject::getIblockIdByCode('for_filter'), $this->forFilterElemId);
        }

        return $this->arResult;
    }

    public function getObjectInfo()
    {
        if($this->arParams['SECTION_CODE'] == 'arenda') {
            $obj_type = '';
        }else {
            $obj_type = $this->arParams['SECTION_CODE'];
        }
        $dbResForFilter = $this->el->GetList(
                array(),
                array(
                    'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'),
                    'CODE' => $this->arParams['ELEMENT_CODE'],
                    'PROPERTY_OBJECT_TYPE' => $obj_type
                ),
                false,
                false,
                array(
                    'ID',
                    'NAME',
                    'ACTIVE',
                    'PROPERTY_OBJECT_ID',
                    'PROPERTY_OBJECT_TYPE',
                    'PROPERTY_ARENDA',
                    'PROPERTY_ARENDA_USD',
                    'PROPERTY_ARENDA_EUR',
                    'PROPERTY_ARENDA_RUB',
                    'PROPERTY_PRICE_USD',
                    'PROPERTY_PRICE_USD_OLD',
                    'PROPERTY_PRICE_EUR',
                    'PROPERTY_PRICE_RUB'
                )
            );

        if($forFilterObject = $dbResForFilter->Fetch()){
            if(($this->arParams['SECTION_CODE'] != $forFilterObject['PROPERTY_OBJECT_TYPE_VALUE']) && ($this->arParams['SECTION_CODE'] != 'arenda')){
                $this->arResult['OBJECT']['STATUS'] = self::STATUS_404;
            }

            if($forFilterObject['ACTIVE'] == 'N'){
                $this->arResult['OBJECT']['STATUS'] = self::STATUS_DISABLED;
            } elseif ($this->arParams['SECTION_CODE'] == 'arenda' && $forFilterObject['PROPERTY_ARENDA_VALUE'] != 1) {
                $this->arResult['OBJECT']['STATUS'] = self::STATUS_DISABLED;
            } else {
                /* Редиректим на раздел объекты с нулевой (или почти нулевой) ценой */
                switch ((int) $forFilterObject['PROPERTY_ARENDA_VALUE']) {

                    case 1:
                        if ($this->arParams['SECTION_CODE'] == 'arenda'
                            && $forFilterObject['PROPERTY_ARENDA_EUR_VALUE'] <= 2
                            && $forFilterObject['PROPERTY_ARENDA_USD_VALUE'] <= 2
                            && $forFilterObject['PROPERTY_ARENDA_RUB_VALUE'] <= 1000
                        ) {
                            $this->arResult['OBJECT']['STATUS'] = self::STATUS_DISABLED;
                        }
                        break;

                    default:
                        if ($this->arParams['SECTION_CODE'] != 'arenda'
                            && $forFilterObject['PROPERTY_PRICE_EUR_VALUE'] <= 2
                            && $forFilterObject['PROPERTY_PRICE_USD_VALUE'] <= 2
                            && $forFilterObject['PROPERTY_PRICE_RUB_VALUE'] <= 1000
                        ) {
                            $this->arResult['OBJECT']['STATUS'] = self::STATUS_DISABLED;
                        }
                }
            }
            
            $this->forFilterElemId = $forFilterObject['ID'];
            if($forFilterObject['PROPERTY_OBJECT_TYPE_VALUE'] == 'kottedzhi_taunkhausy'){
                $dbRes = $this->el->GetList(
                    array(),
                    array(
                        'IBLOCK_CODE' => $forFilterObject['PROPERTY_OBJECT_TYPE_VALUE'],
                        'CODE' => $this->arParams['ELEMENT_CODE']
                    ),
                    false,
                    false,
                    array(
                        'ID',
                        'NAME',
                        'CODE',
                        'ACTIVE',
                        'IBLOCK_ID',
                        'IBLOCK_CODE',
                        'PREVIEW_PICTURE',
                        'PREVIEW_TEXT',
                        'DETAIL_TEXT',
                        'DETAIL_PAGE_URL',
                        'PROPERTY_ADDRESS',
                        'PROPERTY_PHOTOS',
                        'PROPERTY_PRICE_USD',
                        'PROPERTY_PRICE_USD_OLD',
                        'PROPERTY_PRICE_EUR',
                        'PROPERTY_PRICE_RUB',
                        'PROPERTY_ARENDA_USD',
                        'PROPERTY_ARENDA_EUR',
                        'PROPERTY_ARENDA_RUB',
                        'PROPERTY_DIRECTION.NAME',
                        'PROPERTY_REMOTNESS_MKAD',
                        'PROPERTY_HOUSE_AREA',
                        'PROPERTY_TERRITORY_AREA',
                        'PROPERTY_GUARDED',
                        'PROPERTY_MATERIAL',
                        'PROPERTY_FLOOR',
                        'PROPERTY_COMUNICATIONS',
                        'PROPERTY_MAP',
                        'PROPERTY_ARENDA',
                        'PROPERTY_YANDEX_MAP',
                        'PROPERTY_FACING',
                        'PROPERTY_CURRENCY',
                        'PROPERTY_HIDE_PRICES',
                        'PROPERTY_VIDEO',
                        'PROPERTY_village'
                    )
                );

                $object = $dbRes->GetNext();

                if (empty($object)) {
                    $this->arResult['OBJECT']['STATUS'] = self::STATUS_DISABLED;
                }
                
                foreach($object['PROPERTY_PHOTOS_VALUE'] as $photoId){
                    $arPhotos[] = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($photoId);
                }

                if(count($arPhotos) < 1){
                    $arPhotos[] = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($object['PREVIEW_PICTURE']);
                }

                $dbPoselokRes = $this->el->GetList(
                    false,
                    array(
                        'IBLOCK_CODE' => 'for_filter',
                        'ACTIVE' => 'Y',
                        'NAME' => $object['NAME'],
                        'PROPERTY_OBJECT_TYPE' => 'poselki',
                        '>PROPERTY_PRICE_EUR' => 1,
                        '>PROPERTY_PRICE_USD' => 1,
                        '>PROPERTY_PRICE_RUB' => 1000
                    ),
                    false,
                    false,
                    array(
                        'ID',
                        'IBLOCK_ID',
                        'CODE'
                    )
                );

                $poselok = $dbPoselokRes->Fetch();

                $yand = explode(',',$object['PROPERTY_YANDEX_MAP_VALUE']);

                $ar = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($object['PREVIEW_PICTURE']);

                if($object['PROPERTY_PRICE_RUB_VALUE']>1){
                    $rub = \metrium\Price::getFormattedPrice($object['PROPERTY_PRICE_RUB_VALUE']);
                } else {
                    $rub =  \metrium\Price::getFormattedPrice(\metrium\Price::getPriceRub($object['PROPERTY_PRICE_USD_VALUE']));
                }

                if (!empty($object['PROPERTY_VIDEO_VALUE'])) {
                    if (preg_match('#(https|http)://www.youtube.com/watch\?v=(.*)#', $object['PROPERTY_VIDEO_VALUE'], $embed)) {
                        $video = 'https://www.youtube.com/embed/' . $embed[2];
                    }
                }

                $this->arResult['OBJECT'] = array(
                    'ID' => $object['ID'],
                    'ACTIVE' => $object['ACTIVE'],
                    'IBLOCK_ID' => $object['IBLOCK_ID'],
                    'NAME' => $object['NAME'],
                    'PREVIEW_PICTURE' => $ar,
                    'PREVIEW_TEXT' => $object['PREVIEW_TEXT'],
                    'DETAIL_TEXT' => $object['DETAIL_TEXT'],
                    'ADDRESS' => $object['PROPERTY_ADDRESS_VALUE'],
                    'VILLAGE' => $object['PROPERTY_VILLAGE_VALUE'],
                    'CODE' => $object['CODE'],
                    'PHOTOS' => $arPhotos,
                    'HOUSE_AREA' => $object['PROPERTY_HOUSE_AREA_VALUE'],
                    'TERRITORY_AREA' => $object['PROPERTY_TERRITORY_AREA_VALUE'],
                    'GUARDED' => $object['PROPERTY_GUARDED_VALUE'],
                    'MATERIAL' => $object['PROPERTY_MATERIAL_VALUE'],
                    'FLOOR' => str_replace("\n", "<br>",$object['PROPERTY_FLOOR_VALUE']['TEXT']),
                    'COMUNICATIONS' => $object['PROPERTY_COMUNICATIONS_VALUE']['TEXT'],
                    'PRICE_USD_UNFORMATTED' => $object['PROPERTY_PRICE_USD_VALUE'],
                    'DIRECTION' => $object['PROPERTY_DIRECTION_NAME'],
                    'REMOTNESS_MKAD' => $object['PROPERTY_REMOTNESS_MKAD_VALUE'],
                    'LATITUDE' => $yand[0],
                    'LONGITUDE' => $yand[1],
                    'OBJECT_TYPE' => $object['IBLOCK_ID'],
                    'IS_ARENDA' => $object['PROPERTY_ARENDA_VALUE'],
                    'POSELOK_CODE' => $poselok['CODE'],
                    'FACING' => $object['PROPERTY_FACING_VALUE'],
                    'HIDE_PRICES' => $object['PROPERTY_HIDE_PRICES_VALUE'],
                    'DETAIL_PAGE_URL' => $object['DETAIL_PAGE_URL']
                );
                if (isset($video)) {
                    $this->arResult['OBJECT'] = array(
                        'VIDEO' => $video
                    );
                }

                $prices = array(
                    "RUB" => $object["PROPERTY_PRICE_RUB_VALUE"],
                    "USD" => $object["PROPERTY_PRICE_USD_VALUE"],
                    "USD_OLD" => $object["PROPERTY_PRICE_USD_OLD_VALUE"],
                    "EUR" => $object["PROPERTY_PRICE_EUR_VALUE"],
                    "RENT_RUB" => $object["PROPERTY_ARENDA_RUB_VALUE"],
                    "RENT_USD" => $object["PROPERTY_ARENDA_USD_VALUE"],
                    "RENT_EUR" => $object["PROPERTY_ARENDA_EUR_VALUE"],
                );

                $this->arResult["OBJECT"] = array_merge($this->arResult["OBJECT"], metrium\Price::getFormatPrices($object["PROPERTY_CURRENCY_VALUE"], $prices));
            }
            elseif($forFilterObject['PROPERTY_OBJECT_TYPE_VALUE'] == 'poselki'){
                $dbRes = $this->el->GetList(
                    array(),
                    array(
                        'ACTIVE' => 'Y',
                        'IBLOCK_CODE' => $forFilterObject['PROPERTY_OBJECT_TYPE_VALUE'],
                        'CODE' => $this->arParams['ELEMENT_CODE']
                    ),
                    false,
                    false,
                    array(
                        'ID',
                        'NAME',
                        'ACTIVE',
                        'CODE',
                        'IBLOCK_ID',
                        'IBLOCK_CODE',
                        'PREVIEW_PICTURE',
                        'PREVIEW_TEXT',
                        'DETAIL_TEXT',
                        'DETAIL_PAGE_URL',
                        'PROPERTY_PHOTOS',
                        'PROPERTY_START_PRICE_USD',
                        'PROPERTY_START_PRICE_EUR',
                        'PROPERTY_START_PRICE_RUB',
                        'PROPERTY_DIRECTION.NAME',
                        'PROPERTY_REMOTNESS_MKAD',
                        'PROPERTY_HOUSE_AREA_MIN',
                        'PROPERTY_SQUAREMAX',
                        'PROPERTY_TERRITORY_AREA_MIN',
                        'PROPERTY_PLOTSQUAREMAX',
                        'PROPERTY_COMUNICATIONS',
                        'PROPERTY_YANDEX_MAP',
                        'PROPERTY_ARENDA',
                        'PROPERTY_ADDRESS',
                        'PROPERTY_CURRENCY',
                        'PROPERTY_HIDE_PRICES'
                    )
                );

                $object = $dbRes->GetNext();

                if (empty($object)) {
                    $this->arResult['OBJECT']['STATUS'] = self::STATUS_DISABLED;
                }

                foreach($object['PROPERTY_PHOTOS_VALUE'] as $photoId){
                    $arPhotos[] = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($photoId);
                }
                if(count($arPhotos) < 1){
                    $arPhotos[] = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($object['PREVIEW_PICTURE']);
                }

                $ar = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($object['PREVIEW_PICTURE']);

                $this->arResult['OBJECT'] = array(
                    'ID' => $object['ID'],
                    'ACTIVE' => $object['ACTIVE'],
                    'IBLOCK_ID' => $object['IBLOCK_ID'],
                    'NAME' => $object['NAME'],
                    'PREVIEW_PICTURE' => $ar,
                    'PREVIEW_TEXT' => $object['PREVIEW_TEXT'],
                    'DETAIL_TEXT' => $object['DETAIL_TEXT'],
                    'CODE' => $object['CODE'],
                    'PHOTOS' => $arPhotos,
                    'HOUSE_AREA' => empty($object['PROPERTY_HOUSE_AREA_MIN_VALUE']) ? $object['PROPERTY_SQUAREMAX'] : $object['PROPERTY_HOUSE_AREA_MIN_VALUE'],
                    'TERRITORY_AREA' => empty($object['PROPERTY_TERRITORY_AREA_MIN_VALUE']) ? $object['PROPERTY_PLOTSQUAREMAX'] : $object['PROPERTY_TERRITORY_AREA_MIN_VALUE'],
                    'COMUNICATIONS' => $object['PROPERTY_COMUNICATIONS_VALUE']['TEXT'],
                    'PRICE_USD_UNFORMATTED' => $object['PROPERTY_START_PRICE_USD_VALUE'],
                    'DIRECTION' => $object['PROPERTY_DIRECTION_NAME'],
                    'REMOTNESS_MKAD' => $object['PROPERTY_REMOTNESS_MKAD_VALUE'],
                    'LATITUDE' => $object['PROPERTY_LATITUDE_VALUE'],
                    'LONGITUDE' => $object['PROPERTY_LONGITUDE_VALUE'],
                    'OBJECT_TYPE' => $object['IBLOCK_ID'],
                    'IS_ARENDA' => $object['PROPERTY_ARENDA_VALUE'],
                    'ADDRESS' => $object['PROPERTY_ADDRESS_VALUE'],
                    'HIDE_PRICES' => $object['PROPERTY_HIDE_PRICES_VALUE'],
                    'DETAIL_PAGE_URL' => $object['DETAIL_PAGE_URL']
                );

                $prices = array(
                    "RUB" => $object["PROPERTY_START_PRICE_RUB_VALUE"],
                    "USD" => $object["PROPERTY_START_PRICE_USD_VALUE"],
                    "EUR" => $object["PROPERTY_START_PRICE_EUR_VALUE"]
                );

                $this->arResult["OBJECT"] = array_merge($this->arResult["OBJECT"], metrium\Price::getFormatPrices($object["PROPERTY_CURRENCY_VALUE"], $prices));
            }
            elseif($forFilterObject['PROPERTY_OBJECT_TYPE_VALUE'] == 'uchastki'){
                $dbRes = $this->el->GetList(
                    array(),
                    array(
                        'ACTIVE' => 'Y',
                        'IBLOCK_CODE' => $forFilterObject['PROPERTY_OBJECT_TYPE_VALUE'],
                        'CODE' => $this->arParams['ELEMENT_CODE']
                    ),
                    false,
                    false,
                    array(
                        'ID',
                        'NAME',
                        'CODE',
                        'IBLOCK_ID',
                        'IBLOCK_CODE',
                        'PREVIEW_PICTURE',
                        'PREVIEW_TEXT',
                        'DETAIL_PAGE_URL',
                        'DETAIL_TEXT',
                        'PROPERTY_PHOTOS',
                        'PROPERTY_PRICE_USD',
                        'PROPERTY_PRICE_EUR',
                        'PROPERTY_PRICE_RUB',
                        'PROPERTY_DIRECTION.NAME',
                        'PROPERTY_REMOTNESS_MKAD',
                        'PROPERTY_HOUSE_AREA',
                        'PROPERTY_TERRITORY_AREA',
                        'PROPERTY_GUARDED',
                        'PROPERTY_MATERIAL',
                        'PROPERTY_FLOOR',
                        'PROPERTY_COMUNICATIONS',
                        'PROPERTY_MAP',
                        'PROPERTY_ARENDA',
                        'PROPERTY_YANDEX_MAP',
                        'PROPERTY_ADDRESS',
                        'PROPERTY_CURRENCY',
                        'PROPERTY_HIDE_PRICES'
                    )
                );

                $object = $dbRes->GetNext();

                if (empty($object)) {
                    $this->arResult['OBJECT']['STATUS'] = self::STATUS_DISABLED;
                }

                foreach($object['PROPERTY_PHOTOS_VALUE'] as $photoId){
                    $arPhotos[] = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($photoId);
                }
                if(count($arPhotos) < 1){
                    $arPhotos[] = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($object['PREVIEW_PICTURE']);
                }

                $dbPoselokRes = $this->el->GetList(
                    false,
                    array(
                        'IBLOCK_CODE' => 'for_filter',
                        'ACTIVE' => 'Y',
                        'NAME' => $object['NAME'],
                        'PROPERTY_OBJECT_TYPE' => 'poselki',
                        '>PROPERTY_PRICE_EUR' => 1,
                        '>PROPERTY_PRICE_USD' => 1,
                        '>PROPERTY_PRICE_RUB' => 1000
                    ),
                    false,
                    false,
                    array(
                        'ID',
                        'IBLOCK_ID',
                        'CODE',
                        'ACTIVE'
                    )
                );

                $poselok = $dbPoselokRes->Fetch();

                $ar = \metrium\helpers\EliteHelper::ResizePhotoObjectDetail($object['PREVIEW_PICTURE']);

                $yand = explode(',',$object['PROPERTY_YANDEX_MAP_VALUE']);
                $this->arResult['OBJECT'] = array(
                    'ID' => $object['ID'],
                    'ACTIVE' => $object['ACTIVE'],
                    'IBLOCK_ID' => $object['IBLOCK_ID'],
                    'NAME' => $object['NAME'],
                    'PREVIEW_PICTURE' => $ar,
                    'PREVIEW_TEXT' => $object['PREVIEW_TEXT'],
                    'DETAIL_TEXT' => $object['DETAIL_TEXT'],
                    'CODE' => $object['CODE'],
                    'PHOTOS' => $arPhotos,
                    'HOUSE_AREA' => $object['PROPERTY_HOUSE_AREA_VALUE'],
                    'TERRITORY_AREA' => $object['PROPERTY_TERRITORY_AREA_VALUE'],
                    'GUARDED' => $object['PROPERTY_GUARDED_VALUE'],
                    'MATERIAL' => $object['PROPERTY_MATERIAL_VALUE'],
                    'FLOOR' => $object['PROPERTY_FLOOR_VALUE']['TEXT'],
                    'COMUNICATIONS' => $object['PROPERTY_COMUNICATIONS_VALUE']['TEXT'],
                    'PRICE_USD_UNFORMATTED' => $object['PROPERTY_PRICE_USD_VALUE'],
                    'DIRECTION' => $object['PROPERTY_DIRECTION_NAME'],
                    'REMOTNESS_MKAD' => $object['PROPERTY_REMOTNESS_MKAD_VALUE'],
                    'LATITUDE' => $yand[0],
                    'LONGITUDE' => $yand[1],
                    'OBJECT_TYPE' => $object['IBLOCK_ID'],
                    'IS_ARENDA' => $object['PROPERTY_ARENDA_VALUE'],
                    'ADDRESS' => $object['PROPERTY_ADDRESS_VALUE'],
                    'POSELOK_CODE' => $poselok['CODE'],
                    'HIDE_PRICES' => $object['PROPERTY_HIDE_PRICES_VALUE'],
                    'DETAIL_PAGE_URL' => $object['DETAIL_PAGE_URL']
                );

                $prices = array(
                    "RUB" => $object["PROPERTY_PRICE_RUB_VALUE"],
                    "USD" => $object["PROPERTY_PRICE_USD_VALUE"],
                    "EUR" => $object["PROPERTY_PRICE_EUR_VALUE"],
                );

                $this->arResult["OBJECT"] = array_merge($this->arResult["OBJECT"], metrium\Price::getFormatPrices($object["PROPERTY_CURRENCY_VALUE"], $prices));

            }

            $this->arResult['OBJECT']['OBJECT_TYPE'] = $forFilterObject['PROPERTY_OBJECT_TYPE_VALUE'];
            $this->arResult['OBJECT']['FORFILTER_ID'] = $this->forFilterElemId;
        }else {
            $this->arResult['OBJECT']['STATUS'] = self::STATUS_404;
        }
    }

    public function getRelatedObjects($objectType, $priceUSD, $isArenda, $currentObjectId )
    {
//        $likes = \metrium\Likes::getLikes(); // Перенесено в component_epilog.php
        $arFilter = array(
            'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'),
            'PROPERTY_OBJECT_TYPE' => $objectType,
            '!PROPERTY_OBJECT_ID' => $currentObjectId,
            '<=PROPERTY_PRICE_USD' => $priceUSD + ($priceUSD * 0.20),
            '>=PROPERTY_PRICE_USD' => $priceUSD - ($priceUSD * 0.20),
            '!PROPERTY_PHOTO' => false,
            '!CODE' => $this->arParams['ELEMENT_CODE'],
            'ACTIVE' => 'Y'
        );
        if($isArenda){
            $arFilter['PROPERTY_ARENDA'] = 1;
            $arFilter['>PROPERTY_ARENDA_EUR'] = 1;
            $arFilter['>PROPERTY_ARENDA_USD'] = 1;
            $arFilter['>PROPERTY_ARENDA_RUB'] = 1000;
        }else{
            $arFilter['!PROPERTY_ARENDA'] = 1;
            $arFilter['>PROPERTY_PRICE_EUR'] = 1;
            $arFilter['>PROPERTY_PRICE_USD'] = 1;
            $arFilter['>PROPERTY_PRICE_RUB'] = 1000;
        }
        $arSelect = array(
            'ID',
            'CODE',
            'NAME',
            'PROPERTY_DIRECTION',
            'PROPERTY_PHOTO',
            'PROPERTY_REMOTNESS_MKAD',
            'PROPERTY_HOUSE_AREA',
            'PROPERTY_TERRITORY_AREA',
            'PROPERTY_FACING',
            'PROPERTY_OBJECT_TYPE',
            'PROPERTY_OBJECT_ID'
        );

        $dbRes = $this->el->GetList(
            array('PROPERTY_PRICE_USD' => 'ASC', 'SORT' => 'ASC'),
            $arFilter,
            false,
            array("nTopCount"=> 4),
            $arSelect
        );

        if($dbRes->SelectedRowsCount() == 0){
            $arFilter = array(
                'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'),
                'PROPERTY_OBJECT_TYPE' => $objectType,
                '!PROPERTY_OBJECT_ID' => $currentObjectId,
                '!PROPERTY_PHOTO' => false,
                '!CODE' => $this->arParams['ELEMENT_CODE'],
                'ACTIVE' => 'Y'
            );
            if($isArenda){
                $arFilter['PROPERTY_ARENDA'] = 1;
                $arFilter['>PROPERTY_ARENDA_EUR'] = 1;
                $arFilter['>PROPERTY_ARENDA_USD'] = 1;
                $arFilter['>PROPERTY_ARENDA_RUB'] = 1000;
            }else{
                $arFilter['!PROPERTY_ARENDA'] = 1;
                $arFilter['>PROPERTY_PRICE_EUR'] = 1;
                $arFilter['>PROPERTY_PRICE_USD'] = 1;
                $arFilter['>PROPERTY_PRICE_RUB'] = 1000;
            }
            $dbRes = $this->el->GetList(
                array('PROPERTY_PRICE_USD' => 'ASC', 'SORT' => 'ASC'),
                $arFilter,
                false,
                array("nTopCount"=> 4),
                $arSelect
            );
        }

        while($arObject = $dbRes->Fetch()){
            $db_props = CIBlockElement::GetProperty(\metrium\EstateObject::getIblockIdByCode($arObject['PROPERTY_OBJECT_TYPE_VALUE']), $arObject['PROPERTY_OBJECT_ID_VALUE'], array("sort" => "asc"), Array("CODE"=>"PHOTOS"));
            $counter = 0;
            while ($ob = $db_props->GetNext())
            {
                $counter++;
            }

            $ar = \metrium\helpers\EliteHelper::ResizePhotoRelatedObject($arObject['PROPERTY_PHOTO_VALUE']);

            $tempArr= array(
                'ID'=>$arObject['CODE'],
                'NAME' => $arObject['NAME'],
                'DIRECTION' => $arObject['PROPERTY_DIRECTION_VALUE'],
                'PREVIEW_PICTURE' => $ar['src'],
                'REMOTNESS_MKAD' => $arObject['PROPERTY_REMOTNESS_MKAD_VALUE'],
                'TERRITORY_AREA' => $arObject['PROPERTY_TERRITORY_AREA_VALUE'],
                'HOUSE_AREA' => $arObject['PROPERTY_HOUSE_AREA_VALUE'],
                'FACING' => $arObject['PROPERTY_FACING_VALUE'],
                'PHOTOS_COUNT' => $counter,
                'OBJ_ID'=>$arObject['PROPERTY_OBJECT_ID_VALUE']
            );

            $res = $this->el->GetList(
                array(),
                array("ACTIVE" => "Y", "IBLOCK_CODE" => $arObject["PROPERTY_OBJECT_TYPE_VALUE"], "PROPERTY_CRM_ID" => $arObject["CODE"]),
                false,
                false,
                array(
                    "ID",
                    "IBLOCK_ID",
                    "DETAIL_PAGE_URL",
                    "PROPERTY_CURRENCY",
                    "PROPERTY_HIDE_PRICES",
                    "PROPERTY_PRICE_USD",
                    "PROPERTY_PRICE_EUR",
                    "PROPERTY_PRICE_RUB",
                    "PROPERTY_ARENDA_USD",
                    "PROPERTY_ARENDA_EUR",
                    "PROPERTY_ARENDA_RUB"
                )
            )->GetNext();

            $tempArr['DETAIL_PAGE_URL'] = $res['DETAIL_PAGE_URL'];
            $tempArr['HIDE_PRICES'] = $res['PROPERTY_HIDE_PRICES_VALUE'];

            $prices = array(
                "RUB" => $res["PROPERTY_PRICE_RUB_VALUE"],
                "USD" => $res["PROPERTY_PRICE_USD_VALUE"],
                "EUR" => $res["PROPERTY_PRICE_EUR_VALUE"],
                "RENT_RUB" => $res["PROPERTY_ARENDA_RUB_VALUE"],
                "RENT_USD" => $res["PROPERTY_ARENDA_USD_VALUE"],
                "RENT_EUR" => $res["PROPERTY_ARENDA_EUR_VALUE"],
            );
            
            $tempArr = array_merge($tempArr, metrium\Price::getFormatPrices($res["PROPERTY_CURRENCY_VALUE"], $prices));

            $arRelatedObjects[] = $tempArr;
        }
        return $arRelatedObjects;
    }

    public function getSalesObjects($name)
    {
        $dbRes = $this->el->GetList(
            array(
                'SORT' => 'ASC'
            ),
            array(
                'ACTIVE' => 'Y',
                'IBLOCK_CODE' => 'for_filter',
                'NAME' => $name,
                '!NAME' => 'poselki'
            ),
            false,
            false,
            array(
                'ID',
                'IBLOCK_ID',
                'NAME',
                'CODE',
                'PROPERTY_OBJECT_TYPE',
                'PROPERTY_PRICE_USD',
                'PROPERTY_UCHASTOK_TYPE',
                'PROPERTY_HOUSE_AREA',
                'PROPERTY_TERRITORY_AREA'
            )
        );
        
        while($sale = $dbRes->Fetch()){
            $arSales[$sale['PROPERTY_OBJECT_TYPE_VALUE']][] = array(
                'NAME' =>   $sale['NAME'],
                'ID' =>   $sale['CODE'],
                'OBJECT_TYPE' =>    $sale['PROPERTY_OBJECT_TYPE_VALUE'],
                'PRICE_USD' =>  \metrium\Price::getFormattedPrice($sale['PROPERTY_PRICE_USD_VALUE']),
                'HOUSE_AREA' =>   $sale['PROPERTY_HOUSE_AREA_VALUE'],
                'TERRITORY_AREA' =>   $sale['PROPERTY_TERRITORY_AREA_VALUE'],
                'UCHASTOK_TYPE' =>   $sale['PROPERTY_UCHASTOK_TYPE_VALUE']
            );
        }
        return $arSales;
    }

    /**
     * Устанавливает метатеги для страницы
     */
    private function setMeta()
    {
        $dbResForFilter = $this->el->GetList(
            array(),
            array(
                'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'),
                'CODE' => $this->arParams['ELEMENT_CODE']
            ),
            false,
            false,
            array(
                'ID',
                'NAME',
                'ACTIVE',
                'PROPERTY_OBJECT_ID',
                'PROPERTY_OBJECT_TYPE'
            )
        );

        if ($forFilterObject = $dbResForFilter->Fetch()) {
            global $APPLICATION;

            $APPLICATION->SetPageProperty("description", $arResult['OBJECT']['NAME'] .'. Объект ID '. $arResult['OBJECT']['ID'] . '. Элитная загородная недвижимость');

            switch ($forFilterObject['PROPERTY_OBJECT_TYPE_VALUE']) {
                case 'kottedzhi_taunkhausy':
                    $idElement = \metrium\helpers\IBlockHelper::getElementIdByElementCode($this->arParams['ELEMENT_CODE'], \metrium\EstateObject::getIblockIdByCode('kottedzhi_taunkhausy'));
                    $meta = $this->getMeta(\metrium\helpers\IBlockHelper::getIblockIdByCode('kottedzhi_taunkhausy'), $idElement);
                    $APPLICATION->SetPageProperty('title', $meta['ELEMENT_META_TITLE']);

                    break;

                case 'poselki':
                    $idElement = \metrium\helpers\IBlockHelper::getElementIdByElementCode($this->arParams['ELEMENT_CODE'], \metrium\EstateObject::getIblockIdByCode('poselki'));
                    $meta = $this->getMeta(\metrium\helpers\IBlockHelper::getIblockIdByCode('poselki'), $idElement);

                    if(!empty($meta['ELEMENT_META_TITLE'])) {
                        $APPLICATION->SetPageProperty('title', $meta['ELEMENT_META_TITLE']);
                    }else {
                        $APPLICATION->SetPageProperty('title', $forFilterObject['NAME'] . ' - О проекте');
                    }

                    if(!empty($meta['ELEMENT_META_DESCRIPTION'])) {
                        $APPLICATION->SetPageProperty('description', $meta['ELEMENT_META_DESCRIPTION']);
                    }

                    if(!empty($meta['ELEMENT_META_KEYWORDS'])) {
                        $APPLICATION->SetPageProperty('keywords', $meta['ELEMENT_META_KEYWORDS']);
                    }else {
                        $APPLICATION->SetPageProperty("keywords", $arResult['OBJECT']['NAME'] .'. Объект ID '. $arResult['OBJECT']['ID'] . '. Элитная загородная недвижимость');
                    }

                    if($meta['ELEMENT_PAGE_TITLE']) {
                        $this->arResult['H1'] = $meta['ELEMENT_PAGE_TITLE'];
                    }

                    break;

                case 'uchastki':
                    $idElement = \metrium\helpers\IBlockHelper::getElementIdByElementCode($this->arParams['ELEMENT_CODE'], \metrium\EstateObject::getIblockIdByCode('uchastki'));
                    $meta = $this->getMeta(\metrium\helpers\IBlockHelper::getIblockIdByCode('uchastki'), $idElement);

                    $APPLICATION->SetPageProperty('title', $meta['ELEMENT_META_TITLE']);
                    if(!empty($meta['ELEMENT_META_KEYWORDS'])) {
                        $APPLICATION->SetPageProperty('keywords', $meta['ELEMENT_META_KEYWORDS']);
                    }else {
                        $APPLICATION->SetPageProperty('keywords', 'Земельные участки, Московской Области, Новой Москвы, Купить');
                    }

                    if(!empty($meta['ELEMENT_META_DESCRIPTION'])) {
                        $APPLICATION->SetPageProperty('description', $meta['ELEMENT_META_DESCRIPTION']);
                    }
                    break;

                case 'arenda':
                    $APPLICATION->SetPageProperty('title', 'Продажа элитной загородной недвижимости в Москве и Подмосковье: купить дома под ключ и коттеджи в элитных посёлках, смотрите объекты с фото');

                    break;
            }
        }
    }

    private function getMeta($idIblock, $idElement)
    {
        $ipropValues = new InheritedProperty\ElementValues(
            $idIblock,
            $idElement
        );
        return $ipropValues->getValues();
    }
}