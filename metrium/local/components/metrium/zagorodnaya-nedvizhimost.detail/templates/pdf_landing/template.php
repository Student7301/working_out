<?php
$object = $arResult['OBJECT'];
$long = $object['LONGITUDE'];
$lat = $object['LATITUDE'];
$color1 = $object['PDF_COLOR_FIRST'] ? $object['PDF_COLOR_FIRST'] : '#3a3e47';
$color2 = $object['PDF_COLOR_SECOND'] ? $object['PDF_COLOR_SECOND'] : '#049d7e';
?>
<!DOCTYPE html>
<html style="padding: 0; margin: 0;">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Metrium - риелтор нового поколения</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: white;
            width: 1250px;
            color: #202126;
            font-family: "Arial", sans-serif;
        }
        .header {
            background-color: white;
            height: 140px;
            width: 100%
        }
        .header_title {
            background-color: <?= $color1 ?>;
            height: 85px;
        }
        .header_title-text {
            font-size: 20px;
            text-transform: uppercase;
            color: white;
            position: relative;
            display: block;
            margin: 0;
            top: 40px;
            left: 285px;
        }
        .header_address {
            height: 55px;
        }
        .header_address-text {
            position: relative;
            display: block;
            margin: 0px;
            top: 20px;
            left: 285px;
        }
        .header_objectId {
            height: 140px;
            background-color: <?= $color2 ?>;
            width: 230px;
            position: absolute;
            top: 0;
            left: 35px;
        }
        .second_page {
            top: 1755px;
        }
        .header_objectId-text {
            color: white;
            text-transform: uppercase;
            font-size: 25px;
            text-align: center;
            height: 85px;
            margin-top: 40px;
            position: relative;
            display: block;
        }
        .header_objectId-text .bold_text {
            font-size: 36px;
            font-weight: bold;
            margin-top: 15px;
            position: relative;
            display: block;
        }
        .image-block {
            margin: 29px 44px 29px 34px;
            width: 1172px;
            height: 727px;
        }
        .image-block img {
            width: 576px;
            height: 354px;
            float: left;
            margin-bottom: 19px;
        }
        .vertical_separator {
            width: 20px;
            height: 354px;
            float: left;
        }
        .price_block {
            margin-left: 34px;
            margin-right: 44px;
            margin-bottom: 70px;
            width: auto;
            height: 60px;
            background-color: <?= $color1 ?>;
            color: white;
        }
        .price {
            display: block;
            margin: 0;
            font-size: 28px;
            font-weight: bold;
            color: <?= $color2 ?>;
            position: relative;
            top: 15px;
            left: 30px;
        }
        .price_bold {
            color: #ffffff;
            position: relative;
            margin-left: 10px;
        }
        .description_block {
            overflow: auto;
            margin-left: 34px;
            margin-right: 44px;
            margin-bottom: 83px;
            width: 1172px;
        }
        .descripton_block-text {
            width: 620px;
            height: 500px;
            position: relative;
            float: left;
        }
        .description_block-map {
            width: 485px;
            height: 485px;
            position: relative;
            float: right;
        }
        .description_block-map img {
            width: 485px;
            height: 485px;
        }
        .footer {
            height: 115px;
            background-color: <?= $color1 ?>;
            color: white;
        }
        .footer_logo {
            width: 230px;
            height: 80px;
            position: relative;
            display: block;
            float: left;
            margin-left: 50px;
            margin-top: 15px;
        }
        .footer_info {
            float: right;
            margin-top: 35px;
            margin-right: 40px;
        }
        .footer_phone {
            font-size: 28px;
        }
        .footer_link {
            float: right;
            font-weight: bold;
            font-size: 20px;
        }
        .page {
            height: 1700px;
        }
        .props {
            margin-left: 34px;
            margin-top: 40px;
            width: 530px
        }
        .props ul {
            list-style: none;
            font-size: 20px;
            padding: 0;
        }
        li {
            font-weight: bold;
            margin-bottom: 5px;
        }
        li span {
            background: #fff;
            text-align: left;
        }
        .prop_value {
            text-align: right;
            float: right;
            height: 22px;
            color: <?= $color2 ?>;
        }
        .floor_description {
            height: 520px;
            margin-left: 34px;
            margin-right: 44px;
            margin-bottom: 30px;
            font-size: 20px;
        }
        .floor_description .bold {
            font-weight: bold;
        }
        .plans {
            height: 700px;
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="page">
    <div class="header">
        <div class="header_title">
            <p class="header_title-text"><?= $object['NAME'] ?></p>
        </div>
        <div class="header_address">
            <p class="header_address-text"><?= $object['ADDRESS'] ?></p>
        </div>
        <div class="header_objectId">
            <p class="header_objectId-text">ID объекта:<br><span class="bold_text"><?= $object['CODE'] ?></span></p>
        </div>
    </div>
    <div class="image-block">
        <div class="image"><img src="<?= $object['PHOTOS'][0] ?>"></div>
        <div class="vertical_separator"></div>
        <div class="image"><img src="<?= $object['PHOTOS'][1] ?>"></div>
        <div class="image"><img src="<?= $object['PHOTOS'][2] ?>"></div>
        <div class="vertical_separator"></div>
        <div class="image"><img src="<?= $object['PHOTOS'][3] ?>"></div>
    </div>
    <div class="price_block">
        <p class="price">Цена:<span class="price_bold"><?= is_null($object['PRICE_USD']) ? 'По запросу' : $object['PRICE_USD'] . '$' ?></span></p>
    </div>
    <div class="description_block">
        <div class="descripton_block-text">
            <?= $object['DETAIL_TEXT'] ?>
        </div>
        <div class="description_block-map">
            <img style="display: block;" src="https://static-maps.yandex.ru/1.x/?ll=<?= $long . ',' . $lat ?>&size=450,450&z=13&l=map&pt=<?= $long . ',' . $lat ?>,org">
        </div>
    </div>
    <div class="footer">
        <div class="footer_logo">
            <img src="/local/templates/.default/images/logo_elit.png">
        </div>
        <div class="footer_info">
            <span class="footer_phone">+7 (499) 270-20-20</span><br>
            <span class="footer_link">www.metrium.ru</span>
        </div>
    </div>
</div>
<div style="page-break-after: always;"><span style="display:
none;">&nbsp;</span></div>
<div class="page">
    <div class="header">
        <div class="header_title">
            <p class="header_title-text"><?= $object['NAME'] ?></p>
        </div>
        <div class="header_address">
            <p class="header_address-text"><?= $object['ADDRESS'] ?></p>
        </div>
        <div class="header_objectId second_page">
            <p class="header_objectId-text">ID объекта:<br><span class="bold_text"><?= $object['CODE'] ?></span></p>
        </div>
    </div>
    <div class="props">
        <ul>
            <? if ($object['HOUSE_AREA']) { ?>
                <li><span>Площадь дома</span><span class="prop_value"><?= $object['HOUSE_AREA'] ?> м<sup>2</sup></span></li>
            <? } ?>
            <? if ($object['TERRITORY_AREA']) { ?>
                <li><span>Площадь участка</span><span class="prop_value"><?= $object['TERRITORY_AREA'] ?> м<sup>2</sup></span></li>
            <? } ?>
            <? if ($object['GUARDED']) { ?>
                <li><span>Охраняемый</span><span class="prop_value"><?= $object['GUARDED'] ?></sup></span></li>
            <? } ?>
            <? if ($object['MATERIAL']) { ?>
                <li><span>Материал дома</span><span class="prop_value"><?= $object['MATERIAL'] ?></span></li>
            <? } ?>
            <? if ($object['FACING']) { ?>
                <li><span>Отделка</span><span class="prop_value"><?= $object['FACING'] ?></span></li>
            <? } ?>
        </ul>
    </div>
    <div class="floor_description">
        <span class="bold">Этажи/уровни:</span>
        <?= $object['FLOOR'] ?>
    </div>
    <div class="plans"></div>
    <div class="footer">
        <div class="footer_logo">
            <img src="/local/templates/.default/images/logo_elit.png">
        </div>
        <div class="footer_info">
            <span class="footer_phone">+7 (499) 270-20-20</span><br>
            <span class="footer_link">www.metrium.ru</span>
        </div>
    </div>
</div>
</body>
</html>