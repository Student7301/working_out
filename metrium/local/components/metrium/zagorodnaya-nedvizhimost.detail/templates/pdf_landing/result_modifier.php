<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$objectID = $_REQUEST['object'];

if (isset($objectID)) {
    $object = CIBlockElement::GetList(
        array(),
        array(
            'IBLOCK_ID' => IblockUtils::GetIBlockIdByCode('landings'),
            'ID' => $objectID
        ),
        false,
        array('nTopCount' => 1),
        array(
            'ID',
            'IBLOCK_ID',
            'PROPERTY_PDF_COLOR_FIRST',
            'PROPERTY_PDF_COLOR_SECOND'
        )
    )->GetNext();

    $arResult['OBJECT']['PDF_COLOR_FIRST'] = $object['PROPERTY_PDF_COLOR_FIRST_VALUE'];
    $arResult['OBJECT']['PDF_COLOR_SECOND'] = $object['PROPERTY_PDF_COLOR_SECOND_VALUE'];
}