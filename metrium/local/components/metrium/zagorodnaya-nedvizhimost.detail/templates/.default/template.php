<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetPageProperty("description", $arResult['OBJECT']['NAME'] . '. Объект ID ' . $arResult['OBJECT']['ID'] . '. Элитная загородная недвижимость'); ?>
<? if ($arResult['OBJECT']['ACTIVE'] == 'N') { ?>
    <div style="position: fixed; width: 100%; height: 100%; top: 0; left: 0;background-color: rgba(0,0,0,.8); z-index: 1000;">
        <div style="position: absolute; top: 25%; width: 100%; text-align: center">
            <p style="font-size: 40px; color: white;">Объект реализован</p>
            <? if (count($arResult['RELATED_OBJECTS']) > 0): ?>
                <div class="objects-similar">
                    <div class="page-head" style="text-align: center">
                        <div class="page-title page-title--elit" style="margin-right: 0;">Похожие объекты
                            на <?= $arResult['OBJECT']['NAME'] ?></div>
                    </div>
                    <div class="elit-catalog clearfix" style="text-align: left">
                        <div class="container container--elit tile__line-content tile__line-content--flex"
                             style="margin: 0 auto">
                            <? foreach ($arResult['RELATED_OBJECTS'] as $arItem): ?>
                                <a href="../<?= $arItem['ID'] ?>/" class="tile__item">
                                    <div class="tile__item-content">
                                        <div style="background: url(<?= $arItem['PREVIEW_PICTURE'] ?>) no-repeat; background-size: cover"
                                             class="tile__item-img"></div>
                                        <div class="tile__item-info">
                                            <div class="tile__info-content">
                                                <div class="tile__info-name"><?= $arItem['NAME'] ?></div>
                                                <div class="tile__info-address">
                                                    <? if ($arItem['DIRECTION']): ?>
                                                        <?= $arItem['DIRECTION'] ?>
                                                    <? endif; ?>
                                                    <? if ($arItem['REMOTNESS_MKAD'] && $arItem['DIRECTION']): ?>
                                                        ,
                                                    <? endif; ?>
                                                    <? if ($arItem['REMOTNESS_MKAD']): ?>
                                                        <?= $arItem['REMOTNESS_MKAD'] ?> км. от МКАД
                                                    <? endif; ?>
                                                </div>
                                                <div class="tile__info-params">
                                                    <? if ($arItem['HOUSE_AREA']) { ?>
                                                        <div class="tile__info-param"><?= $arItem['HOUSE_AREA'] ?>
                                                            кв.м.
                                                        </div>
                                                    <? } ?>
                                                    <? if ($arItem['TERRITORY_AREA']) { ?>
                                                        <div class="tile__info-param"><?= $arItem['TERRITORY_AREA'] ?>
                                                            соток
                                                        </div>
                                                    <? } ?>
                                                    <? if ($arItem['FACING']) { ?>
                                                        <div class="tile__info-param"><?= $arItem['FACING'] ?></div>
                                                    <? } ?>
                                                </div>
                                                <div class="tile__info-price">
                                                    <?
                                                    unset($hideArendaRub, $hideRub);
                                                    if ($arItem['ARENDA_USD'] == 0 && $arItem['ARENDA_EUR'] == 0) {
                                                        $arItem['ARENDA_USD'] = 'Цена по запросу';
                                                        $hideArendaRub = true;
                                                    } else {
                                                        $arItem['ARENDA_USD'] = $arItem['ARENDA_USD'] . ' $';
                                                        $hideArendaRub = false;
                                                    }

                                                    if ($arItem['PRICE_USD'] == 0 && !isset($arItem['PRICE_EUR'])) {
                                                        $arItem['PRICE_USD'] = 'Цена по запросу';
                                                        $hideRub = true;
                                                    } else {
                                                        $arItem['PRICE_USD'] = $arItem['PRICE_USD'] . ' $';
                                                        $hideRub = false;
                                                    }
                                                    ?>


                                                    <div class="tile__info-price-one">
                                                        <? if ($arItem["HIDE_PRICES"]): ?>
                                                            Цена по запросу
                                                        <? else: ?>
                                                            <? if ($arItem['PRICE_EUR'] != 0): ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda') ? $arItem['ARENDA_EUR'] . ' €' : $arItem['PRICE_EUR'] . ' €' ?>
                                                            <? else: ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda') ? $arItem['ARENDA_USD'] : $arItem['PRICE_USD'] ?>
                                                            <? endif; ?>
                                                        <? endif; ?>
                                                    </div>
                                                    <div class="tile__info-price-two">
                                                        <? if ($arItem["HIDE_PRICES"]): ?>

                                                        <? else: ?>
                                                            <? if ($arItem['PRICE_EUR'] != 0): ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda' && !$hideArendaRub) ? $arItem['ARENDA_RUB'] . ' &#8381;' : '' ?>
                                                                <?= ($arParams['SECTION_CODE'] != 'arenda' && !$hideRub) ? $arItem['PRICE_RUB'] . ' &#8381;' : '' ?>
                                                            <? else: ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda' && !$hideArendaRub) ? $arItem['ARENDA_RUB'] . ' &#8381;' : '' ?>
                                                                <?= ($arParams['SECTION_CODE'] != 'arenda' && !$hideRub) ? $arItem['PRICE_RUB'] . ' &#8381;' : '' ?>
                                                            <? endif; ?>
                                                        <? endif; ?>
                                                    </div>
                                                </div>
                                                <div class="tile__info-id">ID объекта: <?= $arItem['CODE'] ?></div>
                                            </div>
                                        </div>
                                        <div class="tile__special-mark c-like" data-obj_id="<?= $arItem['ID'] ?>"
                                             data-ajax_url="/local/templates/.default/ajax/like.php">
                                            <span class="tile__icon icon-heart"></span>
                                        </div>
                                    </div>
                                </a>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            <? else: ?>
                <div class="page-head">
                    <div class="page-title page-title--elit">Нет похожих объектов</div>
                </div>
            <? endif; ?>
        </div>
    </div>
<? } ?>

<? if ($arParams['SEND'] == 'Y') : ?>
    <script>
        $(document).on('ready', function () {
            $('#send-pdf-icon').trigger('click');
            setTimeout(function () {
                $('.drop-form.send-pdf').css('box-shadow', '0 0 3px 1px red');
            }, 1000);
            setTimeout(function () {
                $('.drop-form.send-pdf').css('box-shadow', 'none');
            }, 2000);
        });
    </script>
<? endif; ?>

<div class="c-popup popup">
    <div class="c-popup-close close-popup">
        <div class="sprite-icon close c-popup-close"></div>
    </div>
    <div class="popup__content"></div>
</div>
<div class="content"><!-- BEGIN header -->
    <div class="header-panel header-panel--alternate c-top-panel-alternate"></div>
    <header class="header clearfix c-top-panel">
        <div class="container">
            <div class="header-panel"><a href="#" title="" class="header-panel__menu c-popup-open"><span
                            class="header-panel__icon">
                <div class="icon-menu"></div></span><span class="header-panel__text">меню</span><!-- popup --></a>
                <div class="c-popup-menu popup clearfix">
                    <div class="popup__overlay"></div>
                    <div class="popup__inner">
                        <div class="popup__content-menu c-popup-content"><a href="#" title=""
                                                                            class="popup__close-menu c-popup-close-menu"><span
                                        class="icon-close"></span>
                                <div class="popup__close-text">Меню</div>
                            </a>
                            <div class="popup__elit-menu">
                                <a href="/zagorodnaya-nedvizhimost/" title="" class="popup__big-title">Элитная загородная недвижимость</a>
                                <a href="/zagorodnaya-nedvizhimost/" title="" class="width_for_a popup__big-btn btn-form"  > Более <?= \metrium\helpers\IBlockHelper::getCountObjectForFilter(''); ?> объектов </a>
                                <a href="/elitnaya-gorodskaya-nedvijimost/" title="" class="popup__big-title">Элитная городская недвижимость</a>
                                <a href="/zagorodnaya-nedvizhimost/" title="" class="width_for_a popup__big-btn btn-form"  > Более <?= \metrium\helpers\IBlockHelper::getCountEliteApartments(); ?> объектов </a>
                            </div>
                            <hr>
                            <div class="popup__new-menu">
                                <ul class="popup__new-menu-list">
                                    <li><a href="/novostroyki/novostroyki-moskvy/" title="" class="popup__new-menu-item">Новостройки Москвы</a></li>
                                    <li><a href="/novostroyki/novostroyki-moskovskoy-oblasti/" title="" class="popup__new-menu-item">Новостройки Подмосковья</a></li>
                                    <li><a href="/novostroyki/novostroyki-novaya-moskva/" title="" class="popup__new-menu-item">Новостройки новой москвы</a></li>
                                </ul>
                            </div>
                            <hr>
                            <div class="popup__main-menu">
                                <ul class="popup__main-menu-list">
                                    <li><a href="/funds/" title="" class="popup__main-menu-item">девелоперам</a></li>
                                    <li><a href="/o-kompanii/" title="" class="popup__main-menu-item">о компании</a></li>
                                    <li><a href="/news/<?= \metrium\helpers\IBlockHelper::getDataLastNews(); ?>" title="" class="popup__main-menu-item">пресс-служба</a></li>
                                    <li><a href="/ipoteka/programmy_bankov/" title="" class="popup__main-menu-item">ипотека</a></li>
                                    <li><a href="/trade-in/" title="" class="popup__main-menu-item">Trade-In</a></li>
                                    <li><a href="/analitika-i-konsalting-dev/" title="" class="popup__main-menu-item">Аналитика и консалтинг</a></li>
                                    <li><a href="/organizatsiya-prodazh/" title="" class="popup__main-menu-item">Организация продаж</a></li>
                                    <li><a href="/investitsii/" title="" class="popup__main-menu-item">Инвестиции</a></li>
                                </ul>
                            </div>
                            <hr>
                            <div class="popup__social-icon">
                                <a href="https://www.facebook.com/metrium" title="" class="popup__social-icon-item" target="_blank"><span class="icon-facebook"></span></a>
                                <a href="https://vk.com/metriumru" title="" class="popup__social-icon-item" target="_blank"><span class="icon-vk"></span></a>
                                <a href="https://www.instagram.com/metriumgroup/" title="" class="popup__social-icon-item" target="_blank"><span class="icon-instagram"></span></a>
                                <a href="https://twitter.com/Metrium2" title="" class="popup__social-icon-item" target="_blank"><span class="icon-twitter"></span></a>
                                <a href="https://ok.ru/group/53323706728662" title="" class="popup__social-icon-item" target="_blank"><span class="icon-odnoklassniki"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/" title="" class="header-panel__logo">
                    <div class="header-panel__logo-inner"><img
                                src="<?= SITE_TEMPLATE_PATH . '/images/content/logo-elit-1920.png' ?>" alt=""></div>
                </a>
                <div class="header-panel__info clearfix">
                    <div class="header-panel__info-title"><a href="/zagorodnaya-nedvizhimost/">Элитная загородная недвижимость</a></div>
                </div>
                <div class="header-panel__left-info">
                    <div class="header-panel__phone">
                        <a href="tel:84997552580" id="phone_moscow">8 (499) 755-25-80</a>
                    </div>
                    <?if($USER->IsAuthorized()):?>
                        <a href="/kabinet/" title="" class="header-panel__personal">
                            <span class="header-panel__personal-text">Личный кабинет</span>
                        </a>
                    <?endif;?>
                    <a href="/kabinet/vybor/" title="" class="header-panel__choice choose right">
                        <span class="header-panel__personal-text">Мой выбор</span>
                        <span class="icon icon-heart"></span>
                        <span class="header-panel__personal-text choose__count"></span>
                    </a>
                </div>
            </div>
        </div>
    </header><!-- END header -->

    <div class="filter__header" style="margin: 70px 0 45px 0; position: relative; top: -10px;">
        <a href="/zagorodnaya-nedvizhimost/kottedzhi_taunkhausy/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'kottedzhi_taunkhausy'){?>active<?}?>">Коттеджи и таунхаусы</a>
        <a href="/zagorodnaya-nedvizhimost/poselki/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'poselki'){?>active<?}?>">Поселки</a>
        <a href="/zagorodnaya-nedvizhimost/uchastki/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'uchastki'){?>active<?}?>">Участки</a>
        <a href="/zagorodnaya-nedvizhimost/arenda/" title="" class="filter__header-link <? if($arParams['SECTION_CODE'] == 'arenda'){?>active<?}?>">Аренда</a>
    </div>


    <div class="elit-detail">
        <div class="elit-table">
            <div class="elit-slider">

                <div class="tabs c-tabs">
                    <div class="tabs__content">
                        <div class="tabs__item active c-tabs-item c-custom-slider c-elite-fotorama">
                            <div data-url-popup="<?= $templateFolder ?>/ajax/elit-gallery.php?IBLOCK_ID=<?= $arResult['OBJECT']['IBLOCK_ID'] ?>&ELEMENT_CODE=<?= $arResult['OBJECT']['CODE'] ?>"
                                 class="zoom-block zoom-block--small c-popup__link">
                                <i class="sprite-icon zoom-small"></i>
                            </div>
                            <div data-nav="thumbs" data-navwidth="80%" data-arrows="always" data-navposition="bottom"
                                 data-width="100%" data-click="true" data-height="600" data-fit="contain"
                                 data-thumbheight="70" data-thumbwidth="115" data-thumbmargin="5" data-loop="true"
                                 class="fotorama fotorama--with-arrows">

                                <? foreach ($arResult['OBJECT']['PHOTOS'] as $photo): ?>
                                    <a href="<?= $photo['PHOTO'] ?>" title="">
                                        <img src="<?= $photo['THUMB'] ?>" alt="">
                                    </a>
                                <? endforeach; ?>
                                <? if (count($arResult['OBJECT']['PHOTOS']) == 0): ?>
                                    <a href="<?= $arResult['OBJECT']['PREVIEW_PICTURE']['PHOTO'] ?>" title="">
                                        <img src="<?= $arResult['OBJECT']['PREVIEW_PICTURE']['THUMB'] ?>" alt="">
                                    </a>
                                <? endif; ?>
                            </div>
                        </div>
                        <div id="map" class="tabs__item c-tabs-item" style="height: 650px">

                        </div>
                        <div id="satellit" class="tabs__item c-tabs-item" style="height: 650px">

                        </div>
                    </div>
                    <div class="elit-actions-absolute">
                        <div class="tabs__links">
                            <div class="photo-block active elit-tabs-link c-tabs-link c-tabs-link-photo foto-button">
                                <i class="sprite-icon map-elit"></i>
                                Фото
                            </div>
                            <? if ($arResult['OBJECT']['LATITUDE'] && $arResult['OBJECT']['LONGITUDE']): ?>
                                <div class="map-block elit-tabs-link c-tabs-link c-tabs-link-map map-button">
                                    <a href="#" title="" data-map-id="map-elit"
                                       data-placemarks-json="<?= $templateFolder ?>/ajax/elite-coordinate.php?lat=<?= $arResult['OBJECT']['LATITUDE'] ?>&lon=<?= $arResult['OBJECT']['LONGITUDE'] ?>"
                                       class="absolute-link c-map-link"></a>
                                    <i class="sprite-icon map-elit"></i>
                                    На карте
                                </div>
                            <? endif; ?>
                        </div>

                        <div class="building-id building-id--new">
                            <div class="building-id__text">ID объекта</div>
                            <div class="building-id__number"><?= $arResult['OBJECT']['CODE'] ?></div>
                        </div>

                        <div class="print-block print-block--white">
                            <a href="<?= $arResult['OBJECT']['DETAIL_PAGE_URL'] ?>pdf/" target="_blank" title=""
                               class="absolute-link"></a>
                            <i class="sprite-icon pdf-black"></i>
                            Печать pdf
                        </div>
                        <div class="print-block print-block--white">
                            <a href="<?= $arResult['OBJECT']['DETAIL_PAGE_URL'] ?>pdf_advanced/" target="_blank"
                               title="" class="absolute-link"></a>
                            <i class="sprite-icon pdf-black"></i>
                            Презентация о проекте
                        </div>
                        <div class="print-block print-block--white no-scroll-action c-accordion">
                            <div class="middle-text" style="padding-top: 7px;">Отправить PDF на email</div>
                            <a href="" title="" id="send-pdf-icon"
                               class="absolute-link c-dropdownform__link-custom"></a>
                            <div class="drop-form drop-form--top contact-form bottom-shadow bottom-shadow--small c-dropdownform__content-custom send-pdf">
                                <form action="/local/templates/.default/ajax/pdf-to-email.php" method="post"
                                      class="c-ajaxForm">
                                    <div class="ajax-form-mess c-ajax-form-mess"></div>
                                    <div class="form-title"></div>
                                    <div class="form-line">
                                        <div class="form-line form-line--custom">
                                            <input type="radio" name="version" id="pdf1" value="simple"
                                                   class="checkbox" <?= (empty($arParams['VERSION']) || $arParams['VERSION'] == 'simple') ? 'checked' : '' ?>>
                                            <label for="pdf1" data-checked="N" class="checkbox-label">
                                                <span class="checkbox-icon checkbox-icon--radio"></span>
                                                <span class="checkbox-text">Краткая версия</span>
                                            </label>
                                        </div>

                                        <div class="form-line form-line--custom">
                                            <input type="radio" name="version" id="pdf2" value="advanced"
                                                   class="checkbox" <?= ($arParams['VERSION'] == 'advanced') ? 'checked' : '' ?>>
                                            <label for="pdf2" data-checked="N" class="checkbox-label">
                                                <span class="checkbox-icon checkbox-icon--radio"></span>
                                                <span class="checkbox-text">Подробная версия</span>
                                            </label>
                                        </div>
                                        <div class="form-line">
                                            <input type="email" id="pdf_email" placeholder="Адрес электронной почты"
                                                   name="email" data-validation="email" class="form-input">
                                        </div>
                                        <input type="hidden" name="id" value="<?=$arResult["OBJECT"]["ID"]?>">
                                        <input type="hidden" name="code" value="<?=$arParams['ELEMENT_CODE']?>">
                                        <input type="hidden" name="type" value="<?=$arParams['SECTION_CODE']?>">
                                        <div style="padding-top: 4%; color: black; font-size: 10px">
                                            <input type="checkbox"  data-validation="required">
                                            Согласен на обработку персональных данных. Ставя отметку, я даю свое <a target="_blank" href="/real_estate/declaration/03.07.2017 Согласие на обработку персональных данных.pdf" style="color:#66a3e4;">согласие на обработку моих персональных данных</a> в соответствии с законом
                                            №152-Ф3 «О персональных данных» от 27.07.2006 г.
                                        </div>
                                        <div class="form-submit bottom-shadow bottom-shadow--small right">
                                            <input value="Отправить" type="submit" name="callback_form"
                                                   class="btn btn--gray btn--large">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <? if ($arResult['OBJECT']['LATITUDE'] && $arResult['OBJECT']['LONGITUDE']): ?>
                            <div class="satellite-block c-fake-tab-active c-mapsat-link mapsat-button">
                                <a href="#" title="" data-type="yandex#satellite" data-map-id="map-elit"
                                   data-placemarks-json="<?= $templateFolder ?>/ajax/elite-coordinate.php?lat=<?= $arResult['OBJECT']['LATITUDE'] ?>&lon=<?= $arResult['OBJECT']['LONGITUDE'] ?>"
                                   class="absolute-link"></a>
                                <i class="sprite-icon satellite"></i>
                                Спутник
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult['OBJECT']['VIDEO'])): ?>
                            <div class="print-block print-block--white print-block--nopad1">
                                <a href="" title="" data-type-popup="iframe"
                                   data-iframe-attr="width='600' height='400' scrolling='no' frameborder='0'"
                                   data-url-popup="<?= $arResult['OBJECT']['VIDEO'] ?>"
                                   class="absolute-link c-popup-link"></a>
                                <i class="play_video"></i>
                                Посмотреть видео
                            </div>
                        <? endif; ?>
                        <div class="green-block no-scroll-action c-accordion">
                            <div class="middle-text">Оставить заявку</div>
                            <a href="" title="" class="absolute-link c-dropdownform__link-custom"></a>
                            <div class="drop-form drop-form--top contact-form bottom-shadow bottom-shadow--small c-dropdownform__content-custom">
                                <form action="<?= $templateFolder ?>/ajax/order-processing.php" method="post"
                                      class="c-ajaxForm">
                                    <div class="ajax-form-mess c-ajax-form-mess"></div>
                                    <div class="form-title">обратная связь</div>
                                    <div class="form-line">
                                        <input type="text" placeholder="Имя, Фамилия" name="name"
                                               data-validation="required" class="form-input">
                                        <div class="form-line">
                                            <input type="text" placeholder="Телефон" name="phone"
                                                   data-validation="number" class="form-input">
                                        </div>
                                        <div class="form-line">
                                            <input type="email" placeholder="Адрес электронной почты" name="email"
                                                   data-validation="email" class="form-input">
                                        </div>
                                        <div class="form-line">
                                            <textarea placeholder="Текст заявки" name="message"
                                                      data-validation="required"
                                                      class="form-input form-input--textarea"></textarea>
                                        </div>
                                        <div style="padding-top: 4%; color: black; font-size: 10px">
                                            <input type="checkbox" data-validation="required">
                                            Согласен на обработку персональных данных. Ставя отметку, я даю свое <a target="_blank" href="/real_estate/declaration/для_элитной_Согласие_на_обработку_персональных_данных.pdf" style="color:#66a3e4;">согласие на обработку моих персональных данных</a> в соответствии с законом
                                            №152-Ф3 «О персональных данных» от 27.07.2006 г.
                                        </div>
                                        <input type="hidden" name="object_id" value="<?=$arResult["OBJECT"]["ID"]?>">
                                        <div class="form-submit bottom-shadow bottom-shadow--small right">
                                            <input value="Отправить" type="submit" name="callback_form"
                                                   class="btn btn--gray btn--large">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="like-block c-like"
                             data-action=""
                             data-obj_id="<?= $arResult['OBJECT']['ID'] ?>"
                             data-ajax_url="/local/templates/.default/ajax/like.php"
                        >
                            <i class="sprite-icon like"></i>
                            <i class="sprite-icon like-active"></i>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="elit-info-wrap">

                <? if ($arResult['OBJECT']): ?>
                    <div class="elit-info c-mCustomScrollbar mCustomScrollbar">
                        <? if ($arParams['SECTION_CODE'] == 'poselki' || $arParams['SECTION_CODE'] == 'kottedzhi_taunkhausy'): ?>
                            <div class="obj-name"><?= isset($arResult['OBJECT']['NAME']) ? $arResult['OBJECT']['NAME'] : "" ?></div>
                        <? else: ?>
                            <? if ($arResult['OBJECT']['POSELOK_CODE']): ?>
                                <a class="sub-directory"
                                   href="/zagorodnaya-nedvizhimost/poselki/<?= $arResult['OBJECT']['POSELOK_CODE'] ?>/?SHOW_TABLE=Y"><?= $arResult['OBJECT']['NAME'] ?></a>
                            <? else: ?>
                                <a class="sub-directory"
                                   href="/zagorodnaya-nedvizhimost/poselki/"><?= $arResult['OBJECT']['NAME'] ?></a>
                            <? endif; ?>
                        <? endif; ?>
                        <div class="obj-direction">Направление: <?= $arResult['OBJECT']['DIRECTION'] ?>
                            , <?= $arResult['OBJECT']['REMOTNESS_MKAD'] ?> км. от МКАД
                        </div>

                        <div class="obj-price <?= ($arResult['OBJECT']['PRICE_USD'] == 0) ? 'price--small' : 'price--dbl' ?>">
                            <div class="obj-price-block">
                                <? if ($arResult["OBJECT"]["HIDE_PRICES"] == "Y"): ?>
                                    Цена по запросу
                                <? elseif ($arParams['SECTION_CODE'] == 'arenda'): ?>
                                    <? if (isset($arResult['OBJECT']['ARENDA_EUR'])): ?>
                                        <?= $arResult['OBJECT']['ARENDA_EUR'] . ' €' ?>
                                        <span class="multi-valute">
                                        <?= $arResult['OBJECT']['ARENDA_RUB'] . ' ₽' ?>
                                    </span>
                                    <? else: ?>
                                        <?= ($arResult['OBJECT']['ARENDA_USD']) ? $arResult['OBJECT']['ARENDA_USD'] . ' $' : 'Цена по запросу' ?>
                                        <span class="multi-valute">
                                        <?= ($arResult['OBJECT']['ARENDA_USD']) ? $arResult['OBJECT']['ARENDA_RUB'] . ' ₽' : '' ?>
                                    </span>
                                    <? endif; ?>
                                <? elseif ($arResult['OBJECT']['PRICE_EUR'] != 0): ?>
                                    <?= $arResult['OBJECT']['PRICE_EUR'] . ' €' ?>
                                    <? if ($arResult['OBJECT']['PRICE_EUR'] != 0): ?>
                                        <span class="multi-valute">
                                        <?= $arResult['OBJECT']['PRICE_RUB'] ?> ₽
                                    </span>
                                    <? endif; ?>
                                <? else: ?>
                                    <?= ($arResult['OBJECT']['PRICE_USD'] == 0) ? 'Цена по запросу' : $arResult['OBJECT']['PRICE_USD'] . ' $' ?>
                                    <? if ($arResult['OBJECT']['PRICE_USD'] != 0): ?>
                                        <span class="multi-valute">
                                        <?= $arResult['OBJECT']['PRICE_RUB'] ?> ₽
                                    </span>
                                    <? endif; ?>
                                <? endif; ?>

                            </div>

                            <a href="" onclick="SCBopen(1); return false;" class="clearfix">
                                <div class="elite-detail-scb">
                                    Узнать подробнее
                                </div>
                            </a>

                            <!--Предложить свою цену -->
                            <div class="offer-price c-dropdownform">
                                <a href="" title="" class="offer-price__link c-dropdownform__link-custom-offer-price">
                                    Предложить свою цену
                                </a>

                                <div class="c-dropdownform__content-custom-offer-price obj-charact" style="display: none; font-size: 14px;">

                                    <form method="POST" action="/local/templates/.default/ajax/sendEmail_myprice.php"
                                          class="c-ajaxForm ">
                                        <div class="ajax-form-mess c-ajax-form-mess"></div>
                                        <br/>

                                        <input type="hidden" name="type_message" data-validation="required"
                                               value="zagorodka" class="form-input">
                                        <input type="hidden" name="Object_id" data-validation="required"
                                               value="<?= $arResult["OBJECT"]["ID"] ?>" class="form-input">
                                        <input type="hidden" name="Object_name" data-validation="required"
                                               value="<?= $arResult["OBJECT"]["NAME"] ?>" class="form-input">
                                        <input type="hidden" name="Object_url" data-validation="required"
                                               value="<?= $arResult["OBJECT"]["DETAIL_PAGE_URL"] ?>" class="form-input">
                                        <? if ($arResult["OBJECT"]["HIDE_PRICES"] == "Y"): ?>
                                            <input type="hidden" name="price" data-validation="required"
                                                   value="Цена по запросу" class="form-input">
                                        <? elseif ($arParams['SECTION_CODE'] == 'arenda'): ?>
                                            <? if (isset($arResult['OBJECT']['ARENDA_EUR'])): ?>
                                                <input type="hidden" name="price" data-validation="required"
                                                       value="Аренда: <?= $arResult['OBJECT']['ARENDA_EUR'] . ' €' ?>"
                                                       class="form-input">
                                                <input type="hidden" name="price_sale" data-validation="required"
                                                       value="Аренда: <?= $arResult['OBJECT']['ARENDA_RUB'] . ' ₽' ?>"
                                                       class="form-input">
                                            <? else: ?>
                                                <input type="hidden" name="price" data-validation="required"
                                                       value="Аренда: <?= ($arResult['OBJECT']['ARENDA_USD']) ? $arResult['OBJECT']['ARENDA_USD'] . ' $' : 'Цена по запросу' ?>"
                                                       class="form-input">
                                                <input type="hidden" name="price_sale" data-validation="required"
                                                       value="Аренда: <?= ($arResult['OBJECT']['ARENDA_USD']) ? $arResult['OBJECT']['ARENDA_RUB'] . ' ₽' : '' ?>"
                                                       class="form-input">
                                            <? endif; ?>
                                        <? elseif ($arResult['OBJECT']['PRICE_EUR'] != 0): ?>
                                            <input type="hidden" name="price" data-validation="required"
                                                   value="<?= $arResult['OBJECT']['PRICE_EUR'] . ' €' ?>"
                                                   class="form-input">
                                            <input type="hidden" name="price_sale" data-validation="required"
                                                   value="<?= $arResult['OBJECT']['PRICE_RUB'] ?> ₽" class="form-input">
                                        <? else: ?>
                                            <input type="hidden" name="price" data-validation="required"
                                                   value="<?= ($arResult['OBJECT']['PRICE_USD'] == 0) ? 'Цена по запросу' : $arResult['OBJECT']['PRICE_USD'] . ' $' ?>"
                                                   class="form-input">
                                            <input type="hidden" name="price_sale" data-validation="required"
                                                   value="<?= $arResult['OBJECT']['PRICE_RUB'] ?> ₽" class="form-input">
                                        <? endif; ?>
                                        <div>
                                            <div class="div_input_my_price"><input type="text" placeholder="Ваше имя"
                                                                                   name="OF-name"
                                                                                   data-validation="required"
                                                                                   class="form-input"></div>
                                            <div class="div_img_my_price"><img src="/upload/Important.png"></div>
                                        </div>
                                        <div>
                                            <div class="div_input_my_price"><input type="text" placeholder="Email"
                                                                                   name="OF-email"
                                                                                   data-validation="email"
                                                                                   class="form-input"></div>
                                            <div class="div_img_my_price"><img src="/upload/Important.png"></div>
                                        </div>
                                        <div>
                                            <div class="div_input_my_price"><input type="text" placeholder="Телефон"
                                                                                   name="OF-phone"
                                                                                   data-validation="required"
                                                                                   class="form-input c-input-phone">
                                            </div>
                                            <div class="div_img_my_price"><img src="/upload/Important.png"></div>
                                        </div>
                                        <div>
                                            <div class="div_input_my_price"><input type="number" placeholder="Ваша цена"
                                                                                   name="OF-price"
                                                                                   data-validation="required"
                                                                                   class="form-input"></div>
                                            <div class="div_img_my_price"><img src="/upload/Important.png"></div>
                                        </div>
                                        <span class="type-currency">
                                        <input type="radio" id="dollar" name="currency" value="dollar"
                                               class="type-currency__input" checked="checked">
                                        <label for="dollar" class="type-currency__label">$</label>
                                    </span>

                                        <span class="type-currency">
                                        <input type="radio" id="euro" name="currency" value="euro"
                                               class="type-currency__input" checked="checked">
                                        <label for="euro" class="type-currency__label">€</label>
                                    </span>
                                        <span class="type-currency">
                                        <input type="radio" id="ruble" name="currency" value="ruble"
                                               class="type-currency__input" checked="checked">
                                        <label for="ruble" class="type-currency__label">Р</label>
                                    </span>
                                    <textarea placeholder="Комментарий" name="OF-message" class="form-input form-input--textarea"></textarea>

                                    <div style="padding-top: 3%; font-size: 10px">
                                        <input type="checkbox" data-validation="required">
                                        Согласен на обработку персональных данных. Ставя отметку, я даю свое <a target="_blank" href="/real_estate/declaration/для_элитной_Согласие_на_обработку_персональных_данных.pdf" style="color:#66a3e4;">согласие на обработку моих персональных данных</a> в соответствии с законом
                                        №152-Ф3 «О персональных данных» от 27.07.2006 г.
                                    </div>
                                    <input value="Отправить" type="submit" name="callback_form" class="btn btn--gray btn--large" style="margin-top:10px">

                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="obj-charact">
                            <? if ($arResult['OBJECT']['HOUSE_AREA']): ?>
                                <div class="obj-charact__line"><span
                                            class="obj-charact__term">Площадь дома:</span><?= $arResult['OBJECT']['HOUSE_AREA'] ?>
                                    кв. м
                                </div>
                            <? endif; ?>
                            <? if ($arResult['OBJECT']['TERRITORY_AREA']): ?>
                                <div class="obj-charact__line"><span
                                            class="obj-charact__term">Площадь участка:</span><?= $arResult['OBJECT']['TERRITORY_AREA'] ?>
                                    соток
                                </div>
                            <? endif; ?>
                            <? if ($arResult['OBJECT']['GUARDED']): ?>
                                <div class="obj-charact__line"><span
                                            class="obj-charact__term">Охраняемый:</span><?= $arResult['OBJECT']['GUARDED'] ?>
                                </div>
                            <? endif; ?>
                            <? if ($arResult['OBJECT']['MATERIAL']): ?>
                                <div class="obj-charact__line"><span
                                            class="obj-charact__term">Материал дома:</span><?= $arResult['OBJECT']['MATERIAL'] ?>
                                </div>
                            <? endif; ?>
                            <? if ($arResult['OBJECT']['FACING']): ?>
                                <div class="obj-charact__line"><span
                                            class="obj-charact__term">Отделка:</span><?= $arResult['OBJECT']['FACING'] ?>
                                </div>
                            <? endif; ?>
                            <? if ($arResult['OBJECT']['FLOOR']): ?>
                                <div class="obj-charact__line"><span
                                            class="obj-charact__term">Этажи/уровни:</span><br><?= $arResult['OBJECT']['FLOOR'] ?>
                                </div>
                            <? endif; ?>
                            <? if ($arResult['OBJECT']['COMUNICATIONS']): ?>
                                <div class="obj-charact__line"><span
                                            class="obj-charact__term">Коммуникации:</span><?= $arResult['OBJECT']['COMUNICATIONS'] ?>
                                </div>
                            <? endif; ?>
                        </div>
                        <? if ($arParams['SECTION_CODE'] != 'poselki'): ?>
                            <div class="obj-add-info obj-add-info--visible editable-text">
                                <p><?= $arResult['OBJECT']['PREVIEW_TEXT'] ?></p>
                            </div>
                            <div class="obj-add-info editable-text c-add-text">
                                <p><?= $arResult['OBJECT']['DETAIL_TEXT'] ?></p>
                            </div>
                            <? if (!empty($arResult['OBJECT']['DETAIL_TEXT']) && ($arResult['OBJECT']['PREVIEW_TEXT'] != $arResult['OBJECT']['DETAIL_TEXT'])): ?>
                                <a href="" title="" class="btn btn--green btn--middle c-more-obj-info">подробнее об
                                    объекте</a>
                            <? endif; ?>
                        <? else: ?>
                            <div id="step1" class="c-step"
                                 style="display:<?= (($_GET['SHOW_TABLE'] == 'Y') && ((count($arResult['SALES']['kottedzhi_taunkhausy']) > 0) || (count($arResult['SALES']['uchastki']) > 0))) ? 'none' : 'block' ?>">
                                <div class="obj-add-info obj-add-info--visible editable-text">
                                    <p><?= $arResult['OBJECT']['PREVIEW_TEXT'] ?></p>
                                </div>
                                <div class="obj-add-info editable-text c-add-text">
                                    <p><?= $arResult['OBJECT']['DETAIL_TEXT'] ?></p>
                                </div>
                                <? if (!empty($arResult['OBJECT']['DETAIL_TEXT']) && ($arResult['OBJECT']['PREVIEW_TEXT'] != $arResult['OBJECT']['DETAIL_TEXT'])): ?>
                                    <a href="" title="" class="btn btn--green btn--middle c-more-obj-info">подробнее об
                                        объекте</a>
                                <? endif; ?>

                                <? if ((count($arResult['SALES']['kottedzhi_taunkhausy']) > 0) || (count($arResult['SALES']['uchastki']) > 0)): ?>
                                    <div class="elit-new-actions">
                                        <a href="" title="" data-step="2"
                                           class="btn btn--light-green btn--middle c-step-link">В продаже</a>
                                    </div>
                                <? endif; ?>
                            </div>
                            <? if ((count($arResult['SALES']['kottedzhi_taunkhausy']) > 0) || (count($arResult['SALES']['uchastki']) > 0)): ?>
                                <div id="step2" class="elit-step-2 c-step"
                                     style="display:<?= (($_GET['SHOW_TABLE'] == 'Y') && (count($arResult['SALES']['kottedzhi_taunkhausy']) > 0)) ? 'block' : 'none' ?>">
                                    <? if (count($arResult['SALES']['kottedzhi_taunkhausy']) > 0): ?>
                                        <div class="step-title">Дома в
                                            продаже: <?= count($arResult['SALES']['kottedzhi_taunkhausy']) ?></div>
                                        <div data-step="1" class="step-close c-step-link"><i
                                                    class="sprite-icon close-elit"></i></div>
                                        <div class="elit-table-wrap">
                                            <div class="elit-table">
                                                <div class="elit-table__row elit-table__row--head">
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort active">ID</a>
                                                    </div>
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort">Тип</a></div>
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort">Пло&shy;щадь
                                                            дома</a></div>
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort">Пло&shy;щадь
                                                            участка</a></div>
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort">Цена</a></div>
                                                </div>
                                                <? foreach ($arResult['SALES']['kottedzhi_taunkhausy'] as $kottedg): ?>
                                                    <div data-url="/zagorodnaya-nedvizhimost/kottedzhi_taunkhausy/<?= $kottedg['ID'] ?>"
                                                         class="elit-table__row c-redirect-link">
                                                        <div class="elit-table__cell"><?= $kottedg['ID'] ?></div>
                                                        <div class="elit-table__cell">Дом</div>
                                                        <div class="elit-table__cell"><?= (((float)$kottedg['HOUSE_AREA']) ? $kottedg['HOUSE_AREA'] . ' кв.м.' : "-") ?></div>
                                                        <div class="elit-table__cell"><?= (((float)$kottedg['TERRITORY_AREA']) ? $kottedg['TERRITORY_AREA'] . ' сот.' : "-") ?></div>
                                                        <div class="elit-table__cell"><?= ($kottedg['PRICE_USD']) ? $kottedg['PRICE_USD'] . ' $' : "Цена по запросу" ?></div>
                                                    </div>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if (count($arResult['SALES']['uchastki']) > 0): ?>
                                        <div class="step-title">Участки в
                                            продаже: <?= count($arResult['SALES']['uchastki']) ?></div>
                                        <div data-step="1" class="step-close c-step-link"><i
                                                    class="sprite-icon close-elit"></i></div>
                                        <div class="elit-table-wrap">
                                            <div class="elit-table">
                                                <div class="elit-table__row elit-table__row--head">
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort active">ID</a>
                                                    </div>
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort">Тип</a></div>
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort">Пло&shy;щадь</a>
                                                    </div>
                                                    <div class="elit-table__cell"><a href="" title=""
                                                                                     class="table-sort">Цена</a></div>
                                                </div>
                                                <? foreach ($arResult['SALES']['uchastki'] as $uchastki): ?>
                                                    <div data-url="/zagorodnaya-nedvizhimost/uchastki/<?= $uchastki['ID'] ?>"
                                                         class="elit-table__row c-redirect-link">
                                                        <div class="elit-table__cell"><?= $uchastki['ID'] ?></div>
                                                        <div class="elit-table__cell"><?= (($uchastki['UCHASTOK_TYPE']) ? $uchastki['UCHASTOK_TYPE'] : "-") ?></div>
                                                        <div class="elit-table__cell"><?= (((float)$uchastki['TERRITORY_AREA']) ? $uchastki['TERRITORY_AREA'] . ' сот.' : "-") ?></div>
                                                        <div class="elit-table__cell"><?= ($uchastki['PRICE_USD']) ? $uchastki['PRICE_USD'] . ' $' : "Цена по запросу" ?></div>
                                                    </div>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <a href="" title="" data-step="1" class="btn btn--green btn--middle c-step-link">Вернуться
                                        к описанию</a>
                                </div>
                            <? endif; ?>
                        <? endif; ?>
                        <div class="contacts">
                            <a href="tel:84997552020" class="phone" id="phone_moscow">8 (499) 755-20-20</a>
                            <? $IBlockID = \metrium\helpers\IBlockHelper::getIblockIdByCode($arParams['SECTION_CODE']); ?>
                            <? $APPLICATION->IncludeComponent(
                                "metrium:manager.single",
                                "zagorod",
                                array(
                                    "ELEMENT_ID" => \metrium\helpers\IBlockHelper::getElementIdByElementCode($arParams['ELEMENT_CODE'], $IBlockID),
                                    "SECTION_ID" => false,
                                    "BLOCK_ID" => $IBlockID,
                                    "COMPONENT_TEMPLATE" => "zagorod",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "86400"
                                ),
                                false,
                                array("HIDE_ICONS" => "Y")
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "metrium:subscribe.form",
                                "elite.detail",
                                array(
                                    "COMPONENT_TEMPLATE" => "elite.detail",
                                    "SUBSCRIPTION_RUBRIC" => "4"
                                ),
                                false
                            ); ?>
                        </div>
                    </div>

                    <div class="ya-share2"
                         style="margin-top: -60px; padding: 0 12% 10px; overflow: auto;-webkit-box-sizing: border-box; box-sizing: border-box;"
                         data-services="vkontakte,facebook,odnoklassniki"
                         data-title="<?= $arResult['OBJECT']['NAME'] ?>"
                         data-image="<?= \metrium\System\Site::getDomain() . $arResult['OBJECT']['PREVIEW_PICTURE']['PHOTO'] ?>"
                         data-description="Мне нравится"
                         data-counter="">
                    </div>

                <? else: ?>
                    <div class="elit-info c-mCustomScrollbar">
                        <div class="obj-name">Объект не продается</div>
                    </div>
                <? endif; ?>
            </div>
        </div>


        <? if ($arResult['OBJECT']['ACTIVE'] == 'Y') { ?>
            <? if (count($arResult['RELATED_OBJECTS']) > 0): ?>
                <div class="objects-similar">
                    <div class="page-head">
                        <div class="page-title page-title--elit">Похожие объекты
                            на <?= $arResult['OBJECT']['NAME'] ?></div>
                    </div>
                    <hr>
                    <div class="elit-catalog clearfix">
                        <div class="container container--elit tile__line-content tile__line-content--flex">
                            <? foreach ($arResult['RELATED_OBJECTS'] as $arItem): ?>
                                <a href="../<?= $arItem['ID'] ?>/" class="tile__item">
                                    <div class="tile__item-content">
                                        <div style="background: url(<?= $arItem['PREVIEW_PICTURE'] ?>) no-repeat; background-size: cover"
                                             class="tile__item-img"></div>
                                        <div class="tile__item-info">
                                            <div class="tile__info-content">
                                                <div class="tile__info-name"><?= $arItem['NAME'] ?></div>
                                                <div class="tile__info-address">
                                                    <? if ($arItem['DIRECTION']): ?>
                                                        <?= $arItem['DIRECTION'] ?>
                                                    <? endif; ?>
                                                    <? if ($arItem['REMOTNESS_MKAD'] && $arItem['DIRECTION']): ?>
                                                        ,
                                                    <? endif; ?>
                                                    <? if ($arItem['REMOTNESS_MKAD']): ?>
                                                        <?= $arItem['REMOTNESS_MKAD'] ?> км. от МКАД
                                                    <? endif; ?>
                                                </div>
                                                <div class="tile__info-params">
                                                    <? if ($arItem['HOUSE_AREA']) { ?>
                                                        <div class="tile__info-param"><?= $arItem['HOUSE_AREA'] ?>
                                                            кв.м.
                                                        </div>
                                                    <? } ?>
                                                    <? if ($arItem['TERRITORY_AREA']) { ?>
                                                        <div class="tile__info-param"><?= $arItem['TERRITORY_AREA'] ?>
                                                            соток
                                                        </div>
                                                    <? } ?>
                                                    <? if ($arItem['FACING']) { ?>
                                                        <div class="tile__info-param"><?= $arItem['FACING'] ?></div>
                                                    <? } ?>
                                                </div>
                                                <div class="tile__info-price">
                                                    <?
                                                    unset($hideArendaRub, $hideRub);
                                                    if ($arItem['ARENDA_USD'] == 0 && $arItem['ARENDA_EUR'] == 0) {
                                                        $arItem['ARENDA_USD'] = 'Цена по запросу';
                                                        $hideArendaRub = true;
                                                    } else {
                                                        $arItem['ARENDA_USD'] = $arItem['ARENDA_USD'] . ' $';
                                                        $hideArendaRub = false;
                                                    }

                                                    if ($arItem['PRICE_USD'] == 0 && !isset($arItem['PRICE_EUR'])) {
                                                        $arItem['PRICE_USD'] = 'Цена по запросу';
                                                        $hideRub = true;
                                                    } else {
                                                        $arItem['PRICE_USD'] = $arItem['PRICE_USD'] . ' $';
                                                        $hideRub = false;
                                                    }
                                                    ?>


                                                    <div class="tile__info-price-one">
                                                        <? if ($arItem["HIDE_PRICES"]): ?>
                                                            Цена по запросу
                                                        <? else: ?>
                                                            <? if ($arItem['PRICE_EUR'] != 0): ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda') ? $arItem['ARENDA_EUR'] . ' €' : $arItem['PRICE_EUR'] . ' €' ?>
                                                            <? else: ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda') ? $arItem['ARENDA_USD'] : $arItem['PRICE_USD'] ?>
                                                            <? endif; ?>
                                                        <? endif; ?>
                                                    </div>
                                                    <div class="tile__info-price-two">
                                                        <? if ($arItem["HIDE_PRICES"]): ?>

                                                        <? else: ?>
                                                            <? if ($arItem['PRICE_EUR'] != 0): ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda' && !$hideArendaRub) ? $arItem['ARENDA_RUB'] . ' &#8381;' : '' ?>
                                                                <?= ($arParams['SECTION_CODE'] != 'arenda' && !$hideRub) ? $arItem['PRICE_RUB'] . ' &#8381;' : '' ?>
                                                            <? else: ?>
                                                                <?= ($arParams['SECTION_CODE'] == 'arenda' && !$hideArendaRub) ? $arItem['ARENDA_RUB'] . ' &#8381;' : '' ?>
                                                                <?= ($arParams['SECTION_CODE'] != 'arenda' && !$hideRub) ? $arItem['PRICE_RUB'] . ' &#8381;' : '' ?>
                                                            <? endif; ?>
                                                        <? endif; ?>
                                                    </div>
                                                </div>
                                                <div class="tile__info-id">ID объекта: <?= $arItem['ID'] ?></div>
                                            </div>
                                        </div>

                                        <div class="tile__special-mark c-like" data-obj_id="<?= $arItem['OBJ_ID'] ?>"
                                             data-ajax_url="/local/templates/.default/ajax/like.php"
                                            data-action="">
                                            <span class="tile__icon icon-heart"></span>
                                        </div>
                                    </div>
                                </a>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            <? else: ?>
                <div class="page-head">
                    <div class="page-title page-title--elit">Нет похожих объектов</div>
                </div>
                <hr>
            <? endif; ?>
        <? } ?>


    </div>
</div>
<div class="clear"></div><!-- BEGIN footer -->

<?php if ($arResult['OBJECT']['LATITUDE'] && $arResult['OBJECT']['LONGITUDE']) { ?>
    <script type="text/javascript">
        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [<?= $arResult['OBJECT']['LATITUDE'] ?>, <?= $arResult['OBJECT']['LONGITUDE'] ?>],
                    zoom: 12,
                    controls: []
                }),

                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: '<?= $arResult['OBJECT']['NAME'] ?>',
                    balloonContent: '<?= $arResult['OBJECT']['NAME'] ?><br />Направление: <?=$arResult['OBJECT']['DIRECTION']?>,  <?=$arResult['OBJECT']['REMOTNESS_MKAD']?> км. от МКАД'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '/local/templates/.default/images/map-icon.png',
                    iconImageSize: [30, 42],
                    iconImageOffset: [-5, -38]
                });

            myMap.geoObjects
                .add(myPlacemark);
        });

        ymaps.ready(function () {
            var myMap = new ymaps.Map('satellit', {
                    center: [<?= $arResult['OBJECT']['LATITUDE'] ?>, <?= $arResult['OBJECT']['LONGITUDE'] ?>],
                    zoom: 15,
                    controls: []
                }),

                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: '<?= $arResult['OBJECT']['NAME'] ?>',
                    balloonContent: '<?= $arResult['OBJECT']['NAME'] ?><br />Направление: <?=$arResult['OBJECT']['DIRECTION']?>,  <?=$arResult['OBJECT']['REMOTNESS_MKAD']?> км. от МКАД'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '/local/templates/.default/images/map-icon.png',
                    iconImageSize: [30, 42],
                    iconImageOffset: [-5, -38]
                });

            myMap.geoObjects
                .add(myPlacemark);
            myMap.setType('yandex#satellite');
        });
    </script>
<?php } ?>

<footer class="footer">
    <div class="description">
        <div class="description__content">
            <div class="description__text clearfix">
                <div class="arrow-top c-arrow-top"><span class="icon-arrow-up2"></span></div>
            </div>
        </div>
    </div>
