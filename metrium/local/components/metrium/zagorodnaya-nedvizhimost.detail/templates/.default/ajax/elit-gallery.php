<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if (!CModule::IncludeModule("iblock")) {
	die();
}?>

<?

$dbRes = CIBlockElement::GetList(
	array(),
	array(
		'IBLOCK_ID' => $_REQUEST['IBLOCK_ID'],
		'CODE' => $_REQUEST['ELEMENT_CODE'],
		'ACTIVE' => 'Y',
	),
	false,
	false,
	array(
		'ID',
		'NAME',
		'PREVIEW_PICTURE',
		'PREVIEW_TEXT',
		'PROPERTY_ADDRESS',
		'CODE',
		'PROPERTY_PHOTOS'
	)
);

$object = $dbRes->Fetch();

$dbRes = CIBlockElement::GetProperty(
	$_REQUEST['IBLOCK_ID'],
	$object['ID'],
	array(),
	array(
		'CODE'=>'PHOTOS'
	)
);

while($arProperties = $dbRes->GetNext()){
    $arFile =  CFile::GetFileArray( $arProperties['VALUE']); // Получить размеры исходного изображения
    $ar = CFile::ResizeImageGet(
        $arProperties['VALUE'],
        array('width' => $arFile['WIDTH'], 'height' => $arFile['HEIGHT']),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        false,
        \metrium\EstateObject::getWaterMarkParams()
    );

	$arPhotos[] = $ar['src'];
}
?>



<div data-nav="thumbs" data-navwidth="80%" data-arrows="always" data-navposition="bottom" data-width="100%" data-click="true" data-height="600" data-fit="contain" data-thumbheight="70" data-thumbwidth="115" data-thumbmargin="5" data-loop="true" class="fotorama fotorama--with-arrows">
	<?foreach($arPhotos as $photo) :?>
		<a href="<?=$photo?>" title="">
			<img src="<?=$photo?>" alt="">
		</a>
	<?endforeach;?>
</div>




<script type="text/javascript">
	$(function () {
		$('.popup-fotorama').css('width', $(window).width()*0.9);

		popupModule.callOnLoadFunction = function () {
			setTimeout(function () {
				sliderInit($('.c-custom-slider').find('.fotorama').data('fotorama').activeIndex);
				$('.popup-fotorama').css('visibility', 'visible');
				$(window).scrollTop(250);
			}, 500);
		}
	});

	function sliderInit(index){
		index = index || 0;
		$('.fotorama').fotorama({
			startindex: index
		});
	}
</script>