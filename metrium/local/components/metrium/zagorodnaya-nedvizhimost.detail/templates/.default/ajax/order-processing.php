<?php
use \metrium\helpers\UserHelper;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule("iblock")){
    die('Error including module iblock');
}

$request = Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest()->toArray();

foreach($request as $type) {
    foreach ($type as $field) {
        $fields[$field['name']] = $field['value'];
    }
}

$el = new CIBlockElement;

$dbRes = $el->GetByID($fields['object_id']);

if($res = $dbRes->Fetch())
{
    $object = $res;
}
else
{
    die(json_encode(array('status' => 'error', 'mess' => 'Не удалось отправить сообщение: объект не найден')));
}

$types = array(
    65 => 'Коттеджи и таунхаусы',
    66 => 'Поселки',
    67 => 'Участки'
);

$arProps = array(
    'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode("orders_on_objects"),
    'IBLOCK_SECTION_ID' => 125,
    'NAME' => 'Заявка на объект '.$object['NAME'],
    'PROPERTY_VALUES' => array(
        'OBJECT_NAME' => $object['NAME'],
        'OBJECT_ID' => $object['ID'],
        'OBJECT_TYPE' => 'Элитная загородная недвижимость. '.$types[$object['IBLOCK_ID']],
        'ORDER_DATE' => date('d.m.Y'),
        'SENDER_NAME' => $fields['name'],
        'SENDER_PHONE' => $fields['phone'],
        'SENDER_EMAIL' => $fields['email'],
        'MESSAGE' => array(
            'VALUE' => array(
                'TYPE' => 'text',
                'TEXT' => $fields['message']
            )
        )
    )
);

$result = $el->Add($arProps);

if($result != false)
{
    $dbRes = $el->GetList(
        array(),
        array(
            'IBLOCK_CODE' => 'managers-emails',
            'PROPERTY_ORDER_SECTION_ID' => 125
        ),
        false,
        false,
        array(
            'IBLOCK_ID',
            'ID',
            'PROPERTY_EMAIL'
        )
    );

    while($account = $dbRes->Fetch())
    {
        if(!empty($account['PROPERTY_EMAIL_VALUE']))
        {
            $accounts[] = $account['PROPERTY_EMAIL_VALUE'];
        }
    }

    /* Выборка пользователей и проверка на привязку к текущему объекту */
    $by = "id";
    $order = "desc";
    $obUsers = CUser::GetList(
        $by,
        $order,
        array("GROUPS_ID" => UserHelper::getGroupIDbyCode("managers"), "ACTIVE" => "Y"),
        array("FIELDS" => array("EMAIL", "NAME"), "SELECT" => array("UF_MANAGER_ELEMENTS"))
    );

    while($arUser = $obUsers->GetNext()) { // Добавляем email менеджера привязанного к объекту

        if (!empty($arUser["UF_MANAGER_ELEMENTS"]) && in_array($fields["object_id"], $arUser["UF_MANAGER_ELEMENTS"])) {
            $accounts[] = $arUser["EMAIL"];
        }
    }

    $arFields = array(
        'OBJECT_NAME' => $object['NAME'],
        'URL' => 'http://www.metrium.ru/zagorodnaya-nedvizhimost/'.$object['SECTION'].'/'.$object['CODE'].'/',
        'EMAIL_TO' => implode(',', $accounts),
        'SENDER_NAME' => $fields['name'],
        'SENDER_PHONE' => $fields['phone'],
        'SENDER_EMAIL' => $fields['email'],
        'OBJECT_TYPE' => 'Элитная загородная недвижимость. '.$types[$object['IBLOCK_ID']],
        'MESSAGE' => $fields['message'],
    );

    CEvent::Send('ORDER_ON_OBJECT', 's1', $arFields);
    die(json_encode(array('status' => 'success', 'mess' => 'Сообщение успешно отправлено!')));
}
else
{
    die(json_encode(array('status' => 'error', 'mess' => 'Не удалось отправить сообщение')));
}