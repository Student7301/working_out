$( document ).ready(function() {
    $('body').on('click', '.c-map-link', function (e) {
        $('#map').css({'display':'block'});
        $('#satellit').css({'display':'none'});
        $('.c-custom-slider').css({'display':'none'});

        $('.map-button').css({'display':'none'});
        $('.mapsat-button').css({'display':'block'});
        $('.foto-button').css({'display':'block'});
    });

    $('body').on('click', '.c-mapsat-link', function (e) {
        $('#map').css({'display':'none'});
        $('#satellit').css({'display':'block'});
        $('.c-elite-fotorama').css({'display':'none'});

        $('.map-button').css({'display':'block'});
        $('.mapsat-button').css({'display':'none'});
        $('.foto-button').css({'display':'block'});
    });

    $('body').on('click', '.foto-button', function (e) {
        $('#map').css({'display':'none'});
        $('#satellit').css({'display':'none'});
        $('.c-elite-fotorama').css({'display':'block'});

        $('.map-button').css({'display':'block'});
        $('.mapsat-button').css({'display':'block'});
        $('.foto-button').css({'display':'none'});
    });
});
