<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
use metrium\System\Site;
$domain = Site::getDomain();
Asset::getInstance()->addJs("//yastatic.net/es5-shims/0.0.2/es5-shims.min.js");
Asset::getInstance()->addJs("//yastatic.net/share2/share.js");
Asset::getInstance()->addString("<meta property=\"og:title\" content=\"{$arResult['OBJECT']['NAME']}\"/>");
Asset::getInstance()->addString("<meta property='og:description' content='Мне нравится'/>");
Asset::getInstance()->addString("<meta property=\"og:image\" content=\"{$domain}{$arResult['OBJECT']['PREVIEW_PICTURE']['PHOTO']}\">");
Asset::getInstance()->addString("<meta property=\"og:type\" content=\"website\">");
Asset::getInstance()->addString("<meta property=\"og:url\" content=\"{$domain}{$arResult['OBJECT']['DETAIL_PAGE_URL']}\">");
use \metrium\Likes;
?>

<script type="text/javascript">
    $(document).ready(function () {
        var likesCount  = <?=($USER->IsAuthorized()) ? Likes::getLikesCountByUserId($USER->GetID()) : Likes::getCookieLikesCount();?>,
            likes       = <?=json_encode(Likes::getLikes());?>,
            likeBlock   = $('.elit-detail .elit-table .c-like'),
            objectId    = likeBlock.data('obj_id').toString();

        $('.choose.right .choose__count').text(likesCount); // Подменяем кол-во лайков

        /* Добавляем / удаляем лайк у объекта */
        if ((likes.indexOf(objectId) != -1 || likes.indexOf(parseInt(objectId)) != -1) && !likeBlock.hasClass('active')) {
            likeBlock.addClass('active');
            likeBlock.attr('data-action', 'dislike');
        } else {
            likeBlock.attr('data-action', 'like');
        }

        /* Устанавливаем лайки для похожих объектов */
        $('.container--elit .c-like').each(function (i, e) {
            var relatedObjectId = $(e).data('obj_id').toString();

            /* Добавляем / удаляем активность у объектов */
            if ((likes.indexOf(relatedObjectId) != -1 || likes.indexOf(parseInt(relatedObjectId)) != -1) && !$(e).hasClass('active')) {
                $(e).addClass('active');
                $(e).attr('data-action', 'dislike');
            } else {
                $(e).attr('data-action', 'like');
            }
        });
    });
</script>