<!DOCTYPE html>
<html style="padding: 0; margin: 0;">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Metrium - риелтор нового поколения</title>
</head>
<style>
    #header {
        width: 100%;
        text-align: center;
    }
    #header div.info {
        text-align: left;
        position: relative;
        display: inline-block;
        background-color: #ececec;
        width: 450px;
        height: 80px;
        box-shadow: 1px 1px 3px 0 rgba(50, 50, 50, 0.3) inset;
    }
    .info > p.phone {
        font-size: 26px;
        display: block;
        font-weight: 300;
        margin: 10px 0 0 70px;
    }
    .info > p {
        margin: 0 0 0 70px;
    }
    #header .logo {
        position: relative;
        display: inline-block;
        z-index: 500;
        width: 240px;
        height: auto;
        line-height: 105px;
        background-color: #f5f2f1;
        box-shadow: -8px 8px 15px 0 rgba(50, 50, 50, 0.3);
    }
    #header .logo img, #header div {
        vertical-align: middle;
    }
    #object-photo {
        text-align: center;
        margin: 30px auto;
        width: 80%;
    }
    #object-photo div.photo {
        position: relative;
        display: inline-block;
        margin: 0 0 0 15%;
        padding: 0;
        width: 70%;
        float: left;
    }
    #object-photo div.photo img.main-img {
        width: 100%;
        display: block;
        min-height: 400px;
    }
    #object-photo span.photo-info {
        position: absolute;
        bottom: 30px;
        left: 0;
        width: 457px;
        height: 96px;
        background-color: #fbfbfb;
        text-align: left;
        padding-left: 40px;
    }
    #object-photo img.shadow {
        display: block;
        margin-left: -8px;
        width: 100%;
    }
    #object-photo .object_id {
        display: inline-block;
        width: 15%;
        height: 125px;
        background-color: #fbfbfb;
        box-shadow: 2px 2px 4px 0px rgba(50, 50, 50, 0.4);
        margin: 0;
        padding: 0;
    }
    #object-text {
        margin: 30px auto;
        width: 60%;
        font-size: 21px;
        color: #4d4d4d;
    }
</style>
<body style="width: 1180px; background-color: #fff; margin: 0 auto; font-family: Arial, sans-serif; padding: 45px 30px;">
<div id="header" style="top: 0;">
    <div class="info" style="margin-right: -5px">
        <p class="phone">8 (499) 270-20-20</p><p>для звонков из Москвы</p>
    </div>
    <div class="logo">
        <img src="/local/templates/.default/images/logo_elite.png">
    </div>
    <div class="info" style="margin-left: -5px">
        <p class="phone">8 (800) 770-00-47</p><p>бесплатный звонок из России</p>
    </div>
</div>
<div class="body">
    <div id="object-photo">
        <div class="photo">
            <img class="main-img" src="<?=$arResult['OBJECT']['PREVIEW_PICTURE']?>">
                    <span class="photo-info">
                        <span style="display: block; text-transform: uppercase;font-size: 17px; color: #000401; font-weight: bold; padding-top: 28px;"><?=$arResult['OBJECT']['NAME']?></span>
                        <span style="display: block; font-size: 15px; color: #898989; margin-top: 7px;">
                            Направление: <?=$arResult['OBJECT']['DIRECTION']?>, <?=$arResult['OBJECT']['REMOTNESS_MKAD']?> км. от МКАД
                        </span>
                    </span>
            <img class="shadow" src="/local/templates/.default/images/elite_shadow.png">
        </div>
        <div class="object_id">
            <span style="display: block; font-size: 20px; padding: 19px 0 0;">ID ОБЪЕКТА</span>
            <span style="display: block; font-size: 40px; color: #00c29c;"><?=$arResult['OBJECT']['CODE']?></span>
        </div>
    </div>
    <div style="clear: both"></div>

    <div id="object-text">
        <?if (isset($arResult['OBJECT']['HOUSE_AREA'])):?>
            <p style="margin: 8px 0;"><b>Площадь дома:</b> <?=$arResult['OBJECT']['HOUSE_AREA']?> кв. м</p>
        <?endif?>
        <p style="margin: 8px 0;"><b>Площадь участка:</b> <?=$arResult['OBJECT']['TERRITORY_AREA']?> соток</p>
        <?if (isset($arResult['OBJECT']['GUARDED'])):?>
            <p style="margin: 8px 0;"><b>Охраняемый:</b> <?=$arResult['OBJECT']['GUARDED']?></p>
        <?endif?>
        <?if (isset($arResult['OBJECT']['MATERIAL'])):?>
            <p style="margin: 8px 0;"><b>Материал дома:</b> <?=$arResult['OBJECT']['MATERIAL']?></p>
        <?endif?>
        <?if (isset($arResult['OBJECT']['FLOOR'])):?>
            <p style="margin: 8px 0;"><b>Этажи/уровни:</b> <?=$arResult['OBJECT']['FLOOR']?></p>
        <?endif?>
    </div>
</div>
<div class="map">
    <p style="position: relative; margin: 25px 0 0; padding: 0; min-height: 50px">
        <?if(isset($arResult['OBJECT']['LONGITUDE']) && isset($arResult['OBJECT']['LATITUDE'])):?>
            <img style="display: block;margin: 0 auto;" src="https://static-maps.yandex.ru/1.x/?ll=<?=$arResult['OBJECT']['LONGITUDE']?>,<?=$arResult['OBJECT']['LATITUDE']?>&size=649,320&z=13&l=map&pt=<?=$arResult['OBJECT']['LONGITUDE']?>,<?=$arResult['OBJECT']['LATITUDE']?>,org">
        <?endif;?>
        <span style="position: absolute;  bottom: -34px; left: 264px; width: 293px; height: 83px; background-color: #ececec; text-align: left; padding-left: 40px; box-shadow: inset 0px 0px 4px 0px rgba(50, 50, 50, 0.3), 0px 8px 13px -5px rgba(50, 50, 50, 0.75);">
                <span style="display: block; text-transform: uppercase;font-size: 24px; color: #3a3d46; font-weight: bold; padding-top: 28px;">
                    <?if ($arResult["OBJECT"]["HIDE_PRICES"]):?>
                        Цена по запросу
                    <?elseif ($arResult['OBJECT']['PRICE_EUR'] != 0):?>
                        <?=$arResult['OBJECT']['PRICE_EUR'] . " €"?>
                    <?else:?>
                        <?=($arResult['OBJECT']['PRICE_USD'] == 0)?'Цена по запросу':$arResult['OBJECT']['PRICE_USD'].' $'?>
                    <?endif;?>
                </span>
            </span>
    </p>
    <?if(isset($arResult['OBJECT']['LONGITUDE']) && isset($arResult['OBJECT']['LATITUDE'])):?>
        <img style="display: block; margin-left: 255px;" src="/local/templates/.default/images/elite_shadow.png">
    <?endif;?>
</div>

<footer style="bottom: 0; margin-top: 80px">
    <div style="padding: 25px 0 27px; background: #ececec;  box-shadow: inset 0px 0px 1px 0px rgba(50, 50, 50, 0.3); font-size: 26px; text-align: center; vertical-align: top; width: 100%;">
        <?=(isset($arResult['OBJECT']['ADDRESS']) ? $arResult['OBJECT']['ADDRESS']:"Направление: " . $arResult['OBJECT']['DIRECTION'] . ", " . $arResult['OBJECT']['REMOTNESS_MKAD'] . " км. от МКАД")?>
    </div>
</footer>

</body>
</html>