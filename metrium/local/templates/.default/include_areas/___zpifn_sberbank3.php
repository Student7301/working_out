<!-- BEGIN main-part -->
<div class="main-part right">
    <div class="building-detail clearfix">
        <div class="main-content joint_venture">
            <h1 class="main-title">ЗПИФН «Сбербанк - Жилая недвижимость 3»</h1>
            <p><b>Уважаемые господа!</b></p>
            <p>В период 16.10.2015 – 16.11.2015 проводится дополнительная выдача инвестиционных паев нового фонда «Сбербанк – Жилая недвижимость 3»:</p>
            <p><b>Минимальная сумма инвестирования: 350 000 рублей</b></p>
            <p>Данный фонд, запущенный управляющей компанией «Сбербанк Управление Активами», уже третий по счету фонд, ориентированный на инвестиции в строящуюся жилую недвижимость Москвы и Московской области.</p>
            <p><b>Цель инвестирования</b></p>
            <p>Целью фонда является получение дохода от реализации квартир, приобретенных на ранней стадии строительства, сформировавшегося за счет развития проекта и дисконта за оптовое приобретение.</p>
            <p><b>Инвестиционная стратегия</b></p>
            <p>Инвестиционной стратегией фонда является приобретение прав на объекты жилой недвижимости (квартиры и апартаменты) по договорам долевого участия в строительстве с последующей их продажей на более поздней стадии строительства по максимально высокой цене. Приобретение квартир осуществляется по ДДУ в соответствии с Федеральным законом № 214-ФЗ.</p>
            <p><b>Факторы инвестиционной привлекательности фонда:</b></p>
                <ul>
                    <li>прозрачные условия инвестирования.</li>
                    <li>выплата инвестиционного дохода 2 раза в год.</li>
                </ul>
            <p><b>Инвестиционный горизонт – 08 апреля 2019 г.</b></p>
            <div class="information_funds">
                <p>Закрытое акционерное общество «Сбербанк Управление Активами» зарегистрировано Московской регистрационной палатой 1 апреля 1996 года. Лицензия ФСФР России на осуществление деятельности по управлению инвестиционными фондами, паевыми инвестиционными фондами и негосударственными пенсионными фондами №21-000-1-00010 от 12 сентября 1996 года. Лицензия ФСФР России №177-06044-001000 от 7 июня 2002 года на осуществление деятельности по управлению ценными бумагами.</p>
                <p>Получить подробную информацию о паевых инвестиционных фондах, ознакомиться с правилами доверительного управления и иными документами, подлежащими раскрытию и предоставлению в соответствии с действующим законодательством, можно в АО «Сбербанк Управление Активами» по адресу: 123317, г. Москва, Пресненская набережная, дом 10, на сайте <a href="http://www.sberbank-am.ru" target="_blank">www.sberbank-am.ru</a>, по телефону (495) 258 05 34. Информация, подлежащая опубликованию в печатном издании, публикуется в «Приложении к Вестнику Федеральной службы по финансовым рынкам». Стоимость инвестиционных паев может увеличиваться и уменьшаться, результаты инвестирования в прошлом не определяют доходы в будущем, государство не гарантирует доходность инвестиций в инвестиционные фонды.</p>
                <p>Прежде чем приобрести инвестиционный пай, следует внимательно ознакомиться с правилами доверительного управления паевым фондом. ЗПИФ недвижимости «Сбербанк – Жилая недвижимость 3» - правила доверительного управления фондом зарегистрированы Банком России 27 августа 2015 года за номером 3030.</p>
            </div>
            <p><a href="/funds/sberbank-zhilaya-nedvizhimost-3/">Информация для девелоперов</a></p>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END main-part -->