<!-- BEGIN main-part -->
<div class="main-part right">
    <div class="building-detail clearfix">
        <div class="main-content faq">
            <div class="main-title">Офисы продаж под ключ</div>
            <div class="investment--office">
                <p>Идеология «Метриум Групп» базируется на обеспечении высокого уровня комфорта клиентам в независимости от того, где они предпочитают совершить сделку – в центральном офисе компании, или же в офисе продаж на объекте/строительной площадке.</p>
                <p>Центральный офис продаж «Метриум Групп» расположен на 35-ом этаже бизнес центра «Nordstar Tower» и призван обеспечить клиентам первоклассный сервис полного цикла, вплоть до внесения оплаты через кассовый узел, расположенный в клиентской части офиса. </p>
                <div class="blocks_about">
                    <div class="about__block about__block--office about__block--m">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118415" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office4.jpg"></div>
                            <div class="link-office">Центральный офис</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="investment--office">
                <p>Расположение, технология строительства, планировка и другие параметры офиса напрямую зависят от того, каким образом организованы бизнес-процессы по реализации того или иного Жилого комплекса. </p>
                <p>Офис полного цикла» дает возможность клиентам не только познакомиться с объектом, выбрать квартиру или апартаменты, но и посмотреть варианты планировочных решений и отделки в демонстрационных залах и квартирах, а также совершить сделку, оформить ипотечный кредит, получить ключи. Офис полного цикла может включать в себя бэк-офис для сотрудников всего департамента продаж: специалистов CRM, юристов, рекламных менеджеров и другого персонала. </p>
                <p>Или же, офис может быть меньшей площади и включать только рабочие места менеджеров, основная функция которых – предоставить покупателю первичную консультацию по объекту, в то время как подписание всех договоров проходит в центральном офисе Метриум Групп. </p>
                <p>Какое решение является оптимальным для каждого конкретного случая – определяется специалистами «Метриум Групп» совместно с застройщиком, исходя из схемы реализации, конкурентных преимуществ, реализуемых метров и  других базовых параметров. </p>
            </div>
            <div class="solutions">
                <p>Разработанные «Метриум групп» решения: </p>
            </div>
            <div class="investment--office">
                <b>1.	Капитальное строение – срок строительства 5-6 месяцев.</b>
                <p>Офис на плитном фундаменте или фундаменте на буронабивных сваях, конструктив – клееная конструкционная балка, бетон. Автономные системы инженерии. </p>
                <div class="blocks_about">
                    <div class="about__block about__block--office about__block--mitems"><a href="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/pdf/zarechnaya.pdf" target="_blank" title="" class="absolute-link c-accordion_link_trigger"></a>
                        <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office15.jpg"></div>
                        <div class="link-office">Офис продаж на Заречной</div>
                    </div>
                    <div class="about__block about__block--office about__block--mitems"><a href="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/pdf/yasniy.pdf" target="_blank" title="" class="absolute-link c-accordion_link_trigger"></a>
                        <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office16.jpg"></div>
                        <div class="link-office">Офис продаж Ясный</div>
                    </div>
                    <!--<div class="about__block about__block--office about__block--mitems"><a href="<?/*$_SERVER['DOCUMENT_ROOT']*/?>/developeram/office_prodazh/pdf/carskayaploshad.pdf" target="_blank" title="" class="absolute-link c-accordion_link_trigger"></a>
                        <div class="img-container"><img src="<?/*= \metrium\System\SiteTemplate::getDefault() */?>/images/content/office17.jpg"></div>
                        <div class="link-office">Офис продаж Царская площадь</div>
                    </div>-->
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="investment--office">
                <b>2.	Быстровозводимая каркасная конструкция – срок строительства 4 месяца.</b>
                <p>Офис на легком фундаменте из винтовых свай, конструктив – каркас, фасад –металл/термодерево/стекло, а также более стандартные решения фасада как HPL панели, керамика и пр. Здание комплектуется полным перечнем инженерных систем: вентиляция, кондиционирование, отопление, автономная система водообеспечения и канализирования, силовые и слаботочные сети, пожарная сигнализация, интернет и пр. </p>
                <div class="blocks_about">

                    <div class="about__block about__block--office about__block--mitems">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118416" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office7.jpg"></div>
                            <div class="link-office">Английская Миля</div>
                        </div>
                    </div>
                    <div class="about__block about__block--office about__block--mitems">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118417" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office8.jpg"></div>
                            <div class="link-office">Болтино</div>
                        </div>
                    </div>
                    <div class="about__block about__block--office about__block--mitems">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118418" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office9.jpg"></div>
                            <div class="link-office">Борисоглебское</div>
                        </div>
                    </div>
                    <div class="about__block about__block--office about__block--s">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118419" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office10.jpg"></div>
                            <div class="link-office">Ривер Парк</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="investment--office">
                <b>3.	Офис в жилых/коммерческих площадях строящегося жилого комплекса, срок реализации около 2 месяцев: </b>
                <p>В том случае, когда стадия готовности проекта высокая, мы предлагаем вариант обустройства маркетингового офиса на перовом этаже жилого здания, плюс организацию шоу-румов в квартирах. </p>
                <div class="blocks_about">
                    <div class="about__block about__block--office about__block--mitems">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118420" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office11.jpg"></div>
                            <div class="link-office">Сампо</div>
                        </div>
                    </div>
                    <!--<div class="about__block about__block--office about__block--mitems">
                        <div data-url-popup="<?/*$_SERVER['DOCUMENT_ROOT']*/?>/developeram/office_prodazh/ajax/slider.php?ID=118421" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?/*= \metrium\System\SiteTemplate::getDefault() */?>/images/content/office12.jpg"></div>
                            <div class="link-office">Новые Ватутинки</div>
                        </div>
                    </div>-->
                    <div class="about__block about__block--office about__block--mitems">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118422" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office13.jpg"></div>
                            <div class="link-office">Стрешнево</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="investment--office">
                <b>4.	Временное решение. Мобильные офисы, строк реализации около 2 календарных дней: </b>
                <p>Небольшие павильоны 20-40м2, выполненные на основе металлического каркаса. Это решение подходит для небольших проектов и как временное, на период строительства основного офиса. В павильоне может быть организовано до 4 рабочих мест, можно расположить макет, офис может быть оснащен WC.  </p>
                <div class="blocks_about">
                    <div class="about__block about__block--office about__block--mitems">
                        <div data-url-popup="<?$_SERVER['DOCUMENT_ROOT']?>/developeram/office_prodazh/ajax/slider.php?ID=118423" class="zoom-block--office c-popup-link">
                            <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office14.jpg"></div>
                            <div class="link-office">Офисный павильон</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END main-part -->