<!-- BEGIN main-part -->
<div class="main-part right">
    <div class="building-detail clearfix">
        <div class="main-content faq">
            <h1 class="main-title">Контакты</h1>
            <div class="investment">
                <p><div>Адрес центрального офиса:</div>125284, Москва, ул. Беговая, 3, БЦ “Нордстар Тауэр”, этаж 35.</p>
                <p id="phone_moscow"><div>Контактный телефон:</div>+7 (499) 755-20-20</p>
                <p><div>Электронная почта:</div>info@metrium.ru</p>
            </div>
            <div class="blocks_about clearfix">
                <div class="about__block about__block--office">
                    <div class="img-container"><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/contacs-office.jpg">
                        <div data-url-popup="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/contacs-office.jpg" data-type-popup="image" class="fullscreen_img c-popup-link">
                            <div class="sprite-icon fullscreen_small"></div>
                        </div>
                    </div>
                    <div class="link-office">Центральный офис</div>
                </div>
            </div>
            <div class="main-title">Схема расположения <br> центрального офиса</div><img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/contacts_map.jpg" class="contacts-map">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END main-part -->