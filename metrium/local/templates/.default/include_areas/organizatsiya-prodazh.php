<!-- BEGIN main-part -->
<div class="main-part right">
	<div class="building-detail clearfix">
		<div class="main-content faq">
			<div class="main-title">Организация продаж недвижимости</div>
			<div class="accordion c-accordion accordion--green accordion--double">
				<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
					<div class="accordion__link-text">
						1. Подготовка к реализации
					</div>
					<div class="accordion-icon bottom-shadow bottom-shadow--small">
						<i class="sprite-icon left-arrow"></i>
					</div>
				</div>
				<div class="accordion__dropdown c-accordion-drop">
					<div class="accordion c-accordion accordion--double accordion--child">
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Экспертиза проектов
							</div>
						</div>
						<div class="accordion__dropdown c-accordion-drop">
							<div class="information_dropdown">
								<ul class="information_dropdown__list">
									<li class="information_dropdown__item"><span class="default-text">Экспертиза планировочных решений</span></li>
									<li class="information_dropdown__item"><span class="default-text">Анализ квартирографии по типологии и балансу</span></li>
									<li class="information_dropdown__item"><span class="default-text">Мониторинг конкурентных объектов, анализ рыночной ситуации</span></li>
									<li class="information_dropdown__item"><span class="default-text">SWOT-анализ проекта</span></li>
								</ul>
							</div>
						</div>
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Стратегия реализации. Ценообразование
							</div>
						</div>
						<div class="accordion__dropdown c-accordion-drop">
							<div class="information_dropdown">
								<ul class="information_dropdown__list">
									<li class="information_dropdown__item"><span class="default-text">Формирование ценовой политики проекта на весь период реализации</span></li>
									<li class="information_dropdown__item"><span class="default-text">Дифференцированная расценка, формирование прайс-листа проекта</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка оптимальных условий реализации (скидки, акции, рассрочки)</span></li>
								</ul>
							</div>
						</div>
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Креативная концепция проекта: подготовка технического задания, поиск подрядчиков, проведение тендеров
							</div>
						</div>
						<div class="accordion__dropdown c-accordion-drop">
							<div class="information_dropdown">
								<ul class="information_dropdown__list">
									<li class="information_dropdown__item"><span class="default-text">Нейминг</span></li>
									<li class="information_dropdown__item"><span class="default-text">Визуализация бренда</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка фирменного стиля</span></li>
									<li class="information_dropdown__item"><span class="default-text">Визуализация объекта</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка сайта проекта</span></li>
									<li class="information_dropdown__item"><span class="default-text">Изготовление макета</span></li>
									<li class="information_dropdown__item"><span class="default-text">Офис продаж на объекте: изготовление, внутреннее и внешнее оформление</span></li>
									<li class="information_dropdown__item"><span class="default-text">Обустройство въездной группы</span></li>
									<li class="information_dropdown__item"><span class="default-text">Оформление объекта рекламными материалами</span></li>
									<li class="information_dropdown__item"><span class="default-text">Навигация к месту застройки</span></li>
								</ul>
							</div>
						</div>
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Продвижение проекта
							</div>
						</div>
						<div class="accordion__dropdown c-accordion-drop">
							<div class="information_dropdown">
								<ul class="information_dropdown__list">
									<li class="information_dropdown__item"><span class="default-text">Выбор наиболее эффективных каналов коммуникации: реклама, PR, управление социальными сетями и блогами, promo- и seo- продвижение, клиентские и партнерские мероприятия и т.д.</span></li>
									<li class="information_dropdown__item"><span class="default-text">Бюджетирование программы продвижения</span></li>
									<li class="information_dropdown__item"><span class="default-text">Планирование и проведение рекламной кампании</span></li>
									<li class="information_dropdown__item"><span class="default-text">PR продвижение проекта</span></li>
								</ul>
							</div>
						</div>
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Юридическое сопровождение
							</div>
						</div>
						<div class="accordion__dropdown c-accordion-drop">
							<div class="information_dropdown">
								<ul class="information_dropdown__list">
									<li class="information_dropdown__item"><span class="default-text">Юридическая экспертиза правоустанавливающей документации</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка финансово-юридической схемы реализации</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка проектной декларации</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка форм договоров приобретения с учетом требований банков и регистрирующих органов</span></li>
									<li class="information_dropdown__item"><span class="default-text">Организация «Открытия адреса»: консультирование застройщика, экспертиза пакета документов для подачи на регистрацию</span></li>
								</ul>
							</div>
						</div>
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Ипотечное кредитование
							</div>
						</div>
						<div class="accordion__dropdown c-accordion-drop">
							<div class="information_dropdown">
								<ul class="information_dropdown__list">
									<li class="information_dropdown__item"><span class="default-text">Проведение переговоров с банками-партнерами </span></li>
									<li class="information_dropdown__item"><span class="default-text">Консультирование застройщика по вопросам ипотечного кредитования</span></li>
									<li class="information_dropdown__item"><span class="default-text">Формирование пакета документов для аккредитации объекта, передача пакета в банки</span></li>
									<li class="information_dropdown__item"><span class="default-text">Контроль процесса и сроков аккредитации объекта в банках</span></li>
									<li class="information_dropdown__item"><span class="default-text">Согласование с банками формы договора приобретения</span></li>
									<li class="information_dropdown__item"><span class="default-text">Согласование условий кредитования, преференций</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка схемы работы с банками и логистики документооборота по ипотечным сделкам</span></li>
									<li class="information_dropdown__item"><span class="default-text">Разработка совместных с банками рекламных мероприятий (выставки, семинары, презентации объектов в банках, рекламные буклеты и т.д.)</span></li>
								</ul>
							</div>
						</div>
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Организация CRM системы, включающей блоки продаж, аналитики и консалтинга, ипотеки, регистрации и оформления
							</div>
						</div>
						<div class="accordion__dropdown c-accordion-drop">
							<div class="information_dropdown">
								<ul class="information_dropdown__list">
									<li class="information_dropdown__item"><span class="default-text">Анализ бизнес-процесса застройщика</span></li>
									<li class="information_dropdown__item"><span class="default-text">Модификация разработанного решения на базе Microsoft Dynamics CRM под нужды застройщика</span></li>
									<li class="information_dropdown__item"><span class="default-text">Внедрение разработанного решения на сервер застройщика</span></li>
									<li class="information_dropdown__item"><span class="default-text">Покупка пользовательских и серверных лицензий</span></li>
									<li class="information_dropdown__item"><span class="default-text">Обучение пользователей работе с решением</span></li>
								</ul>
							</div>
						</div>
						<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
							<div class="accordion__link-text">
								Подбор и обучение сотрудников офиса продаж
							</div>
						</div>
					</div>
				</div>
				<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
					<div class="accordion__link-text">
						2. Реализация
					</div>
					<div class="accordion-icon bottom-shadow bottom-shadow--small">
						<i class="sprite-icon left-arrow"></i>
					</div>
				</div>
				<div class="accordion__dropdown c-accordion-drop">
					<div class="accordion c-accordion accordion--double accordion--child">
						<div class="accordion__item">
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Прием и фиксация звонков и источников рекламы, а также обращений в офис продаж на объекте
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Консультации покупателей
								</div>
							</div>
							<div class="accordion__dropdown c-accordion-drop">
								<div class="information_dropdown">
									<ul class="information_dropdown__list">
										<li class="information_dropdown__item"><span class="default-text">Консультации покупателей в офисе "Метриум Групп"</span></li>
										<li class="information_dropdown__item"><span class="default-text">Консультации покупателей и показы объекта в офисе продаж на объекте </span></li>
									</ul>
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Сопровождение ипотечных сделок
								</div>
							</div>
							<div class="accordion__dropdown c-accordion-drop">
								<div class="information_dropdown">
									<ul class="information_dropdown__list">
										<li class="information_dropdown__item"><span class="default-text">Консультации покупателей по ипотечным программам</span></li>
										<li class="information_dropdown__item"><span class="default-text">Подбор ипотечной программы</span></li>
										<li class="information_dropdown__item"><span class="default-text">Формирование пакета документов для подачи в банк на одобрение кредита</span></li>
										<li class="information_dropdown__item"><span class="default-text">Контроль процесса и сроков аккредитации</span></li>
									</ul>
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Подготовка и организация подписания договора приобретения
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Регистрация договора приобретения
								</div>
							</div>
							<div class="accordion__dropdown c-accordion-drop">
								<div class="information_dropdown">
									<ul class="information_dropdown__list">
										<li class="information_dropdown__item"><span class="default-text">Формирование пакета документов для государственной регистрации договора приобретения</span></li>
										<li class="information_dropdown__item"><span class="default-text">Подача документов в орган регистрации</span></li>
										<li class="information_dropdown__item"><span class="default-text">В случае «приостановки» регистрационных действий осуществление всех правомерных действий с целью устранения замечаний органа регистрации и возобновления регистрационных действий</span></li>
										<li class="information_dropdown__item"><span class="default-text">Получение из органа регистрации договора, прошедшего регистрацию, свидетельства о собственности</span></li>
										<li class="information_dropdown__item"><span class="default-text">Передача экземпляров зарегистрированных договоров застройщику и покупателю</span></li>
									</ul>
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Регулярная подготовка отчетов по результатам реализации
								</div>
							</div>
							<div class="accordion__dropdown c-accordion-drop">
								<div class="information_dropdown">
									<ul class="information_dropdown__list">
										<li class="information_dropdown__item"><span class="default-text">Количество первичных обращений (звонки, консультации на объекте)</span></li>
										<li class="information_dropdown__item"><span class="default-text">Количество авансов</span></li>
										<li class="information_dropdown__item"><span class="default-text">Прогноз количества сделок на следующий период</span></li>
										<li class="information_dropdown__item"><span class="default-text">Количество заключенных сделок в отчетном периоде</span></li>
										<li class="information_dropdown__item"><span class="default-text">Контроль поступления денежных средств</span></li>
										<li class="information_dropdown__item"><span class="default-text">Оценка эффективности рекламной кампании с учетом соотношения затрат и количества первичных обращений</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
					<div class="accordion__link-text">
						3. Постпродажное обслуживание
					</div>
					<div class="accordion-icon bottom-shadow bottom-shadow--small">
						<i class="sprite-icon left-arrow"></i>
					</div>
				</div>
				<div class="accordion__dropdown c-accordion-drop">
					<div class="accordion c-accordion accordion--double accordion--child">
						<div class="accordion__item">
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Оформления прав собственности на объекты для покупателей и застройщика
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Взаиморасчеты по обмерам БТИ
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Подписание актов приема-передачи в отношении введенных в эксплуатацию объектов
								</div>
							</div>
							<div class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
								<div class="accordion__link-text">
									Проведение разъяснительных встреч, постоянное информирование и решение спорных вопросов с физическими и юридическими лицами
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix">
</div>
<!-- END main-part -->