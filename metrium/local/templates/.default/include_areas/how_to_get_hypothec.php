<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

    <div class="main-part right">
        <div class="content-full">
            <div class="main-title">Как взять ипотеку?</div>
            <div class="seo-text">Компания «Метриум Групп» предлагает своим клиентам различные способы расчета при покупке недвижимости. Мы активно сотрудничаем с различными банками-пантерами, чьи программы рады вам предложить. Что же делать, если вы готовы приобрести недвижимость по ипотеке, но ещё не выбрали объект?</div>
            <div class="credit-steps">
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 1
                    </div>
                    <div class="step-text">Определитесь с размером собственных средств, которые вы сможете направить на покупку недвижимости.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 2
                    </div>
                    <div class="step-text">Просмотрите программы банков-партнеров. Принимая во внимание, что минимальный первоначальный взнос по ипотеке составляет порядка 10% от стоимости будущей квартиры, посчитайте максимально возможную стоимость жилья. Например, если собственные средства составляют порядка 500 000 руб., то максимальная стоимость квартиры составит 5 000 000 руб.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 3
                    </div>
                    <div class="step-text">Учитывая рассчитанную сумму, выберите из представленных объектов те, которые попадают под данное условие. Новостройка или готовое жилье, Москва или Подмосковье, однокомнатная или трехкомнатная – выбор за вами!</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 4
                    </div>
                    <div class="step-text">Однозначный выбор сложно сделать, не посетив объект. Договоритесь с менеджером «Метриум Групп» о показе и забронируйте понравившуюся вам квартиру.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 5
                    </div>
                    <div class="step-text">Получите консультацию по ипотечному кредитованию. Вам расскажут о списке необходимых документов, специальных условиях рассчитают платежи.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 6
                    </div>
                    <div class="step-text">Соберите пакет документов, необходимых для подачи заявки.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 7
                    </div>
                    <div class="step-text">Обратитесь либо в отделение выбранного банка, либо в наш центральный офис. У нас Вы сможете подать заявления сразу в несколько банков.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 8
                    </div>
                    <div class="step-text">Дождитесь решения банков о предоставлении кредита. Это займет от 3 до 7 рабочих дней.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 9
                    </div>
                    <div class="step-text">Мы согласуем с банком необходимые договоры на приобретение объекта и дату сделки.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 10
                    </div>
                    <div class="step-text">Подпишите в банке кредитный договор и договор на объект с собственником.</div>
                </div>
                <div class="credit-step">
                    <div class="step-number">
                        <div class="step-icon"><i class="sprite-icon blue-right-arrow"></i></div>Шаг 11
                    </div>
                    <div class="step-text">Мы зарегистрируем договор на объект, и вы станете собственником новой квартиры.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>