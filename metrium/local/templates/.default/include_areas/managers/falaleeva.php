<div class="face-contact bottom-shadow">
	<div class="face-image">
		<img title="Фалалеева Е.jpg" src="/upload/medialibrary/3a4/_i_-_.jpg" alt="Фалалеева Е.jpg" height="234" width="158">
	</div>
	<div class="face-title">
		Контактное лицо
	</div>
	<div class="face-link bottom-shadow c-show-contact">
		<i class="sprite-icon redirect"></i>
	</div>
	<div class="face-info c-face-info clearfix">
		<div class="column left">
			<span class="face-name">Екатерина Фалалеева,</span>Корпоративный директор
		</div>
		<div class="column right">
			<span class="phones">+7 (499) 270-20-20</span><span class="emalis"></span>
		</div>
	</div>
</div>
<br>