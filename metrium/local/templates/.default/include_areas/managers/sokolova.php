<div class="face-contact bottom-shadow">
    <div class="face-image"><img src="/local/templates/.default/images/sokolova.jpg"></div>
    <div class="face-title">Контактное лицо</div>
    <div class="face-link bottom-shadow c-show-contact"><i class="sprite-icon redirect"></i></div>
    <div class="face-info c-face-info clearfix">
        <div class="column left"><span class="face-name">Анна Соколова,</span>Директор Департамента аналитики и консалтинга</div>
        <div class="column right"><span class="phones">+7 (499) 270-20-20</span><span class="emalis"></span></div>
    </div>
</div>