<!-- BEGIN main-part -->
<div class="main-part right">
    <div class="building-detail clearfix">
        <div class="main-content faq">
            <h1 class="main-title">Центральный офис</h1>
            <div class="investment">
                <p>Центральный офис компании расположен на 35-ом этаже башни Nordstar Tower, в трех минутах от станции метро Беговая. Для клиентов, предпочитающих посетить центральный офис «Метриум Групп» на автомобиле, мы предлагаем бесплатный охраняемый подземный паркинг.</p>
                <p>В центральном офисе «Метриум Групп» клиент может получить полный спектр услуг: от консультации по выбору недвижимости и подписания полного комплекта договоров, до оформления заявки на ипотеку и оплаты бронирования выбранной квартиры.</p>
            </div>
            <?$APPLICATION->IncludeComponent(
                "metrium:office.photogallery",
                ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "WIDTH" => "",
                    "HEIGHT" => "",
                    "THUMB_WIDTH" => "255px",
                    "THUMB_HEIGHT" => "256px",
                    "ELEMENT_ID" => "110323"
                ),
                false
            );?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END main-part -->