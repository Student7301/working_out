<!-- BEGIN main-part -->
<div class="main-part right">
    <div class="building-detail clearfix">
        <div class="main-content faq">
            <h1 class="main-title">Стационарные офисы</h1>
            <div class="investment">
                <p>
                    При старте реализации масштабного жилого проекта компания «Метриум Групп», как правило, принимает решение о строительстве стационарного офиса продаж на строительной площадке.
                </p>
                <p>
                    Стационарный офис продаж представляет собой уникальное и высокотехнологичное здание, обладающее привлекательной архитектурой, на строительство которого, включая отделку и меблировку, требуется не более 3-4 месяцев. Здание возводится на легком фундаменте и имеет полный спектр автономных инженерных систем: водоснабжение, канализирование, отопление, систему приточно-вытяжной вентиляции, кондиционирование, высокоскоростной интернет.
                </p>
                <p>
                    В здании, как правило, кроме зала продаж, организуются зоны переговорных комнат, зона ипотечных консультантов и консультантов по отделке, зал для сотрудников бэк-офиса, подготавливающих договора, кабинеты руководителей. Кроме того, в здании выделяются помещения мини-кухни, гардеробных и комнат для хранения спецодежды, необходимой для посещения стройки.
                </p>
                <p>
                    Планировочные решения офисов продаж могут быть выбраны из 4-х типовых вариантов, или спроектированы индивидуально. Индивидуальное проектирование и строительство по нетиповому проекту, как правило, увеличивает время и стоимость строительства на 20-25%.
                </p>
            </div>
            <div class="blocks_about clearfix">
                <div class="about__block about__block--office about__block--mitems">
                    <a title="" data-accordion="#office-tab1" class="absolute-link c-accordion_link_trigger" ></a>
                    <div class="img-container">
                        <img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office1.jpg">
                    </div>
                    <div class="link-office">
                        Типовые решения офисов продаж
                    </div>
                </div>
                <div class="about__block about__block--office about__block--mitems">
                    <a title="" data-accordion="#office-tab2" class="absolute-link c-accordion_link_trigger" ></a>
                    <div class="img-container">
                        <img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office2.jpg">
                    </div>
                    <div class="link-office">
                        Архитектура офисов продаж
                    </div>
                </div>
                <div class="about__block about__block--office about__block--mitems">
                    <a title="" data-accordion="#office-tab3" class="absolute-link c-accordion_link_trigger" ></a>
                    <div class="img-container">
                        <img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/office3.jpg">
                    </div>
                    <div class="link-office">
                        Демонстрационные квартиры (Show room)
                    </div>
                </div>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab1" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            Типовые решения офисов продаж
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "metrium:office.photogallery",
                            ".default",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ELEMENT_ID" => "110335",
                                "WIDTH" => "",
                                "HEIGHT" => "",
                                "THUMB_WIDTH" => "255px",
                                "THUMB_HEIGHT" => "256px",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "86400"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab1" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            Офис продаж RIVER-PARK
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                            <p>Срок строительства 4 месяца от проекта до открытия.</p>
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "metrium:office.photogallery",
                            ".default",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ELEMENT_ID" => "110326",
                                "WIDTH" => "",
                                "HEIGHT" => "",
                                "THUMB_WIDTH" => "255px",
                                "THUMB_HEIGHT" => "256px",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "86400"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab1" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            Офис продаж ЖК "БОЛТИНО"
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                            <p>Срок строительства 3 месяца.</p>
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "metrium:office.photogallery",
                            ".default",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ELEMENT_ID" => "110329",
                                "WIDTH" => "",
                                "HEIGHT" => "",
                                "THUMB_WIDTH" => "255px",
                                "THUMB_HEIGHT" => "256px",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "86400"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab1" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            Офис продаж ЖК "ВИДНОЕ"
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "metrium:office.photogallery",
                            ".default",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ELEMENT_ID" => "110332",
                                "WIDTH" => "",
                                "HEIGHT" => "",
                                "THUMB_WIDTH" => "255px",
                                "THUMB_HEIGHT" => "256px",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "86400"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="investment">
                <p>
                    Интерьеры офиса продаж могут быть выполнены в стиле «Метриум Групп», или же в уникальном стиле жилого комплекса, где расположен офис продаж.
                </p>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab1" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            ПРИМЕР №1: ОФИС ПРОДАЖ В СТИЛЕ «МЕТРИУМ ГРУПП»
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "metrium:office.photogallery",
                            ".default",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ELEMENT_ID" => "110324",
                                "WIDTH" => "",
                                "HEIGHT" => "",
                                "THUMB_WIDTH" => "255px",
                                "THUMB_HEIGHT" => "256px",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "86400"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab1" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            ПРИМЕР №2: ОФИС ПРОДАЖ В СТИЛЕ ЖК «РИВЕР ПАРК»
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "metrium:office.photogallery",
                            ".default",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ELEMENT_ID" => "110330",
                                "WIDTH" => "",
                                "HEIGHT" => "",
                                "THUMB_WIDTH" => "255px",
                                "THUMB_HEIGHT" => "256px",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "86400"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab2" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            Архитектура офисов продаж
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                            <p>
                                Планировочное, так и архитектурное решение офиса продаж может быть продиктовано множеством факторов:
                            </p>
                            <ul>
                                <li>офис продаж может строиться на 10 лет, а может требовать демонтажа и перевозки на другой участок для повторного использования;</li>
                                <li>архитектура может определяться архитектурным стилем продаваемого объекта и его окружением;</li>
                                <li>архитектурное решение в значительной степени определяет стоимость здания офиса продаж, затраты на которое должны быть соизмеримы с классом проекта, среднемесячным объемом реализации, общей площадью проекта.</li>
                            </ul>
                            <p>
                                Кроме типовых решений, приведенных выше, офисы продаж могут обладать еще более современной и выдающейся архитектурой.
                            </p>
                            <div>
                                <div style="float: left; width:49%">
                                    <img title="ofis-prodazh-1.jpg" src="/upload/medialibrary/5fb/ofis_prodazh_1.jpg" alt="ofis-prodazh-1.jpg">
                                </div>
                                <div style="float: right; width:49%">
                                    <img title="ofis-prodazh-2.jpg" src="/upload/medialibrary/e96/ofis_prodazh_2.jpg" alt="ofis-prodazh-2.jpg">
                                </div>
                            </div>
                            <div class="clearfix">
                            </div>
                            <p>
                                Кроме того, архитектурное решение может предполагать «сборно-разборность», когда стратегия девелопера позволяет после завершения продаж перевезти офис на другой девелоперский проект. Пример архитектуры, позволяющий выполнить демонтаж и перевозку здания, приведен ниже:
                            </p>
                            <div>
                                <div style="float: left; width:49%">
                                    <img title="ofis-prodazh-3.jpg" src="/upload/medialibrary/6d8/ofis_prodazh_3.jpg" alt="ofis-prodazh-3.jpg">
                                </div>
                                <div style="float: right; width:49%">
                                    <img title="ofis-prodazh-4.jpg" src="/upload/medialibrary/209/ofis_prodazh_4.jpg" alt="ofis-prodazh-4.jpg">
                                </div>
                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </div>
            </div><div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab2" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            Примеры благоустройства территории офиса продаж
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                            <p>
                                В рамках реализации задачи по организации офисов продаж, компания «Метриум Групп» также готова выполнить благоустройство территории офиса продаж.
                            </p>
                            <p>
                                Качественное благоустройство позволяет сформировать комфортную и привлекательную среду, привлечь покупателей с детьми, сделать их пребывание комфортным, что положительно сказывается на темпах продаж, а также позволяет выделить проект среди конкурентов.
                            </p>
                            <img title="ofis-prodazh-5.jpg" src="/upload/medialibrary/7db/ofis_prodazh_5.jpg" alt="ofis-prodazh-5.jpg"> <img title="ofis-prodazh-6.jpg" src="/upload/medialibrary/e52/ofis_prodazh_6.jpg" alt="ofis-prodazh-6.jpg">
                            <div class="clearfix">
                            </div>
                            <p>
                                Разработка проекта благоустройства может быть выполнена как российским, так и иностранным архитектором, что может не только повысить его качество, но и дать проекту дополнительную известность и PR.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion c-accordion">
                <div class="accordion__item">
                    <div id="office-tab3" class="accordion__link bottom-shadow bottom-shadow--small c-accordion-link accordion-parthner">
                        <div class="accordion__link-text">
                            Демонстрационные квартиры (SHOW ROOM)
                        </div>
                        <div class="accordion-icon bottom-shadow bottom-shadow--small">
                            <i class="sprite-icon bottom-arrow"></i>
                        </div>
                    </div>
                    <div class="accordion__dropdown parthner_dropdown c-accordion-drop">
                        <div class="editable-text editable-text--no-padd">
                            <p>
                                В каждом стационарном офисе продаж компании «Метриум Групп» располагается демонстрационная квартира (show room) – это модель в натуральную величину одной или двух наиболее удачных квартир или апартаментов продаваемого жилого комплекса.
                            </p>
                            <p>
                                Демонстрационные квартиры в размерах точно соответствуют строящемуся объекту, внутри выполняется отделка по специально разработанному проекту, а также полная меблировка и декор.
                            </p>
                            <p>
                                Демонстрационная квартира позволяет:
                            </p>
                            <ul>
                                <li>Показать объемно-габаритные характеристики квартир, когда строительство находится на ранней стадии (котлован или строительство конструктива)</li>
                                <li>Создать клиенту положительное впечатление от квартиры с качественной отделкой, меблировкой и декором, а не от квартиры «в бетоне»</li>
                                <li>Показать примеры отделки, чтобы на стадии покупки клиент мог заказать отделку будущей квартиры</li>
                            </ul>
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "metrium:office.photogallery",
                            ".default",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "WIDTH" => "",
                                "HEIGHT" => "",
                                "THUMB_WIDTH" => "255px",
                                "THUMB_HEIGHT" => "256px",
                                "ELEMENT_ID" => "110322",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "86400"
                            )
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix">
</div>
<!-- END main-part -->