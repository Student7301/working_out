<!-- BEGIN main-part -->
<div class="main-part right">
    <div class="building-detail clearfix">
        <div class="main-content faq">
            <div class="main-title">Вакансии</div>
            <div class="investment">
                <p>Сплоченный коллектив и слаженная командная работа – то, что помогает достигать нам поставленных целей и быть успешными в бизнесе. </p>
                <p>Работа в «Метриум Групп» - это: </p>
                <ul>
                    <li>динамичные и амбициозные задачи,</li>
                    <li>возможность приобретения новых знаний и опыта,</li>
                    <li>сотрудничество в команде профессионалов, ярких, энергичных и талантливых личностей,</li>
                    <li>источник вдохновения и возможность воплощения своих идей,</li>
                    <li>достойное вознаграждение.</li>
                </ul>
                <p>Приглашаем к сотрудничеству. Ждем от Вас стремления развиваться вместе с компанией, разделять ее ценности, работать на результат.<br>Если Вас заинтересовала какая-либо наша вакансия, Вы можете отправить свое резюме по адресу <a href="mailto:hr@metrium.ru">hr@metrium.ru</a> или заполнить <a href="/upload/common/Анкета кандидата.docx">анкету</a>. <br>Также следите за новыми вакансиями компании на <a href="http://hh.ru/employer/1070191" target="_blank">HeadHunter</a>.</p>
            </div>
            <?$APPLICATION->IncludeComponent(
                "metrium:vacancy.list",
                ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "CACHE_TIME" => "86400",
                    "CACHE_TYPE" => "A"
                ),
                false
            );?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END main-part -->