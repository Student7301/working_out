<!-- BEGIN main-part -->
<div class="main-part right">
	<div class="building-detail clearfix">
		<div class="main-content faq">
			<div class="main-title">
				 Контакты
			</div>
			<div class="investment" style="width: 370px;float: left;" itemscope itemtype="http://schema.org/Organization">
				<span itemprop="name" style="font-size: 30px;">«Метриум Групп»</span>
				<p>
				</p>
				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					 Адрес центрального офиса:

					<span itemprop="postalCode">125284,</span>
					<span itemprop="addressLocality">Москва,</span>
					<span itemprop="streetAddress"> ул. Беговая, 3, БЦ “Нордстар Тауэр”, этаж 35.</span>

				</div>
				<p>
				</p>
				<p>
				</p>
				<div>
					 Контактный телефон:
				</div>
				 <span itemprop="telephone"><a href="tel:+74997552020/" id="phone_moscow">+7 (499) 755-20-20</a></span>
				<p>
				</p>
				<div>
					 Электронная почта:
				</div>
 				<span itemprop="email"><a href="mailto:info@metrium.ru">info@metrium.ru</a><br></span>
				 <br>
			</div>

			<div class="" style="text-align: center;">
                <a target="_blank" href="/news/detail/metrium-grupp-rieltor-1-moskovskogo-regiona/">
                    <img alt="imgpsh_fullsize.png" src="/upload/medialibrary/4d7/imgpsh_fullsize.png" height="180" style="float:left" title="imgpsh_fullsize.png">
                </a>
                <a target="_blank" href="/news/detail/metrium-grupp-priznana-rieltorom-goda-2016-po-versii-urban-awards/">
                    <img alt="winner_UA-2016-o-kompanii.jpg" src="/upload/medialibrary/731/winner_ua_2016_o_kompanii.jpg" height="180" style="float:left" title="winner_UA-2016-o-kompanii.jpg">
                </a>
            </div>
            <div class="clearfix">
            </div>
			<div class="investment">
 <i>Материалы и информация, в том числе цены, опубликованные на настоящем сайте, носят исключительно информационный характер&nbsp;</i><i>и ни при каких условиях не являются публичной офертой, определяемой положениями статьи 437 Гражданского кодекса РФ.&nbsp;</i><i>Для получения подробной информации о наличии и стоимости квартир необходимо обращаться к менеджерам отдела продаж.</i>
			</div>
			<div class="blocks_about clearfix">
				<div class="about__block about__block--office">
					<div class="img-container">
 <img src="<?= \metrium\System\SiteTemplate::getDefault() ?>/images/content/contacs-office.jpg">
					</div>
					<div class="link-office">
						 Центральный офис
					</div>
				</div>
				<div class="right">
					<div class="investment align-right">
						<p>
						</p>
						<div>
							 Департамент контроля качества
						</div>
						<div>
							 «Метриум Групп»
						</div>
						<div>
							<a href="tel:+74997552020/"> +7 (499) 755-2020</a>
						</div>
						<p>
						</p>
						<p>
						</p>
						<div>
							 Электронная почта:
						</div>
 <a href="mailto:quality@metrium.ru">quality@metrium.ru</a><br>
						<p>
						</p>
						<div class="c-dropdownform no-scroll-action content-drop contact_drop">
 <a href="" class="btn btn--middle c-dropdownform__link">Оставить заявку о качестве обслуживания</a>
							<div class="drop-form drop-form--bottom-left contact-form bottom-shadow bottom-shadow--small c-dropdownform__content">
								 <?$APPLICATION->IncludeComponent(
	"metrium:contacts.form",
	".default",
	Array(
	)
);?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="main-title">
				 Схема расположения <br>
				 центрального офиса
			</div>
            <div id="contacts_map" class="contacts_map"></div>
            <script>
                ymaps.ready(function () {
                    var myMap = new ymaps.Map('contacts_map', {
                            center: [55.776451, 37.553594],
                            zoom: 16
                        }, {
                            searchControlProvider: 'yandex#search'
                        }),

                        // Создаём макет содержимого.
                        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                        ),

                        myPlacemarkWithContent = new ymaps.Placemark([55.776451, 37.553594], {
                            hintContent: '«Метриум Групп»',
                            balloonContent: '125284, Москва, ул. Беговая, 3, <br />БЦ “Нордстар Тауэр”, этаж 35.',
                            //iconContent: '12'
                        }, {
                            // Опции.
                            // Необходимо указать данный тип макета.
                            iconLayout: 'default#imageWithContent',
                            // Своё изображение иконки метки.
                            iconImageHref: '/local/templates/.default/images/map-balloon.png',
                            // Размеры метки.
                            iconImageSize: [120, 69],
                            // Смещение левого верхнего угла иконки относительно
                            // её "ножки" (точки привязки).
                            iconImageOffset: [-60, -60],
                            // Смещение слоя с содержимым относительно слоя с картинкой.
                            iconContentOffset: [15, 15],
                            // Макет содержимого.
                            iconContentLayout: MyIconContentLayout
                        });
                        myMap.geoObjects.add(myPlacemarkWithContent);
                });
            </script>
		</div>
	</div>
</div>
<div class="clearfix">
</div>
<!-- END main-part -->