<!-- BEGIN main-part -->
<div class="main-part right">
    <div class="building-detail clearfix">
        <div class="main-content joint_venture">
            <div class="main-title">Мониторинг прессы</div>
            <p>Аналитическое подразделение компании «Метриум Групп» проводит регулярный обзор СМИ, освещающих события на рынке жилой недвижимости Московского региона и формирует подборку главных новостей, аналитических материалов и пресс-релизов участников рынка. Мониторинг позволяет оперативно отслеживать и быть в курсе ключевых событий на рынке жилья, своевременно реагируя на них.</p>
            <h2>Мониторинг рынка новостроек от "Метриум Групп"</h2>
            <?$APPLICATION->IncludeComponent(
                "metrium:press.monitoring",
                ".default",
                array()
            );?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END main-part -->