<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use metrium\helpers\FbHelper;

define('METRIUM_FB_PAGE_ID', 'metrium');

$fb = new FbHelper(array('page_id' => METRIUM_FB_PAGE_ID));
$html = $fb->parsePage()->getHtml();
$next = $fb->getNextData();
?>

<div id="social-activity">
    <div class="container">
        <header>
            <p class="hash-tag">Официальный хештег — <a>#metrium</a></p>
            <nav class="social-links">
                <ul class="navbar-right social-links">
                    <li><a target="_blank" rel="nofollow" title="Facebook" class="facebook" href="https://www.facebook.com/metrium"></a></li>
                </ul>
            </nav>
            <p class="metrium_in_social">Metrium в социальных сетях</p>
        </header>
        <div class="fb-entries clearfix">
            <?= $html; ?>
        </div>
        <footer>
            <a
                href="/local/templates/.default/ajax/social_fb.php"
                data-href="/local/templates/.default/ajax/social_fb.php<?= !empty($next) ? '?next_from=' . $next : '' ?>"
                class="btn"
            >Загрузить еще</a>
        </footer>
    </div>
</div>