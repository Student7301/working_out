function getTablesView(){
	'use strict';

	var tiles = $(".c-tile");

	if(tiles.length){
		tiles.jqfader({
			randomize: true,
			speed: 50,
			animate: 100
		});	
	}
}
 
function checkAgreeSendInformation(obj){
	if (obj.checked){
		$('input[name=subscribe_form]').attr("disabled", false);
	} else {
		$('input[name=subscribe_form]').attr("disabled", true);
	}
}

//склонение окончаний	
function declension(num, expressions) {
	var result;
	count = num % 100;
	if (count >= 5 && count <= 20) {
		result = expressions['2'];
	} else {
		count = count % 10;
		if (count == 1) {
			result = expressions['0'];
		} else if (count >= 2 && count <= 4) {
			result = expressions['1'];
		} else {
			result = expressions['2'];
		}
	}
	return result;
}

function storageAvailable(type) {
	try {
		var storage = window[type],
			x = '__storage_test__';
		storage.setItem(x, x);
		storage.removeItem(x);
		return true;
	}
	catch(e) {
		return false;
	}
}

function getSeoPosition(slider, header, seoBlock){
	'use strict';

	if($(window).width() > 1345){
		var sliderHeight = slider.height(),
			headerHeight = header.height();

		if(header.hasClass('active')){
			seoBlock.css( { top : sliderHeight + 53} );
		}
		else{
			seoBlock.css( { top : sliderHeight + headerHeight + 53} );
		}
	}
	else{
		seoBlock.removeAttr('style');
	}
}

function sliceText(str, count){
	'use strict';

	return str.slice(0, count);
}

function creditSendAjax(form, dataForm){
	'use strict';

	showPreloader();

	$.ajax({
		url: form.data('ajax-url'),
		data: dataForm,
		type : 'POST',
		success: function (data) {
			if(dataForm.tableStep > 1){
				$('.show-more').remove();
				$(".objects-table").last().after(data);
			}
			else{
                var mainTitle = $(".main-title").offset().top;
				$('.c-filter-result').html(data);
                $('html, body').animate({
					scrollTop: $(".filter-result").offset().top - mainTitle
				}, 1000);
			}
		},
		error: function (er) {
			console.log(er);
		},
		complete: function(){
			hidePreloader();
		}
	});
}

function getTableRows(elem){
	'use strict';

	var elems = $('.c-load-table');

	showPreloader();
	$.ajax({
		url: elem.data('table'),
		success: function (data) {
			$('.c-table-result').html(data);

			if (elem.hasClass('c-tbl_switch')) {
				elems.removeClass('active');
				elem.addClass('active');
			}
		},
		error: function (er) {
			console.log(er);
		},
		complete: function(){
			hidePreloader();
		}
	});
}

function imageMap(image){
	'use strict';

	// подсветка карты 
	var popupImage = image.data('popup-image'),
		size = image.data('size') || 800;

	image.mapster({
		scaleMap: true,
		fill: true,
		fillColor: 'ee6300',
		fillOpacity: 0.47,
		stroke: true,
		strokeColor: 'c75708',
		strokeOpacity: 1,
		strokeWidth: 1,
		isSelectable: false,
		onClick: function(){

			if(popupImage == "N"){
				$('.tabs__link.c-tabs-link.active').prev().trigger('click');
				$('.corpuses .c-tabs-link.corpus_' + $(this).data('corpus')).trigger('click');

				/*var accordion = $('.c-corpus-list').children().eq($(this).data('corpus')),

				firstCorp.trigger('click');

				firstCorp.next().find('.c-accordion-link:first').trigger('click');*/
			}
			else{
				window.open(this.href, "_blank");
			}
		}
	});

	image.mapster('resize', size, 0, 1000);
}


function selectsMainSearch(selectsList, autocompleteInput, lang){
	'use strict';

	var searchData = {},
		inputs = $('.c-search-input');

	searchData.selects = [];
	searchData.type = 'realty';
	searchData.inputs = [];
	searchData.checkboxes = [];
	
	if($('.c-text_ser').hasClass('id')) {
		searchData.selects = [];
		searchData.type = 'id';
		searchData.inputs = [];
		searchData.checkboxes = [];
	} else {
		for(var i=0; i < selectsList.length; i++){
			var elem = $(selectsList[i]);

			if(elem.val() != null && elem.val() != ''){
				searchData.selects.push( [elem.attr('name') , elem.val()] );
			}
		}
		
		var sCheck = $('.s_check input[type="checkbox"]:checked');
		for(var q=0; q < $(sCheck).length; q++){
			var elem = sCheck[q];
			searchData.checkboxes.push( [$(elem).attr('name') , 'Y'] );
		}

		selectsList.SumoSelect({
			placeholder: lang,
			csvDispCount: 3,
			captionFormat: '{0} Selected',
			floatWidth: 100,
			forceCustomRendering: false,
			nativeOnDevice: [ 'Opera Mini', 'Silk'],
			outputAsCSV : false,
			csvSepChar : ',',
			okCancelInMulti: false,
			triggerChangeCombined : true,
			selectAll: false,
			selectAlltext: 'Select All'

		})
		selectsList.on("change",function(){
			var elem = $(this),
				chooseSelect = $('.c-choose-type'),
				customSelects = $(".c-second-step .c-custom-select");

			if(elem.hasClass('c-choose-type')){
				var wrap = $('#'+elem.find(':selected').data('type'));

				wrap.addClass('active');
				wrap.siblings().removeClass('active');

				customSelects.each(function(){
					$(this)[0].sumo.unSelectAll();
				});
			} 

			searchData.selects = []

			for(var i=0; i < selectsList.length; i++){
				var elem = $(selectsList[i]);

				if(elem.val() != null){
					searchData.selects.push( [elem.attr('name') , elem.val()] );
				}
			}

			if(autocompleteInput.length){
				searchAutocomplete(autocompleteInput, searchData);
			}
		});
		$('.s_check input[type="checkbox"]').on('change',function(){
			setTimeout(function(){
				var sCheck = $('.s_check input[type="checkbox"]:checked');
				searchData.checkboxes = [];
				searchData.selects = [];
				searchData.inputs = [];
				for(var q=0; q < $(sCheck).length; q++){
					var elem = sCheck[q];
					searchData.checkboxes.push( [$(elem).attr('name') , 'Y'] );
				}
				
				for(var i=0; i < selectsList.length; i++){
					var elem = $(selectsList[i]);

					if(elem.val() != null && elem.val() != ''){
						searchData.selects.push( [elem.attr('name') , elem.val()] );
					}
				}
				
				for(var i=0; i < inputs.length; i++){
					var elem = $(inputs[i]);
					if(elem.val() != ''){
						searchData.inputs.push( [elem.attr('name') , elem.val() ]);
					}
				}
				
				if(autocompleteInput.length){
					searchAutocomplete(autocompleteInput, searchData);
				}
			},300);
		});

		$('body').on('click', '.c-search-switch', function(){
			var searchType = $(this).data('search'),
				customSearch = $('.c-custom-search'),
				selectList = customSearch.find("select.c-custom-select");

			if(searchType == 'show'){
				customSearch.removeClass('disable');

				searchData.type = 'realty';

				if(autocompleteInput.length){
					searchAutocomplete(autocompleteInput, searchData);
				}
			}
			else{
				customSearch.addClass('disable');

				selectList[0].sumo.unSelectAll();


				searchData.type = 'id';
				searchData.selects = [];

				if(autocompleteInput.length){
					searchAutocomplete(autocompleteInput, searchData);
				}
			}
		});
	}
	
	inputs.on('blur', function(){
		if(this.value != ''){
			searchData.inputs.push( [this.name , this.value ]);
		}

		if(autocompleteInput.length){
			searchAutocomplete(autocompleteInput, searchData);
		}
	})

	if(autocompleteInput.length){
		searchAutocomplete(autocompleteInput, searchData);
	}
}

function searchAutocomplete(input, formData){
	formData.searchText = '';

	input.autocomplete({
		serviceUrl: input.data('ajaxurl'),
		ajaxSettings: { dataType: 'JSON', data: formData },
		width : 645,
		deferRequestBy: 150,
		onSearchStart : function(){
			formData.searchText = this.value;
			input.autocomplete('setOptions', {ajaxSettings : { dataType: 'JSON', data: formData }});
		},
		transformResult: function(response) {
			return {
				suggestions: $.map(response.list, function(dataItem) {
					if(dataItem.description != null){
						return { value: dataItem.description, data: dataItem.url };
					}
					else{
						return { value: dataItem.value, data: dataItem.url };
					}
				})
			};
		},
		onSelect: function (suggestion) {
			if(suggestion.data && suggestion.data != ''){
				window.open(suggestion.data, "_blank");
			}
		}
	});
}

function _getPopupFilterInputs(elem, min, max, FilterValues){
	if(elem.data('filtervalue') == 'area'){
		FilterValues.area.min = min;
		FilterValues.area.max = max;
	}
	else{
		FilterValues.price.min = min;
		FilterValues.price.max = max;
	}

	siteYandexMap.getFilteredCollection(FilterValues);
}

function popupFilter(){
	var FilterValues = {},
		links = $('.c-switch-filter .c-filter-link'),
		dropfilters = $('.c-switch-filter .c-filter-drop'),
		checkOptions = $('.c-switch-filter .checkbox-label'),
		inputOptions = $('.c-switch-filter .c-input-option');

	FilterValues.checkValues = [];
	FilterValues.price = {};
	FilterValues.area = {};
	FilterValues.links = [];

	links.each(function(){
		if($(this).hasClass('active')){
			FilterValues.links.push($(this).data("filtervalue"));
		}
	});

	$.each(checkOptions, function(){
		var label = this;

		if($(label).data('checked') == "Y"){
			FilterValues.checkValues.push($(this).prev().val());
		}
	});

	$.each(inputOptions, function(){
		var elem = $(this),
			inputs = elem.find('input[type="text"]');

		if(elem.closest('.c-filter-drop').data('filtervalue') == 'area'){
			var item1 = inputs.eq(0).val(),
				item_c1 = item1.replace(/\s+/g, ''),
				item2 = inputs.eq(1).val(),
				item_c2 = item2.replace(/\s+/g, '');
			FilterValues.area.min = item_c1;
			FilterValues.area.max = item_c2;
		}
		else{
			var item1 = inputs.eq(2).val(),
				item_c1 = item1.replace(/\s+/g, ''),
				item2 = inputs.eq(3).val(),
				item_c2 = item2.replace(/\s+/g, '');
			FilterValues.price.curr = $('.c-priceval').find('option:selected').val();
			FilterValues.price.min = item_c1;
			FilterValues.price.max = item_c2;
		}

	});
	 ymaps.ready(function (){

		siteYandexMap.getFilteredCollection(FilterValues);
	});

	$('.c-filter-link').on('click', function(e){
		e.preventDefault();

		$(this).toggleClass('active');

		FilterValues.links = [];

		links.each(function(){
			if($(this).hasClass('active')){
				FilterValues.links.push($(this).data("filtervalue"));
			}
		});

		siteYandexMap.getFilteredCollection(FilterValues);

	});

	dropfilters.find('a').on('click', function(e){
		e.preventDefault();
	});

	$.each(dropfilters, function(){
		var elem = $(this),
			checkOptions = elem.find('.checkbox-label'),
			inputOption = elem.find('.c-input-option');

		if(inputOption.length){

			var inputs = elem.find('input[type="text"]'),
				clear = elem.find('.c-clear-inputs'),
				select = elem.find('.c-priceval');

			if(elem.data('filtervalue') == 'area') {
				inputs.on('blur', function(){
				var item1 = inputs.eq(0).val(),
					item_c1 = item1.replace(/\s+/g, ''),
					item2 = inputs.eq(1).val(),
					item_c2 = item2.replace(/\s+/g, '');
				var min = item_c1,
					max = item_c2;
				_getPopupFilterInputs(elem, min, max, FilterValues);
				});

				inputs.on('keypress', function(e){
					var item1 = inputs.eq(0).val(),
						item_c1 = item1.replace(/\s+/g, ''),
						item2 = inputs.eq(1).val(),
						item_c2 = item2.replace(/\s+/g, '');
					var min = item_c1,
						max = item_c2;
					_getPopupFilterInputs(elem, min, max, FilterValues);
				});
				
				var rangeList_m = $(".c-range_m"),
					bool = false;
				if (rangeList_m.length > 0) {
					$.each(rangeList_m, function (i) {
						rangeList_m[i].noUiSlider.on('change', function(values){
							var item1 = inputs.eq(0).val(),
								item_c1 = item1.replace(/\s+/g, ''),
								item2 = inputs.eq(1).val(),
								item_c2 = item2.replace(/\s+/g, '');
							var min = item_c1,
								max = item_c2;
							_getPopupFilterInputs(elem, min, max, FilterValues);								
						});
					});
				}

				function editInp() {
					var minInput = inputs.eq(0),
						maxInput = inputs.eq(1);
					minInput.val(parseInt(minInput.attr('data-default')));
					maxInput.val(parseInt(maxInput.attr('data-default')));
					_getPopupFilterInputs(elem, minInput.data('default'), maxInput.data('default'), FilterValues);
				}

				clear.on('click', function(e){
					e.preventDefault();
					editInp();
				});
			} else {
				inputs.on('blur', function(){
					var item1 = inputs.eq(2).val(),
						item_c1 = item1.replace(/\s+/g, ''),
						item2 = inputs.eq(3).val(),
						item_c2 = item2.replace(/\s+/g, '');
					var min = item_c1,
						max = item_c2;
					_getPopupFilterInputs(elem, min, max, FilterValues);
				});

				inputs.on('keypress', function(e){
					var item1 = inputs.eq(2).val(),
						item_c1 = item1.replace(/\s+/g, ''),
						item2 = inputs.eq(3).val(),
						item_c2 = item2.replace(/\s+/g, '');
					var min = item_c1,
						max = item_c2;
					_getPopupFilterInputs(elem, min, max, FilterValues);
				});
				
				var rangeList_m = $(".c-range_m");
				if (rangeList_m.length > 0) {
					$.each(rangeList_m, function (i) {
						rangeList_m[i].noUiSlider.on('change', function(values){
							setTimeout(function(){
								var item1 = inputs.eq(2).val(),
									item_c1 = item1.replace(/\s+/g, ''),
									item2 = inputs.eq(3).val(),
									item_c2 = item2.replace(/\s+/g, '');
								var min = item_c1,
									max = item_c2;
								_getPopupFilterInputs(elem, min, max, FilterValues);
							},300);
						});
					});
				}

				function editInp() {
					var minInput = inputs.eq(2),
						maxInput = inputs.eq(3);
					if(select.find('option:selected').val() == 'USD') {
						minInput.val(parseInt(minInput.attr('data-defaultUSD')));
						maxInput.val(parseInt(maxInput.attr('data-defaultUSD')));
					} else {
						minInput.val(parseInt(minInput.attr('data-default')));
						maxInput.val(parseInt(maxInput.attr('data-default')));
					}
					_getPopupFilterInputs(elem, minInput.data('default'), maxInput.data('default'), FilterValues);
				}

				clear.on('click', function(e){
					e.preventDefault();
					editInp();
				});
			}
		}

		if(checkOptions.length){
			checkOptions.off('click').on( "click", function() {
				var label = this;

				FilterValues.checkValues = [];
				label.dataset.checked == "Y" ? label.dataset.checked = "N" : label.dataset.checked = "Y";

				$.each(checkOptions, function(){
					var label = $(this)[0];

					if($(label).data('checked') == "Y"){
						FilterValues.checkValues.push($(this).prev().val());
					}
				});

				siteYandexMap.getFilteredCollection(FilterValues);
			});
		}

	});
}

function getCardHeight(item){
	var neighbor = item.next(),
		max = item.height();

	if(neighbor.length){
		if(neighbor.height() > max){
			max = neighbor.height();
		}
		item.css("height", max);
		neighbor.css("height", max);
	}
}

function sliderInit(index){
	index = index || 0;
	$('.fotorama').fotorama({
		startindex: index
	});
}

function getCurrentRoadText(ind){
	var elems = $('.c-road-text').children();

	elems.hide();
	elems.eq(ind).show();
}

function getRouteToActiveTab(){
	if($('.c-road-tabs').length){
		$('.c-road-tabs').find('.c-map-link.active').trigger('click');
	}
	if($('.c-dev-map-btns').length){
		$('.c-dev-map-btns').find('.c-map-link.active').trigger('click');
	}
}

function closeElemByClick(clickElem, elemToHide, flag){
	$(document).on('click', function (e){
		if (!clickElem.is(e.target) && clickElem.has(e.target).length === 0) {
			elemToHide.hide();
			clickElem.find('.c-dropdownform__link').removeClass('active');

			if(flag){
				clickElem.find('[data-cliked]').attr('data-cliked', 'N');
			}
		}
	});
}

function switchMapFilter(level){
	var filterLinks = $('.c-switch-filter').find('.c-filter-link');

	if (level == 1){
		filterLinks.removeClass('disable');
	}
	else{
		var n = 0;
		while(n < 3){
			filterLinks.eq(n).addClass('disable');
			n++;
		}
		
	}
}

function mainFilterDirections(customFilter){

	var directions = customFilter.find('.c-direction-select-able .checkbox-label'),
		selectDirectionsWrap = customFilter.find('.c-selected-direction'),
		selectAllLink = customFilter.find('.c-select-all'),
		clearAllLink = customFilter.find('.c-deselect-all');

	directions.on('click', function(){
		var elem = this;

		elem.dataset.checked == 'Y' ? elem.dataset.checked = 'N' : elem.dataset.checked = 'Y';
	});

	selectAllLink.on('click', function(e){
		e.preventDefault();

		$.each(directions, function(){
			var elem = $(this);

			if (this.dataset.checked != 'Y'){
				elem.trigger('click');
			}
		});
	});

	clearAllLink.on('click', function(e){
		e.preventDefault();
		$.each(directions, function(){
			var elem = $(this);

			if (this.dataset.checked == 'Y'){
				elem.trigger('click');
			}
		});
	});

	function removeDirection(direction){
		var label = customFilter.find('label[for="'+direction+'"]');
		label.trigger('click');
	}

	$('body').on('click', '.c-remove-direction', function(e){
		e.preventDefault();
		e.stopPropagation();

		removeDirection($(this).parent().data('direction'));
	});

	$.each(directions, function(){
		var elem = $(this);

		elem.on('click',function(){
			if (this.dataset.checked == 'Y'){
				var text = elem.find('.checkbox-text').text();
				
				if(text.indexOf("(") > 0){
					text = text.substr(0, text.indexOf("("));
				}

				selectDirectionsWrap.append('<div class="selected-direction__item" data-direction="'+elem.attr('for')+'"><i class="sprite-icon close-white-small c-remove-direction"></i>'+text+'</div>')
			}
			else{
				selectDirectionsWrap.find('[data-direction = "'+elem.attr('for')+'"]').remove();
			}
		});
	});
}

function getCurrPercent(elem, textBlock){
	var fullSumm = parseInt($('#summ')[0].noUiSlider.get()),
		currVal = parseInt(elem.val().replace(/\s+/g, ''));

	if(currVal){
		var p = (100 * currVal / fullSumm).toFixed(2);

		textBlock.text('- что составляет '+p+'% от полной стоимости').removeClass('hidden');
		$('.c-first-summ-percent').val(p);
	}
}

function formValidate(){
	var validatorLanguage = {
		errorTitle: 'Ошибка отправки формы!',
		requiredFields: 'Необходимо заполнить обязательные поля',
		badTime: 'Введите корректное время',
		badEmail: 'Введите корректный e-mail адрес',
		badTelephone: 'Введите корректный номер телефона',
		badSecurityAnswer: 'Ответ на секретный вопрос не верен',
		badDate: 'Введите корректную дату',
		lengthBadStart: 'Значение должно быть между ',
		lengthBadEnd: ' знаками',
		lengthTooLongStart: 'Значение поля больше, чем ',
		lengthTooShortStart: 'Значение поля меньше, чем ',
		notConfirmed: 'Input values could not be confirmed',
		badDomain: 'Введите корректное доменное имя',
		badUrl: 'Значение поля не является корректным url-адресом',
		badCustomVal: 'Зачение поля не верно',
		andSpaces: ' и пробелов ',
		badInt: 'Поле может содержать только цифры',
		badSecurityNumber: 'Your social security number was incorrect',
		badUKVatAnswer: 'Incorrect UK VAT Number',
		badStrength: 'The password isn\'t strong enough',
		badNumberOfSelectedOptionsStart: 'You have to choose at least ',
		badNumberOfSelectedOptionsEnd: ' answers',
		badAlphaNumeric: 'The input value can only contain alphanumeric characters ',
		badAlphaNumericExtra: ' и ',
		wrongFileSize: 'Максимальный размер файла %s',
		wrongFileType: 'Разрешениы следующие форматы файлов: %s',
		groupCheckedRangeStart: 'Выберите между ',
		groupCheckedTooFewStart: 'Выберите не меньше, чем ',
		groupCheckedTooManyStart: 'Выберите не больше, чем ',
		groupCheckedEnd: ' item(s)',
		badCreditCard: 'The credit card number is not correct',
		badCVV: 'The CVV number was not correct',
		wrongFileDim : 'Incorrect image dimensions,',
		imageTooTall : 'the image can not be taller than',
		imageTooWide : 'the image can not be wider than',
		imageTooSmall : 'the image was too small',
		min : 'мин.',
		max : 'макс.',
		imageRatioNotAccepted : 'Image ratio is not accepted'
	};

	$.validate({
		validateOnBlur : false,
		errorMessagePosition : 'top',
		scrollToTopOnError : false,
		language: validatorLanguage,
		borderColorOnError : '#e41919',
		onSuccess : function($form){
			// send form by ajax
			if($form.hasClass('c-ajaxForm')){

				sendFormbyAjax($form);

				return false;
			}
			
		}
	});
}

// compare-width
function getColswidth(){
	if($('.c-compare').length){
		var cols = $('.c-compare').find('.compare__cols'),
			colsCount = $('.c-compare-head').children().length-1;

		$.each(cols, function(){
			var elem = $(this);

			if(elem.index() > 0){
				if(elem.find('p').length){
					elem.css('width', 'auto');
					elem.find('p').css('width', Math.round(($('.container').width() - 155)/colsCount - 31));
				}
			}
		});
	}
}

// compare-height
function getColsHeight(){
	if($('.c-compare').length){
		var objects = $('.c-compare-obj');

		$.each(objects, function(){
			var elems = $(this).children(),
				maxHeight = 0;

			$.each(elems, function(){
				var children = $(this).find('.table-inner'),
					imageBlock = $(this).find('.compare-image-block'),
					currHeight;
				if(children.length){
					currHeight = children.height();
				}
				else{
					currHeight = imageBlock.height();
				}

				if (currHeight > maxHeight){
					maxHeight = currHeight;
				}
			});

			elems.css('height', maxHeight + 20);
		});
	}
}

// no-ui-slider
function rangeNoUiSlider(range){
	var isRange = false,
		values_min = parseInt(range.data('min')) || 0,
		values_max = parseInt(range.data('max')) || 100,
		values_current = parseInt(range.data('val-min')) || 10,
		values_currentMin = parseInt(range.data('val-min')) || 0,
		values_currentMax = parseInt(range.data('val-max')) || 20,
		valMinRUB = parseInt(range.data('val-min-rub')) || 0,
		valMaxRUB = parseInt(range.data('val-max-rub')) || false,
		step = range.data('step') || 1,
		minInputName = range.data('min-input') || '',
		maxInputName = range.data('max-input') || '',
		inputMove = range.data('move') || '',
		noInputs = range.data('noinp') || '',
		formatPrice = range.data('format') || false,
		maxPlus = range.data('max-plus') || false,
		hiddenMin = range.data('hidden-min'),
		hiddenMax = range.data('hidden-max'),
		slider = range[0];

	if (range.data('range') !== undefined){
		isRange = true
	}

	if(isRange){
		noUiSlider.create(slider, {
			start: [ values_currentMin, values_currentMax ],
			step: step,
			connect: false,
			animate: false,
			range: {
				'min': values_min,
				'max': values_max
			}
		});
	}
	else{
		noUiSlider.create(slider, {
			start: values_current,
			step: step,
			connect: false,
			animate: false,
			range: {
				'min': values_min,
				'max': values_max
			}
		});
	}
	
	function updateSliderRange ( min, max ) {
		$(range)[0].noUiSlider.updateOptions({
			range: {
				'min': min,
				'max': max
			}
		});
		$(range)[0].noUiSlider.set([min, max]);
	}

	if(valMaxRUB != false) {
		$('.price_elite, .price_elite_m').on("change", function(e) {
			var opt_c = $(this).select2("val");
			if(opt_c == "RUB") {
				updateSliderRange(valMinRUB, valMaxRUB);
			} else {
				updateSliderRange(values_min, values_max);
			}
		});
	}

	// get inputs
	var handles = range.find('.noUi-origin'),
		minInput, maxInput,
		currValues = slider.noUiSlider.get();
	if(noInputs == '') {
		range.append('<input type="text" name="' + minInputName + '" class="range-input c-range-min">');
		minInput = range.find('.c-range-min');

		if(isRange){
			range.append('<input type="text" name="' + maxInputName + '" class="range-input c-range-max">');
			maxInput = range.find('.c-range-max');
		}
	} else {
		minInput = $('input[name="'+minInputName+'"]');

		if(isRange){
			maxInput = $('input[name="'+maxInputName+'"]');
		}
	}
	hiddenMin = $('input[name="'+hiddenMin+'"]');
	hiddenMax = $('input[name="'+hiddenMax+'"]');
	setTimeout(function(){
		
		if (maxPlus == true) {
			var leng = parseInt(currValues[1]).toFixed().length,
				lengMin = parseInt(currValues[0]).toFixed().length,
				maxval = parseInt(currValues[1]),
				minval = parseInt(currValues[0])
			hiddenMin.val(parseInt(currValues).toFixed());
			if (lengMin >= 4 && lengMin < 7 ) {
				minInput.next('.input-postfix').html('тыс');
				minInput.val((minval / 1000).toPrecision(3));
			} else if (lengMin >= 7 && lengMin < 10) {
				minInput.next('.input-postfix').html('млн');
				minInput.val((minval / 1000000).toPrecision(3));
			} else if (lengMin >= 10) {
				minInput.next('.input-postfix').html('млрд');
				minInput.val((minval / 1000000000).toPrecision(3));
			} else {
				minInput.next('.input-postfix').html('');
				minInput.val(minval);
			}
			hiddenMin.val(parseInt(currValues[0]).toFixed());
			if(maxInput){
				if(inputMove == '') {
					maxInput.css('left', handles.eq(1).position().left + 35);
				}
				
				if (leng >= 4 && leng < 7 ) {
					maxInput.next('.input-postfix').html('тыс');
					maxInput.val((maxval / 1000).toPrecision(3));
				} else if (leng >= 7 && leng < 10) {
					maxInput.next('.input-postfix').html('млн');
					maxInput.val((maxval / 1000000).toPrecision(3));
				} else if (leng >= 10) {
					maxInput.next('.input-postfix').html('млрд');
					maxInput.val((maxval / 1000000000).toPrecision(3));
				} else {
					maxInput.next('.input-postfix').html('');
					maxInput.val(maxval);
				}
				hiddenMax.val(parseInt(currValues[1]).toFixed());
			}
			
			if(valMaxRUB != false) {
				var opt_c = $('.price_elite, .price_elite_m').select2("val");
				if(opt_c == "RUB") {
					if(hiddenMax.val() == valMaxRUB) {
						maxInput.val(maxInput.val()+'+')
					}
				} else {
					if(hiddenMax.val() == values_max) {
						maxInput.val(maxInput.val()+'+')
					}
				}
			} else {
				if(hiddenMax.val() == values_max) {
					maxInput.val(maxInput.val()+'+')
				}
			}
		} else {
			minInput.css('left', handles.eq(0).position().left + 35);
			minInput.val(parseInt(currValues).toFixed());

			if(isRange){
				minInput.val(parseInt(currValues[0]).toFixed());

				maxInput.css('left', handles.eq(1).position().left + 35);
				maxInput.val(parseInt(currValues[1]).toFixed());
			}
			
			if(formatPrice == '') {
				getPriceFormat(minInput);
				if(maxInput){
				getPriceFormat(maxInput);
				}
			}
		}
	}, 300);
	slider.noUiSlider.on('update', function(values){
		setTimeout(function(){
			if(inputMove == '') {
				minInput.css('left', handles.eq(0).position().left + 35);
			}
			if (maxPlus == true) {
				var leng = parseInt(values[1]).toFixed().length,
					lengMin = parseInt(values[0]).toFixed().length,
					maxval = parseInt(values[1]),
					minval = parseInt(values[0])
				hiddenMin.val(parseInt(values).toFixed());
				if (lengMin >= 4 && lengMin < 7 ) {
					minInput.next('.input-postfix').html('тыс');
					minInput.val((minval / 1000).toPrecision(3));
				} else if (lengMin >= 7 && lengMin < 10) {
					minInput.next('.input-postfix').html('млн');
					minInput.val((minval / 1000000).toPrecision(3));
				} else if (lengMin >= 10) {
					minInput.next('.input-postfix').html('млрд');
					minInput.val((minval / 1000000000).toPrecision(3));
				} else {
					minInput.next('.input-postfix').html('');
					minInput.val(minval);
				}
				hiddenMin.val(parseInt(values[0]).toFixed());
				if(maxInput){
					if(inputMove == '') {
						maxInput.css('left', handles.eq(1).position().left + 35);
					}
					
					if (leng >= 4 && leng < 7 ) {
						maxInput.next('.input-postfix').html('тыс');
						maxInput.val((maxval / 1000).toPrecision(3));
					} else if (leng >= 7 && leng < 10) {
						maxInput.next('.input-postfix').html('млн');
						maxInput.val((maxval / 1000000).toPrecision(3));
					} else if (leng >= 10) {
						maxInput.next('.input-postfix').html('млрд');
						maxInput.val((maxval / 1000000000).toPrecision(3));
					} else {
						maxInput.next('.input-postfix').html('');
						maxInput.val(maxval);
					}
					hiddenMax.val(parseInt(values[1]).toFixed());
				}
				
				if(valMaxRUB != false) {
					var opt_c = $('.price_elite, .price_elite_m').select2("val");
					if(opt_c == "RUB") {
						if(hiddenMax.val() == valMaxRUB) {
							maxInput.val(maxInput.val()+'+')
						}
					} else {
						if(hiddenMax.val() == values_max) {
							maxInput.val(maxInput.val()+'+')
						}
					}
				} else {
					if(hiddenMax.val() == values_max) {
						maxInput.val(maxInput.val()+'+')
					}
				}
			} else {
				minInput.val(parseInt(values).toFixed());
				if(maxInput){
					minInput.val(parseInt(values[0]).toFixed());
					if(inputMove == '') {
						maxInput.css('left', handles.eq(1).position().left + 35);
					}
					maxInput.val(parseInt(values[1]).toFixed());
				}
				if(formatPrice == '') {
					getPriceFormat(minInput);
					if(maxInput){
					getPriceFormat(maxInput);
					}
				}
			}
		}, 50);
	});

	slider.noUiSlider.on('change', function(values){
		if(range.closest('.c-ajax-filter').length){
			minInput.trigger('focus').trigger('blur');
		}

		if(range.hasClass('c-defPriceRange')){
			$('.c-additionPriceRange')[0].noUiSlider.set(slider.noUiSlider.get());		
		}
	});
	
	if(maxPlus == true) {
		minInput.focusout(function(){
			slider.noUiSlider.set([this.value, null]);
			if(inputMove == '') {
				minInput.css('left', handles.eq(0).position().left + 35);
			}
			minInput.prop('disabled', true);
			maxInput.prop('disabled', true);
			setTimeout(function(){
				customFilterActions.sendAjax();
				minInput.prop('disabled',false);
				maxInput.prop('disabled',false);
			},500);
		});
	
		minInput.focus(function(){
			$(this).next('.input-postfix').html('');
			$(this).val(hiddenMin.val());
		});

		if(maxInput){
			maxInput.focusout(function(){
				slider.noUiSlider.set([null, this.value]);
				if(inputMove == '') {
					maxInput.css('left', handles.eq(1).position().left + 35);
				}
				
				minInput.prop('disabled', true);
				maxInput.prop('disabled', true);
				setTimeout(function(){
					customFilterActions.sendAjax();
					minInput.prop('disabled',false);
					maxInput.prop('disabled',false);
				},500);
			});
				
			maxInput.focus(function(){
				$(this).next('.input-postfix').html('');
				$(this).val(hiddenMax.val());
			});
		}
	} else {
		minInput.on('blur', function(){
			slider.noUiSlider.set([this.value, null]);
			if(inputMove == '') {
				minInput.css('left', handles.eq(0).position().left + 35);
			}
		});
		

		if(maxInput){
			maxInput.on('blur', function(){
				slider.noUiSlider.set([null, this.value]);
				if(inputMove == '') {
					maxInput.css('left', handles.eq(1).position().left + 35);
				}
			});
		}
	
	}
}

function showPreloader(){
	$('.c-preloader').addClass('active').show();
}
var filterTimer;
function hidePreloader(){
	$('.c-preloader').removeClass('active').hide();
	if($('.c-aside-popup').length && !$('.select2-container').hasClass('select2-dropdown-open')){
		/*$('.c-elit-catalog').on('mouseover', function(){ 
			filterTimer = setTimeout(function(){  $('.sprite-icon.close-elit').trigger('click');   }, 2000);
			$('.c-elit-catalog').off('mouseover');
		});
		$('#aside-filter').on('mouseover', function(){
			clearTimeout(filterTimer);
		});*/
	}
}

function getPriceFormat(input){
	if(!input.val()){
		return;
	}
	var val = input.val().replace(/\s+/g, '');

	val = parseInt(val).toFixed(0);

	input.val(val);

	input.priceFormat({
		prefix: '',
		centsSeparator: '.',
		thousandsSeparator: ' ',
		centsLimit: 0
	});		
}

function getFormValues(form) {
	var form_arr = {};
	form_arr.text = [];
	form_arr.select = [];
	form_arr.radio = [];
	form_arr.checkbox = [];
	form_arr.hidden = [];

	$.each(form[0], function () {
		var tag = this.tagName.toLowerCase(),
			type = this.type,
			$this = $(this);

		if (type == undefined) {
			type = tag;
		}
		if (type == 'text' || type == 'email' || type == 'date' || type == 'number' || type == 'tel' || type == 'textarea') {
			if($this.hasClass('select2-focusser') || $(this).hasClass('select2-input')) {
			} else {
				form_arr.text.push({
					"name": $this.attr('name'),
					"value": $this.val()
				});
			}
		} else if (type == 'select-one') {
			form_arr.select.push({
				"name": $this.attr('name'),
				"value": $this.val()
			});
		} else if (type == 'checkbox') {
			if ($this.prop("checked") == true) {
				var a1 = $this.attr('name');
				form_arr.checkbox.push({
					"name": $this.attr('name'),
					"value": $this.val()
				});
			}
		} else if (type == "radio") {
			if ($this.prop("checked") == true) {
				form_arr.radio.push({
					"name": $this.attr('name'),
					"value": $this.val()
				});
			}
		} else if (type == "hidden"){
			form_arr.hidden.push({
				"name": $this.attr('name'),
				"value": $this.val()
			});
		}
	});

	return form_arr;
}

function checked() {
	var new_c = $('.c-search_check[data-check="new"]'),
		second = $('.c-search_check[data-check="second"]'),
		cottage = $('.c-search_check[data-check="cottage"]'),
		plots = $('.c-search_check[data-check="plots"]'),
		elem = '.c-search_reg',
		num = 0;

	if ($('input[name="type"]').val() != 'id') {
		if(new_c.prop('checked') == true){
			$(elem).prop('disabled',false).prop('checked',true);
		} else if(second.prop('checked') == true) {
			$(elem).prop('disabled',false).prop('checked',true);
		} else if(cottage.prop('checked') == true || plots.prop('checked') == true) {
			$(elem).prop('disabled',true).prop('checked',false);
			$(elem+'[data-city="region"]').prop('disabled',false).prop('checked',true);
		} else {
			$(elem).prop('disabled',false).prop('checked',false);
		}
	} else {
		$('.search-price').find('select').prop('disabled',true);
		$('.search-price .SumoSelect').addClass('disable');
	}
	
}

function sendFormbyAjax(form){

	var url = form.attr('action'),
		messblock = form.find('.c-ajax-form-mess');

	messblock.hide();
	messblock.removeClass('ajax-form-mess--error');
	messblock.html('');
	showPreloader();
	var emailSubscriber = $(form).find('input[name=email]').val();
	$.ajax({
		url: url,
		type : 'POST',
		dataType: 'JSON',
		data: getFormValues(form),
		success: function (json) {
			messblock.html(json.mess);
			if(json.status == 'error'){
				messblock.addClass('ajax-form-mess--error');
			}else{
				(window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() { 
					rrApi.setEmail(emailSubscriber);	
				});
				
			}
			
			messblock.show();

		},
		error: function (er) {
			messblock.html('Ошибка передачи формы');
			messblock.addClass('ajax-form-mess--error');
			messblock.show();
		},
		complete: function(){
			hidePreloader();
		}
	});
}

// флаг для ообзначения, что открыт попап с картой
var popupFilterMap = false;

$(function () {
	var body = $('body');
	// main blocks
	getTablesView();

	imageMap($('.imageMapster'));
	//reviews
    $('.c-review-author').each(function (){
        var authorLenght = $(this).width();
        $(this).parent().find('.c-review-text-ar').css('margin-left', authorLenght);
	});

	// menu
	body.on('click', '.c-menu-icon', function () {
		var menu = $(this).parent().find('.c-menu-wrap');
		menu.toggle();
		closeElemByClick(menu.parent(), menu);
	})
	.on('click', '.c-dropdownform__link', function(){
		closeElemByClick($('.c-dropdownform'), $(this).next('.c-dropdownform__content'));
	})
	.on('click', '.c-show-contact', function () {
		var info = $(this).parent().find('.c-face-info');

		info.fadeIn();
		closeElemByClick(info.parent(), info);
	})
	.on('click', '.c-compare', function () {
		var compare = $(this).find('.c-compare-drop'),
			closeIcon = compare.find('.c-close-drop');

		compare.fadeIn();
		closeElemByClick(compare.parent(), compare);

		closeIcon.on('click', function (e) {
			e.preventDefault();
			e.stopPropagation();
			compare.hide();
		})
	}).on('click', '.c-accordion_link_trigger', function () {
		var data = $(this).data('accordion');
		$(data).trigger('click');
	})

	.on('click', '.c-showrt', function(e){
		e.preventDefault();

		var link = this;

		if($('.c-compare-building').length) {
			var compare_objects = [];

			$('.c-compare-building').each(function(){
			   compare_objects.push(this.dataset.id);
			});

			$.ajax({
				url: link.dataset.ajax_url,
				type: 'POST',
				data: {objects: compare_objects},
				success: function(){
					window.location.href = '/compare/';
				}
			});
		}
	})
	.on('click', '.c-compare-link', function(e){

		e.preventDefault();
		e.stopPropagation();

		if(this.dataset.to_compare != 'false'){

			var elem = this,
				obj = [];
			obj.push(elem.dataset.object_id);

			showPreloader();

			$.ajax({
				url: elem.dataset.ajax_url,
				type: 'POST',
				data: { objects: obj },
				success: function (count){
					$('.compare-info').html('К сравнению '+count+' объект(-ов)');
					elem.dataset.to_compare = false;
					$(elem).addClass('disable');
				},
				complete: function(){
					hidePreloader();
				}
			});
		}
	}).on('click', '.c-like', function(){
		var count_elem = $('.choose__count'),
			count = parseInt(count_elem.html()),
			elem = $(this);
		if($(this).hasClass('active')) {
			count_elem.html(count - 1);
		} else {
			count_elem.html(count + 1);
		}
	}).on('click', '.c-accordion-search', function(){
		if($(this).hasClass('active')) {
			$(this).find('.c-accordion-text').text('Поиск недвижимости');
		} else {
			$(this).find('.c-accordion-text').text('Развернуть поисковую панель');
		}
	});
	$('.btn_id').on('click',function(e){
		e.preventDefault();
		if($(this).hasClass('back')){
			$(this).text('Поиск по ID');
			$(this).removeClass('back');
			$('.s_check').find('input[type="checkbox"]').prop('disabled',false);
			$('.search-price').find('select, input[type="text"]').prop('disabled',false);
			$('.search-price .SumoSelect').removeClass('disable');
			$('.c-text_ser').removeClass('id').text('Ключевые слова');
			$('input[name="type"]').val('realty');
		} else {
			$(this).text('Вернуться назад');
			$(this).addClass('back');
			$('.s_check').find('input[type="checkbox"]').prop('disabled',true).prop('checked', false);
			$('.search-price').find('select, input[type="text"]').prop('disabled',true);
			$('.search-price .SumoSelect').addClass('disable');
			$('.c-text_ser').addClass('id').text('Введите ID объекта');
			$('.fliter_check__item').hide();
			$('.fliter_check__item input[type="text"]').val('');
			$('input[name="type"]').val('id');
			$.each($(".search_select"),function(i){
				$(".search_select")[i].sumo.unSelectAll();
			});
			
		}
		selectsMainSearch($('.c-custom-select'), $('#autocomplete'));
	});
	
	$('.с-show_count').on('click',function(e){
		e.preventDefault();
		var show = $(this).data('show');
		$('input[name="show"]').val(show);
		$('input[name="pagen"]').val('1');
		$('.search_f').submit();
	});

	var dropDownLinks = $('.c-selectDropdownLink');
	if(dropDownLinks.length){
		dropDownLinks.on('click', function (ev) {
			ev.preventDefault();

			var jQLink = $(this),
				dropDown = jQLink.parent().find('.c-selectDropdown');

			dropDownLinks.not(jQLink).attr('data-cliked', 'N');

			if(jQLink.attr('data-cliked') == 'Y'){
				dropDown.hide();
				jQLink.attr('data-cliked', 'N');
			} else {
				dropDown.fadeIn();
				closeElemByClick(dropDown.parent(), dropDown, true);
				jQLink.attr('data-cliked', 'Y');
			}

			sessionStorage.setItem('currentAdditionFilterIndex', jQLink.parent().index());
		});
	}
		
	$('.c-page_pag').on('click',function(e){
		e.preventDefault();
		var page = $(this).data('page');
		$('input[name="pagen"]').val(page);
		$('.search_f').submit();
	});
	
	$('#btn_search').on('click',function(){
		$('input[name="pagen"]').val('1');
	});


	$('.c-clearSession').on('click', function (e){
		e.preventDefault();
 
		sessionStorage.clear();
		sessionEmpty = true;

		if (this.getAttribute('href') && this.getAttribute('href').length)
			window.location.href = this.getAttribute('href');
	});
	
	function searchBlocks (num) {
		
		$.ajax({
		  url: $('.search_param').data('url'),
		  dataType: 'json',
		  type:'GET',
		  success:function(data){
			$('.fliter_check__item').hide();
			$.each(data[num],function(i){
				$('#'+data[num][i]).show();
			})
		  },
		  error: function (err) {
			console.log(err);
		}
		});
		
		$('.fliter_check__item input[type="text"]').val('');
		$.each($(".search_select"),function(i){
			$(".search_select")[i].sumo.unSelectAll();
		});
	}
	
	
	$('.c-search_check').on('change',function(e){
		e.preventDefault();
		
		checked();
		var new_c = $('.c-search_check[data-check="new"]'),
			second = $('.c-search_check[data-check="second"]'),
			cottage = $('.c-search_check[data-check="cottage"]'),
			plots = $('.c-search_check[data-check="plots"]'),
			elem = '.c-search_reg',
			num = 0;
		if (new_c.prop('checked') == true && second.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true) {
			num = 1;
		} else if(second.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true) {
			num = 2;
		} else if (new_c.prop('checked') == true && second.prop('checked') == true && plots.prop('checked') == true || new_c.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true || new_c.prop('checked') == true && plots.prop('checked') == true) {
			num = 3;
		} else if (new_c.prop('checked') == true && cottage.prop('checked') == true || new_c.prop('checked') == true && second.prop('checked') == true && cottage.prop('checked') == true) {
			num = 4;
		} else if (plots.prop('checked') == true && cottage.prop('checked') == true) {
			num = 5;
		} else if (second.prop('checked') == true && cottage.prop('checked') == true) {
			num = 6;
		} else if (second.prop('checked') == true && plots.prop('checked') == true) {
			num = 7;
		} else if (new_c.prop('checked') == true && second.prop('checked') == true) {
			num = 8;
		} else if (new_c.prop('checked') == true) {
			num = 12;
		} else if (plots.prop('checked') == true) {
			num = 9;
		} else if (cottage.prop('checked') == true) {
			num = 10;
		} else if (second.prop('checked') == true) {
			num = 11;
		} else {
			num = 0;
		}
		
		searchBlocks(num);
		
	});
	if(body.find('.pagewrap--novostroyki').length) {
		body.addClass('novostroyki');
	} else {
		body.removeClass('novostroyki');
	}
	body.on('change', '.c-corp-novostruyki', function() {
		if($('.c-section-novostroyki').find('.active').length) {
			$('.c-section-novostroyki-label').show();
		} else {
			$('.c-section-novostroyki-label').hide();
		}
	});

	// load more text about
	body.on('click', '.c-load-text', function (e) {
		e.preventDefault();

		var elem = $(this),
			textWrap = body.find(elem.data('block'));

		showPreloader();
		$.ajax({
			url: elem.data('url'),
			dataType: 'html',
			success: function (data) {
				textWrap.append(data);

				elem.hide();
			},
			error: function (err) {
				console.log(err);
			},
			complete: function(){
				hidePreloader();
			}
		});
	});

	// get same card height
	var cardList = $('.c-same-height');
	if(cardList.length){
		$.each(cardList, function(){
			var heBlock = $(this).find('.new-stock').outerHeight(true);
			$(this).find('.new-stock__hide').height(heBlock);
		});
		$.each(cardList, function(){
			if($(this).index() % 2 != 0){
				getCardHeight($(this));
			}
		});
	}

	// format price
	var priceInputs = $('.c-format-price');
	if(priceInputs.length){
		$.each(priceInputs, function(){
			var elem = $(this);

			getPriceFormat(elem);	

			elem.on('change', function(){
				getPriceFormat(elem);
			});
		});
	}

	// custom selects
	$(".custom-select").select2({
		minimumResultsForSearch: Infinity
	});

	$(".custom-select--two").select2({
		minimumResultsForSearch: Infinity
	});

	// tabs

	body.on('click', '.c-tabs-link', function (e) {
		e.preventDefault();

		var link = $(this),
			contents = link.closest('.c-tabs').children('.tabs__content').children('.c-tabs-item');

		link.addClass('active').siblings().removeClass('active');
		contents.removeClass('active');
		contents.eq(link.index()).addClass('active');
		if(link.hasClass('c-visible')) {
			if(!link.hasClass('tabs__link--hidden')) {
					link.addClass('tabs__link--hidden');
					$('.c-visible').not(link).removeClass('tabs__link--hidden');
				}
		}
	})
	.on('click', '.c-roadtext-link', function (e) {
		e.preventDefault();
		var link = $(this);
		getCurrentRoadText(link.index());
	})
	.on('click', '.c-map-link', function (e) {
		e.preventDefault();
		siteYandexMap.init($(this));
	})
	.on('click', '.c-jc-objects-link', function (e) {
		e.preventDefault();

		var map = $('#big-map');

		$(this).data('placemarks-json', $('.c-dbl-map').data('placemarks-json'));
		siteYandexMap.init($(this));

		setTimeout(function(){
			map.append('<a href="" title="" class="c-back-link c-map-link back-map-link" data-map-id="big-map" data-placemarks-json="'+$('.c-dbl-map').data('placemarks-json')+'" data-elit="Y" data-has-child="Y">Вернуться к ЖК</a>');
			switchMapFilter(2);
		}, 300);

	})
	.on('click', '.c-back-link', function(e){
		e.preventDefault();
		switchMapFilter(1);
	})
	

	.on('click', '.c-remove-compare', function (e) {
		e.preventDefault();

		var elem = $(this);

		elem.closest('.c-compare-obj').slideUp('100', function () {


			showPreloader();

			$.ajax({
				url: elem.data('ajax-url'),
				data: { OBJ_ID: elem.data('obj-id') },
				success: function (data) {
					console.log('Элемент удален');
				},
				error: function (err) {
					console.log(err);
				},
				complete: function(){
					hidePreloader();
				}
			});
		})
	})
	.on('keypress', '.c-clearable', function () {
		var input = $(this),
			value = input.val(),
			clearLink = input.parent().find('.c-clear-input');

		value.length > 0 ? clearLink.show() : clearLink.hide();

		clearLink.on('click', function (e) {
			e.preventDefault();

			input.val('');
			$(this).hide();

		});
	});

	$('.c-limited-text').each(function(){
		var elem = $(this),
			len = elem.data('length');

		if (elem.text().length > len){
			elem.text(sliceText(elem.text(), len)+'...');
		}
	});

	//compare-block
	getColswidth();
	getColsHeight();
	setTimeout(function(){
		$('.c-dev-map-btns').find('.c-map-link.active').trigger('click');
	},3000);
	if($('.c-load-table.active').length){
		getTableRows($('.c-load-table.active'));
	}

	$('.c-dev-map-btns').on('click', '.c-map-link', function () {
		$(this).addClass('active').siblings().removeClass('active');
	});

    if($('.c-input-phone').length) {
        $('.c-input-phone').mask("+7 (999) 999-9999");
    }
/*dropdown в элитке*/
	$('body').on('click', '.c-dropdownform__link-custom', function(e){
	    e.preventDefault();
	    $('.c-dropdownform__link-custom').not($(this)).removeClass('active');
        $('.c-dropdownform__content-custom').slideUp(300);
	    if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.c-dropdownform__content-custom').slideUp(300);
        } else {
            $(this).addClass('active');
            $(this).next('.c-dropdownform__content-custom').slideDown(300);
        }
    });
	/**/
	var seoBlock = $('.c-seo-suggestions');
	if(seoBlock.length && storageAvailable('sessionStorage')){
		seoBlock.find('.seo-suggestions__item a').on('click', function (e) {
			e.preventDefault();

			sessionStorage.removeItem('customFilter');

			window.location.href = this.getAttribute('href');
		});
	}

	// fixed header
	var fxHeader = $('.c-fixed-header'),
		sliderElit = $('.c-slider-replaced'),
		newContent = $('.c-new-content'),
		suggestions = $('.cq-seo-suggestions');
	if(fxHeader.length){
		if(!sliderElit.length || sliderElit.is(':empty')){
			suggestions.addClass('no-slider');
		}
		else{
			suggestions.hide();
			setTimeout( function(){
				getSeoPosition(sliderElit, fxHeader, suggestions);
				suggestions.show();
			}, 1000 );
		}
		suggestions.addClass('c-mCustomScrollbar');
		
		$(window).on('scroll', function(){
			if($(window).scrollTop() > 50){
				sliderElit.addClass('active-header');
				newContent.addClass('active-header');
				fxHeader.addClass('active');
				suggestions.addClass('active');
			}
			else{
				sliderElit.removeClass('active-header');
				newContent.removeClass('active-header');
				fxHeader.removeClass('active');
				suggestions.removeClass('active');
			}
			getSeoPosition(sliderElit, fxHeader, suggestions);
		});

		$(window).resize(function(){
			if(sliderElit.length){
				getSeoPosition(sliderElit, fxHeader, suggestions);
			}
		});
	}


	body.on('click', '.vprodazhe-table-row .t_d:not(.reserved)', function (e) {
		e.preventDefault();
		popupModule.init($('.c-popup'), $(this).parent());
	});


	// poups 
	body.on('click', '.c-popup-link', function (e) {
		e.preventDefault();
		popupModule.init($('.c-popup'), $(this));
	});

	body.on('click', '.c-popup-link2', function (e) {
		e.preventDefault();
		popupModule.init($('.c-popup2'), $(this));
	});

	if($('#newYearMess').length){
		popupModule.callOnLoadFunction = function () {
			$('.c-popup').addClass('new-image');
			$('.b-modal').addClass('transparented');
		};
		popupModule.callOnCloseFunction = function () {
			$('.c-popup').removeClass('new-image');
			$('.b-modal').removeClass('transparented');
		};
		$('#newYearMess').trigger('click');
	}

	// validation
	var validatorLanguage = {
		errorTitle: 'Ошибка отправки формы!',
		requiredFields: 'Необходимо заполнить обязательные поля',
		badTime: 'Введите корректное время',
		badEmail: 'Введите корректный e-mail адрес',
		badTelephone: 'Введите корректный номер телефона',
		badSecurityAnswer: 'Ответ на секретный вопрос не верен',
		badDate: 'Введите корректную дату',
		lengthBadStart: 'Значение должно быть между ',
		lengthBadEnd: ' знаками',
		lengthTooLongStart: 'Значение поля больше, чем ',
		lengthTooShortStart: 'Значение поля меньше, чем ',
		notConfirmed: 'Input values could not be confirmed',
		badDomain: 'Введите корректное доменное имя',
		badUrl: 'Значение поля не является корректным url-адресом',
		badCustomVal: 'Зачение поля не верно',
		andSpaces: ' и пробелов ',
		badInt: 'Поле может содержать только цифры',
		badSecurityNumber: 'Your social security number was incorrect',
		badUKVatAnswer: 'Incorrect UK VAT Number',
		badStrength: 'The password isn\'t strong enough',
		badNumberOfSelectedOptionsStart: 'You have to choose at least ',
		badNumberOfSelectedOptionsEnd: ' answers',
		badAlphaNumeric: 'The input value can only contain alphanumeric characters ',
		badAlphaNumericExtra: ' и ',
		wrongFileSize: 'Максимальный размер файла %s',
		wrongFileType: 'Разрешениы следующие форматы файлов: %s',
		groupCheckedRangeStart: 'Выберите между ',
		groupCheckedTooFewStart: 'Выберите не меньше, чем ',
		groupCheckedTooManyStart: 'Выберите не больше, чем ',
		groupCheckedEnd: ' item(s)',
		badCreditCard: 'The credit card number is not correct',
		badCVV: 'The CVV number was not correct',
		wrongFileDim: 'Incorrect image dimensions,',
		imageTooTall: 'the image can not be taller than',
		imageTooWide: 'the image can not be wider than',
		imageTooSmall: 'the image was too small',
		min: 'мин.',
		max: 'макс.',
		imageRatioNotAccepted: 'Image ratio is not accepted'
	};

	$.validate({
		validateOnBlur: false,
		errorMessagePosition: 'top',
		scrollToTopOnError: false,
		language: validatorLanguage,
		borderColorOnError: '#e41919',
		onSuccess : function($form){
			// send form by ajax
			if($form.hasClass('c-ajaxForm')){

				sendFormbyAjax($form);

				return false;
			}
		}
	});

	// выводим карту с маршрутом на первый активный таб
	if ($('.c-road-tabs').length) {
		getRouteToActiveTab();
	}

	var geolocationLinks = $('.c-geolocationLink');
	if (typeof(ymaps)!='undefined') {
		ymaps.ready(function (){
			if (geolocationLinks.length) {
				siteYandexMap.getCurrentUserPosition();
			}
		});
	}

	// Карта сабагентам
	var subagents_map = $('#subagents_map');
	if(subagents_map.length) {
		siteYandexMap.init(subagents_map);
	}

	// зум карты
	body.on('click', '.c-zoom-map', function (e) {
		e.preventDefault();

		var link = $(this);

		popupFilterMap = true;

		$('.c-popup .popup__content').empty();
		$('.c-popup').removeAttr('style');

		popupModule.callOnLoadFunction = function () {
			setTimeout(function () {
				siteYandexMap.init(link);
			}, 300);
		}

		popupModule.callOnCloseFunction = function () {
			popupFilterMap = false;
			getRouteToActiveTab();
		}

		popupModule.init($('.c-popup'), link);
	});

	// аккордион

	var accordionsLinks = body.find('.c-accordion-link');
	if (accordionsLinks.length) {
		$.each(accordionsLinks, function () {
			var link = $(this),
				accordion = link.closest('.c-accordion'),
				drops = accordion.find('.c-accordion-drop'),
				links = accordion.find('.c-accordion-link');

			link.on('click', function (e) {
				e.preventDefault();
				var elem = $(this),
					currDrop = elem.next('.c-accordion-drop');

				links.not(elem).removeClass('active');
				elem.toggleClass('active');

				currDrop.slideToggle(function () {
					if(!accordion.hasClass('no-scroll-action')){
						$('html, body').stop().animate({ scrollTop: elem.offset().top - 200 });
					}
				});

				drops.not(currDrop).slideUp();

				if (currDrop.find('.c-direction-list').length) {
					var list = currDrop.find('.c-direction-list'),
						checkboxes = list.find('.checkbox-wrap'),
						n = 0, _height = 0;

					while ((n < 5) && (n < checkboxes.length)) {
						_height = _height + checkboxes.eq(n).height() + 16;
						n++;
					}

					list.height(_height + 5);
				}
				var dropRange = currDrop.find('.c-range');
				if(dropRange.length){
					$.each(dropRange, function(){
						var elem = $(this),
							handles = elem.find('.noUi-origin');

						elem.find('.c-range-min').css('left', handles.eq(0).position().left + 35);
						elem.find('.c-range-max').css('left', handles.eq(1).position().left + 35);
					});
				}
			})
		});
	}
/*иконка "закрыть" для формы "предложить свою цену*/
    $('body').on('click', '.c-dropdownform__content-custom .c-aside-close', function(e) {
        e.preventDefault();
        $(this).parent('.c-dropdownform__content-custom').slideUp(300);
        $(this).parent('.c-dropdownform__content-custom').siblings('.c-dropdownform__link-custom').removeClass('active');
        $('div.elit-detail').css('z-index', '1');
    });
/*end*/

		$('body').on('click', '.c-dropdownform__link', function(e){
		e.preventDefault();
		var elem = $(this);
		
		elem.parents('.aside-menu').addClass('z_aside');
		
		if (elem.hasClass('active')) {
			elem.removeClass('active');
			elem.next('.c-dropdownform__content').slideUp(300);


		} else {


			$('.c-dropdownform__link').not(elem).removeClass('active');
			elem.addClass('active');
			$('.c-dropdownform__link').not(elem).next('.c-dropdownform__content').hide();
			elem.next('.c-dropdownform__content').slideDown(300);
		}

	});

	var customScrollbar = $('.c-mCustomScrollbar');
	if (customScrollbar.length) {
		$.each(customScrollbar, function(){
			var elem = $(this),
				options = {
					scrollButtons:{ enable: true }
				};

			elem.mCustomScrollbar(options);
		});
	}

	body.on('click', '.c-aside-popup-link', function (e) {
		e.preventDefault();

		var elemId = $(this).data('aside-id');

		body.find('.c-aside-popup').removeClass('active');
		body.find(elemId).addClass('active');
		if (elemId == '#aside-filter'){
			$('.c-aside-layer').addClass('active');
			body.addClass('no-scroll');
		}		
	})
	.on('click', '.c-aside-close', function (e) {
		e.preventDefault();

		$(this).closest('.c-aside-popup').removeClass('active');
		$('.c-aside-layer').removeClass('active');
		body.removeClass('no-scroll');
	})
	.on('click', '.c-more-obj-info', function (e) {
		e.preventDefault();

		$(this).siblings('.c-add-text').slideToggle(100);
	})
	.on('click', '.accordion_trigger__link', function () {
		$('.accordion_trigger').trigger('click');
	})
	.on('click', '.c-fake-tab-active', function (e) {
		e.preventDefault();

		var tabs = $(this).closest('.c-tabs'),
			mapLink = tabs.find('.c-tabs-link-map'),
			photoLink = tabs.find('.c-tabs-link-photo'),
			items = tabs.find('.c-tabs-item'),
			links = tabs.find('.c-tabs-link');

		items.removeClass('active');
		links.removeClass('active');
		links.eq(photoLink.index()).addClass('active');
		items.eq(mapLink.index()).addClass('active');
	})
	.on('click', '.c-tabs-link-photo', function () {
		//siteYandexMap.destroy();
	});
	$('.c-subagents').mouseleave(function(){
		var link = $(this).find('.c-dropdownform__link');
		if(link.hasClass('active')) {
			link.trigger('click');
		}
	});

	// загрузка фото по датам
	var dataSlider = $('#date-slider');
	if (dataSlider.length) {
		var buttons = $('.c-date-buttons'),
			ajax_url = buttons.data('ajax-url');

		body.on('click', '.c-change-month, .c-change-year', function (e) {
			e.preventDefault();

			var elem = $(e.target);

			if (!elem.hasClass('disable')) {

				elem.addClass('active');
				elem.siblings().removeClass('active');

				var _year = buttons.find('.c-change-year.active'),
					_month = buttons.find('.c-change-month.active');

				showPreloader();

				$.ajax({
					url: ajax_url,
					data: { year: _year.data('year'), month: _month.data('month') },
					dataType: 'JSON',
					success: function (data) {
						var fotorama = dataSlider.data('fotorama'),
							template = '',
							months = buttons.find('.c-change-month');
						fotorama.destroy();

						if (data.images !== null && data.images !== undefined) {

							$.each(data.images, function () {
								var links = $(this);
								template += '<a href="' + links[0]['PHOTO'] + '" title=""><img src="' + links[0]['THUMB'] + '" alt=""></a>';
							});

							dataSlider.html(template);
							dataSlider.fotorama();
						}
						
						if (elem.hasClass('c-change-year')) {
							if (data.dis_months !== undefined) {
								months.removeClass('disable');
								for (i = 0; i < data.dis_months.length; i++) {
									months.eq(data.dis_months[i]).addClass('disable');
								}
							}
							months.removeClass('active');
							months.eq(data.active_month-1).addClass('active');
						}
					},
					error: function (er) {
						console.log(er)
					},
					complete: function(){
						hidePreloader();
					}
				});
			}
		});
	}

	var rangeList = $(".c-range");
	if (rangeList.length > 0) {
		$.each(rangeList, function () {
			var elem = $(this);

			rangeNoUiSlider(elem);

			if (elem.attr('id') == 'summ') {
				elem[0].noUiSlider.on("change", function () {
					getCurrPercent($('.c-first-summ'), $('.c-summ-describe'));
				});
			}
		});
	}

	// ипотечный калькулятор
	var creditForm = $('#credit-form');
	if (creditForm.length > 0) {
		var fistSummInput = creditForm.find('.c-first-summ'),
		decsribeBlock = creditForm.find('.c-summ-describe');

		fistSummInput.on('keydown', function (e) {
			if (e.keyCode < 48 || (e.keyCode > 57 && e.keyCode < 96 )) {
				return false;
			}
			$(this).on('blur', function () {
				getCurrPercent($(this), decsribeBlock);
			});
		});

		$.validate({
			form: '#credit-form',
			validateOnBlur: false,
			errorMessagePosition: 'top',
			scrollToTopOnError: false,
			language: validatorLanguage,
			borderColorOnError: '#e41919',
			onSuccess: function ($form) {
				var formData = {};

				formData.summ = creditForm.find('[name="summ"]').val();
				formData.firstSumm = creditForm.find('[name="firstSumm"]').val();
				formData.age = creditForm.find('[name="age"]').val();
				formData.sex = creditForm.find('[name="sex"]:checked').val();
				formData.type = creditForm.find('[name="type"]:checked').val();
				formData.ready = creditForm.find('[name="ready"]:checked').val();
				formData.certificate = creditForm.find('[name="certificate"]:checked').val();
				formData.experience = creditForm.find('[name="experience"]:checked').val();
				formData.workType = creditForm.find('[name="work-type"]:checked').val();
				formData.period = parseInt($('#period')[0].noUiSlider.get()).toFixed();
				formData.monthSumm = creditForm.find('[name="monthSumm"]').val();
				formData.firstSummPercent = $('.c-first-summ-percent').val();

				creditSendAjax(creditForm, formData);

				body.on('click', '#show_more',function(e){
					e.preventDefault();

					formData.tableStep = $(e.target).data('page');
					creditSendAjax(creditForm, formData);
				});

				return false;
			}
		});
	}

	// Лайк объекта
	body.on('click', '.c-like', function () {
			var elem = this,
				src_url = elem.dataset.ajax_url;

			showPreloader();

			$.ajax({
				url: src_url,
				data: { OBJ_ID: elem.dataset.obj_id, ACTION: elem.dataset.action},
				dataType: 'JSON',
				success: function (data) {
					if (data['status']) {
						$(elem).toggleClass('active');

						if (elem.dataset.action == 'like') {
							//меняем action
							elem.dataset.action = 'dislike';
						}
						else {
							if ($(elem).closest('.c-my-choice').length) {
								$(elem).closest('.new-building').remove();
							}
							else {
								elem.dataset.action = 'like';
							}
						}
					}
				},
				error: function (er) {
					console.log(er)
				},
				complete: function(){
					hidePreloader();
				}
			});

	})
	.on('click', '.c-plus_input', function () {

		var paste = '<div class="form-line"><input type="email" placeholder="e-mail" data-validation="email" class="form-input"><span class="plus-input c-plus_input">+</span></div>',
			btn = $(this),
			elem = btn.parents('.form-line');

		elem.find('.c-plus_input').remove();
		elem.after(paste);
	})
	.on('click', '.delete-manager', function () {
		var elem = $(this).parents('.managers__item');

		elem.slideUp(200, function () {

			showPreloader();
			$.ajax({
				url: elem.data('ajax-url'),
				data: { managerId: elem.data('manager-id') },
				success: function () {
					console.log('Менеджер удален')
				},
				error: function (er) {
					console.log(er)
				},
				complete: function(){
					hidePreloader();
				}
			});
		});
	})
	.on('click', '.c-enable_input', function () {
		var elem = $(this).parents('.c-container_input').find('.c-form_input');
		elem.removeAttr('disabled').focus();
	});

	var customFilter = $('.c-ajax-filter, .c-additionFilter-trade-in');
	if (customFilter.length) {
		customFilterActions.init(customFilter);
	}

	body.on('click', '.c-step-link', function(e){
		e.preventDefault();

		var arrSteps = ($(this).data('step') + '').split(',');
		steps = $('.c-step');
		steps.hide();

		jQuery.each(arrSteps,function () {
			$('#step'+this).fadeIn(100);
		});

	/*	currStep.fadeIn(100, function(){
			var scrollBlock = currStep.find('.c-mCustomScrollbar');
			if(scrollBlock.length){
				scrollBlock.css('height', '520px');
			}
		});
	*/
	});

	body.on('click', '.c-redirect-link', function(e){
		
		e.preventDefault();
		window.location.href = $(this).data('url');

	}).on('click', '.c-load-table', function(e){
		e.preventDefault();

		getTableRows($(this));
	});

	$('#aside-filter form').on('submit', function(){
		customFilterActions.sendAjax();
		$('.c-aside-close').trigger('click');
		return false;
	});

	if($('#open_modal').length){
		popupModule.callOnLoadFunction = function () {
			$('.c-popup').addClass('new-image');
			$('.b-modal').addClass('transparented');
			$('body').css('overflow','hidden');
		};
		popupModule.callOnCloseFunction = function () {
			$('.c-popup').removeClass('new-image');
			$('.b-modal').removeClass('transparented');
			$('body').removeAttr('style');
		}
		$('#open_modal').trigger('click');
	}

	$('.c-clear_form input[type="submit"]').on('click',function(){
		setTimeout(function(){
			$('.c-clear_form').trigger( 'reset' );
		},300);
	});
	
	if(!$('.c-slider-replaced').length || $(window).height() > 880){
		var winh = $(window).height();
		if(!$('.c-slider-replaced').length) {
			winh = winh+470;
		} else {
			winh = winh-90;
		}
		$('.elit-sug .elit-container .seo-suggestions').css('top', winh+'px');
	}

	$(window).on('scroll', function(){
		var $sideButtons = $('.side_buttons');
		var top = $(window).scrollTop();

		if (top >= 775) {
			$sideButtons.addClass('fixed');
		} else if (top < 775) {
			$sideButtons.removeClass('fixed');
		}
	});
    $('.c-dropdownform-hover').hover(function () {
        $(this).find('.drop-form').show().find('*').filter(':input:visible:first').focus();
		$(".c-dropdownform-hover .drop-form").not($(this).find('.drop-form')).hide();
        closeElemByClick($('.c-dropdownform-hover'), $(this).find('.drop-form'));
    });
    $('.c-dropdownform__link').click(function () {
        $(this).parent().find('.drop-form').show().find('*').filter(':input:visible:first').focus();
        $(".c-dropdownform .drop-form").not($(this).parent().find('.drop-form')).hide();
        $('html, body').animate({ scrollTop: $(this).parent().find('.drop-form').offset().top - 200 }, 500);
        closeElemByClick($('.c-dropdownform'), $(this).parent().find('.drop-form'));
    });
    $('.c-dropdownform__link-custom').click(function () {
    	$('div.elit-detail').css('z-index', '2');
        $(this).parent().find('*').filter(':input:visible:first').focus();
    });
});


function setPlaceHolders(){
	var input = document.getElementsByTagName('input'); // get all text fields
	var cls = "placeholdr"; // set name of the class

	if (input) { // if fields found
		for (var i=0; i < input.length; i++) {
			var t = input[i];
			var txt = t.getAttribute("placeholder");
			if (!txt) {
				continue;
			}
			if (txt.length > 0) { // if placeholder found
				t.className = t.value.length == 0 ? t.className+" "+cls : t.className; // add class
				t.value = t.value.length > 0 ? t.value : txt; // if no value found

				t.onfocus = function() { // on focus
				this.className = this.className.replace(cls);
				this.value = this.value == this.getAttribute("placeholder") ? "" : this.value;
				}

				t.onblur = function() { // on focus out
					if (this.value.length == 0) {
					this.value = this.getAttribute("placeholder");
					this.className = this.className+" "+cls; // add class
					}
				}
			}
		}
	}
};

