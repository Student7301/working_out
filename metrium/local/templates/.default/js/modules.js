// custom filter

sessionStorage.setItem('ajaxL',0);
sessionStorage.removeItem('currentAdditionFilterIndex');

var customFilterActions = (function () {
	var module = {},
		isFilterEmpty = true,
		firstPageFlag = true,
		sessionEmpty = true,
		isDirectionChecked = false,
		isAlreayUsed = false,
		prevPage = 1,
		staticStore = {
			timer: [300, 500, 100],
			suggText: 'Всего предложений: ',
			suggTest: ['предложение','предложения','предложений'],
			linkPrev: 'Предыдущая',
			separateText: '...',
			linkNext: 'Следующая',
			defaultPrice: 'Цена по запросу',
			idText: 'ID объекта',
			ajaxLikeUrl: '/local/templates/.default/ajax/like.php'
		};


	module.init = function (filter) {

		this.filter = filter;
		this.directions = this.filter.find('.c-direction-select-able .checkbox-label');
		this.directionsCheckbox = $('.c-direction-select-able .checkbox');
		this.directionsCheckboxPopup = $('.c-ajax-filter .direction-list .checkbox'); // чекбоксы в всплывашке фильтра
		this.buildings = this.filter.find('.c-building-select-able .checkbox-label');
		this.checkOptions = this.filter.find('.c-check-options .checkbox-label');
		this.inputOptions = this.filter.find('.c-input-option');
		this.selectAllLink = this.filter.find('.c-select-all');
		this.clearAllLink = this.filter.find('.c-deselect-all');
		this.removeDirectionLink = this.filter.find('.c-remove-direction');
		this.ajaxUrl = this.filter.data('ajax_url');
		this.pagination = $('.c-pagination'),
		this.paginationLinks = this.pagination.find('.c-pagination-link'),
		this.showBy = $('.c-show-by'),
		this.sort = $('.c-sort-elit'),
		this.mapLink = $('.c-filter-map'),
		this.stopAjax = false;
		this.needAutoSend = true;
		this.defaultFilter = window.defaultFilterInputOptions || {};
		this.sendBtn = this.filter.find('.с-btn-search');

		if(filter.hasClass('c-no-autosend'))
		{
            this.needAutoSend = false;
		}

		this.selectAllLink.on('click', function (e) {
			e.preventDefault();
			module._onSelectAllDirections();
		});

		this.clearAllLink.on('click', function (e) {
			e.preventDefault();
			module._onClearAllDirections();
		});

		this.removeDirectionLink.on('click', function (e) {
			e.preventDefault();
			module._onRemoveDirection($(this).parent().data('value'));
		});

		this.checkOptions.on('click', function (e) {
			module._onCheckOptionsClick(this);
		});

		this.inputOptions.on('blur', function () {
			if (!isAlreayUsed) {
				module._onInputChange();
			}
			isAlreayUsed = false;
		});

		this.inputOptions.on('keyup', function (ev) {
			if (ev.keyCode == 13) {
				isAlreayUsed = true;
				module._onInputChange();
			}
		});

		this.showBy.find('.c-show-by-link').on('click', function (e) {
			e.preventDefault();
            if (!module.needAutoSend) {
                module.needAutoSend = true;
                module._onShowByLinkClick($(this));
                module.needAutoSend = false;
                return;
            }
            module._onShowByLinkClick($(this));
		});
		this.showBy.find('.c-clear-all').on('click', function (e) {
			e.preventDefault();
			module.showBy.find('.active').removeClass('active');
			firstPageFlag = true;
			module.sendAjax();
		});

		this.sort.find('.c-sort-link').on('click', function (e) {
			e.preventDefault();
            module.needAutoSend = true;
			module._onSortLinkClick($(this));
            module.needAutoSend = false;
		});

		this.paginationLinks.on('click', function (e) {
			e.preventDefault();
                module.needAutoSend = true;
                module._onPaginationLinkClick($(this));
                module.needAutoSend = false;
		});

		initAdditionFilter();

		this.initDirection();


		if (sessionStorage.getItem('customFilter')){
			sessionEmpty = false;
            this.sendAjax();
		}

		this.sendBtn.on('click', function (e) {
            e.preventDefault();
            if (!module.needAutoSend) {
                module.needAutoSend = true;
                history.pushState(null, null, '/trade-in/');
                $('.c-elit-navigation__link').removeClass('active');
                module.sendAjax();
                module.needAutoSend = false;
            }
		});
	}

	module._onRemoveDirection = function (value) {
		this.stopAjax = true;

		this.removeDirection(value);

		this.stopAjax = false;
		isDirectionChecked = true;
		this.sendAjax();
	}

	module._onSelectAllDirections = function () {
		this.stopAjax = true;

		// чекбоксы в попапе фильтра
		$.each(module.directionsCheckboxPopup, function () {
			var elem = $(this);			
			if (!elem.is(':checked') && !elem.hasClass('active')) {				
				elem.next().trigger('click');
			}
		});

		// чекбоксы в простом фильтре
		$.each(module.directionsCheckbox, function () {
			var elem = $(this);				
			if (!elem.is(':checked')  && !elem.hasClass('active')) {					
				elem.next().trigger('click');				
			}
		});

		this.stopAjax = false;
		isDirectionChecked = true;
		this.sendAjax();

		return false;
	}

	module._onInputChange = function (elem) {
		sessionStorage.removeItem('currentAdditionFilterIndex');

		this.sendAjax();
	}

	module._onClearAllDirections = function () {
		this.stopAjax = true;		
		// чекбоксы в попапе фильтра
		$.each(module.directionsCheckboxPopup, function () {
			var elem = $(this);
			if (elem.is(':checked') || elem.hasClass('active')) {				
				elem.next().trigger('click');				
			}
		});
		
		// чекбоксы в простом фильтре
		$.each(module.directionsCheckbox, function () {
			var elem = $(this);
			if (elem.is(':checked') || elem.hasClass('active')) {				
				elem.next().trigger('click');				
			}
		});

		this.stopAjax = false;
		isDirectionChecked = true;
		this.sendAjax();

		return false;
	}

	module._onSelectDirection = function (label) {
			var selectDirectionsWrap = this.filter.find('.c-selected-direction'),
			jqLabel = $(label),
			input = jqLabel.prev('input');

		if (label.dataset.checked == 'Y') {
			var text = jqLabel.find('.checkbox-text').text();

			text = text.substr(0, text.indexOf("("));

			selectDirectionsWrap.append('<div class="selected-direction__item" data-value="' + input.val() + '"><i class="sprite-icon close-white-small c-remove-direction"></i>' + text + '</div>');
		}
		else {
			selectDirectionsWrap.find('[data-value = "' + input.val() + '"]').remove();
		}

		this.updateRemoveLinks();

		this.removeDirectionLink.off('click').on('click', function (e) {
			e.preventDefault();
			module._onRemoveDirection($(this).parent().data('value'));
		});

		isDirectionChecked = true;

		this.sendAjax();
	}

	module._onCheckOptionsClick = function (elem) {
		var input = $(elem).prev(),
			additionInput = this.additionFilter.filter.find('[value="' + input.val() + '"]');

		if (input.attr('type') == 'radio') {
            var container = $(elem).parent().parent();
            $('label', container).each(function() {
                this.dataset.checked = 'N';
            });
			elem.dataset.checked = 'Y';
		}
		else {
			if(elem.dataset.checked == 'Y'){
				elem.dataset.checked = 'N';
				//input.prop('checked',false);
				if(additionInput.length){
					additionInput.removeClass('active');
				}
			} else {
				elem.dataset.checked = 'Y';
				//input.prop('checked',true);
				if(additionInput.length){
					additionInput.addClass('active');
				}
			}
		}

		isDirectionChecked = false;

		this.sendAjax();
	}

	module._onPaginationLinkClick = function(elem){

		prevPage = this.pagination.find('.active').children('.c-pagination-link').data('page_num');

		elem.parent().siblings().removeClass('active');		
		elem.parent().addClass('active');

		firstPageFlag = true;

		sessionStorage.removeItem('currentAdditionFilterIndex');

		this.sendAjax();
	}

	module._onShowByLinkClick = function(elem){

		elem.parent().siblings().removeClass('active');
		elem.parent().addClass('active');

		firstPageFlag = false;

		sessionStorage.removeItem('currentAdditionFilterIndex');

		this.sendAjax();
	}

	module._onSortLinkClick = function(elem){
		elem.siblings().removeClass('active');
		elem.addClass('active');
		elem[0].dataset.sort_order == 'desc' ? elem[0].dataset.sort_order = 'asc' : elem[0].dataset.sort_order = 'desc';
		sessionStorage.removeItem('currentAdditionFilterIndex');

		this.sendAjax();
		$('.c-new-propose').remove();
	}

	module._onAjaxSuccess = function(json){

		var customTemplates = {};

		$('.c-sort-result').text(staticStore.suggText + json.itemsCount);

		$('.fixed-bottom.c-aside-close').addClass('text-result-filter-button');

		$('.c-sortResultPoup').text('(' + json.itemsCount + ') ' + declension(json.itemsCount, staticStore.suggTest));

		// показывать по:
		var currShowbyItem = this.showBy.find('[data-item_count = "'+json.showBy+'"]');

		this.showBy.children().removeClass('active');
		currShowbyItem.parent().addClass('active');

		// пагинатор:
		var pagerTemplate = '',
			currPage = json.pager.active;

		if(parseInt(currPage) != 1){
			pagerTemplate += '<li class="pagination__item prev">\
								<a class="pagination__link c-pagination-link" href="" title="" data-page_num="minus">' + staticStore.linkPrev + '</a>\
							</li>';
		}

		$.each(json.pager.pages, function(key, val){
			if(val != 'separator'){
				var itemClass= 'pagination__item';

				if(val == currPage){
					itemClass = 'pagination__item active';
				}
				pagerTemplate += '<li class="'+itemClass+'">\
								<a class="pagination__link c-pagination-link" href="" title="" data-page_num="'+val+'">'+val+'</a>\
							</li>';
			}
			else{
				pagerTemplate += '<li class="pagination__item separate">\
								<span class="separate__text">' + staticStore.separateText + '</span>\
							</li>';	
			}
		});

		if(parseInt(currPage) != json.pager.pages.length){
			pagerTemplate += '<li class="pagination__item next">\
								<a class="pagination__link c-pagination-link" href="" title="" data-page_num="plus">' + staticStore.linkNext + '</a>\
							</li>';
		}

		customTemplates.paginator = pagerTemplate;

		// сортировка:
		this.sort.find('.c-sort-link').removeClass('active');
		
		if(json.sortBy.type != ''){
			this.sort.find('[data-sort-type="'+json.sortBy.type+'"]').addClass('active');
		}
		
		// список объектов:

		var template = '<div class="elit-container clearfix">';

		$.each(json.items, function(key, obj){
			if(key % 2 == 0 && key != 0){
				template += '</div><hr><div class="elit-container clearfix">';
			}

			template += '<div class="elit-item">\
							<div class="building-image">\
								<a class="absolute-link" href="'+obj.url+'" title=""></a>';

			if(obj.image != ''){
				template += '<img src="'+obj.image+'" alt="" >';
			}

			template += '</div>'; // close building-image

			template += '<div class="building-info-wrap">\
							<div class="building-info">\
								<div class="building-info__col left">\
									<a class="building-name" href="' + obj.url + '" title="">' +obj.name +'</a>\
									<div class="building-direction">' + obj.direction + '</div>\
									<div class="building-direction">' + obj.status + '</div>\
									<div class="building-actions clearfix">';

			if(obj.photoCount != undefined){
				template += '<div class="building-foto left">\
								<i class="sprite-icon foto-black"></i>\
								<span class="building-foto__count">'+obj.photoCount+'</span>\
							</div>';
			}

			if(obj.videoCount != undefined){
				template += '<div class="building-video left">\
								<i class="sprite-icon video-black"></i>\
								<span class="building-video__count">'+obj.videoCount+'</span>\
							</div>';
			}

			template += '</div>\
								</div>\
								<div class="building-info__col building-info__col--small right">\
									<div class="building-id building-id--price">';

			if(obj.price.usd !== staticStore.defaultPrice){
				template += '<div class="price price--dbl">'+obj.price.usd+'<span class="multi-valute">'+obj.price.rub+'</span></div>';
			}
			else{
				template += '<div class="price">' + staticStore.defaultPrice + '</div>';
			}

			template += '</div>';

			if(obj.id != null){
				template += '<div class="building-id building-id--text">\
								<div class="building-id__text">' + staticStore.idText + '</div>\
								<div class="building-id__number">'+obj.id+'</div>\
							</div>';
			}

			if(obj.propsList.length > 0){
				template += '<div class="info-list">';

				$.each(obj.propsList, function(key, val){
					template += '<div class="info-list__item">'+val+'</div>';
				});

				template += '</div>';
			}

			template += '		</div>\
							</div>\
						</div>'; // close building-info-wrap

			// like block
			var _class = 'like-block c-like';

			if(obj.likeBlock.action == "dislike"){
				_class += ' active';
			}

			template += '<div class="'+_class+'" data-obj_id="'+obj.objId+'" data-action="'+obj.likeBlock.action+'" data-ajax_url="' + staticStore.ajaxLikeUrl + '">\
							<i class="sprite-icon like-elit"></i>\
							<i class="sprite-icon like-elit-active"></i>\
						</div>';
							
			template += '</div>'; // close elit-item
		});

		template += '</div><hr>'; // close elit-container

		customTemplates.items = template;

		// слайдер
		var sliderTemplate = '';
		if(json.sliderImgs.length){
			sliderTemplate += '<div data-nav="false" data-autoplay="6000" data-arrows="false" data-width="100%" data-click="false" data-ratio="32/11" data-fit="cover" class="fotorama elit-addition-slider">';

			$.each(json.sliderImgs, function(key, obj){
	
				sliderTemplate += '<div class="position-block fotorama-image-block">\
									<a href="'+obj.url+'" title="" class="absolute-link"></a>\
									<img src="'+obj.image+'" alt="">\
								  </div>';
			});

			sliderTemplate += '</div><hr>';
		}
		else{
			$('.c-seo-suggestions').addClass('no-slider');
		}

		customTemplates.slider = sliderTemplate;

		return customTemplates;
	}

	module.initDirection = function () {
		var selectDirectionsWrap = this.filter.find('.c-selected-direction');

		module.stopAjax = true;

		$.each(module.directions, function() {
			if(this.dataset.checked == 'Y'){
				module._onSelectDirection(this);
			} else {
				$(this).prev('input').removeAttr('checked');
			}
		});

		module.stopAjax = false;

		module.directions.off('click').on('click', function () {
			var elem = this,
				input = $(elem).prev('input'),
				additionInput = module.additionFilter.filter.find('[value="' + input.val() + '"]');

			if(elem.dataset.checked == 'Y'){
				elem.dataset.checked = 'N';

				if(additionInput.length){
					additionInput.removeClass('active');
				}
			} else {
				elem.dataset.checked = 'Y';
				if(additionInput.length){
					additionInput.addClass('active');
				}
			}

			module._onSelectDirection(elem);
			if (isDirection){
				var link_u = document.location.pathname.split('/');
				//window.location.href = '/'+link_u[1]+'/';
				history.pushState(null, null, '/'+link_u[1]+'/');
				$('.page-title-wrap').hide();
			}
		});
	}

	module.removeDirection = function (value) {
		var input = this.filter.find('input[value="' + value + '"]');
		
		$(input).next('label').trigger('click');
	}

	module.updateRemoveLinks = function () {
		this.removeDirectionLink = this.filter.find('.c-remove-direction');
	}

	module.updatePaginationLinks = function () {
		this.paginationLinks = this.pagination.find('.c-pagination-link');

		this.paginationLinks.on('click', function (e) {
			e.preventDefault();
            module.needAutoSend = true;
			module._onPaginationLinkClick($(this));
            module.needAutoSend = false;
		});
	}

	module.updateRangeOptions = function (currData) {
		$.each(currData.filter.inputOptions, function(){
			var inputName = this.name,
				inputVal = this.value,
				inputEl = $('input[name="'+inputName+'"]'),
				wrapRange = inputEl.parents('.main-filter__drop');

			if (!wrapRange.length) {
				wrapRange = inputEl.parents('.c-rangeWrap');
			}

			var range = wrapRange.find('.c-range');

			if(inputName != 'currentValue') {
				var valueToEnter = [inputVal, null];

				if(!inputEl.hasClass('c-range_from')){
					valueToEnter = [null, inputVal];
				}

				setTimeout(function(){
					range[0].noUiSlider.set(valueToEnter);
				},10);
				
			} else {
				$('select[name="'+inputName+'"]').val(inputVal).trigger("change");
			}
		});

		return false;
	}

	module.getPaginationValue = function(flag){
		var currPage;

		if(!flag){
			currPage = 1;
		}
		else{
			var link = this.pagination.find('.active');

			var num = link.children().data('page_num');

			switch(num){
				case 'plus':
					currPage = prevPage + 1;
					break;
				case 'minus':
					currPage = prevPage - 1;
					break;
				default:
					currPage = num;
					break;
			}
		}

		return currPage;
	}

	module.getShowByValue = function(){
		var link = this.showBy.find('.active');

		return link.children().data('item_count');
	}

	module.getSortByValue = function(){
		var link = this.sort.find('.active'),
			sort = {};

		if(link.length){
			sort.type = link.data('sort-type');
			sort.order = link[0].dataset.sort_order;
		}

		return sort;
	}

	module.getAjaxData = function(){
		var dataObj = {};
		dataObj.filter = this.getFilterValues();

		dataObj.showBy = this.getShowByValue();

		dataObj.sort = this.getSortByValue();

		var flag = true,
			ajaxCount = parseInt(sessionStorage.getItem('ajaxL'));

		/* показывать ли первую страницу и новые предложения */
		if(ajaxCount > 0){
			flag = JSON.stringify(dataObj.filter) == JSON.stringify(JSON.parse(sessionStorage.getItem('customFilter')).filter) && firstPageFlag;
		}

		dataObj.currPage = this.getPaginationValue(flag);

		return dataObj;
	}

	module.sendAjax = function () {
		/*if (this.stopAjax) {
			return;
		}*/

		var currData,
			ajaxCount = parseInt(sessionStorage.getItem('ajaxL'));

		/* загружаем из сессии, если есть в них данные фильтра*/
        currData = module.getAjaxData();
		/*if(!sessionEmpty && ajaxCount == 0){
			var retrievedObject = sessionStorage.getItem('customFilter');
			currData = JSON.parse(retrievedObject);

			setFilterValuesToHtml(currData);
		} else {
			currData = module.getAjaxData();
		}*/

		updateSessionStorage(currData);

        if(!this.needAutoSend) {
            return;
        }

        sessionStorage.setItem('ajaxL', ajaxCount + 1);
        showPreloader();
		
		$.ajax({
			url: module.ajaxUrl,
			data: currData,
			dataType: 'JSON',
			success: function (data) {

				var flag = $('.c-ajax-filter').hasClass('c-regenerateble') && parseInt(data.itemsCount) != 0;
				
				isFilterEmpty = checkIsFilterEmpty(currData.filter);

				if (flag){
					var elems = $('.c-ajax-filter .c-regenOption, .c-additionFilter .c-regenOption'),
						additionElems = $('.c-additionFilter .c-regenOption');

					$('.regenerated').removeClass('regenerated');

					$.each(data.filters, function(i, obj){
						var container = module.filter.find('[data-container="' + obj.name + '"]'),
							additionContainer = module.additionFilter.filter.find('[data-container="' + obj.name + '"]');

						if(!container.hasClass('no_regen')) {
							container.hide();
							if(!module.checkOptions.parents('.c-check-options').hasClass('n-regen')) {
								module.checkOptions.parents('.checkbox-wrap').hide();
								module.additionFilter.checkOptions.parents('.checkbox-wrap').hide();
							}

							if (obj.items != undefined && obj.items != null) {

								if(obj.items.length){
									container.show();
									obj.items.map(function(val, key){
										var currItem = elems.find('[data-hash="' + val.hash + '"]'),
											additionItem = additionElems.find('[data-hash="' + val.hash + '"]');

										switch(val.type){
											case "text":
												if(!currItem.length){
													currItem = elems.find('[name="' + val.name + '"]');
												};
												currItem.value = val.value;
												break;
											case "check":
												currItem.next('.checkbox-label').find('.checkbox-text').text(val.name + ' (' + val.count + ')');

												if(additionItem.length){
													additionItem.next('.checkbox-label').find('.checkbox-text').text(val.name + ' (' + val.count + ')');
												}
												if(val.checked){
													currItem[0].setAttribute('checked', true);
													currItem.next('.checkbox-label')[0].setAttribute('data-checked', 'Y');

													if(additionItem.length){
														additionItem.removeAttr('checked');
														additionItem.addClass('active');
													}
												}
												setTimeout(function(){
													currItem.parents('.checkbox-wrap').show();
												}, staticStore.timer[0]);
												break;
											case "radio":
												currItem.parents('.checkbox-wrap').show();
												currItem.next('.checkbox-label').find('.checkbox-text').text(val.name + ' (' + val.count + ')');
												break;
											default:
												break;
										}

										currItem.addClass('regenerated');
									});
								}
							}

							var hiddenOptions = container.find('[type="checkbox"]').not('.regenerated'),
								additionHiddenOptions = additionContainer.find('[type="checkbox"]').not('.regenerated');
							
							$.each(hiddenOptions, function(){
								disableHiddenOptions($(this));
							});

							$.each(additionHiddenOptions, function(){
								disableHiddenOptions($(this));
							});
						}
					});

				}
				
				var slider = $('.c-slider-replaced'),
					catalog = $('.c-elit-catalog'),
					newBlock = $('.c-new-propose'),
					seoSugg = $('.elit-sug .c-seo-suggestions');

				var html = module._onAjaxSuccess(data);

				if(parseInt(data.itemsCount) != 0){
                    $('.c-pagination').html(html.paginator);
                    if(data.districtName != false && data.districtName != undefined) {
                        catalog.html('<div class="c-new-propose position-block"><div class="page-title-wrap"><div class="page-title--elit">'+data.districtName+'</div></div><hr></div>'+html.items);
                    } else {
                        catalog.html(html.items);
                    }

					if(html.slider.length){
						slider.html(html.slider);
						slider.show();
						slider.next('hr').show();
						slider.find('.fotorama').fotorama();
					}
					else{
						slider.empty();
						slider.hide();
						slider.next('hr').hide();
					}

					
					if(parseInt(data.itemsCount) <= 4) {
						seoSugg.hide();
					} else {
						seoSugg.show();
					}
				}
				else{
					$('.c-pagination').html('');
					catalog.html('<div class="elit-container elit-container--no-separator"><p class="empty-catalog">Не найдено объектов с указанными параметрами.</p></div>');
					seoSugg.hide();
				}

				$(document).scrollTop(300);

				if(isFilterEmpty && parseInt(data.pager.active) == 1){

					newBlock.show();
					newBlock.next('hr').show();

					slider.hide();
					slider.next('hr').hide();

				} else {
					newBlock.hide();
					newBlock.next('hr').hide();

					slider.hide();
					slider.next('hr').hide();
				}

				$(document).scrollTop(0);

				module.updatePaginationLinks();

				setTimeout(function(){
					getSuggestionPosition();
				}, staticStore.timer[0]);

				var filterIndex = sessionStorage.getItem('currentAdditionFilterIndex');
				if (filterIndex && parseInt(sessionStorage.getItem('ajaxL')) !== 0) {
					$('.c-additionFilter').children().eq(filterIndex).find('.c-selectDropdownLink').trigger('click');
				}
			},
			error: function (er) {
				console.log(er)
			},
			complete: function(){
				hidePreloader();
			}
		});
	}

	module.getFilterValues = function () {
		var filterValues = {};

		filterValues.directions = [];
		filterValues.buildings = [];
		filterValues.inputOptions = [];
		filterValues.checkOptions = [];

		$.each(this.buildings, function () {
			if(this.dataset !== undefined && this.dataset.checked == 'Y') {
				filterValues.buildings.push($('input[id="' + this.getAttribute('for') + '"]').val());
			}
		});

		$.each(this.directions, function () {
			if(this.dataset !== undefined && this.dataset.checked == 'Y') {
				filterValues.directions.push($('input[id="' + this.getAttribute('for') + '"]').val());
			}
		});

		$.each(this.checkOptions, function () {
			if(this.dataset !== undefined && this.dataset.checked == 'Y') {
				filterValues.checkOptions.push({
					"name": $('input[id="' + this.getAttribute('for') + '"]').attr('name'),
					"value": $('input[id="' + this.getAttribute('for') + '"]').val()
				});
			}
		});

		$.each(this.inputOptions, function () {
			var elem = this;

			if (elem.value) {
				filterValues.inputOptions.push({
					"name": elem.getAttribute('name'),
					"value": elem.value
				});
			}
		});

		return filterValues;
	}

	var setFilterValuesToHtml = function (dataFilter) {

		module.stopAjax = true;

		setTimeout(function () {

			if(dataFilter.filter.directions.length){
				dataFilter.filter.directions.map(function(val, key){
					module.directions.map(function(i, elem){
						var text = $(elem).find('.checkbox-text').text();
						if(text.indexOf(val) >= 0 || $(elem).prev().val() == val){
							$(elem).trigger('click');
						}
						return;
					});
				});
			}

			if(dataFilter.filter.checkOptions.length){
				dataFilter.filter.checkOptions.map(function(val, key){
					var checkTemp = module.checkOptions.prev('input[name="' + val.name + '"]');

					checkTemp.map(function(i, elem){
						if(elem.value == val.value){
							$(elem).next('label').trigger('click');
							return;
						}
					});
				});
			}

			dataFilter.filter.inputOptions.map(function(obj, key){
				module.filter.find('[name="' + obj.name + '"]').val(obj.value);
				switch(true){
					case (obj.name.indexOf('From') > 0):
						var range = module.filter.find('[name="' + obj.name + '"]').siblings('.c-range'),
							range2 = module.additionFilter.filter.find('[name="' + obj.name + '2"]').siblings('.c-range');
						if(range.length){
							$(range)[0].noUiSlider.set([obj.value, null]);
						}
						if(range2.length){
							range2[0].noUiSlider.set([obj.value, null]);
						}
						break;
					case (obj.name.indexOf('To') > 0):
						var range = module.filter.find('[name="' + obj.name + '"]').siblings('.c-range'),
							range2 = module.additionFilter.filter.find('[name="' + obj.name + '2"]').siblings('.c-range');

						if(range.length){
							$(range)[0].noUiSlider.set([null, obj.value]);
						}
						if(range2.length){
							range2[0].noUiSlider.set([null, obj.value]);
						}
						break;
					default:
						module.filter.find('[name="' + obj.name + '"]').val(obj.value);
						break;
				}
			});

			module.stopAjax = false;
		}, staticStore.timer[1]);

		return false;
	}

	var getSuggestionPosition = function () {
		var winh = $(window).height();

		if(winh > 880){
			var slider = $('.c-slider-replaced');

			(!slider.length || slider.hasClass('none_slider')) ? winh = winh + 470 : winh = winh - 90;

			$('.elit-sug .c-seo-suggestions').css('top', winh+'px');
		}

		return false;
	}

	var checkIsFilterEmpty = function (dataObj) {
		var flag = true;

		if(dataObj.checkOptions.length !== 0){
			flag = false;
			return flag;
		}

		if(dataObj.directions.length !== 0){
			flag = false;
			return flag;
		}

		dataObj.inputOptions.map( function (val, key) {
			switch(val.name){
				case 'currentValue':
					if(module.defaultFilter['currentValue'] !== 'USD'){
						flag = false;
					}
					break;
				case 'name':
					if(module.defaultFilter['name'] !== val.value){
						flag = false;
					}
					break;
				case 'id':
					if(module.defaultFilter['id'] !== val.value){
						flag = false;
					}
					break;
				default:
					if(module.defaultFilter[val.name] !== parseInt(val.value)){
						flag = false;
					}
					break;
			}
		});

		return flag;
	}

	var updateSessionStorage = function (data) {
		sessionEmpty = false;
        sessionStorage.setItem('customFilter', JSON.stringify(data));
	}

	var onAdditionFilterCheckClick = function (elem) {
		var value = elem.prev('input').val(),
			moduleLabel = module.filter.find('[value="' + value + '"]').next('label');
		setTimeout(function () {

			if(elem.prev('input')[0].checked){
				elem.prev('input').toggleClass('active');
			}
			// else {
			// 	elem.prev('input').removeClass('active');
			// }

			elem.prev('input')[0].checked = false;

			moduleLabel.trigger('click');
		}, staticStore.timer[2]);
	}

	var initAdditionFilter = function () {
		var additionFilter = {},
			filter = $('.c-additionFilter'),
            triggerBtn = filter.find('.c-triggerInputChange'),
			range = filter.find('.c-range, .c-range2'),
			minInput = range.closest('.c-rangeWrap').find('[name="priceFromInput2"]'),
			maxInput = range.closest('.c-rangeWrap').find('[name="priceToInput2"]'),
			selectUSDRUB = range.closest('.c-rangeWrap').find('[name="currentValue"]'),
			priceInput = module.filter.find('[name="priceToInput"]'),
			modulePriceRange = priceInput.closest('.c-rangeWrap').find('.c-range, .c-range2');

		module.additionFilter = additionFilter;
		additionFilter.filter = filter;

		additionFilter.directions = filter.find('.c-direction-select-able .checkbox-label');
		additionFilter.checkOptions = filter.find('.c-check-options .checkbox-label');

		/* изменение цены */
		selectUSDRUB.on('change', function(){
			var mainSelectItem = priceInput.closest('.c-rangeWrap').find('[name="currentValue"]');

			mainSelectItem.val(selectUSDRUB.val());
			mainSelectItem.trigger('change');
		});

		triggerBtn.on('click', function(e) {
			e.preventDefault();

			minInput.trigger('focus');

			setTimeout(function(){
				minInput.trigger('blur');

				var priceInput = module.filter.find('[name="priceToInput"]'),
					modulePriceRange = priceInput.closest('.c-rangeWrap').find('.c-range');

				modulePriceRange[0].noUiSlider.set(range[0].noUiSlider.get());

			}, staticStore.timer[2]);
		});

		/* направления */
		additionFilter.directions.on('click', function(){
			onAdditionFilterCheckClick($(this));
		});

		/* тип объекта */
		additionFilter.checkOptions.on('click', function(){
			onAdditionFilterCheckClick($(this));
		});

		return false;
	}

	var disableHiddenOptions = function (item) {
		if(!item.closest('.c-direction-list').length){
			item[0].removeAttribute('checked');
			item.next('.checkbox-label')[0].setAttribute('data-checked', 'N');
		} else if (!isDirectionChecked){
			item.parents('.checkbox-wrap').hide();
		}
	}

	return module;

}());

//popups

var popupModule = (function () {
	var module = {},
		bPopup;

	module.init = function (elem, link) {

		this.close();

		module.type = link.data('type-popup') || 'ajax';
		module.url = link.data('url-popup') || '/';

		if (link.data('iframe-attr') && link.data('iframe-attr').length > 0) {
			module.iframeattr = link.data('iframe-attr') || 'width="600" height="400"';
		}

		var props = {
			content: module.type,
			closeClass: 'c-popup-close',
			contentContainer: '.popup__content',
			loadUrl: module.url,
			modalColor: '#838690',
			opacity: 1,
			iframeAttr: module.iframeattr,
			follow: [true, false],
			loadCallback: function () {
				$('.c-popup-close').show();
				module.callOnLoadFunction();
			},
			onClose: function () {
				$(self.content).empty();
				$('.c-popup-close').hide();
				module.callOnCloseFunction();
				popupModule.resetCallBacksFunction();
				$('.select2-drop').hide();
			}
		}

		bPopup = elem.bPopup(props);
	}

	module.callOnLoadFunction = function () {

	};

	module.callOnCloseFunction = function () {

	};

	module.close = function(){
		if (bPopup) {
			bPopup.close();
		}
	}

	module.reposition = function(){
		if (bPopup) {
			bPopup.reposition(100);
		}
	}

	module.resetCallBacksFunction = function () {

		module.callOnLoadFunction = function () {
			console.log('Loaded');
		};

		module.callOnCloseFunction = function () {

		};
	}

	return module;
}());