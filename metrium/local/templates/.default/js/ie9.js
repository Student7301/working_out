$(function () {
	var links = $('body').find('.absolute-link');

	$.each(links, function(){
		var $this = $(this),
			parent = $this.parent();

		$this.siblings('img').wrap('<a href="'+$(this).attr('href')+'" class="fake-abs-link"></a>');
		if(!parent.hasClass('green-block')){
			$this.siblings('.middle-text').wrap('<a href="'+$(this).attr('href')+'" class="fake-abs-link"></a>');
		}
		$this.siblings('span').wrap('<a href="'+$(this).attr('href')+'" class="fake-abs-link"></a>');
		$this.siblings('.building-image').wrap('<a href="'+$(this).attr('href')+'" class="fake-abs-link"></a>');
		$this.siblings('.building-caption').wrap('<a href="'+$(this).attr('href')+'" class="fake-abs-link"></a>');

		if(parent.hasClass('tile')){
			parent.find('.fake-abs-link').css('height', parent.height());
		}
	});
});