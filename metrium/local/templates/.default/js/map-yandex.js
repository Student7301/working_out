var siteYandexMap = (function () {

	var module = {},
		map,
		clusterer,
		currRoute,
		hasChild,
		hasCluster,
		collection,
		customYmaps,
		coords_m,
		zoom;

	module.config = {
		center: [55.7559, 37.506675],
		zoom: 12,
		controls: ['zoomControl']
	};

	module.isPopupFilter = false;

	ymaps.ready(function () {
		customYmaps = ymaps;
	});

	module.init = function (link) {
		var location = {},
			type = link.data('type') || 'yandex#map',
			hasRoad = link.data('road') || false,
			hasElitFilter = link.data('has-filter') || false;

		coords_m = link.data('coord-default') || false;
		zoom = link.data('map-zoom') || false;

		hasChild = link.data('has-child') || false;
		link.data('has-cluseter') == "N" ? hasCluster = false : hasCluster = true;
		link.data('singlepm') == "Y" ? this.singlePlacemark = true : this.singlePlacemark = false;
		location.from = link.data('road-from') || 'Москва';
		location.to = link.data('road-to') || 'Москва';
		if(link.data('road-transit')) {
			location.transit = link.data('road-transit');
		}
		this.customSearch = link.data('custom-search') || false;
		this.id = link.data('map-id') || 'map';
		this.elit = link.data('elit') || false;
		this.jcId = link.data('jc_id') || false;
		this.type = link.data('jc_type') || false;

		ymaps.ready(function () {

			if(customYmaps){

				if(map){
					module.destroy(map);
				}

				map = new customYmaps.Map(module.id, module.config);

				map.setType(type);
				map.behaviors.disable('scrollZoom');

				hasRoad == 'Y' ? getRoad(location, link.index()) : getPlasemarks(link.data('placemarks-json'), hasElitFilter);

				var temp = '<div class="new-map_ico_clust"><span>$[properties.geoObjects.length]</span></div>';

				squareClusterLayout = customYmaps.templateLayoutFactory.createClass(temp);

				var ClusterHintLayout = ymaps.templateLayoutFactory.createClass(
						'Место с несколькими объектами'
					);

				if (jSCB){
					ymaps = customYmaps;
				}

				clusterer = new customYmaps.Clusterer({
					clusterIconLayout: squareClusterLayout,
					clusterIconShape: {
						type: 'Rectangle',
						coordinates: [
							[0, 0], [40, 40]
						]
					},
					groupByCoordinates: false,
					clusterDisableClickZoom: true,
					clusterHideIconOnBalloonOpen: false,
					geoObjectHideIconOnBalloonOpen: false,
					hasBalloon: false,
					hasHint: true,
					hintContentLayout: ClusterHintLayout
				});


				clusterer.events.add('click', function (e) {

					var geoObject = e.get('target');

					if(!clusterer.getObjectState(geoObject).isShown) {
						map.setCenter( geoObject.geometry.getCoordinates(), map.getZoom() + 1 , {
							checkZoomRange: true
						});
					}

				});

				if(module.customSearch == "Y"){

					var url = "/local/templates/.default/js/search-address.js",
						searchForm = $('.yandex-search-form');

					$.getScript( url, function() {

						$.each(searchForm, function(){
							var searchControl = new SearchAddress(map, $(this), customYmaps),
								id = $(this).find('.yandex-suggest').attr('id'),
								suggestView = new customYmaps.SuggestView(id);
						});

					});
					
					
				}
				
				
			}
		});
	};
	
	module.setCenter = function(coords, zoom) {
		map.setCenter(coords,zoom);
	}

	module.destroy = function(_map){
		_map.destroy();
	};

	module.getFilteredCollection = function(values){
		getCurrCollectionOnMap(values);
	}

	module.getCurrentUserPosition = function () {
        ymaps.geolocation.get({
            autoReverseGeocode: true
        }).then(function (result) {
            var userPosition = result.geoObjects.get(0).geometry.getCoordinates(),
            	geoLocationLinks = $('.c-geolocationLink');

            $.each(geoLocationLinks, function () {
            	var href = this.getAttribute('href'),
            		string = href.slice(0, href.indexOf('rtext=') + 6) + userPosition[0] + ',' + userPosition[1] + href.slice(href.indexOf('rtext=') + 6);
            	
            	this.setAttribute('href', string);
            });
        });
    }

	function getCurrCollectionOnMap(values){

		var searchResult = new customYmaps.GeoQueryResult();
		// filter type
		if(values.links && values.links.length){
			$.each(values.links, function(key, val){
				searchResult = collection.search('properties.type = "'+val+'"').add(searchResult);
			});
		}
		else{
			searchResult = collection;
		}

		// filter apartmentType
		var currSearch = new customYmaps.GeoQueryResult();
		if(values.checkValues.length){
			$.each(values.checkValues, function(key, val){
				currSearch = searchResult.search('properties.filterType = "'+val+'"').add(currSearch);
			});
			searchResult = currSearch;
		}

		// filter price
		if( values.price.min && values.price.max){
			if ($('.c-priceval').find('option:selected').val() == 'USD') {
				currSearch = searchResult.search('properties.filterPriceMinUSD >= '+parseInt(values.price.min)).search('properties.filterPriceMinUSD <= '+parseInt(values.price.max));
				currSearch = currSearch.search('properties.filterPriceMaxUSD >= '+parseInt(values.price.min)).add(currSearch);
			} else {
				currSearch = searchResult.search('properties.filterPriceMin >= '+parseInt(values.price.min)).search('properties.filterPriceMin <= '+parseInt(values.price.max));
				currSearch = currSearch.search('properties.filterPriceMax >= '+parseInt(values.price.min)).add(currSearch);
			}
			searchResult = currSearch;
		}
		
		// filter area
		if( values.area.min && values.area.max){
			currSearch = searchResult.search('properties.filterAreaMin >= '+parseInt(values.area.min)).search('properties.filterAreaMin <= '+parseInt(values.area.max));
			currSearch = currSearch.search('properties.filterAreaMax >= '+parseInt(values.area.min)).add(currSearch);
			searchResult = currSearch;
		}

		map.geoObjects.removeAll();
		clusterer.removeAll();
		if(searchResult.getLength() != 0){

			searchResult.each(function(pm){
				var coords = pm.geometry.getCoordinates();

				if( (coords[0].toString().match(/^([0-9][0-9]\.[0-9])\w+/) != null) && (coords[1].toString().match(/^([0-9][0-9]\.[0-9])\w+/) != null) ){
					if(hasCluster){
						clusterer.add(pm);
					}
					else{
						map.geoObjects.add(pm);
					}
				}
			});

			if(hasCluster){
				map.geoObjects.add(clusterer);

				setTimeout( function(){
					map.setBounds(clusterer.getBounds(), {
						checkZoomRange: true,
						zoomMargin: 30
					});
				}, 300);
			}
			else{
				if (module.singlePlacemark) {
					map.setCenter(searchResult.get(0).geometry.getCoordinates(), 14, {
						checkZoomRange: true,
					});
				} else {
					map.setBounds(map.geoObjects.getBounds(), {
						checkZoomRange: true,
						zoomMargin: 30
					});
				}
			}
		}
	}

	function getPlasemarks(url, filterFlag){

		if(sessionStorage.getItem('collection') && popupFilterMap){
			collection = customYmaps.geoQuery(JSON.parse(sessionStorage.getItem('collection')));

			addPlacemarks(collection);

			if (coords_m != false) {
				setTimeout(function(){
					siteYandexMap.setCenter(coords_m,zoom);
				},1000)
			}

		} else {

			var data = {};

			if(module.jcId){
				data = { OBJ_ID : module.jcId, lvl: 2,  type: module.type};
			}
			else{
				$('.c-back-link').remove();
			}

			if(filterFlag == "Y"){
				data = customFilterActions.getAjaxData();
			}

			showPreloader();

			$.ajax({
				url: url,
				data: data,
				dataType: 'JSON',
				success: function(json){
					collection = customYmaps.geoQuery(json);

	            	addPlacemarks(collection);

	            	if (popupFilterMap) {
	            		sessionStorage.setItem('collection', JSON.stringify(json));
	            	}
				},
				error: function(err){
					console.log(err);
				},
				complete: function(){
					hidePreloader();
					if (coords_m != false) {
						setTimeout(function(){
							siteYandexMap.setCenter(coords_m,zoom);
						},1000)
					}
				}
			});
		}
	}

	function addPlacemarks(){
		setBalloonTemplate();

		if(module.isPopupFilter){
			popupFilter();
		}
		else{
			collection.each(function(pm){
				var coords = pm.geometry.getCoordinates();
				if( (coords[0].toString().match(/([0-9][0-9]\.[0-9])\w+/) != null) && (coords[1].toString().match(/([0-9][0-9]\.[0-9])\w+/) != null) ){
					if(hasCluster){
						clusterer.add(pm);
					}
					else{
						map.geoObjects.add(pm);
					}
				}
			});

			if(hasCluster){
				map.geoObjects.add(clusterer);

				setTimeout( function(){
					map.setBounds(clusterer.getBounds(), {
						checkZoomRange: true,
						zoomMargin: 30
					});
				}, 400);
			}
			else{
				if (module.singlePlacemark) {
					map.setCenter(collection.get(0).geometry.getCoordinates(), 14, {
						checkZoomRange: true,
					});
				} else {
					map.setBounds(map.geoObjects.getBounds(), {
						checkZoomRange: true,
						zoomMargin: 30
					});
				}
			}
		}
	}

	function setIconTemplate(isElitPlacemark, placemark){

		if(isElitPlacemark == "Y"){
			var layout = '',
				_coordinates;

			var temp = '<div class="new-map_ico"></div>';
			layout = customYmaps.templateLayoutFactory.createClass(temp);
			_coordinates = [ [0, 0], [30, 40] ];

			placemark.options.set({
				iconLayout: layout,
				iconShape: {
					type: 'Rectangle',
					coordinates: _coordinates
				}
			});
		}
		else{
			placemark.options.set({
				iconLayout: 'default#image',
				iconImageHref: '/local/templates/.default/images/map-icon.png',
				iconImageSize: [35, 53],
				iconImageOffset: [-15, -53]
			});
		}
	}

	function setBalloonTemplate(){
		
		var contentLayout = customYmaps.templateLayoutFactory.createClass( "<div class='custom-content-wrap'>{{ properties.iconContent }}</div>"),
			tempalte = '';

		tempalte += '<div class="custom-balloon">' +
					'<a class="close sprite-icon close-elit" href=""></a>';

		tempalte += '<div class="custom-balloon__content">' +
							'$[[options.contentLayout observeSize minWidth=100 maxWidth=400 maxHeight=220]]' +
						'</div>' +
					'</div>';
		balloonLayout = customYmaps.templateLayoutFactory.createClass(
			tempalte,

		{
			build: function() {
				balloonLayout.superclass.build.call(this);

				var geoObject = this.getData().geoObject,
					map = geoObject.getMap(),
					coords = geoObject.geometry.getCoordinates(),
					container =  $(this.getParentElement());


					container.find('.custom-balloon').each( function() {
						$(this).css( {
							left: -Math.round($(this).outerWidth()),
							top: -($(this).outerHeight())
						});
						var zoom = map.getZoom(),
							width = $(this).outerWidth(),
							height = $(this).outerHeight(),
							projection = map.options.get('projection'),
							global = projection.toGlobalPixels(coords, zoom),
							center = map.getGlobalPixelCenter(),
							balloonGlobalBounds = [ [ global[0] - Math.round(width), global[1] + 0],
								[ global[0] + Math.round(width)*0.35, global[1] - height - 25]],
							bounds = map.getBounds(),
							globalBounds = [ projection.toGlobalPixels(bounds[0], zoom),
								projection.toGlobalPixels(bounds[1], zoom)],

							pan = [ 0, 0];

						if ( balloonGlobalBounds[0][0] < globalBounds[0][0] ) {
							pan[0] = balloonGlobalBounds[0][0] - globalBounds[0][0] - 50
						} else if ( balloonGlobalBounds[1][0] > globalBounds[1][0]) {
							pan[0] = balloonGlobalBounds[1][0] - globalBounds[1][0] + 50
						}
						if ( balloonGlobalBounds[0][1] > globalBounds[0][1] ) {
							pan[1] = balloonGlobalBounds[0][1] - globalBounds[0][1] + 50
						} else if ( balloonGlobalBounds[1][1] < globalBounds[1][1]) {
							pan[1] = balloonGlobalBounds[1][1] - globalBounds[1][1] - 50
						}
						if (pan[0] || pan[1]) {
							center[0] += pan[0];
							center[1] += pan[1];
							map.panTo(projection.fromGlobalPixels(center, zoom), { delay: 0, duration: 500});
						}

					})
					.on('click', '.close', function() {
						map.balloon.close();

						return false;
					});
			},
			clear: function() {
				$('.custom-balloon').off();
				balloonLayout.superclass.clear.call(this);
			}
		});
		
		collection.each(function(pm){

			setIconTemplate(module.elit, pm);

			var contentTemplate = '';

			if(pm.properties.get('address')){
				contentTemplate += '<div class="object-address">'+pm.properties.get('address')+'</div>';
			}

			contentTemplate += '<div class="object-info">';

			if(pm.properties.get('image')){
				contentTemplate += '<div class="object-image">';
				pm.properties.get('url') ? contentTemplate += '<a href="'+pm.properties.get('url')+'" title="" target="_blank"><img src="'+pm.properties.get('image')+'" alt=""></a>' : contentTemplate += '<img src="'+pm.properties.get('image')+'" alt="">';
				contentTemplate += '</div>';
			}

			contentTemplate += '<div class="object-info-list">';

			if(pm.properties.get('hintContent')){
				pm.properties.get('url') ? contentTemplate += '<a class="object-name" href="'+pm.properties.get('url')+'" target="_blank">'+pm.properties.get('hintContent')+'</a>' : contentTemplate += '<div class="object-name">'+pm.properties.get('hintContent')+'</div>'
			}

			var propList = pm.properties.get('info_list');
			if(propList){
				for (property in propList) {
					contentTemplate += '<div class="object-line">'+propList[property]+'</div>';
				}
			}

		   	contentTemplate += '</div></div>';

		   	if(hasChild == 'Y' && pm.properties.get('type') != 'elit' && parseInt(pm.properties.get('objCount')) != 0){
				contentTemplate += '<a class="objects-link c-jc-objects-link" href="" title="" data-jc_id="'+pm.properties.get('id')+'" data-map-id="big-map" data-elit="Y" data-has-cluseter="N" data-has-child="N" data-jc_type="'+pm.properties.get('type')+'">'+pm.properties.get('objCount')+'</a>';
			}

			if(contentTemplate != '<div class="object-info"><div class="object-info-list"></div></div>') {
				// Создание вложенного макета содержимого балуна.
				MyBalloonContentLayout = customYmaps.templateLayoutFactory.createClass(
					contentTemplate
				);
				
				pm.options.set({
					balloonShadow: false,
					balloonLayout: balloonLayout,
					balloonContentLayout: MyBalloonContentLayout,
					balloonPanelMaxMapArea: 0,
					hideIconOnBalloonOpen: false
				});
			}
		});
	}

	function getRoad(location, ind){

		// Создаем массив точек через которые должен проходить маршрут
		var points = [ location.from ];
		if(location.transit) {
			points.push(location.transit);
		}
		points.push(location.to);

		customYmaps.route(
			points,
			{
				mapStateAutoApply: true,
				zoomMargin: 70
			}
		).then(function(route){
			currRoute && map.geoObjects.remove(currRoute);

			route.getPaths().options.set({
				strokeColor: '74261c'
			});

			var points = route.getWayPoints();

			// Назначаем иконки только конечным точкам
			if(location.transit) {
				var last_elem = 2;
				points.get(1).options.set({
					iconLayout: 'none'
				});
			}else {
				var last_elem = 1;
			}
			points.get(last_elem).options.set({
				iconLayout: 'default#image',
				iconImageHref: '/local/templates/.default/images/map-icon-black.png',
				iconImageSize: [35, 53],
				iconImageOffset: [-15, -53]
			});

			points.get(0).options.set({
				iconLayout: 'default#image',
				iconImageHref: '/local/templates/.default/images/map-icon-black.png',
				iconImageSize: [35, 53],
				iconImageOffset: [-15, -53]
			});

			map.geoObjects.add(currRoute = route);
		});

		getCurrentRoadText(ind);
	}

	return module;

})();