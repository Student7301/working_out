var siteGoogleMap = (function () {

	var module = {},
		styleArray = [
			{
				featureType: "all",
				stylers: [
					{ saturation: -80 }
				]
			},
			{
				featureType: "road.arterial",
				elementType: "geometry",
				stylers: [
					{ hue: "#00ffee" },
					{ saturation: 50 }
				]
			},
			{
				featureType: "poi.business",
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			}
		],
		flag= false;

	module.id= 'map';
	module.center = {lat: 55.7559, lng: 37.506675};
 
	module.config = {
		center: module.center,
	    scrollwheel: false,
	    styles: styleArray,
	    zoom: 16,
	    disableDefaultUI: true
	};

	module.init = function () {
		var map = new google.maps.Map(document.getElementById(module.id), module.config);

		var marker = new google.maps.Marker({
			position: module.center,
			map: map,
			icon: 'images/map-icon-square.png',
			title: ''
		});
	};

	module.getGoogleScript = function(link){
		module.id = link.data('map-id');
		if (!flag){
			var script = document.createElement( 'script' );
			script.type = 'text/javascript';
			script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDXERtEORlSWyG-NA0NcShFNIYBbSrUsr4&signed_in=true&callback=siteGoogleMap.init";
			$("head").append( script );
			flag = true;
		}
		else{
			module.init();
		}
	}
 
	return module;
 
})();
