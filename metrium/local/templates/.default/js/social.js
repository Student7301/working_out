$(document).ready(function() {
    var FbEntries = {};
    FbEntries.busy = false;
    FbEntries.uri = location.href;

    if (!$('#social-activity .fb-entries').length) {
        return;
    }
    FbEntries.pages = {
        next: $('#social-activity .fb-entry').length,
        hasMore: true
    };

    var masonry = new Masonry($('#social-activity .fb-entries').get(0, {
        //columnWidth: 352,
        itemSelector: '.fb-entry'
    }));

    $('#social-activity .fb-entries').data('masonry', masonry);
    $('#social-activity .fb-entries').addClass('loaded');

    FbEntries.loadNextPage = function () {
        var GET = {};
        var options = arguments[0] || {};
        if (FbEntries.busy == false && FbEntries.pages.hasMore) {
            FbEntries.busy = true;
            delete options.success;
            var ajaxOptions = {
                type: 'GET',
                dataType: 'json',
                data: $.extend(GET, {'start': FbEntries.pages.next}),
                url: FbEntries.uri,
                success: function (data) {
                    var newItems = $('<div></div>').html(data.content).find('.fb-entry');

                    if ($('#social-activity .fb-entry:last').length) {
                        $('#social-activity .fb-entry:last').after(newItems);
                    } else {
                        $('#social-activity .fb-entries').html(newItems);
                    }

                    var msnry = $('#social-activity .fb-entries').data('masonry');
                    msnry.appended(newItems.toArray());
                    setTimeout(function(){
                        msnry.layout();
                    }, 150);
                    FbEntries.pages = data.pages;
                    FbEntries.busy = false;

                    if (data.next_from != undefined && data.next_from.length) {
                        var href = $('#social-activity footer > a.btn').attr('href');
                        var parseUrl = new URL('http://metrium.ru' + href);
                        var sign = (parseUrl.search.length) ? '&' : '?';

                        $('#social-activity footer > a').attr('data-href', href + sign + 'next_from=' + data.next_from);
                    }
                }
            };
            ajaxOptions = $.extend(ajaxOptions, options);
            return $.ajax(ajaxOptions);
        }
    };

    $('body').on('click', '#social-activity footer .btn', function (e) {
        var $me = $(this);
        e.preventDefault();
        FbEntries.uri = $me.attr('data-href');
        var jqXHR = FbEntries.loadNextPage({
            beforeSend: function () {
                $me.append('<i class="fa fa-spinner fa-spin"></i>')
            }
        });
        if (jqXHR) {
            jqXHR.done(function () {
                if (!FbEntries.pages.hasMore) {
                    $me.closest('footer').remove();
                }
            }).always(function () {
                $me.find('.fa-spin').remove();
            })
        }
    });

    // Клик по блоку
    $('#social-activity').on('click', '.fb-entry', function (e) {
        var url = $(e.currentTarget).find('#social_link').val();
        window.open(url);
    });
});