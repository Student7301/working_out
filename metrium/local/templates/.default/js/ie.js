$(function(){
	var ieVersion = (function() { if (new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null) { return parseFloat( RegExp.$1 ); } else { return false; } })(),
		ie11 = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
	
	if ( (ieVersion !== false) || ie11){
		var wrap = $('.c-bg-elit'),
			currImg = $('.c-bg-elit .active');

		console.log(currImg);

		currImg.css( {"height": "100%", "width": "auto" } );
	    if (currImg.width() < wrap.width()) {
	        currImg.css({
	            "height": "auto",
	            "width": "100%"
	        });
	    }
	}
});