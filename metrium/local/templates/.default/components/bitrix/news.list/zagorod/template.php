<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<?php

global $dir_photos;

function read($url)
{
    global $dir_photos;
    if ($handle = opendir($_SERVER['DOCUMENT_ROOT'] . $url)) {
        while (false !== ($file = readdir($handle))) {
            if(is_dir($_SERVER['DOCUMENT_ROOT'] . $url . $file) && $file != '.' && $file != '..') {
                read($url . $file . '/');

            }elseif(is_file($_SERVER['DOCUMENT_ROOT'] . $url . $file) && pathinfo($_SERVER['DOCUMENT_ROOT'] . $url . $file)['extension'] == 'jpg'
                || pathinfo($_SERVER['DOCUMENT_ROOT'] . $url . $file)['extension'] == 'jpeg'
                || pathinfo($_SERVER['DOCUMENT_ROOT'] . $url . $file)['extension'] == 'bmp'
                || pathinfo($_SERVER['DOCUMENT_ROOT'] . $url . $file)['extension'] == 'png'
                || pathinfo($_SERVER['DOCUMENT_ROOT'] . $url . $file)['extension'] == 'gif') {

                $qwerty = pathinfo($_SERVER['DOCUMENT_ROOT'] . $url . $file)['basename'];
                $dir_photos[$qwerty] = $url . $file;
            }
        }
        closedir($handle);
    }
}

foreach ($arResult['ITEMS'] as $item) { ?>
    <? $dir_photos = array(); ?>
    <? $url = '/upload/check_photo/' . $item['CODE'] . '/'; ?>
    <? read($url); ?>
    <hr style="height: 5px; background: darkgray">
    CODE: <?=$item['CODE']?> </br>

    <? $link = \metrium\System\Site::getDomain() . '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=' . $item['IBLOCK_ID'] . '&type=zagorodnaya_nedvizhimost&ID=' . $item['ID'] . '&lang=ru&find_section_section=0&WF=Y'; ?>
    <a href="<?= $link ?>" target="_blank"><?= $link ?></a>

    <? if(!$dirEmpty = opendir($_SERVER['DOCUMENT_ROOT'] . '/upload/check_photo/' . $item['CODE'] . '/')) { ?>
        <p style="color:red">Каталог не существует</p>
    <? }else { ?>
        <div id="<?=$item['ID']?>">
        <div style="width: 100%; overflow: auto; zoom: 1;">

            <!--ID: <?=$item['ID']?> </br>-->

            <!--<?=$item['IBLOCK_CODE']?></br>-->



            <!-- Вывод PREVIEW_PICTURE и если существует картинка с таким именим из каталога -->
            <p style="background: lightgrey;padding: 10px 5px;">PREVIEW_PICTURE</p>
            <div style="float: left">
                <img src="<?= CFile::GetPath($item['PREVIEW_PICTURE']['ID']) ?>" width="250px" height="auto"><br /><?= $item['PREVIEW_PICTURE']['FILE_NAME'] ?>
            </div>
            <? if(!empty($dir_photos[$item['PREVIEW_PICTURE']['FILE_NAME']])) { ?>
                <img src="<?= $dir_photos[$item['PREVIEW_PICTURE']['FILE_NAME']] ?>" width="250px" height="auto">
            <? }elseif(!empty($dir_photos[$item['PREVIEW_PICTURE']['ORIGINAL_NAME']])) { ?>
                <img src="<?= $dir_photos[$item['PREVIEW_PICTURE']['ORIGINAL_NAME']] ?>" width="250px" height="auto">
            <? }elseif(!empty($dir_photos[substr_replace($item['PREVIEW_PICTURE']['ORIGINAL_NAME'], 'jpg', -3, 3)])) { ?>
                <img src="<?= $dir_photos[substr_replace($item['PREVIEW_PICTURE']['ORIGINAL_NAME'], 'jpg', -3, 3)] ?>" width="250px" height="auto">
            <? }elseif($dirEmpty) { ?>
                Нет файла
            <? } ?>
        </div>

        <!-- Вывод PHOTO и если существует картинка с таким именим из каталога -->
        <p style="background: lightgrey;padding: 10px 5px;">PHOTO</p>
        <? foreach ($item['PROPERTIES']['PHOTOS']['VALUE'] as $photo) { ?>
            <? $file_array = CFile::GetFileArray($photo); ?>
            <div style="overflow: auto; zoom: 1;float: left; margin-top: 10px;">
                <div style="float: left">
                    <img src="<?= CFile::GetPath($file_array['SRC']) ?>" width="150px" height="auto"><br /><?= $file_array['FILE_NAME'] ?>
                </div>

                <? if(!empty($dir_photos[$file_array['FILE_NAME']])) { ?>
                    <img src="<?= $dir_photos[$file_array['FILE_NAME']] ?>" width="150px" height="auto">
                <? }elseif(!empty($dir_photos[$file_array['ORIGINAL_NAME']])) { ?>
                    <img src="<?= $dir_photos[$file_array['ORIGINAL_NAME']] ?>" width="250px" height="auto">
                <? }elseif(!empty($dir_photos[substr_replace($file_array['ORIGINAL_NAME'], 'jpg', -3, 3)])) { ?>
                    <img src="<?= $dir_photos[substr_replace($file_array['ORIGINAL_NAME'], 'jpg', -3, 3)] ?>" width="250px" height="auto">
                <? }elseif($dirEmpty) { ?>
                    Нет файла
                <? } ?>
            </div>
        <? } ?>
        <div style="clear: both;"></div>

        <!-- Вывод всех картинок из каталога -->
        <? if(!empty($dir_photos)) { ?>
            <p style="background: lightgrey;padding: 10px 5px;">Все картинки из каталога</p>
            <? foreach ($dir_photos as $key => $photo) { ?>
                <div style="overflow: auto; zoom: 1;float: left; margin-top: 10px;">
                    <div style="float: left">
                        <img src="<?= $photo ?>" width="150px" height="auto"><br /><?= $key ?>
                    </div>
                </div>
            <? } ?>
        <? } ?>
        <div style="clear: both;"></div>

        <br/>
        <div style="width: 100%" id="qw<?=$item['ID']?>">
            <button onclick="someFunc(<?=$item['ID']?>)">Подтвердить</button>
        </div>
    </div>
    <? } ?>
<? } ?>


<hr>
<?=$arResult["NAV_STRING"]?>
<script>
    function someFunc(id)
    {
        $.ajax({
            type: "GET",
            url: "photo_get.php",
            data: "id="+id,
            success: function(html){
                $("#"+id).html("<p>Добавлено!!!</p>");
            }
        });
    }
</script>