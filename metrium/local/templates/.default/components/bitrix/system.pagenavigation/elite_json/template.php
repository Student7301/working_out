<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];

if(!$arResult["NavShowAlways"]){
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>

<?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

// to show always first and last pages
$arResult["nStartPage"] = 1;
$arResult["nEndPage"] = $arResult["NavPageCount"];

$sPrevHref = '';
if ($arResult["NavPageNomer"] > 1){
	$bPrevDisabled = false;

	if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)	{
		$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
	}else{
		$sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
	}
}else{
	$bPrevDisabled = true;
}

$sNextHref = '';
if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]){
	$bNextDisabled = false;
	$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
}else{
	$bNextDisabled = true;
}

$arPager = array();
$bFirst = true;
$bPoints = false;
do
{
	if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2){

		if ($arResult["nStartPage"] == $arResult["NavPageNomer"]) {
			$arPager['active'] = $arPager['pages'][] = $arResult["nStartPage"];
		}elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false) {
			$arPager['pages'][] = $arResult["nStartPage"];
		}else {
			$arPager['pages'][] = $arResult["nStartPage"];
		}

		$bFirst = false;
		$bPoints = true;
	}else{
		if ($bPoints){
			$arPager['pages'][] = 'separator';
			$bPoints = false;
		}
	}
	$arResult["nStartPage"]++;
} while($arResult["nStartPage"] <= $arResult["nEndPage"]);
echo json_encode($arPager);
?>