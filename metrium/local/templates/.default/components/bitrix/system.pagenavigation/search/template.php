<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>

<?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
if($arResult["bDescPageNumbering"] === true)
{
	// to show always first and last pages
	$arResult["nStartPage"] = $arResult["NavPageCount"];
	$arResult["nEndPage"] = 1;

	$sPrevHref = '';
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
	{
		$bPrevDisabled = false;
		if ($arResult["bSavePage"])
		{
			$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
		}
		else
		{
			if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1))
			{
				$sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
			}
			else
			{
				$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
			}
		}
	}
	else
	{
		$bPrevDisabled = true;
	}
	
	$sNextHref = '';
	if ($arResult["NavPageNomer"] > 1)
	{
		$bNextDisabled = false;
		$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
	}
	else
	{
		$bNextDisabled = true;
	}
	?>


    <ul class="pagination right">
        <li class="pagination__item prev c-page_pag" data-page="<?=($arResult["nStartPage"] - 1)?>"><?if ($bPrevDisabled):?><a href="" class="pagination__link"><?=GetMessage("nav_prev")?></a><?else:?><a href="" class="pagination__link" id="<?=$ClientID?>_previous_page"><?=GetMessage("nav_prev")?></a><?endif;?></li>
	<?
	$bFirst = true;
	$bPoints = false;
	do
	{
		$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;
		if ($arResult["nStartPage"] <= 2 || $arResult["NavPageCount"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
		{

			if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
	?>
        <li class="pagination__item active" data-page="<?=$NavRecordGroupPrint?>"><a title="" class="pagination__link"><?=$NavRecordGroupPrint?></a></li>
	<?
			elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
	?>
        <li class="pagination__item c-page_pag" data-page="<?=$NavRecordGroupPrint?>"><a href="" class="pagination__link" ><?=$NavRecordGroupPrint?></a></li>
	<?
			else:
	?>
        <li class="pagination__itemc-page_pag" data-page="<?=$NavRecordGroupPrint?>"><a href="" class="pagination__link" ><?=$NavRecordGroupPrint?></a></li>
	<?
			endif;
			$bFirst = false;
			$bPoints = true;
		}
		else
		{
			if ($bPoints)
			{
	?><li class="pagination__item separate"><span class="separate__text">...</span></li><?
				$bPoints = false;
			}
		}
		$arResult["nStartPage"]--;
	} while($arResult["nStartPage"] >= $arResult["nEndPage"]);
}
else
{
	// to show always first and last pages
	$arResult["nStartPage"] = 1;
	$arResult["nEndPage"] = $arResult["NavPageCount"];

	$sPrevHref = '';
	if ($arResult["NavPageNomer"] > 1)
	{
		$bPrevDisabled = false;
		
		if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
		{
			$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
		}
		else
		{
			$sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
		}
	}
	else
	{
		$bPrevDisabled = true;
	}

	$sNextHref = '';
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
	{
		$bNextDisabled = false;
		$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
	}
	else
	{
		$bNextDisabled = true;
	}
	?>


        <ul class="pagination right">
			<?if($arResult["NavPageNomer"] != 1):?>
            	<li class="pagination__item prev c-page_pag" data-page="<?=($arResult["NavPageNomer"] -1 )?>"><?if ($bPrevDisabled):?><a href="" class="pagination__link"><?=GetMessage("nav_prev")?></a><?else:?><a href="" class="pagination__link" id="<?=$ClientID?>_previous_page"><?=GetMessage("nav_prev")?></a><?endif;?></li>
			<?endif;?>

            <?
	$bFirst = true;
	$bPoints = false;
	do
	{
		if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
		{

			if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
	?>
            <li class="pagination__item active"><a title="" class="pagination__link"><?=$arResult["nStartPage"]?></a></li>
	<?
			elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
	?>
            <li class="pagination__item c-page_pag" data-page="<?=$arResult["nStartPage"]?>"><a href="" class="pagination__link"><?=$arResult["nStartPage"]?></a></li>
	<?
			else:
	?>
            <li class="pagination__item c-page_pag" data-page="<?=$arResult["nStartPage"]?>"><a href="" class="pagination__link" ><?=$arResult["nStartPage"]?></a></li>
	<?
			endif;
			$bFirst = false;
			$bPoints = true;
		}
		else
		{
			if ($bPoints)
			{
	?><li class="pagination__item separate"><span class="separate__text">...</span></li><?
				$bPoints = false;
			}
		}
		$arResult["nStartPage"]++;
	} while($arResult["nStartPage"] <= $arResult["nEndPage"]);
}

if ($arResult["bShowAll"]):
	if ($arResult["NavShowAll"]):
?>
            <li class="pagination__item"><a href="" class="pagination__link" class="nav-page-pagen" ><?=GetMessage("nav_paged")?></a></li>
<?
	else:
?>
<!--            <li class="pagination__item"><a href="" class="nav-page-all" href="--><?//=$arResult["sUrlPath"]?><!--?--><?//=$strNavQueryString?><!--SHOWALL_--><?//=$arResult["NavNum"]?><!--=1">--><?//=GetMessage("nav_all")?><!--</a></li>-->
<?
	endif;
endif;
?>
			<?if(($arResult['NavPageNomer'] + 1) <= $arResult['NavPageCount']):?>
				<li class="pagination__item next c-page_pag" data-page="<?=($arResult["NavPageNomer"] + 1)?>">
					<?if ($bNextDisabled):?><a href="" class="pagination__link" ><?=GetMessage("nav_next")?></a><?else:?><a href="" class="pagination__link" id="<?=$ClientID?>_next_page"><?=GetMessage("nav_next")?></a><?endif;?>
				</li>
			<?endif;?>
	</ul>


<?CJSCore::Init();?>
<script type="text/javascript">
	BX.bind(document, "keydown", function (event) {

		event = event || window.event;
		if (!event.ctrlKey)
			return;

		var target = event.target || event.srcElement;
		if (target && target.nodeName && (target.nodeName.toUpperCase() == "INPUT" || target.nodeName.toUpperCase() == "TEXTAREA"))
			return;

		var key = (event.keyCode ? event.keyCode : (event.which ? event.which : null));
		if (!key)
			return;

		var link = null;
		if (key == 39)
			link = BX('<?=$ClientID?>_next_page');
		else if (key == 37)
			link = BX('<?=$ClientID?>_previous_page');

		if (link && link.href)
			document.location = link.href;
	});
</script>