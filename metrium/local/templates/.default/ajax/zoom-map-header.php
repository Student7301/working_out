<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule('iblock')) {
    die();
}
    $el = new CIBLockElement;
    /* Получить список типов помещений */

    /*$dbRes = CIBlockProperty::GetPropertyEnum("articletypeCalc", Array('VALUE' => 'ASC'), Array("IBLOCK_ID"=> \metrium\EstateObject::getIblockIdByCode('novostroyki')));
    while($arApType = $dbRes->Fetch())
    {
        $arApTypes[] = array('ID' => $arApType['ID'], 'NAME' => $arApType['VALUE']);
    }*/
    /* Получить значение минимальной цены новостроек */

    $dbRes = $el->GetList( array('PROPERTY_price' => 'ASC'),  array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), '>PROPERTY_price' => 0, 'ACTIVE' => 'Y',  ),
                            false,
                            array("nTopCount" => 1),
                            array('ID', 'IBLOCK_ID', 'PROPERTY_price')
    );

    if($arApartment = $dbRes->Fetch()){
        $minPrice[] = $arApartment['PROPERTY_PRICE_VALUE'];
    }
    /* Получение минимальной цены элитных новостроек */

    $dbRes = $el->GetList( array('PROPERTY_PRICE_RUB' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), '>PROPERTY_PRICE_RUB' => 1, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_RUB') );

    if($arApartment = $dbRes->Fetch()){
        $minPrice[] = $arApartment['PROPERTY_PRICE_RUB_VALUE'];
    }
    /* Получение минимальной площади коттеджей и поселков */
    $dbRes = $el->GetList( array('PROPERTY_PRICE_RUB' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'), '>PROPERTY_PRICE_RUB' => 1, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_RUB') );

    if($arItem = $dbRes->Fetch()){
        $minPrice[] = $arItem['PROPERTY_PRICE_RUB_VALUE'];
    }


    $minPrice = min($minPrice);


    $minPriceFormatted = \metrium\Price::getFormattedPrice($minPrice);
    $minPriceCurrent = (isset($_REQUEST['priceMin'])) ? $_REQUEST['priceMin'] : $minPrice; // Определение текущей позиции фильтра

    /* Получить значение максимальной цены квартир новостроек  */

    $dbRes = $el->GetList(
        array('PROPERTY_price' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), '>PROPERTY_price' => 1, 'ACTIVE' => 'Y', ),
        false,
        array("nTopCount" => 1),
        array('ID', 'IBLOCK_ID', 'PROPERTY_price')
    );

    if($arApartment = $dbRes->Fetch()){
        $maxPrice[] = $arApartment['PROPERTY_PRICE_VALUE'];
    }

/* Получение максимальной цены элитных новостроек */

    $dbRes = $el->GetList( array('PROPERTY_PRICE_RUB' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), '>PROPERTY_PRICE_RUB' => 1, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_RUB') );

    if($arApartment = $dbRes->Fetch()){
        $maxPrice[] = $arApartment['PROPERTY_PRICE_RUB_VALUE'];
    }
    /* Получение максимальной площади коттеджей и поселков */
    $dbRes = $el->GetList( array('PROPERTY_PRICE_RUB' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'), '>PROPERTY_PRICE_RUB' => 1, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_RUB') );

    if($arItem = $dbRes->Fetch()){
        $maxPrice[] = $arItem['PROPERTY_PRICE_RUB_VALUE'];
    }



    $maxPrice = max($maxPrice);

    $maxPriceFormatted = \metrium\Price::getFormattedPrice($maxPrice);
    $maxPriceCurrent = (isset($_REQUEST['priceMax'])) ? $_REQUEST['priceMax'] : $maxPrice; // Определение текущей позиции фильтра








    /* Получить значение минимальной площади новостроек */

    $dbRes = $el->GetList( array('PROPERTY_spacedesign' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), '>PROPERTY_spacedesign' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign') );

    if($arApartment = $dbRes->Fetch()){
        $minArea[] = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
    }

    /* Получение минимальной площади элитных новостроек */

    $dbRes = $el->GetList( array('PROPERTY_AREA' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), '>PROPERTY_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_AREA') );

    if($arApartment = $dbRes->Fetch()){
        $minArea[] = $arApartment['PROPERTY_AREA_VALUE'];
    }
    /* Получение минимальной площади коттеджей и поселков */
    $dbRes = $el->GetList( array('PROPERTY_AREA' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'), '!PROPERTY_OBJECT_TYPE' => 'uchastki', '>PROPERTY_HOUSE_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_HOUSE_AREA') );

    if($arItem = $dbRes->Fetch()){
        $minArea[] = $arItem['PROPERTY_HOUSE_AREA_VALUE'];
    }

    // В качестве минимального значения площади дляобщего фильтра по всем объектам выбираем минимальное значение площади

        $minArea = min($minArea);
    $minAreaCurrent = (isset($_REQUEST['areaMin'])) ? $_REQUEST['areaMin'] : $minArea; // Определение текущей позиции фильтра

    /* Получить значение максимальной площади новостроек */

    $dbRes = $el->GetList( array('PROPERTY_spacedesign' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), '>PROPERTY_spacedesign' => 0, 'ACTIVE' => 'Y', ),
        false,
        array("nTopCount" => 1),
        array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign')
    );

    if($arApartment = $dbRes->Fetch()){
        $maxArea[] = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
    }

    /* Получение максимальной площади элитных новостроек */

    $dbRes = $el->GetList( array('PROPERTY_AREA' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), '>PROPERTY_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_AREA') );

    if($arApartment = $dbRes->Fetch()){
        $maxArea[] = $arApartment['PROPERTY_AREA_VALUE'];
    }
    /* Получение максимальной площади коттеджей и поселков */
    $dbRes = $el->GetList( array('PROPERTY_AREA' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'), '!PROPERTY_OBJECT_TYPE' => 'uchastki', '>PROPERTY_HOUSE_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_HOUSE_AREA') );

    if($arItem = $dbRes->Fetch()){
        $maxArea[] = $arItem['PROPERTY_HOUSE_AREA_VALUE'];
    }

    // В качестве максимального значения площади дляобщего фильтра по всем объектам выбираем максимальное значение площади

    $maxArea = max($maxArea);

    $maxAreaCurrent = (isset($_REQUEST['areaMax'])) ? $_REQUEST['areaMax'] : $maxArea; // Определение текущей позиции фильтра



?>
<div class="search-line">
    <form action="" method="" class="yandex-search-form">
        <div class="search-input">
            <i class="sprite-icon search"></i>
            <input type="text" class="form-input search-query c-clearable yandex-suggest"  id="suggest2">
            <i class="sprite-icon close-filter c-clear-input clear-input"></i>
        </div>
        <input type="submit" value="Найти" class="btn btn--small-blue">
        <span class="help-inline invisible">Пожалуйста исправьте ошибку в этом поле</span>
    </form>
</div>
<div class="main-filter main-filter--popup bottom-shadow c-switch-filter">
    <div class="main-filter__item c-filter-link <?=($_REQUEST['TYPE'] == 'new')?'active':''?>" data-filterValue="new">
        <a href="" title="" class="main-filter__link">Новостройки</a>
    </div>
    <div class="main-filter__item c-filter-link <?=($_REQUEST['TYPE'] == 'elitCity')?'active':''?>" data-filterValue="elitCity">
        <a href="" title="" class="main-filter__link">Элитная городская</a>
    </div>
    <div class="main-filter__item c-filter-link <?=($_REQUEST['TYPE'] == 'elit')?'active':''?>" data-filterValue="elit">
        <a href="" title="" class="main-filter__link">Элитная загородная</a>
    </div>
<!--    <div class="main-filter__item c-filter-drop" data-filtervalue="apartmentType">
        <a href="" title="" class="main-filter__link">Тип помещения</a>
        <div class="main-filter__dropdown">
            <?/*foreach($arApTypes as $arItem):*/?>
                <div class="checkbox-wrap">
                    <input class="checkbox" type="checkbox" name="type[]" <?/*=(in_array($arItem['NAME'], $_REQUEST['type']))?'checked':''*/?> id="type<?/*=$arItem['ID']*/?>" value="<?/*=$arItem['NAME']*/?>">
                    <label class="checkbox-label" for="type<?/*=$arItem['ID']*/?>" data-checked="<?/*=(in_array($arItem['NAME'], $_REQUEST['type']))?'Y':'N'*/?>">
                        <span class="checkbox-icon"></span>
                        <span class="checkbox-text"><?/*=$arItem['NAME']*/?></span>
                    </label>
                </div>
            <?/*endforeach;*/?>
        </div>
    </div>-->
    <div class="main-filter__item c-filter-drop" data-filtervalue="area">
        <a href="" title="" class="main-filter__link">Площадь</a>
        <div class="main-filter__dropdown main-filter__dropdown--right c-input-option">
            <div class="position-block">
                <input class="form-input form-input--middle filter-input c-format-price" type="text" name="areaFrom" value="<?=$minArea?>" data-default="<?=$minArea?>">
                <span class="input-prefix">от</span>
            </div>
			<div class="position-block position-block--range">
				<div class="elite-slider c-range_m" data-min="<?=$minArea?>" data-max="<?=$maxArea?>" data-val-min="<?=$minArea?>" data-val-max="<?=$maxArea?>" data-range="" data-noinp="true" data-move="true" data-min-input="areaFrom" data-max-input="areaTo"></div>
			</div>
            <div class="position-block">
                <input class="form-input form-input--middle filter-input c-format-price" type="text" name="areaTo" value="<?=$maxArea?>" data-default="<?=$maxArea?>">
                <span class="input-prefix">до</span>
            </div>
            <a href="" title="" class="btn btn--middle c-clear-inputs">Сбросить</a>
        </div>
    </div>

    <div class="main-filter__item c-filter-drop" data-filtervalue="price">
        <a href="" title="" class="main-filter__link">Цена</a>
        <div class="main-filter__dropdown main-filter__dropdown--right c-input-option c-price_dropd">
            <div class="position-block">
                <select class="custom-select c-priceval price_elite">
                    <option value="USD">USD</option>
                    <option value="RUB">RUB</option>
                </select>
            </div>
            <div class="position-block">
                <input class="form-input form-input--middle filter-input c-format-price" type="text" name="priceFrom" value="0" data-default="<?=$minPrice?>" data-defaultUSD="<?=metrium\Price::getPriceUsd($minPrice)?>">
                <span class="input-prefix">от</span>
            </div>
			<div class="position-block position-block--range">
				<div class="elite-slider c-range_m" data-min="<?=metrium\Price::getPriceUsd($minPrice)?>" data-max="<?=metrium\Price::getPriceUsd($maxPrice)?>" data-val-min="<?=metrium\Price::getPriceUsd($minPrice)?>" data-val-max="<?=metrium\Price::getPriceUsd($maxPrice)?>" 
					data-val-min-rub="<?=$minPrice?>" data-val-max-rub="<?=$maxPrice?>" data-range="" data-noinp="true" data-move="true" data-min-input="priceFrom" data-max-input="priceTo"></div>
			</div>
            <div class="position-block">
                <input class="form-input form-input--middle filter-input c-format-price" type="text" name="priceTo" value="0" data-default="<?=$maxPrice?>" data-defaultUSD="<?=metrium\Price::getPriceUsd($maxPrice)?>">
                <span class="input-prefix">до</span>
            </div>
            <a href="" title="" class="btn btn--middle c-clear-inputs">Сбросить</a>
        </div>
    </div>



    <div class="main-filter__icon">
        <i class="sprite-icon map-filter"></i>
    </div>
</div>
<?php
/*
<div class="legend">
	<span class="legend__item"><img src="/local/templates/.default/images/new.png" alt=""> - Новостройки</span>
	<span class="legend__item"><img src="/local/templates/.default/images/elit.png" alt=""> - Элитная недвижимость</span>
	<span class="legend__item"><img src="/local/templates/.default/images/cluster.png" alt=""> - Место с несколькими объектами</span>
</div>
*/
?>

<div id="big-map" class="popup-map"></div>

<script type="text/javascript">
    $(function() {
        siteYandexMap.isPopupFilter = true;

        $.each($('.c-format-price'), function(){
            var elem = $(this);

            getPriceFormat(elem);
        });

        var select = $(".c-priceval");
        select.select2({
            minimumResultsForSearch: Infinity
        }).on("select2-open", function (e) {
            e.preventDefault();
            $('.c-price_dropd').parent('.main-filter__item').addClass('active');
        }).on("select2-close", function (e) {
            e.preventDefault();
            setTimeout(function(){
                $('.c-price_dropd').parent('.main-filter__item').removeClass('active');
            },1000);
        });
		
		var rangeList_m = $(".c-range_m");
		if (rangeList_m.length > 0) {
			$.each(rangeList_m, function (i) {
				var elem = $(this);
				rangeNoUiSlider(elem);
				rangeList_m[i].noUiSlider.on('update', function(values){
					
				});
			});
		}
		
		

    });
</script>

