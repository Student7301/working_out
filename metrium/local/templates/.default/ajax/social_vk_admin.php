<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use metrium\helpers\VkHelper;

if (!empty($_GET['hashtag'])) {
    $start = !empty($_GET['next_from']) ? $_GET['next_from'] : '';
    $vk = new VkHelper();

    echo $vk->parseHashtag($_GET['hashtag'], $start)->getJson(true);
}