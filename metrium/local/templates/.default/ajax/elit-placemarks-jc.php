<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//Файл работает в двух режимах - либо выгружаем ЖК из раздела ($_REQUEST['SECTION']), либо выгружаем корпуса ЖК по его ID ($_REQUEST['OBJ_ID'])
// Каждый из режимов работает с одним из двух уровней детализации: 1 - Все объекты, 2 - Корпусы объекта, если передан ID, lvl и type

$objects = array('type' => "FeatureCollection", "features" => array());

    \CModule::IncludeModule('iblock');
    $el = new CIBlockElement;

    /*************************************************************************************************************/
    /************************Если переданы параметры запроса lvl=2, type, и OBJ_ID, значит************************/
    /***********************необходимо загружать корпусы выбранного объекта *********************************/
    /***************************************************************************************************************/
    if(
        $_REQUEST['OBJ_ID'] &&
        ($_REQUEST['lvl'] == '2') &&
        $_REQUEST['type']
    ){
        switch($_REQUEST['type']){
            case 'new':
                //$_REQUEST['OBJ_ID'] = 72968;
                $dbRes = $el->GetList(
                    array(),
                    array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('corpuses'),
                          'ACTIVE' => 'Y',
                          'PROPERTY_OBJECT_ID' => $_REQUEST['OBJ_ID']//19769
                          ),
                    false,
                    false,
                    array('ID', 'NAME', 'IBLOCK_ID', 'CODE', 'PREVIEW_PICTURE', 'PROPERTY_LATITUDE', 'PROPERTY_LONGITUDE', 'PROPERTY_project', 'PROPERTY_YANDEX_MAP')
                );
                while ($arItem = $dbRes->Fetch()) {

                    // Получение максимального и минимального значения цены квартир у корпуса

                    $sections = \metrium\EstateObject::getSectionsByCorpuses($arItem['ID']);
                    $minPrice = \metrium\EstateObject::getApartmentMinPrice($sections);
                    $maxPrice = \metrium\EstateObject::getApartmentMaxPrice($sections);

                    $minPriceFormatted = \metrium\Price::getFormattedPrice($minPrice);
                    $maxPriceFormatted = \metrium\Price::getFormattedPrice($maxPrice);

                    // Получить значение минимальной площади квартиры
                    $dbApRes = $el->GetList( array('PROPERTY_SPACEDESIGN' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), 'PROPERTY_SECTION_ID' => $sections,'>PROPERTY_spacedesign' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign') );

                    if($arApartment = $dbApRes->Fetch()){
                        $minArea = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
                    }
                    // Получить значение минимальной площади квартиры
                    $dbApRes = $el->GetList( array('PROPERTY_SPACEDESIGN' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), 'PROPERTY_SECTION_ID' => $sections,'>PROPERTY_spacedesign' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign') );

                    if($arApartment = $dbApRes->Fetch()){
                        $maxArea = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
                    }


                   // echo '<pre>';  print_r($arItem); echo '<pre>';
                    if(

                        !($minPrice > 0)  ||
                        !($maxPrice > 0)  ||
                        !($minArea > 0)  || // Не удалось установить значение площади
                        !($maxArea > 0)  || // Не удалось установить значение площади
                        !$arItem['PROPERTY_YANDEX_MAP_VALUE']
                    ){
                        //echo '<pre>'; print_r($arItem); echo '</pre>';
                        continue;
                    }



                    // Получить изображение объекта
                    $dbObjRes = $el->GetList( array(), array('IBLOCK_CODE' => 'novostroyki', 'ID' => $_REQUEST['OBJ_ID'],'ACTIVE' => 'Y'),
                        false, false,  array('ID', 'NAME', 'CODE', 'PREVIEW_PICTURE', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'PROPERTY_EXTERNAL_OBJECT', 'PROPERTY_EXTERNAL_OBJECT_URL' ));
                    if( $arObj = $dbObjRes->Fetch() ){
                        $ar = CFile::ResizeImageGet($arObj['PREVIEW_PICTURE'], Array("width" => 116, "height" => 80));
                        $image = $ar['src'];
                        $extUrl = (!empty($arObj['PROPERTY_EXTERNAL_OBJECT_VALUE'])) ? $arObj['PROPERTY_EXTERNAL_OBJECT_URL_VALUE'] : '';
                    }

                    // Получить название секции по коду
                    $dbSectRes = CIBlockSection::GetList( array("SORT"=>"ASC"),array( 'ID' => $arObj['IBLOCK_SECTION_ID']), false, array('CODE', 'ID'), false );
                    if($arSectCode = $dbSectRes->Fetch()){
                        $sectionCode = $arSectCode['CODE'];
                    }
                    $url =  (!empty($extUrl)) ? $extUrl : '/novostroyki/' . $sectionCode . '/' . $arObj['CODE'] . '/v-prodazhe/';

                $coords = explode(',', $arItem['PROPERTY_YANDEX_MAP_VALUE']);
                $objects['features'][] = array(
                        'type' => 'Feature',
                        'geometry' => array(
                            'type' => 'Point',
                            'coordinates' => array(
                                $coords[0],
                                $coords[1])
                        ),
                        'properties' => array(
                            'hintContent' => $arItem['NAME'],
                            "iconColor" => '#66a3e4',
                            'info_list' => array(
                                'price' => 'Цена : ' . $minPriceFormatted . ' - ' . $maxPriceFormatted . ' - руб.',
                                'area' => 'Площадь квартир: ' . $minArea . ' - ' . $maxArea . ' м2',
                            ),
                            //"objCount" => 1,
                            'image' => $image,
                            "url" => $url,
                            'id' => $arItem['ID'],
                            "type" => "new",
                            "filterType" => 'Квартира',
                            "filterPriceMin" => $minPrice,
                            "filterPriceMax" => $maxPrice,
                            "filterPriceMinUSD" => (string) metrium\Price::getPriceUsd($minPrice),
                            "filterPriceMaxUSD" => (string) metrium\Price::getPriceUsd($maxPrice),
                            "filterAreaMin" => $minArea,
                            "filterAreaMax" => $maxArea,

                        )
                    );



                }

                    break;
            case 'elitCity':
               // $_REQUEST['OBJ_ID'] = 23664;
                $dbRes = $el->GetList(
                    array(),
                    array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_corpuses'),
                        'ACTIVE' => 'Y', 'PROPERTY_building' => $_REQUEST['OBJ_ID']//23664
                    ),
                    false,
                    false,
                    array('ID',
                        'NAME',
                        'IBLOCK_ID',
                        'PREVIEW_PICTURE',
                        'PROPERTY_YANDEX_MAP',
                        'PROPERTY_building',
                        'PROPERTY_FLATS_TYPE',
                    )
                );
                while ($arItem = $dbRes->Fetch()) {
                    //получаем мининмальную и минимальную цену в данном ЖК

                    $dbApRes = $el->GetList( array('PROPERTY_PRICE_USD' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $_REQUEST['OBJ_ID'],'>PROPERTY_PRICE_USD' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_USD', 'PROPERTY_building') );

                    if($arApartment = $dbApRes->Fetch()){
                        $minPrice = $arApartment['PROPERTY_PRICE_USD_VALUE'];
                    }

                    $dbApRes = $el->GetList( array('PROPERTY_PRICE_USD' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $_REQUEST['OBJ_ID'],'>PROPERTY_PRICE_USD' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_USD', 'PROPERTY_building') );

                    if($arApartment = $dbApRes->Fetch()){
                        $maxPrice = $arApartment['PROPERTY_PRICE_USD_VALUE'];
                    }

                    $minPriceFormatted = \metrium\Price::getFormattedPrice($minPrice);
                    $maxPriceFormatted = \metrium\Price::getFormattedPrice($maxPrice);

                    //получаем мининмальную и минимальную площадь квартиры в данном ЖК

                    $dbApRes = $el->GetList( array('PROPERTY_AREA' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $_REQUEST['OBJ_ID'],'>PROPERTY_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_AREA', 'PROPERTY_building') );

                    if($arApartment = $dbApRes->Fetch()){
                        $minArea = $arApartment['PROPERTY_AREA_VALUE'];
                    }

                    $dbApRes = $el->GetList( array('PROPERTY_AREA' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $_REQUEST['OBJ_ID'],'>PROPERTY_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_AREA', 'PROPERTY_building') );

                    if($arApartment = $dbApRes->Fetch()){
                        $maxArea = $arApartment['PROPERTY_AREA_VALUE'];
                    }


                    if(
                        !($minPrice > 0)  ||
                        !($maxPrice > 0)  ||
                        !($minArea > 0)  || // Не удалось установить значение площади
                        !($maxArea > 0)  || // Не удалось установить значение площади
                        !$arItem['PROPERTY_YANDEX_MAP_VALUE']

                    ){

                        continue;
                    }


// Получить изображение обънекта
                    $dbObjRes = $el->GetList( array(), array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_buildings'), 'ID' => $_REQUEST['OBJ_ID'],'ACTIVE' => 'Y'),
                        false, false,  array('ID', 'NAME', 'PREVIEW_PICTURE', 'IBLOCK_ID', 'PROPERTY_address', 'CODE' ));
                    if( $arObj = $dbObjRes->Fetch() ){
                        $ar = CFile::ResizeImageGet($arObj['PREVIEW_PICTURE'], Array("width" => 116, "height" => 80));
                        $image = $ar['src'];
                        //$objName = $arObjPic['NAME'];
                        $address = $arObj['PROPERTY_ADDRESS_VALUE'];
                        $url = '/elitnaya-gorodskaya-nedvijimost/elitnie-jilie-kompleksi/' . $arObj['CODE'] . '/?SHOW_TABLE=Y';
                    }




                    $yand = explode(',', $arItem['PROPERTY_YANDEX_MAP_VALUE']);
                    $objects['features'][] = array(
                        'type' => 'Feature',
                        'geometry' => array(
                            'type' => 'Point',
                            'coordinates' => array(
                                $yand[0],
                                $yand[1])
                        ),
                        'properties' => array(
                            'hintContent' => $arItem['NAME'],
                            "iconColor" => '#009d7e',
                            'info_list' => array(
                                'price' => 'Цена : ' . $minPriceFormatted . ' - ' . $maxPriceFormatted . ' - руб.',
                                'area' => 'Площадь квартир: ' . $minArea . ' - ' . $maxArea . ' м2',
                            ),
                            //"objCount" => 1,
                            'image' => $image,
                            "url" => $url,
                            'id' => $arItem['ID'],
                            "type" => "elitCity",
                            "filterType" => 'Апартаменты',
                            "filterPriceMin" => $minPrice,
                            "filterPriceMax" => $maxPrice,
                            "filterPriceMinUSD" => (string) metrium\Price::getPriceUsd($minPrice),
                            "filterPriceMaxUSD" => (string) metrium\Price::getPriceUsd($maxPrice),
                            "filterAreaMin" => $minArea,
                            "filterAreaMax" => $maxArea,

                        )
                    );



                }


            break;
            default:
            break;


        }

        echo json_encode($objects);
        exit;

    }

/*********************************************************************************************************************/
/*****************************  Выборка данных для первого уровня карты *********************************************/
/*********************************************************************************************************************/


    $obCache = new CPHPCache();

    $cacheId = 'arYaMapObjects';
    $cachePath = '/arYaMapObjects/';

    if( $obCache->InitCache(3600, $cacheId, $cachePath) )// Если кэш валиден
    {
        $objects = $obCache->GetVars();// Извлечение переменных из кэша
    }
    elseif( $obCache->StartDataCache()  )// Если кэш невалиден
    {


    /*******************************/
    /* Получаем список новостроек */
    /*******************************/

    $dbRes = $el->GetList(
        array(),
        array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki'), 'ACTIVE' => 'Y', /*'!SECTION_CODE' => 'novostroyki-sankt-piterburga'*/),
        false,
        false,
        array('ID',
                'NAME',
                'CODE',
                'IBLOCK_ID',
                'PREVIEW_PICTURE',
                'IBLOCK_SECTION_ID',
                'PROPERTY_YANDEX_MAP',
                'PROPERTY_deadline',
                'PROPERTY_address',
                'PROPERTY_SPACE_ALL',
                'PROPERTY_FLATS_TYPE',
                'PROPERTY_EXTERNAL_OBJECT',
                'PROPERTY_EXTERNAL_OBJECT_URL',
                'PROPERTY_HIDE_PRICES'
        )
    );
    while ($arItem = $dbRes->Fetch()) {

        //получаем максимальную и минимальную цену в данном ЖК

        $corpuses = \metrium\EstateObject::getCorpusesByObjects($arItem['ID'],array('ID', 'PROPERTY_YANDEX_MAP'), true);
       //echo '<pre>'; print_r($corpuses); echo '</pre><hr>';
        if(!empty($corpuses))
            $sections = \metrium\EstateObject::getSectionsByCorpuses($corpuses);
        if(!empty($sections)) {
            $minPrice = \metrium\EstateObject::getApartmentMinPrice($sections);
            $maxPrice = \metrium\EstateObject::getApartmentMaxPrice($sections);

            $minPriceFormatted = \metrium\Price::getFormattedPrice($minPrice);
            $maxPriceFormatted = \metrium\Price::getFormattedPrice($maxPrice);


        }
//        echo '<pre>'; print_r($arItem['ID']); echo '</pre>';
//        echo '<pre>'; print_r($minPrice); echo '</pre>';
//        echo '<pre>'; print_r($maxPrice); echo '</pre>__';
        $objCount = count($corpuses);

        ///////////////////////////////////////////////////////////////////////////


        // Получить значение минимальной площади квартиры
        $dbApRes = $el->GetList( array('PROPERTY_SPACEDESIGN' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), 'PROPERTY_SECTION_ID' => $sections,'>PROPERTY_spacedesign' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign') );

        if($arApartment = $dbApRes->Fetch()){
            //echo '<pre>'; print_r($arApartment); echo '</pre>';
            $minArea = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
        }
        // Получить значение минимальной площади квартиры
        $dbApRes = $el->GetList( array('PROPERTY_SPACEDESIGN' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'), 'PROPERTY_SECTION_ID' => $sections,'>PROPERTY_spacedesign' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign') );

        if($arApartment = $dbApRes->Fetch()){
            $maxArea = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
        }

        /* Исключить из выборки объекты, с некорректными данными, которые участвуют в отображениии и фильтрации */

        if(
            !($minPrice > 0)  ||
            !($maxPrice > 0)  ||
            !($minArea > 0)  || // Не удалось установить значение площади
            !($maxArea > 0)  || // Не удалось установить значение площади
            !$arItem['PROPERTY_YANDEX_MAP_VALUE']
        ){
            continue;
        }

        $ar = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array("width" => 116, "height" => 80));
        $image = $ar['src'];
        // Получить название секции по коду
        $dbSectRes = CIBlockSection::GetList( array("SORT"=>"ASC"),array( 'ID' => $arItem['IBLOCK_SECTION_ID']), false, array('CODE', 'ID'), false );
        if($arSectCode = $dbSectRes->Fetch()){
            $sectionCode = $arSectCode['CODE'];
        }
        ///////////////////////////////////
        $url = (!empty($arItem['PROPERTY_EXTERNAL_OBJECT_VALUE'])) ? $arItem['PROPERTY_EXTERNAL_OBJECT_URL_VALUE'] : '/novostroyki/' . $sectionCode . '/' . $arItem['CODE'] . '/v-prodazhe/';
        $apType = implode(', ', $arItem['PROPERTY_FLATS_TYPE_VALUE']);
        $coords = explode(',', $arItem['PROPERTY_YANDEX_MAP_VALUE']);
		$tempArr = array(
                'type' => 'Feature',
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array(
                        $coords[0],
                        $coords[1])
                ),
                'properties' => array(
                    'hintContent' => $arItem['NAME'],
                    "iconColor" => '#66a3e4',
                    'info_list' => array(
                        'price' => ($arItem['CODE'] != 'zelenograd') ? ($arItem['PROPERTY_HIDE_PRICES_VALUE'] != 'Y') ? 'Цена: ' . $minPriceFormatted . ' руб. - ' . $maxPriceFormatted . ' руб.' : "Цена по запросу" : 'Цена: 1 721 139 руб. - 6 536 815 руб.',
                        'address' => 'Адрес: ' . $arItem['PROPERTY_ADDRESS_VALUE'],
                        'type' => 'Тип помещения: ' . $apType ,
                        'period' => 'Срок сдачи: ' . $arItem['PROPERTY_DEADLINE_VALUE'],
                        'area' => ($arItem['CODE'] != 'zelenograd') ? 'Площадь квартир : ' . $minArea . ' - ' . $maxArea .' м2' : 'Площадь квартир : 21.7 - 79.5 м2',

                    ),
                    "objCount" => $objCount . ' корпусов',
                    'image' => $image,
                    "url" => $url,
                    'price' => ($arItem['CODE'] != 'zelenograd') ? ($arItem['PROPERTY_HIDE_PRICES_VALUE'] != 'Y') ? $minPriceFormatted . ' - ' . $maxPriceFormatted . ' руб.' : "Цена по запросу" : 'Цена: 1 721 139 руб. - 6 536 815 руб.',
                    'id' => $arItem['ID'],
                    "type" => "new",
                    "filterType" => "Квартира",
                    "filterPriceMin" => $minPrice,
                    "filterPriceMax" => $maxPrice,
                    "filterPriceMinUSD" => (string) metrium\Price::getPriceUsd($minPrice),
                    "filterPriceMaxUSD" => (string) metrium\Price::getPriceUsd($maxPrice),
                    "filterAreaMin" => $minArea,
                    "filterAreaMax" => $maxArea,

                )
            );
        $objects['features'][] = $tempArr;


    }


    /****************************************/
    /* Получаем список элитных новостройки */
    /***************************************/

        $dbRes = $el->GetList(
            array(),
            array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_buildings'), 'ACTIVE' => 'Y'),
            false,
            false,
            array('ID',
                'NAME',
                'CODE',
                'IBLOCK_ID',
                'PREVIEW_PICTURE',
                'PROPERTY_YANDEX_MAP',
                'PROPERTY_address',
                'PROPERTY_complete_date',
                'PROPERTY_SPACE_ALL',
                'PROPERTY_FLATS_TYPE',
                'PROPERTY_BUILDING_PROCESS'
            )
        );
    while ($arItem = $dbRes->Fetch()) {
    //echo '<pre>'; print_r($arItem); echo '<pre>';

        //получаем мининмальную и минимальную цену в данном ЖК

        $dbApRes = $el->GetList( array('PROPERTY_PRICE_USD' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $arItem['ID'],'>PROPERTY_PRICE_USD' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_USD', 'PROPERTY_building') );

        if($arApartment = $dbApRes->Fetch()){
            $minPrice = $arApartment['PROPERTY_PRICE_USD_VALUE'];
        }

        $dbApRes = $el->GetList( array('PROPERTY_PRICE_USD' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $arItem['ID'],'>PROPERTY_PRICE_USD' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE_USD', 'PROPERTY_building') );

        if($arApartment = $dbApRes->Fetch()){
            $maxPrice = $arApartment['PROPERTY_PRICE_USD_VALUE'];
        }

        $minPriceFormatted = \metrium\Price::getFormattedPrice($minPrice);
        $maxPriceFormatted = \metrium\Price::getFormattedPrice($maxPrice);

        //получаем мининмальную и минимальную площадь квартиры в данном ЖК

        $dbApRes = $el->GetList( array('PROPERTY_AREA' => 'ASC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $arItem['ID'],'>PROPERTY_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_AREA', 'PROPERTY_building') );

        if($arApartment = $dbApRes->Fetch()){
            $minArea = $arApartment['PROPERTY_AREA_VALUE'];
        }

        $dbApRes = $el->GetList( array('PROPERTY_AREA' => 'DESC'), array( 'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'PROPERTY_building' => $arItem['ID'],'>PROPERTY_AREA' => 0, 'ACTIVE' => 'Y', ), false, array("nTopCount" => 1),  array('ID', 'IBLOCK_ID', 'PROPERTY_AREA', 'PROPERTY_building') );

        if($arApartment = $dbApRes->Fetch()){
            $maxArea = $arApartment['PROPERTY_AREA_VALUE'];
        }

        //$apType = implode(', ', $arItem['PROPERTY_FLATS_TYPE_VALUE']);


        $url = '/elitnaya-gorodskaya-nedvijimost/elitnie-jilie-kompleksi/' . $arItem['CODE'] . '/?SHOW_TABLE=Y';


        //Получить кол-во корпусов

        $corpuses = array();
        $dbCorpRes = $el->GetList(array(), array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_corpuses'), 'ACTIVE' => 'Y', '!PROPERTY_YANDEX_MAP' => false, 'PROPERTY_building' => $arItem['ID']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_building', 'PROPERTY_YANDEX_MAP'));
        while($arCorp = $dbCorpRes->Fetch()){
                /* Проверить наличие квартир по привязке к корпусу */
                $dbApartments = $el->GetList(array(), array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('elite_room'), 'ACTIVE' => 'Y', 'PROPERTY_CORPUS' => $arCorp['ID'], '>PROPERTY_PRICE_USD' => 0, '!PROPERTY_PRICE_USD' => false, '>PROPERTY_AREA' => 0, '!PROPERTY_AREA' => false), false, false, array('ID', 'PROPERTY_CORPUS', 'PROPERTY_PRICE_USD', 'PROPERTY_AREA'));
                if( $dbApartments->SelectedRowsCount() > 0 ){
                   $corpuses[] = $arCorp['ID'];
                }
        }
    $objCount = count($corpuses);
    ///////////////////////////////////////////////////////////////////////////

        /* Исключить из выборки объекты, с некорректными данными, которые участвуют в отображениии и фильтрации */

        if(
            !($minPrice > 0)  ||
            !($maxPrice > 0)  ||
            !($minArea > 0)  || // Не удалось установить значение площади
            !($maxArea > 0)  || // Не удалось установить значение площади
            !$arItem['PROPERTY_YANDEX_MAP_VALUE']
        ){
            continue;
        }

        $ar = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array("width" => 116, "height" => 80));
        $image = $ar['src'];

        $coords = explode(',', $arItem['PROPERTY_YANDEX_MAP_VALUE']);
        $objects['features'][] = array(
            'type' => 'Feature',
            'geometry' => array(
                'type' => 'Point',
                'coordinates' => array(
                    $coords[0],
                    $coords[1])
            ),
            'properties' => array(
                'hintContent' => $arItem['NAME'],
                "iconColor" => '#009d7e',
                'info_list' => array(
                    'price' => 'Цена: ' . $minPriceFormatted . '$ - ' . $maxPriceFormatted . '$',
                    'address' => 'Адрес: ' . $arItem['PROPERTY_ADDRESS_VALUE'],
                    'period' => 'Срок сдачи: ' . $arItem['PROPERTY_COMPLETE_DATE_VALUE'],
                     'area' => 'Площади квартир : ' . $minArea . ' - ' . $maxArea . ' м2',
                    'type' => 'Тип помещения: ' . $apType,
                ),
                "objCount" => $objCount . ' корпусов',
                'image' => $image,
                "url" => $url,
                'price' => $minPriceFormatted . '$ - ' . $maxPriceFormatted . '$',
                'id' => $arItem['ID'],
                "type" => "elitCity",
                "filterType" => "Апартаменты",
                "filterPriceMin" => (string) metrium\Price::getPriceRub($minPrice),
                "filterPriceMax" => (string) metrium\Price::getPriceRub($maxPrice),
                "filterPriceMinUSD" => $minPrice,
                "filterPriceMaxUSD" => $maxPrice,
                "filterAreaMin" => $minArea,
                "filterAreaMax" => $maxArea,

            )
        );

}

    /****************************************/
    /* Получаем список загородной недвижимости */
    /***************************************/

    $dbRes = $el->GetList(
        array(),
        array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('for_filter'), 'ACTIVE' => 'Y'),
        false,
        false,
        array('ID',
            'IBLOCK_ID',
            'NAME',
            'PROPERTY_LATITUDE',
            'PROPERTY_LONGITUDE',
            'PROPERTY_HOUSE_AREA',
            'PROPERTY_TERRITORY_AREA',
            'PROPERTY_REMOTNESS_MKAD',
            'PROPERTY_DIRECTION',
            'PROPERTY_OBJECT_ID',
            'PROPERTY_OBJECT_TYPE',
            'PROPERTY_ARENDA',
            'PROPERTY_UCHASTOK_TYPE',
            'PROPERTY_PRICE_USD',
            'PROPERTY_PRICE_EUR',
            'PROPERTY_PRICE_RUB',
            //'PROPERTY_CRM_ID',


        )
    );
    while ($arItem = $dbRes->Fetch()) {
    //echo '<pre>'; print_r($arItem); echo '</pre><hr>';

    $price = $arItem['PROPERTY_PRICE_USD_VALUE'];
    $priceFormatted = \metrium\Price::getFormattedPrice($price);


    switch($arItem['PROPERTY_OBJECT_TYPE_VALUE']){
        case 'kottedzhi_taunkhausy':
            $type = 'Коттедж';
            $filterType = 'Коттедж';
            $hintContent = 'Коттедж:' . $arItem['NAME'];
            $filterArea = $arItem['PROPERTY_HOUSE_AREA_VALUE'];
            $houseArea = 'Площадь дома: ' . $arItem['PROPERTY_HOUSE_AREA_VALUE'] . 'м2';
            $dbCotRes = $el->GetList(array(), array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('kottedzhi_taunkhausy'), 'ID' => $arItem['PROPERTY_OBJECT_ID_VALUE']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_CRM_ID', 'PREVIEW_PICTURE'));
            if($arCotRes = $dbCotRes->Fetch()){
                $objectId = $arCotRes['PROPERTY_CRM_ID_VALUE'];
                $ar = CFile::ResizeImageGet($arCotRes['PREVIEW_PICTURE'], Array("width" => 116, "height" => 80));
                $image = $ar['src'];
            }
        break;
        case 'uchastki':
            $type = 'Участок';
            $filterType = 'Земельный участок';
            $hintContent = 'Участок:'  . $arItem['NAME'];
            $filterArea = $arItem['PROPERTY_TERRITORY_AREA_VALUE'];
            $houseArea = 'Площадь дома: - ';
            $dbUchRes = $el->GetList(array(), array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('uchastki'), 'ID' => $arItem['PROPERTY_OBJECT_ID_VALUE']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_CRM_ID', 'PREVIEW_PICTURE', 'CODE'));
            if($arUchRes = $dbUchRes->Fetch()){
                $objectId = $arUchRes['CODE'];
                $ar = CFile::ResizeImageGet($arUchRes['PREVIEW_PICTURE'], Array("width" => 116, "height" => 80));
                $image = $ar['src'];
            }
        break;
        case 'poselki':
            $type = 'Поселок';
            $filterType = 'Коттедж';
            $hintContent = 'Поселок:' . $arItem['NAME'];
            $filterArea = $arItem['PROPERTY_HOUSE_AREA_VALUE'];
            $houseArea = 'Площадь дома: ' . $arItem['PROPERTY_HOUSE_AREA_VALUE'] . 'м2';
            $dbPosRes = $el->GetList(array(), array('IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('poselki'), 'ID' => $arItem['PROPERTY_OBJECT_ID_VALUE']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_CRM_ID', 'PREVIEW_PICTURE'));
            if($arPosRes = $dbPosRes->Fetch()){
                $objectId = $arPosRes['PROPERTY_CRM_ID_VALUE'];
                $ar = CFile::ResizeImageGet($arPosRes['PREVIEW_PICTURE'], Array("width" => 116, "height" => 80));
                $image = $ar['src'];
            }
        break;
        default:
            $hintContent = false;
            $filterArea = 0;
            continue 2;
        break;

    }
        $url = '/zagorodnaya-nedvizhimost/' . $arItem['PROPERTY_OBJECT_TYPE_VALUE'] . '/' . $objectId . '/';

        if( !($filterArea > 0)  ||  // Не удалось установить значение площади
            !$arItem['PROPERTY_LATITUDE_VALUE'] ||  // Не установлено значение широты
            !$arItem['PROPERTY_LONGITUDE_VALUE'] || // Не установлено значение долготы
            !($arItem['PROPERTY_LATITUDE_VALUE'] > $arItem['PROPERTY_LONGITUDE_VALUE']) // Значение широты должно быть больше значения долготы

        ){
            continue;
        }
     $objects['features'][] = array(
            'type' => 'Feature',
            'geometry' => array(
                'type' => 'Point',
                'coordinates' => array(
                    $arItem['PROPERTY_LATITUDE_VALUE'],
                    $arItem['PROPERTY_LONGITUDE_VALUE'])
            ),
            'properties' => array(
                'hintContent' => $hintContent,
                "iconColor" => '#009d7e',
                'info_list' => array(
                    'price' => 'Цена : ' . $priceFormatted . '$',
                    'territory_area' => 'Площадь участка: ' . $arItem['PROPERTY_TERRITORY_AREA_VALUE'] . ' сот.',
                    'house_area' => $houseArea,
                    'direction' => 'Направление: ' . $arItem['PROPERTY_DIRECTION_VALUE'],
                    'remotness_mkad' => 'Удаленность от МКАД: ' . $arItem['PROPERTY_REMOTNESS_MKAD_VALUE'] . 'км.',
                    'type' => 'Тип объекта: ' . $type,
                ),
                "objCount" => 1,
                'image' => $image,
                "url" => $url,
                'price' => $priceFormatted . '$',
                'id' => $arItem['ID'],
                "type" => "elit",
                "filterType" => $filterType,
                "filterPriceMin" => (string) metrium\Price::getPriceRub($price),
                "filterPriceMax" => (string) metrium\Price::getPriceRub($price),
                "filterPriceMinUSD" => $price,
                "filterPriceMaxUSD" => $price,
                "filterAreaMin" => $filterArea,
                "filterAreaMax" => $filterArea,

            )
        );


}

        $obCache->EndDataCache($objects);// Сохраняем переменные в кэш.
    }

echo json_encode($objects);