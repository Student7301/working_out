<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;

$fields = [];
$request = Application::getInstance()->getContext()->getRequest()->toArray();

if($request['template_type'] == 'mobile') {
    $fields = $request;
}else {
    foreach ($request as $type) {
        foreach ($type as $field) {
            $fields[$field['name']] = $field['value'];
        }
    }
}

$arFields = array(
    "NAME" => $fields["OF-name"],
    "EMAIL" => $fields["OF-email"],
    "PHONE" => $fields["OF-phone"],
    "OFFER_PRICE" => $fields["OF-price"] . ' ' . $fields['currency'],
    "MESSAGE" => $fields["OF-message"],
    'OBJECT_NAME' => $fields['Object_name'],
    'OBJECT_ID' => $fields['Object_id'],
    'OBJECT_URL' => $fields['Object_url'],
    'PRICE' => $fields['price'],
    'PRICE_SALE' => $fields['price_sale']
);

if($arFields['NAME'] == '' || $arFields['EMAIL'] == '' || $arFields['PHONE'] == '' || $fields["OF-price"] == '') {
    die(json_encode(array('result' => 'error', 'mess' => 'Заполните обязательные поля!')));
}

if(iconv_strlen($arFields['NAME'],'UTF-8') > 96) {
    die(json_encode(array('result' => 'error', 'mess' => 'Имя не может быть длиннее 96 символов!')));
}

if(iconv_strlen($arFields['MESSAGE'],'UTF-8') > 512) {
    die(json_encode(array('result' => 'error', 'mess' => 'Сообщение не может содержать больше 512 символов!')));
}

if(!is_numeric($fields["OF-price"])) {
    die(json_encode(array('result' => 'error', 'mess' => 'Предлагаемая цена должна быть числом!')));
}

if(!preg_match('/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/', $arFields['EMAIL'])) {
    die(json_encode(array('result' => 'error', 'mess' => 'Не верный формат E-Mail!')));
}

define('TYPE_EMAIL', 3469);
if($fields['type_message'] == 'zagorodka') {
    define('EMAIL_TEMPLATE', 96);
}elseif($fields['type_message'] == 'newbuilding') {
    define('EMAIL_TEMPLATE', 95);
}elseif($fields['type_message'] == 'eliteCITY') {
    define('EMAIL_TEMPLATE', 97);
}

$send = new CEvent();
if (!CEvent::Send(TYPE_EMAIL, 's1', $arFields, "Y", EMAIL_TEMPLATE)) {
    die(json_encode(array('result' => 'error', 'mess' => 'Ошибка отправки сообщения!')));
} else {
    die(json_encode(array('result' => 'success', 'mess' => 'Сообщение успешно отправлено!')));
}






