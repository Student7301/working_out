<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use metrium\helpers\VkHelper;

if (!empty($_GET['element_code'])) {

    if(!CModule::IncludeModule("iblock")){
        die('Error including module iblock');
    }

    $object = CIBlockElement::GetList(
        array(),
        array(
            'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('novostroyki'),
            //Не раскомментировывать это нужно для СЕО
            //'SECTION_ID' => \metrium\EstateObject::getSectionIdBySectionCode($this->arParams['SECTION_CODE']),
            'CODE' => $_GET['element_code']
        ),
        false,
        array(),
        array(
            'ID',
            'NAME',
            'IBLOCK_ID',
            'PROPERTY_HASHTAGS'
        )
    )->Fetch();

    /* Хештеги */
    $arHashtags = array();

    foreach ($object['PROPERTY_HASHTAGS_VALUE'] as $ht) {
        $hashtag = json_decode($ht);
        $arHashtags = array_merge($arHashtags, $hashtag->values);
    }

    $offset = 5;
    $from = !empty($_GET['next_from']) ? $_GET['next_from'] : 0;

    $vk = new VkHelper();
    $arHashtags = $vk->sortByTimestamp($arHashtags);
    
    $params = array(
        'next_from' => $from + 5,
        'hasMore' => ($from + 5 < count($arHashtags) - 1)
    );

    $vk->setParams($params);
    echo $vk->parsePosts(array_slice($arHashtags, $from, $offset))->getJson();
}