<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('search');
CModule::IncludeModule('iblock');
use \metrium\Search;

$sphinxRes = [];

if($_REQUEST){
    //Разбираем параметры
    foreach($_REQUEST as $inputs) {
        foreach ($inputs as $input) {
            $params[$input[0]] = str_replace(' ','',$input[1]);
        }
    }
    $params['valute'] = Search::RUB;
    $params['searchText'] = $_REQUEST['searchText'];
    $params['autocomplite'] = true;
    $search = new Search();
    $sphinxRes  = $search->getTradeInFilteredByText($params);
}

$res = array('list' => array());

foreach($sphinxRes as $row){
    $res['list'][] = array(
        'value' => '',
        'description' => str_replace('\\', '', html_entity_decode(htmlspecialchars_decode($row), ENT_QUOTES)),
    );
}

echo json_encode($res);
?>