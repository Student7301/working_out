<div class="new-building_modal">
	<p class="new-building_modal__h1">Конец доступной</p>
	<p class="new-building_modal__h2">Ипотеке!</p>
	<p class="new-building_modal__h3">Завершение программы господдержки в феврале!</p>
	<span class="new-building_modal__span">Успейте</span>
	<div><a data-url-popup="/local/templates/.default/ajax/modalSendForm.php" class="new-building_modal__btn c-popup_open" href="">Оставить заявку</a></div>
</div>
<script>
	$(function(){
		$('.c-popup_open').on('click',function(e){
			e.preventDefault();
			$('.popup__content').empty();
			popupModule.init($('.c-popup'), $(this));
		});
	});
</script>