<?
$response = array();
if (empty($_REQUEST['ACTION']) && empty($_REQUEST['OBJ_ID'])) {
    $response['status']= false;
    echo json_encode($response);
    return;
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule('iblock')) {
    $response['status']= false;
    echo json_encode($response);
    return;
}
global $USER;
if($_REQUEST['ACTION'] == 'like') {
    $response['status'] = \metrium\Likes::addLike($_REQUEST['OBJ_ID']);
}elseif($_REQUEST['ACTION'] == 'dislike') {
    $response['status'] = \metrium\Likes::delLike($_REQUEST['OBJ_ID']);
}else {
    $response['status'] = false;
}
echo json_encode($response);
?>