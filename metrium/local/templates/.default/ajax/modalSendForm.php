<div class="drop-form drop-form--center contact-form bottom-shadow bottom-shadow--small c-dropdownform__content" style="display: block;">
	<form action="/local/components/metrium/novostroyki.o-proekte/templates/.default/ajax/order-processing.php?LIST=Y" method="post" class="c-ajaxForm has-validation-callback">
		<div class="ajax-form-mess c-ajax-form-mess"></div>
		<div class="form-title">Оставить заявку на объект</div>
		<div class="form-line">
			<input type="text" placeholder="Имя, Фамилия" name="name" data-validation="required" class="form-input">
		</div>
		<div class="form-line">
			<input type="text" placeholder="Телефон" name="phone" data-validation="number" class="form-input">
		</div>
		<div class="form-line">
			<input type="email" placeholder="Адрес электронной почты" name="email" class="form-input">
		</div>
		<div class="form-line">
			<textarea placeholder="Текст заявки" name="message" data-validation="required" class="form-input form-input--textarea"></textarea>
		</div>
		<input type="hidden" name="object_id" value="107222">
		<div class="form-submit bottom-shadow bottom-shadow--small right">
			<input value="Отправить" type="submit" name="order_form" class="btn btn--gray btn--large">
		</div>
	</form>
</div>

<script>
	// validation
	var validatorLanguage = {
		errorTitle: 'Ошибка отправки формы!',
		requiredFields: 'Необходимо заполнить обязательные поля',
		badTime: 'Введите корректное время',
		badEmail: 'Введите корректный e-mail адрес',
		badTelephone: 'Введите корректный номер телефона',
		badSecurityAnswer: 'Ответ на секретный вопрос не верен',
		badDate: 'Введите корректную дату',
		lengthBadStart: 'Значение должно быть между ',
		lengthBadEnd: ' знаками',
		lengthTooLongStart: 'Значение поля больше, чем ',
		lengthTooShortStart: 'Значение поля меньше, чем ',
		notConfirmed: 'Input values could not be confirmed',
		badDomain: 'Введите корректное доменное имя',
		badUrl: 'Значение поля не является корректным url-адресом',
		badCustomVal: 'Зачение поля не верно',
		andSpaces: ' и пробелов ',
		badInt: 'Поле может содержать только цифры',
		badSecurityNumber: 'Your social security number was incorrect',
		badUKVatAnswer: 'Incorrect UK VAT Number',
		badStrength: 'The password isn\'t strong enough',
		badNumberOfSelectedOptionsStart: 'You have to choose at least ',
		badNumberOfSelectedOptionsEnd: ' answers',
		badAlphaNumeric: 'The input value can only contain alphanumeric characters ',
		badAlphaNumericExtra: ' и ',
		wrongFileSize: 'Максимальный размер файла %s',
		wrongFileType: 'Разрешениы следующие форматы файлов: %s',
		groupCheckedRangeStart: 'Выберите между ',
		groupCheckedTooFewStart: 'Выберите не меньше, чем ',
		groupCheckedTooManyStart: 'Выберите не больше, чем ',
		groupCheckedEnd: ' item(s)',
		badCreditCard: 'The credit card number is not correct',
		badCVV: 'The CVV number was not correct',
		wrongFileDim: 'Incorrect image dimensions,',
		imageTooTall: 'the image can not be taller than',
		imageTooWide: 'the image can not be wider than',
		imageTooSmall: 'the image was too small',
		min: 'мин.',
		max: 'макс.',
		imageRatioNotAccepted: 'Image ratio is not accepted'
	};

	$.validate({
		validateOnBlur: false,
		errorMessagePosition: 'top',
		scrollToTopOnError: false,
		language: validatorLanguage,
		borderColorOnError: '#e41919',
		onSuccess : function($form){
			// send form by ajax
			if($form.hasClass('c-ajaxForm')){
				sendFormbyAjax($form);

				return false;
			}

		}
	});
</script>