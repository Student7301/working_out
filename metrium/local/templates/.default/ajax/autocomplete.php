<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('search');
CModule::IncludeModule('iblock');
use \metrium\Search;
use \metrium\helpers\IBlockHelper;
use \metrium\QuickSearch;

if($_REQUEST){
    //Разбираем параметры
    foreach($_REQUEST as $inputs) {
        foreach ($inputs as $input) {
            if($input[0] == 'finish'){
                $params[$input[0]] = $input[1];
            }else{
                $params[$input[0]] = str_replace(' ','',$input[1]);
            }
        }
    }
    $params['type'] = $_REQUEST['type'];
    $params['searchText'] = $_REQUEST['searchText'];
}

$sphinxRes  =\metrium\Search::getSphinxResultQuick($params);

$res = array('list' => array());
foreach($sphinxRes as $row){
    $res['list'][] = array(
        'value' => htmlspecialchars_decode($row['item_id']),
        'description' => htmlspecialchars_decode(str_replace('\\', '', $row['autocomplite'])),
    );
}

echo json_encode($res);
?>