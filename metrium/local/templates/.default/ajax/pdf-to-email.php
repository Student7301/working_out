<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
use metrium\helpers\PDFHelper;

$fields = [];
$request = Application::getInstance()->getContext()->getRequest()->toArray();

if($request['template'] == 'mobile') {
    $fields = $request;
}else {
    foreach ($request as $type) {
        foreach ($type as $field) {
            $fields[$field['name']] = $field['value'];
        }
    }
}
$version = $fields["version"];
$email = $fields["email"];
$objectID = $fields["id"];
$objectCode = $fields["code"];
$objectIBCode = $fields["type"];

$arErrors = [];
if (empty($objectID) || empty($objectCode) || empty($objectIBCode)) {
    $arErrors[] = "Не указаны параметры объекта. Попробуйте еще раз";
}
if (empty($email) && !preg_match("/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD", $email)) {
    $arErrors[] = "Неверно указан email";
}
if (!empty($arErrors)) {
    die(json_encode(array('result' => 'error', 'mess' => implode('<br>', $arErrors))));
}

if ($version != "advanced") {
    $version = "simple";
}

if ($fileID = PDFHelper::checkPDFFile($objectIBCode, $objectID, $version)) {
    if ($result = PDFHelper::sendPDF($email, $fileID)) {
        die(json_encode(array('result' => 'success', 'mess' => 'PDF презентация успешно отправлена')));
    }
}

PDFHelper::createPDF($objectIBCode, $objectCode, $version);

if ($fileID = PDFHelper::checkPDFFile($objectIBCode, $objectID, $version)) {
    if ($result = PDFHelper::sendPDF($email, $fileID)) {
        die(json_encode(array('result' => 'success', 'mess' => 'PDF презентация успешно отправлена')));
    }
}

die(json_encode(array('result' => 'error', 'mess' => 'Произошла ошибка при генерации и отправке PDF')));

