<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Localization\Loc;
if (defined('IS_ENG')) {
	Loc::setCurrentLang(LANG_EN);
}
Loc::loadLanguageFile(__FILE__);
$selects = \metrium\Search::getOptions();
?>

<div class="main-search search_param" data-url="/local/templates/.default/ajax/items.json">
	<form action="<?if(defined('IS_ENG')):?>/en<?endif;?>/novostroyki/filtered/" method="">
        <div class="c-custom-search">
			<input type="hidden" name="type" value="realty">
			<div class="clearfix s_check">
				<div class="checkbox-wrap left">
					<input class="checkbox c-search_reg" data-city="moscow" type="checkbox" name="moscow" id="qeady816">
					<label class="checkbox-label" for="qeady816">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('MOSCOW')?></span>
					</label>
				</div>
				<div class="checkbox-wrap left">
					<input class="checkbox c-search_reg" data-city="region" type="checkbox" name="region" id="qeady8165">
					<label class="checkbox-label" for="qeady8165">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('MOSCOW_REGION')?></span>
					</label>
				</div>
				<div class="checkbox-wrap left">
					<input class="checkbox c-search_reg" data-city="new" type="checkbox" name="new_moscow" id="qeady816586">
					<label class="checkbox-label" for="qeady816586">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('NEW_MOSCOW')?></span>
					</label>
				</div>
			</div>
            <div class="search-price filter-line">
                <div id="priceTotal" class="fliter_check__item" style="display:block;">
                    <div class="select-wrap select-wrap--small active">
                        <label class="search-label search-label--inline"><?=Loc::getMessage('PRICE')?></label>
                        <span><?=Loc::getMessage('FROM')?></span>
                        <div class="filter_check__select">
                            <input class="form-input c-search-input c-format-price" type="text" name="price_from" value="<?=$_GET['price_from']?>">
                        </div>
                        <span><?=Loc::getMessage('TO')?></span>
                        <div class="filter_check__select">
                            <input class="form-input c-search-input c-format-price" type="text" name="price_to" value="<?=$_GET['price_to']?>">
                        </div>
                    </div>
                </div>
                <div id="roomsTotal" class="fliter_check__item" style="display:block;">					
                    <div class="select-wrap select-wrap--small active">
						<label class="search-label search-label--inline"><?=Loc::getMessage('ROOMS')?></label>
						<span><?=Loc::getMessage('FROM')?></span>
						<div class="filter_check__select">
							<select class="c-custom-select" name="rooms_from">
								<option value="0" <?if($_REQUEST['rooms_from'] == '0'):?>selected<?endif;?>><?=Loc::getMessage('STUDIO')?></option>
								<option value="1" <?if($_REQUEST['rooms_from'] == '1'):?>selected<?endif;?>>1</option>
								<option value="2" <?if($_REQUEST['rooms_from'] == '2'):?>selected<?endif;?>>2</option>
								<option value="3" <?if($_REQUEST['rooms_from'] == '3'):?>selected<?endif;?>>3</option>
								<option value="4" <?if($_REQUEST['rooms_from'] == '4'):?>selected<?endif;?>><?=Loc::getMessage('FOUR_AND_MORE')?></option>
							</select>
						</div>
						
						<span><?=Loc::getMessage('TO')?></span>
						<div class="filter_check__select">
							<select class="c-custom-select" name="rooms_to">
								<option value="0" <?if($_REQUEST['rooms_to'] == '0'):?>selected<?endif;?>><?=Loc::getMessage('STUDIO')?></option>
								<option value="1" <?if($_REQUEST['rooms_to'] == '1'):?>selected<?endif;?>>1</option>
								<option value="2" <?if($_REQUEST['rooms_to'] == '2'):?>selected<?endif;?>>2</option>
								<option value="3" <?if($_REQUEST['rooms_to'] == '3'):?>selected<?endif;?>>3</option>
								<option value="4" <?if(($_REQUEST['rooms_to'] == '4')||empty($_REQUEST['rooms_to'])):?>selected<?endif;?>><?=Loc::getMessage('FOUR_AND_MORE')?></option>
							</select>
						</div>					   
					</div>	
                </div>
            </div>
        </div>
		<div class="fliter_check__item fliter_check__item--no-border" style="display:block;">					
			<div class="search-column search-column--full c-step4">
				<label class="search-label c-text_ser"><?=Loc::getMessage('KEYWORDS')?></label>
				<div class="autocomplete-input">
					<input type="text" value="" name="searchText" id="autocomplete" data-ajaxUrl = "/local/templates/.default/ajax/autocomplete-novostroyki.php" class="form-input">
				</div>
				<div class="autocomplete-btn">
					<input type="submit" value="<?=Loc::getMessage('FIND_BUTTON')?>" class="btn btn--green btn--middle">
				</div>
			</div>
		</div>	
    </form>
</div>

<script type="text/javascript">

	$(function () {
        selectsMainSearch($('.c-custom-select'), $('#autocomplete'), '<?=Loc::getMessage('SELECT_ITEMS')?>');

        // format price
        $('.c-format-price').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ' ',
            centsLimit: 0
        });
    });
</script>