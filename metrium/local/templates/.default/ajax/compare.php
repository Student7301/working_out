<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!isset($_SESSION['COMPARE_OBJECTS']))
{
    $_SESSION['COMPARE_OBJECTS'] = array();
}

$objects = $_REQUEST['objects'];

$_SESSION['COMPARE_OBJECTS'] = array_merge($_SESSION['COMPARE_OBJECTS'], $objects);
$_SESSION['COMPARE_OBJECTS'] = array_unique($_SESSION['COMPARE_OBJECTS']);

$count = count($_SESSION['COMPARE_OBJECTS']);

echo $count;