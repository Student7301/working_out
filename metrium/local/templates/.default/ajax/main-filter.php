<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule('iblock')) {
    die();
}
$el = new CIBLockElement;

/* Выборка количества квартир по районам */
$dbRes = $el->GetList( array(), array("IBLOCK_ID"=> \metrium\EstateObject::getIblockIdByCode('novostroyki'), 'ACTIVE' => 'Y'), array('PROPERTY_district'), false, array());
while($arObject = $dbRes->Fetch() ){
    if ( ($arObject['CNT'] > 0) && ($arObject['PROPERTY_DISTRICT_ENUM_ID'] > 0 ) ) {
        $arDistricts[$arObject['PROPERTY_DISTRICT_ENUM_ID']]['OBJECTS_COUNT'] = $arObject['CNT'];
    }
}

/* Выборка районов*/

$dbRes = CIBlockProperty::GetPropertyEnum("district", Array('VALUE' => 'ASC'), Array("IBLOCK_ID"=> \metrium\EstateObject::getIblockIdByCode('novostroyki')));
while($arDistrict = $dbRes->Fetch())
{
    if(in_array($arDistrict['ID'], array_keys($arDistricts) ) ) {
        // Получаем имена районов
        $arDistricts[$arDistrict['ID']]['NAME'] =  $arDistrict['VALUE'];
        $arDistricts[$arDistrict['ID']]['ID'] =  $arDistrict['ID'];
    }
}

/* Получить списое ЖК из всех разделов */

$dbRes = CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID" => \metrium\EstateObject::getIblockIdByCode('novostroyki'), '!IBLOCK_SECTION_ID' => false, "CODE" => "EXTERNAL_OBJECT", 'XML_ID' => 'Y' ));
if($arEnum = $dbRes->GetNext())
{
    $enumExternalObjectId = $arEnum['ID'];
}

$dbRes = $el->GetList(
    array('NAME' => 'ASC'),
    array(
        "IBLOCK_ID"=> \metrium\EstateObject::getIblockIdByCode('novostroyki'),
        'ACTIVE' => 'Y',
        "!SECTION_ID" => false,
        '!PROPERTY_EXTERNAL_OBJECT' => $enumExternalObjectId/*815897*/ // Объект не является ссылкой на другой сайт
        ),
    false,
    false,
    array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_EXTERNAL_OBJECT')
);
while($arObject = $dbRes->Fetch() ){
    $arObjects[] = array(
        'ID' => $arObject['ID'],
        'NAME' => $arObject['NAME'],
    );
}

/* Получить список типов помещений */

/*$dbRes = CIBlockProperty::GetPropertyEnum("articletypeCalc", Array('VALUE' => 'ASC'), Array("IBLOCK_ID"=> \metrium\EstateObject::getIblockIdByCode('novostroyki')));
while($arApType = $dbRes->Fetch())
{
    $arApTypes[] = array('ID' => $arApType['ID'], 'NAME' => $arApType['VALUE']);
}*/




/***************************************************************************************************************/
/************************************* Выборка параметров квартир для фильтра  *********************************/
/***************************************************************************************************************/

/* Выборка типов отделки */

$dbRes = CIBlockProperty::GetPropertyEnum("FACING", Array('VALUE' => 'ASC'), Array("IBLOCK_ID"=> \metrium\EstateObject::getIblockIdByCode('apartments')));
while($arFacingType = $dbRes->Fetch())
{
    $arFacingTypes[] = array('ID' => $arFacingType['ID'], 'NAME' => $arFacingType['VALUE']);
}


/* Получить значение минимального этажа */

$dbRes = $el->GetList(
    array('PROPERTY_FLOOR' => 'ASC'),
    array(
        'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'),
        '>PROPERTY_FLOOR' => 0,
        'ACTIVE' => 'Y',
    ),
    false,
    array("nTopCount" => 1),
    array('ID', 'IBLOCK_ID', 'PROPERTY_FLOOR')
);

if($arApartment = $dbRes->Fetch()){
    $minFlat = $arApartment['PROPERTY_FLOOR_VALUE'];
}
else{
    $minFlat = 1;
}
$minFlatCurrent = (isset($_REQUEST['floorMin'])) ? $_REQUEST['floorMin'] : $minFlat; // Определение текущей позиции фильтра

/* Получить значение максимального этажа */

$dbRes = $el->GetList(
    array('PROPERTY_FLOOR' => 'DESC'),
    array(
        'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'),
        '>PROPERTY_FLOOR' => 0,
        'ACTIVE' => 'Y',
    ),
    false,
    array("nTopCount" => 1),
    array('ID', 'IBLOCK_ID', 'PROPERTY_FLOOR')
);

if($arApartment = $dbRes->Fetch()){
    $maxFlat = $arApartment['PROPERTY_FLOOR_VALUE'];
}
else{
    $maxFlat = 50;
}
$maxFlatCurrent = (isset($_REQUEST['floorMax'])) ? $_REQUEST['floorMax'] : $maxFlat; // Определение текущей позиции фильтра

/* Получить значение минимальной цены */

$dbRes = $el->GetList(
    array('PROPERTY_price' => 'ASC'),
    array(
        'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'),
        '>PROPERTY_price' => 0,
        'ACTIVE' => 'Y',
    ),
    false,
    array("nTopCount" => 1),
    array('ID', 'IBLOCK_ID', 'PROPERTY_price')
);

if($arApartment = $dbRes->Fetch()){
    $minPrice = $arApartment['PROPERTY_PRICE_VALUE'];
    $minPriceFormatted = \metrium\Price::getFormattedPrice($arApartment['PROPERTY_PRICE_VALUE']);
}
else{
    $minPrice = 1;
}
$minPriceCurrent = (isset($_REQUEST['priceMin'])) ? $_REQUEST['priceMin'] : $minPrice; // Определение текущей позиции фильтра

/* Получить значение максимальной цены */

$dbRes = $el->GetList(
    array('PROPERTY_price' => 'DESC'),
    array(
        'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'),
        '>PROPERTY_price' => 0,
        'ACTIVE' => 'Y',
    ),
    false,
    array("nTopCount" => 1),
    array('ID', 'IBLOCK_ID', 'PROPERTY_price')
);

if($arApartment = $dbRes->Fetch()){
    $maxPrice = $arApartment['PROPERTY_PRICE_VALUE'];
    $maxPriceFormatted = \metrium\Price::getFormattedPrice($arApartment['PROPERTY_PRICE_VALUE']);
}
else{
    $maxPrice = 50;
}
$maxPriceCurrent = (isset($_REQUEST['priceMax'])) ? $_REQUEST['priceMax'] : $maxPrice; // Определение текущей позиции фильтра

/* Получить значение минимальной площади */

$dbRes = $el->GetList(
    array('PROPERTY_spacedesign' => 'ASC'),
    array(
        'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'),
        '>PROPERTY_spacedesign' => 0,
        'ACTIVE' => 'Y',
    ),
    false,
    array("nTopCount" => 1),
    array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign')
);

if($arApartment = $dbRes->Fetch()){
    $minArea = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
}
else{
    $minArea = 1;
}
$minAreaCurrent = (isset($_REQUEST['areaMin'])) ? $_REQUEST['areaMin'] : $minArea; // Определение текущей позиции фильтра

/* Получить значение максимальной площади */

$dbRes = $el->GetList(
    array('PROPERTY_spacedesign' => 'DESC'),
    array(
        'IBLOCK_ID' => \metrium\EstateObject::getIblockIdByCode('apartments'),
        '>PROPERTY_spacedesign' => 0,
        'ACTIVE' => 'Y',
    ),
    false,
    array("nTopCount" => 1),
    array('ID', 'IBLOCK_ID', 'PROPERTY_spacedesign')
);

if($arApartment = $dbRes->Fetch()){
    $maxArea = $arApartment['PROPERTY_SPACEDESIGN_VALUE'];
}
else{
    $maxArea = 50;
}
$maxAreaCurrent = (isset($_REQUEST['areaMax'])) ? $_REQUEST['areaMax'] : $maxArea; // Определение текущей позиции фильтра

?>
<div class="main-filter-wrap main-filter-wrap--wide">
    <div class="main-filter__title">Поиск по параметрам</div>
    <div class="main-filter main-filter--popup main-filter--wide clearfix">
        <form action="/novostroyki/filtered/" method="GET">
            <div class="filters-line left">
                <div class="main-filter__item c-customFilterPopup">
                    <a href="" title="" class="main-filter__link">Район</a>
                    <div class="main-filter__dropdown main-filter__dropdown--wide">
                        <div class="selected-direction selected-direction--blue c-selected-direction"></div>
                        <div class="direction-list mCustomScrollbar--blue c-mCustomScrollbar c-direction-list c-direction-select-able">
                            <?foreach($arDistricts as $arItem):?>
                                <div class="checkbox-wrap">
                                    <input class="checkbox" type="checkbox" name="direction[]" <?=(in_array($arItem['ID'], $_REQUEST['direction']))?'checked':''?>  id="direction<?=$arItem['ID']?>" value="<?=$arItem['ID']?>" >
                                    <label class="checkbox-label" for="direction<?=$arItem['ID']?>" data-checked="<?=(in_array($arItem['ID'], $_REQUEST['direction']))?'Y':'N'?>">
                                        <span class="checkbox-icon"></span>
                                        <span class="checkbox-text"><?=$arItem['NAME']?> (<?=($arItem['OBJECTS_COUNT'] > 0) ? $arItem['OBJECTS_COUNT'] : 0 ?>)</span>
                                    </label>
                                </div>
                            <?endforeach;?>
                        </div>

                        <div class="filter-bottom-actions">
                            <a class="c-select-all select-all" href="" title="">
                                <span class="select-all__icon sprite-icon checkbox-black"></span>
                                <span class="select-all__text">Выбрать все</span>
                            </a>

                            <a class="c-deselect-all deselect-all" href="" title="">
                                <span class="deselect-all__text">Сбросить все</span>
								<span class="deselect-all__icon">
									<i class="sprite-icon close-white-big"></i>
								</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="main-filter__item c-customFilterPopup">
                    <a href="" title="" class="main-filter__link">ЖК</a>
                    <div class="main-filter__dropdown main-filter__dropdown--wide">
                        <div class="selected-direction selected-direction--blue c-selected-direction"></div>
                        <div class="direction-list mCustomScrollbar--blue c-mCustomScrollbar c-direction-list c-direction-select-able">
                            <?foreach($arObjects as $arItem):?>
                                <div class="checkbox-wrap">
                                    <input class="checkbox" type="checkbox" name="jc[]" <?=(in_array($arItem['ID'], $_REQUEST['jc']))?'checked':''?> id="jc<?=$arItem['ID']?>" value="<?=$arItem['ID']?>" >
                                    <label class="checkbox-label" for="jc<?=$arItem['ID']?>" data-checked="<?=(in_array($arItem['ID'], $_REQUEST['jc']))?'Y':'N'?>">
                                        <span class="checkbox-icon"></span>
                                        <span class="checkbox-text"><?=$arItem['NAME']?></span>
                                    </label>
                                </div>
                            <?endforeach;?>
                        </div>

                        <div class="filter-bottom-actions">
                            <a class="c-select-all select-all" href="" title="">
                                <span class="select-all__icon sprite-icon checkbox-black"></span>
                                <span class="select-all__text">Выбрать все</span>
                            </a>

                            <a class="c-deselect-all deselect-all" href="" title="">
                                <span class="deselect-all__text">Сбросить все</span>
								<span class="deselect-all__icon">
									<i class="sprite-icon close-white-big"></i>
								</span>
                            </a>
                        </div>
                    </div>
                </div>
<!--                <div class="main-filter__item">
                    <a href="" title="" class="main-filter__link">Тип помещения</a>
                    <div class="main-filter__dropdown">
                        <?/*foreach($arApTypes as $arItem):*/?>
                            <div class="checkbox-wrap">
                                <input class="checkbox" type="checkbox" name="type[]" <?/*=(in_array($arItem['ID'], $_REQUEST['type']))?'checked':''*/?> id="type<?/*=$arItem['ID']*/?>" value="<?/*=$arItem['ID']*/?>">
                                <label class="checkbox-label" for="type<?/*=$arItem['ID']*/?>">
                                    <span class="checkbox-icon"></span>
                                    <span class="checkbox-text"><?/*=$arItem['NAME']*/?></span>
                                </label>
                            </div>
                        <?/*endforeach;*/?>
                    </div>
                </div>-->
                <div class="main-filter__item">
                    <a href="" title="" class="main-filter__link">Вид отделки</a>
                    <div class="main-filter__dropdown">
                        <?foreach($arFacingTypes as $arItem):?>
                            <div class="checkbox-wrap">
                                <input class="checkbox" type="checkbox" name="ready[]" <?=(in_array($arItem['ID'], $_REQUEST['ready']))?'checked':''?> id="ready<?=$arItem['ID']?>" value="<?=$arItem['ID']?>">
                                <label class="checkbox-label" for="ready<?=$arItem['ID']?>">
                                    <span class="checkbox-icon"></span>
                                    <span class="checkbox-text"><?=$arItem['NAME']?></span>
                                </label>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
                <div class="main-filter__item">
                    <a href="" title="" class="main-filter__link">Комнат</a>
                    <div class="main-filter__dropdown">
                        <div class="checkbox-wrap checkbox-wrap--inline">
                            <input class="checkbox" type="checkbox" name="rooms[]" <?=(in_array('1', $_REQUEST['rooms']))?'checked':''?> id="room1" value="1">
                            <label class="checkbox-label" for="room1">
                                <span class="checkbox-icon"></span>
                                <span class="checkbox-text">1</span>
                            </label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--inline">
                            <input class="checkbox" type="checkbox" name="rooms[]" <?=(in_array('2', $_REQUEST['rooms']))?'checked':''?> id="room2" value="2">
                            <label class="checkbox-label" for="room2">
                                <span class="checkbox-icon"></span>
                                <span class="checkbox-text">2</span>
                            </label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--inline">
                            <input class="checkbox" type="checkbox" name="rooms[]" <?=(in_array('3', $_REQUEST['rooms']))?'checked':''?> id="room3" value="3">
                            <label class="checkbox-label" for="room3">
                                <span class="checkbox-icon"></span>
                                <span class="checkbox-text">3</span>
                            </label>
                        </div>
                        <div class="checkbox-wrap checkbox-wrap--inline">
                            <input class="checkbox" type="checkbox" name="rooms[]" <?=(in_array('4', $_REQUEST['rooms']))?'checked':''?> id="room4" value="4">
                            <label class="checkbox-label" for="room4">
                                <span class="checkbox-icon"></span>
                                <span class="checkbox-text">4</span>
                            </label>
                        </div>
                    </div>
                </div>

<!--                <div class="main-filter__item">-->
<!--                    <a href="" title="" class="main-filter__link">Этаж</a>-->
<!--                    <div class="main-filter__dropdown">-->
<!--                        <div class="range-slider small-bottom-shadow c-range-slider">-->
<!--                            <div class="val-text left c-min-val">-->
<!--                                --><?//=$minFlat?>
<!--                            </div>-->
<!--                            <div class="val-text right range-input--max c-max-val">-->
<!--                                --><?//=$maxFlat?>
<!--                            </div>-->
<!--                            <div class="c-range range-block" data-min="--><?//=$minFlat?><!--" data-max="--><?//=$maxFlat?><!--" data-step="1" data-val-min="--><?//=$minFlatCurrent?><!--" data-val-max="--><?//=$maxFlatCurrent?><!--" data-min-input="floorMin" data-max-input="floorMax" data-range="Y"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="main-filter__item c-filter-drop" data-filtervalue="flat">
                    <a href="" title="" class="main-filter__link">Этаж</a>
                    <div class="main-filter__dropdown main-filter__dropdown--right c-inputs">
                        <div class="position-block">
                            <input class="form-input form-input--middle filter-input" type="text" name="floorMin" value="<?=$minFlat?>" data-default="<?=$minFlat?>">
                            <span class="input-prefix">от</span>
                        </div>
                        <div class="position-block">
                            <input class="form-input form-input--middle filter-input" type="text" name="floorMax" value="<?=$maxFlat?>" data-default="<?=$maxFlat?>">
                            <span class="input-prefix">до</span>
                        </div>
                        <a href="" title="" class="btn btn--middle c-clear-all">Сбросить</a>
                    </div>
                </div>


<!--                <div class="main-filter__item">-->
<!--                    <a href="" title="" class="main-filter__link">Площадь</a>-->
<!--                    <div class="main-filter__dropdown">-->
<!--                        <div class="range-slider small-bottom-shadow c-range-slider">-->
<!--                            <div class="val-text left c-min-val">-->
<!--                                --><?//=$minArea?><!-- кв.м.-->
<!--                            </div>-->
<!--                            <div class="val-text right range-input--max c-max-val">-->
<!--                                --><?//=$maxArea?><!-- кв.м.-->
<!--                            </div>-->
<!--                            <div class="c-range range-block" data-min="--><?//=$minArea?><!--" data-max="--><?//=$maxArea?><!-- " data-step="1" data-val-min="--><?//=$minAreaCurrent?><!--" data-val-max="--><?//=$maxAreaCurrent?><!--" data-min-input="areaMin" data-max-input="areaMax" data-range="Y"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="main-filter__item c-filter-drop" data-filtervalue="area">
                    <a href="" title="" class="main-filter__link">Площадь</a>
                    <div class="main-filter__dropdown main-filter__dropdown--right c-inputs">
                        <div class="position-block">
                            <input class="form-input form-input--middle filter-input c-format-price" type="text" name="areaMin" value="<?=$minArea?>" data-default="<?=$minArea?>">
                            <span class="input-prefix">от</span>
                        </div>
                        <div class="position-block">
                            <input class="form-input form-input--middle filter-input c-format-price" type="text" name="areaMax" value="<?=$maxArea?>" data-default="<?=$maxArea?>">
                            <span class="input-prefix">до</span>
                        </div>
                        <a href="" title="" class="btn btn--middle c-clear-all">Сбросить</a>
                    </div>
                </div>

<!--                <div class="main-filter__item">-->
<!--                    <a href="" title="" class="main-filter__link">Цена</a>-->
<!--                    <div class="main-filter__dropdown">-->
<!--                        <div class="range-slider small-bottom-shadow c-range-slider">-->
<!--                            <div class="val-text left c-min-val">-->
<!--                                --><?//=$minPriceFormatted?><!-- руб.-->
<!--                            </div>-->
<!--                            <div class="val-text right range-input--max c-max-val">-->
<!--                                --><?//=$maxPriceFormatted?><!-- руб.-->
<!--                            </div>-->
<!--                            <div class="c-range range-block" data-min="--><?//=$minPrice?><!--" data-max="--><?//=$maxPrice?><!--" data-step="100000" data-val-min="--><?//=$minPriceCurrent?><!--" data-val-max="--><?//=$maxPriceCurrent?><!--" data-min-input="priceMin" data-max-input="priceMax" data-range="Y"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="main-filter__item c-filter-drop" data-filtervalue="price">
                    <a href="" title="" class="main-filter__link">Цена</a>
                    <div class="main-filter__dropdown main-filter__dropdown--right c-inputs">
                        <div class="position-block">
                            <input class="form-input form-input--middle filter-input c-format-price" type="text" name="priceMin" value="<?=$minPrice?>" data-default="<?=$minPrice?>">
                            <span class="input-prefix">от</span>
                        </div>
                        <div class="position-block">
                            <input class="form-input form-input--middle filter-input c-format-price" type="text" name="priceMax" value="<?=$maxPrice?>" data-default="<?=$maxPrice?>">
                            <span class="input-prefix">до</span>
                        </div>
                        <a href="" title="" class="btn btn--middle c-clear-all">Сбросить</a>
                    </div>
                </div>

            </div>
            <div class="right-actions right">
                <div class="block-search block-search--green left c-popup-close">
                    <i class="sprite-icon close-white-large"></i>
                    <i class="sprite-icon search-white"></i>
                </div>

<!--                <div class="block-map block-map--white left c-zoom-map" data-map-id="big-map" data-url-popup="ajax/zoom-map-header.php" data-placemarks-json="ajax/elit-placemarks-jc.json" data-elit="Y" data-has-balloon="Y" data-custom-search="Y">-->
<!--                    <i class="sprite-icon map"></i>-->
<!--                    <i class="sprite-icon search"></i>-->
<!--                </div>-->
                <div data-map-id="big-map" data-url-popup="/local/templates/.default/ajax/zoom-map-header.php?TYPE=new" data-placemarks-json="/local/templates/.default/ajax/elit-placemarks-jc.php" data-elit="Y" data-custom-search="Y"  data-has-child="Y" data-has-balloon="Y" class="block-map bottom-shadow left c-zoom-map c-dbl-map">
                    <i class="sprite-icon map"></i>
                    <i class="sprite-icon search"></i>
                </div>

                <button type="submit" class="btn btn--filter">Искать <i class="sprite-icon range-arrow-right"></i></button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $.each($('.c-customFilterPopup'), function(){
            mainFilterDirections($(this));
        });

        $('.c-mCustomScrollbar').mCustomScrollbar();

        $('.c-clear-all').on('click', function(e){
            e.preventDefault();

            var inputs = $(this).closest('.c-inputs').find('input[type="text"]');

            $.each(inputs, function(){
                this.value = this.dataset.default;

                getPriceFormat($(this));
            });
        });

        $.each($('.c-format-price'), function(){
            var elem = $(this);

            getPriceFormat(elem);  
        });
    });
</script>