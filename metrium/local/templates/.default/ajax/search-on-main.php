<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Localization\Loc;
if (defined('IS_ENG')) {
    Loc::setCurrentLang(LANG_EN);
}
Loc::loadLanguageFile(__FILE__);
$selects = \metrium\Search::getOptions();
?>

<div class="main-search search_param" data-url="/local/templates/.default/ajax/items.json">
	<form action="<?if(defined('IS_ENG')):?>/en<?endif;?>/search/" method="">
        <div class="c-custom-search">
			<input type="hidden" name="type" value="realty">
			<div class="clearfix s_check">
				<div class="checkbox-wrap left">
					<input data-check="new" class="checkbox c-search_check" type="checkbox" name="novostroyki" id="ready815">
					<label class="checkbox-label" for="ready815">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('NEW_DEVELOPMENTS')?></span>
					</label>
				</div>
				<div class="checkbox-wrap left">
					<input data-check="second" class="checkbox c-search_check" type="checkbox" name="elite" id="ready8155">
					<label class="checkbox-label" for="ready8155">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('REAL_ESTATE')?></span>
					</label>
				</div>
				<div class="checkbox-wrap left">
					<input data-check="cottage" class="checkbox c-search_check" type="checkbox" name="kit" id="ready815586">
					<label class="checkbox-label" for="ready815586">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('TOWNHOUSES')?></span>
					</label>
				</div>
				<div class="checkbox-wrap left">
					<input data-check="plots" class="checkbox c-search_check" type="checkbox" name="uchastki" id="ready81558">
					<label class="checkbox-label" for="ready81558">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('LAND')?></span>
					</label>
				</div>
			</div>
			<div class="clearfix s_check">
				<div class="checkbox-wrap left">
					<input class="checkbox c-search_reg" data-city="moscow" type="checkbox" name="moscow" id="qeady815">
					<label class="checkbox-label" for="qeady815">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('MOSCOW')?></span>
					</label>
				</div>
				<div class="checkbox-wrap left">
					<input class="checkbox c-search_reg" data-city="region" type="checkbox" name="region" id="qeady8155">
					<label class="checkbox-label" for="qeady8155">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('MOSCOW_REGION')?></span>
					</label>
				</div>
				<div class="checkbox-wrap left">
					<input class="checkbox c-search_reg" data-city="new" type="checkbox" name="new_moscow" id="qeady815586">
					<label class="checkbox-label" for="qeady815586">
						<span class="checkbox-icon"></span>
						<span class="checkbox-text"><?=Loc::getMessage('NEW_MOSCOW')?></span>
					</label>
				</div>
			</div>
			<div class="filter_check">
				<div id="areaTotal" class="fliter_check__item">
					<div class="select-wrap select-wrap--small active">
						<label class="search-label search-label--inline"><?=Loc::getMessage('TOTAL_AREA')?></label>
						<span><?=Loc::getMessage('FROM')?></span>
						<div class="filter_check__select">
							<input class="form-input c-search-input" type="text" name="area_from">
						</div>
						<span><?=Loc::getMessage('TO')?></span>
						<div class="filter_check__select">
							<input class="form-input c-search-input" type="text" name="area_to">
						</div>
					</div>
				</div>
				<div id="typeFinishing" class="fliter_check__item">
					<div class="select-wrap select-wrap--small active">
						<label class="search-label search-label--inline"><?=Loc::getMessage('TYPE_FINISHING')?>:</label>
						<span></span>
						<div class="filter_check__select">
							<select class="c-custom-select search_select" name="typeFinishing">
								<option></option>
								<?foreach($selects['TYPE_FINISHING'] as $finish):?>
									<option value="<?=$finish?>"><?=$finish?></option>
								<?endforeach;?>
							</select>
						</div>
					</div>
				</div>
				<div id="yearEnd" class="fliter_check__item">
					<div class="select-wrap select-wrap--small active">
						<label class="search-label search-label--inline"><?=Loc::getMessage('COMPLETION_YEAR')?></label>
						<span><?=Loc::getMessage('WITH')?></span>
						<div class="filter_check__select">
							<select tabindex="-1" class="c-custom-select search_select" name="year_from">
								<option></option>
								<option value="Y"><?=Loc::getMessage('ALREADY_BUILDED')?></option>
								<?foreach($selects['YEAR'] as $year):?>
									<option value="<?=$year?>"><?=$year?></option>
								<?endforeach;?>
							</select>
						</div>
						<span><?=Loc::getMessage('BY')?></span>
						<div class="filter_check__select">
							<select tabindex="-1" class="c-custom-select search_select" name="year_to">
								<option></option>
								<option value="Y"><?=Loc::getMessage('ALREADY_BUILDED')?></option>
								<?foreach($selects['YEAR'] as $year):?>
									<option value="<?=$year?>"><?=$year?></option>
								<?endforeach;?>
							</select>
						</div>
					</div>
				</div>
				<div id="areaLand" class="fliter_check__item">
					<div class="select-wrap select-wrap--small active">
						<label class="search-label search-label--inline"><?=Loc::getMessage('TERRITORY_AREA')?></label>
						<span><?=Loc::getMessage('FROM')?></span>
						<div class="filter_check__select">
							<input class="form-input c-search-input" type="text" name="territory_area_from">
						</div>
						<span><?=Loc::getMessage('TO')?></span>
						<div class="filter_check__select">
							<input class="form-input c-search-input" type="text" name="territory_area_to">
						</div>
					</div>
				</div>
				<div id="decoration" class="fliter_check__item">
					<div class="select-wrap select-wrap--small active">
						<label class="search-label search-label--inline"><?=Loc::getMessage('FINISHINGS')?>:</label>
						<span></span>
						<div class="filter_check__select">
							<select class="c-custom-select search_select" name="finish">
								<option></option>
								<?foreach($selects['FINISH'] as $finish):?>
									<option value="<?=$finish?>"><?=$finish?></option>
								<?endforeach;?>
							</select>
						</div>
					</div>
				</div>
			</div>
            <div class="search-price filter-line">
                <div class="price-inline">
                    <label class="search-label"><?=Loc::getMessage('PRICE')?></label>
                    <div class="select-wrap select-wrap--small active">
                        <select class="c-custom-select" name="valute">
                            <? foreach ($selects['THIRD'] as $option) :?>
                                <option value="<?=$option['value']?>"><?=$option['name']?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="search-price__input">
                    <label class="search-label search-label--inline"><?=Loc::getMessage('FROM')?></label>
                    <div class="form-input-wrap">
                        <input type="text" name="price_from" value="" class="form-input c-search-input c-format-price">
                    </div>
                    <label class="search-label search-label--inline"><?=Loc::getMessage('TO')?></label>
                    <div class="form-input-wrap">
                        <input type="text" name="price_to" value="" class="form-input c-search-input c-format-price">
                    </div>
                </div>
            </div>

        </div>

        <div class="search-column search-column--full c-step4">
            <label class="search-label c-text_ser"><?=Loc::getMessage('KEYWORDS')?></label>
            <div class="autocomplete-input">
                <input type="text" value="" name="searchText" id="autocomplete" data-ajaxUrl = "/local/templates/.default/ajax/autocomplete.php" class="form-input">
            </div>
			<a class="btn btn--middle btn_id"><?=Loc::getMessage('FIND_BY_ID')?></a>
			<div class="autocomplete-btn">
				<input type="submit" value="<?=Loc::getMessage('FIND_BUTTON')?>" class="btn btn--green btn--middle">
			</div>
        </div>
    </form>
</div>

<script type="text/javascript">
	$(function () {
        selectsMainSearch($('.c-custom-select'), $('#autocomplete'), '<?=Loc::getMessage('SELECT_ITEMS')?>');

        // format price
        $('.c-format-price').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ' ',
            centsLimit: 0
        });
	checked();

	$('.btn_id').on('click',function(e){
		e.preventDefault();
		if($(this).hasClass('back')){
			$(this).text('<?=Loc::getMessage('FIND_BY_ID')?>');
			$(this).removeClass('back');
			$('.s_check').find('input[type="checkbox"]').prop('disabled',false);
			$('.search-price').find('select, input[type="text"]').prop('disabled',false);
			$('.search-price .SumoSelect').removeClass('disable');
			$('.c-text_ser').removeClass('id').text('<?=Loc::getMessage('KEYWORDS')?>');
			$('input[name="type"]').val('realty');
		} else {
			$(this).text('<?=Loc::getMessage('RETURN')?>');
			$(this).addClass('back');
			$('.s_check').find('input[type="checkbox"]').prop('disabled',true).prop('checked', false);
			$('.search-price').find('select, input[type="text"]').prop('disabled',true);
			$('.search-price .SumoSelect').addClass('disable');
			$('.c-text_ser').addClass('id').text('<?=Loc::getMessage('WRITE_ID')?>');
			$('.fliter_check__item').hide();
			$('.fliter_check__item input[type="text"]').val('');
			$('input[name="type"]').val('id');
			$.each($(".search_select"),function(i){
				$(".search_select")[i].sumo.unSelectAll();
			});
			
		}
		selectsMainSearch($('.c-custom-select'), $('#autocomplete'), '<?=Loc::getMessage('SELECT_ITEMS')?>');
	});
	
	function searchBlocks (num) {
		
		$.ajax({
		  url: $('.search_param').data('url'),
		  dataType: 'json',
		  type:'GET',
		  success:function(data){
			$('.fliter_check__item').hide();
			$.each(data[num],function(i){
				$('#'+data[num][i]).show();
			})
		  },
		  error: function (err) {
			console.log(err);
		}
		});
		
		$('.fliter_check__item input[type="text"]').val('');
		$.each($(".search_select"),function(i){
			$(".search_select")[i].sumo.unSelectAll();
		});
	}
	
	$('.c-search_check').on('change',function(e){
		e.preventDefault();
		var new_c = $('.c-search_check[data-check="new"]'),
			second = $('.c-search_check[data-check="second"]'),
			cottage = $('.c-search_check[data-check="cottage"]'),
			plots = $('.c-search_check[data-check="plots"]'),
			elem = '.c-search_reg',
			num = 0;
		
		checked();

        if (new_c.prop('checked') == true && second.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true) {
			num = 1;
		} else if(second.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true) {
			num = 2;
		} else if (new_c.prop('checked') == true && second.prop('checked') == true && plots.prop('checked') == true || new_c.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true || new_c.prop('checked') == true && plots.prop('checked') == true) {
			num = 3;
		} else if (new_c.prop('checked') == true && cottage.prop('checked') == true || new_c.prop('checked') == true && second.prop('checked') == true && cottage.prop('checked') == true) {
			num = 4;
		} else if (plots.prop('checked') == true && cottage.prop('checked') == true) {
			num = 5;
		} else if (second.prop('checked') == true && cottage.prop('checked') == true) {
			num = 6;
		} else if (second.prop('checked') == true && plots.prop('checked') == true) {
			num = 7;
		} else if (new_c.prop('checked') == true && second.prop('checked') == true) {
			num = 8;
		} else if (new_c.prop('checked') == true) {
			num = 12;
		} else if (plots.prop('checked') == true) {
			num = 9;
		} else if (cottage.prop('checked') == true) {
			num = 10;
		} else if (second.prop('checked') == true) {
			num = 11;
		} else {
			num = 0;
		}
		
		searchBlocks(num);
		
	});

    });
</script>