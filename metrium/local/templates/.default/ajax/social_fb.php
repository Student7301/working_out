<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use metrium\helpers\FbHelper;

$url = !empty($_GET['next_from']) ? $_GET['next_from'] : '';
$fb = new FbHelper(array('next_from' => $url));

echo $fb->parsePage(false)->getJson();