<?
$MESS['NEW_DEVELOPMENTS'] = "New developments";
$MESS['REAL_ESTATE'] = "Real estate for sale";
$MESS['TOWNHOUSES'] = "Houses and townhouses";
$MESS['LAND'] = "Land plots";
$MESS['MOSCOW'] = "Moscow";
$MESS['MOSCOW_REGION'] = "Moscow region";
$MESS['NEW_MOSCOW'] = "New Moscow";
$MESS['TOTAL_AREA'] = "Total area";
$MESS['FROM'] = "from";
$MESS['TO'] = "to";
$MESS['COMPLETION_YEAR'] = "Year of construction completion";
$MESS['WITH'] = "from";
$MESS['BY'] = "to";
$MESS['ALREADY_BUILDED'] = "Already builded";
$MESS['FINISHINGS'] = "Finishings";
$MESS['PRICE'] = "Price";
$MESS['RETURN'] = "Return";
$MESS['FIND_BY_ID'] = "Find by ID";
$MESS['FIND_BUTTON'] = "Find";
$MESS['KEYWORDS'] = "Keywords";
$MESS['TERRITORY_AREA'] = "Land area";
$MESS['WRITE_ID'] = "Enter ID";
$MESS['SELECT_ITEMS'] = "Select items";
$MESS['TYPE_FINISHING'] = 'Finish type';
?>