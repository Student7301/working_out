<?
$MESS['NEW_DEVELOPMENTS'] = "Новостройки";
$MESS['REAL_ESTATE'] = "Вторичная недвижимость";
$MESS['TOWNHOUSES'] = "Коттеджи и таунхаусы";
$MESS['LAND'] = "Участки";
$MESS['MOSCOW'] = "Москва";
$MESS['MOSCOW_REGION'] = "Московская область";
$MESS['NEW_MOSCOW'] = "Новая Москва";
$MESS['TOTAL_AREA'] = "Общая площадь";
$MESS['FROM'] = "от";
$MESS['TO'] = "до";
$MESS['COMPLETION_YEAR'] = "Год окончания строительства";
$MESS['WITH'] = "с";
$MESS['BY'] = "по";
$MESS['ALREADY_BUILDED'] = "Готовое";
$MESS['FINISHINGS'] = "Отделка";
$MESS['PRICE'] = "Цена";
$MESS['ROOMS'] = "Число комнат";
$MESS['KEYWORDS'] = "Ключевые слова";
$MESS['RETURN'] = "Вернуться назад";
$MESS['FIND_BY_ID'] = "Поиск по ID";
$MESS['FIND_BUTTON'] = "Найти";
$MESS['TERRITORY_AREA'] = "Площадь участка";
$MESS['WRITE_ID'] = "Введите ID объекта";
$MESS['SELECT_ITEMS'] = "Выбрать пункты";
$MESS['STUDIO'] = 'Студия';
$MESS['FOUR_AND_MORE'] = '4 и более';
?>