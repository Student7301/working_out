    <?use Bitrix\Main\Page\Asset;?>
    <div class="footer__direction">
        <div class="footer__direction-content">
            <div class="footer__direction-title">Направления</div>
            <div class="footer__direction-items">
                <?$APPLICATION->IncludeComponent(
                    "metrium:direction.list",
                    "zagorod-new",
                    array(
                        "COMPONENT_TEMPLATE" => "zagorod-new",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "86400",
                        "COUNT" => "15",
                        "IBLOCK_CODE" => $arParams['SECTION_CODE']
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
    <div class="footer__menu">
        <div class="footer__menu-content">
            <div class="footer__menu-logo"><img src="<?= SITE_TEMPLATE_PATH . '/images/content/footer-logo.png' ?>"></div>
            <div class="footer__menu-links">
                <a href="/novostroyki/" title="" class="footer__menu-link">Новостройки</a>
                <a href="/zagorodnaya-nedvizhimost/poselki/" title="" class="footer__menu-link">КОТТЕДЖНЫЕ ПОСЕЛКИ</a>
                <a href="/ipoteka/programmy_bankov/" title="" class="footer__menu-link">ИПОТЕКА</a>
                <a href="/trade-in/" title="" class="footer__menu-link">TRADE-IN</a>
                <a href="/analitika-i-konsalting-dev/" title="" class="footer__menu-link">АНАЛИТИКА И КОНСАЛТИНГ</a>
                <a href="/organizatsiya-prodazh/" title="" class="footer__menu-link">ОРГАНИЗАЦИЯ ПРОДАЖ</a>
                <a href="/investitsii/" title="" class="footer__menu-link">ИНВЕСТИЦИИ</a>
                <a href="/o-kompanii/" title="" class="footer__menu-link">О КОМПАНИИ</a>
            </div>
        </div>
    </div>
    <div class="footer__callback-info">
        <div class="footer__callback-info-content">
            <div class="footer__subscription">
                <div class="footer__subscription-text">ЕЖЕНЕДЕЛЬНАЯ РАССЫЛКА АКТУАЛЬНЫХ ВЫГОДНЫХ ПРЕДЛОЖЕНИЙ</div>
                    <?$APPLICATION->IncludeComponent(
                        "metrium:subscribe.form",
                        "zagorod-new",
                        array(
                            "COMPONENT_TEMPLATE" => "zagorod-new",
                            "SUBSCRIPTION_RUBRIC" => "4"
                        ),
                        false
                    );?>
                <div class="footer__subscription-text--small">При полном или частичном использовании материалов ссылка на www.metrium.ru обязательна.</div>
            </div>
            <div class="footer__social-info clearfix">
                <div class="footer__social-content clearfix">
                    <div class="footer__email">
                        <div class="email-bl">Электронная почта:</div>
                        <a href="mailto:info@metrium.ru" title="" class="email-link">info@metrium.ru</a>
                        <a href="/sitemap.php" title="" class="map-link">Карта сайта</a>
                    </div>
                    <div class="footer__social-link">
                        <a href="https://www.facebook.com/metrium" title="" class="social-link icon-social-fb" target="_blank"></a>
                        <a href="https://vk.com/metriumru" title="" class="social-link icon-social-vk" target="_blank"></a>
                        <a href="https://www.instagram.com/metriumgroup/" title="" class="social-link icon-social-instagram" target="_blank"></a>
                        <a href="https://twitter.com/Metrium2" title="" class="social-link icon-social-twitter" target="_blank"></a>
                        <a href="https://ok.ru/group/53323706728662" title="" class="social-link icon-social-ok" target="_blank"></a>
                    </div>
                </div>
                <div class="footer__callback clearfix">
                    <div class="footer__phone-number">
                        <a href="tel:84951048744" id="phone_moscow">+7 (495) 104-87-44</a>
                    </div>
                    <input type="button" name="name" value="Заказать звонок" class="footer__callback-btn btn-form" onclick="SCBopen(1); return false;">
                    <div class="footer__subscription-text--small">© 2017 Metrium Group. All rights reserved.</div>
                </div>
            </div>
        </div>
    </div>
    <!--LiveInternet counter-->
    <script type="text/javascript"><!--
        document.write("<a href='//www.liveinternet.ru/click' style='position: relative'"+
            "target=_blank><img src='//counter.yadro.ru/hit?t44.6;r"+
            escape(document.referrer)+((typeof(screen)=="undefined")?"":
            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
            ";"+Math.random()+
            "' alt='' title='LiveInternet' "+
            "border='0' width='31' height='31' style='position: absolute; bottom: 10px;'><\/a>")
        //--></script>
    <!--/LiveInternet-->
    <!--  AdRiver code START. Type:counter(zeropixel) Site: metrium PZ: 0 BN: 0 -->
    <script type="text/javascript">
        var RndNum4NoCash = Math.round(Math.random() * 1000000000);
        var ar_Tail='unknown'; if (document.referrer) ar_Tail = escape(document.referrer);
        document.write('<img src="' + ('https:' == document.location.protocol ? 'https:' : 'http:') + '//ad.adriver.ru/cgi-bin/rle.cgi?' + 'sid=193229&bt=21&pz=0&rnd=' + RndNum4NoCash + '&tail256=' + ar_Tail + '" border=0 width=1 height=1>')
    </script>
    </footer>
    <div class="preloader c-preloader"></div>
    <?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/plugins.js')?>
    <?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.js')?>
    <?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/modal.js')?>

    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 952495884;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>

    <?Asset::getInstance()->addString("<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\"></script>");?>
    <?Asset::getInstance()->addString("<script type=\"text/javascript\" async src=\"http://smartcallback.ru/api/SmartCallBack.js?t=uLWNsem2hlZO3ZONIiOC\" charset=\"utf-8\"></script>");?>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter18350230 = new Ya.Metrika({ id:18350230, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/18350230" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!--Google-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-59013509-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!--Google-->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '823081721188267', {
            em: 'insert_email_variable,'
        });
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1066320783433419&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->


    </body>
    </html>