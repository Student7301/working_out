function getRouteToActiveTab(){
    $('.c-road-tabs').find('.c-map-link.active').trigger('click');
}

function checkAgreeSendInformation(obj){
	if (obj.checked){
		$('input[name=subscribe_form]').attr("disabled", false);
	} else {
		$('input[name=subscribe_form]').attr("disabled", true);
	}
}

function getMessInLandscape(){

    if($('#landscapeMess').length){

		if(window.innerHeight < window.innerWidth && $(window).width() < 800 || window.innerHeight > window.innerWidth && $(window).width() < 560) {
			if (window.innerHeight < window.innerWidth)
			{
				var text_q = "вертикальное";
				popupModule.callOnLoadFunction = function () {
					$('.c-popup').addClass('new-image');
					$('.b-modal').addClass('transparented');
					$('.c-popup-close').addClass('hidden');
				};

				popupModule.callOnCloseFunction = function () {
					$('.c-popup').removeClass('new-image');
					$('.b-modal').removeClass('transparented');
					$('.c-popup-close').removeClass('hidden');
				};
				setTimeout(function(){
					$('#landscapeMess').click();
				},1);
				setTimeout(function(){
					var str = $('.c-repl').text();
					$('.c-repl').text(str.replace(' ', text_q));
				},1000);


			}
			else{
				popupModule.close();
			}
		}
    }
}

function getCurrentRoadText(ind){
    var elems = $('.c-road-text').children();

    elems.hide();
    elems.eq(ind).show();
}

function showPreloader(){       
    $('.c-preloader').show();       
}       
function hidePreloader(){       
    $('.c-preloader').hide();       
}

function getTableRows(elem){
    var elems = $('.c-load-table');

    showPreloader();
    $.ajax({
        url: elem.data('table'),
        success: function (data) {
            $('.c-table-result').html(data);

            if (elem.hasClass('c-tbl_switch')) {
                elems.removeClass('active');
                elem.addClass('active');
            }
        },
        error: function (er) {
            console.log(er)
        },      
        complete: function(){       
            hidePreloader();        
        }
    });
}

function getCallbackScript(){
    $.getScript( 'http://smartcallback.ru/api/SmartCallBack.js', function() {

        function CallTouchPhoneReplace(){
            jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 1000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 2000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 3000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 4000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 5000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 6000);
        }
        jQuery('.apartment_link').on('click', function(){
            CallTouchPhoneReplace();
        });
        jQuery('.t_r div').on('click', function(){
            CallTouchPhoneReplace();
        });

        $('.scb_but_img').on('click', function(){
            ga('send', 'event', 'zayavka', 'scb');
            yaCounter18350230.reachGoal('SCB_CALLBACK');
        });

        setTimeout(function(){
            $('.c-phone-tile').removeClass('disable');
            SCBinit("uLWNsem2hlZO3ZONIiOC");
        }, 3000);

        $('.c-phone-tile').on('click', function(e){
            e.preventDefault();
            if(!$(this).hasClass('disable')){
                SCBchange('YES');
                SCBopen(1);
            }
        });
    });
}

function initCustomSelect(elem){
    elem.SumoSelect({
        placeholder: 'Выбрать пункты',
        csvDispCount: 3,
        captionFormat: '{0} Selected',
        floatWidth: 300,
        forceCustomRendering: false,
        nativeOnDevice: [ 'Opera Mini', 'Silk'],
        outputAsCSV : false,
        csvSepChar : ',',
        okCancelInMulti: false,
        triggerChangeCombined : true,
        selectAll: false,
        selectAlltext: 'Select All'

    });
}


function sendFormbyAjax(form){
    var url = form.attr('action'),
        messblock = form.find('.c-ajax-form-mess');
    messblock.hide();
    messblock.removeClass('ajax-form-mess--error');
    messblock.html('');
    showPreloader();

    $.ajax({
        url: url,
        type : 'POST',
        dataType: 'JSON',
        data: getFormValues(form),
        success: function (json) {
            messblock.html(json.mess);
            if(json.status == 'error'){
                messblock.addClass('ajax-form-mess--error');
            }
            messblock.show();
			(window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() { 
					rrApi.setEmail(emailSubscriber);	
			});

        },
        error: function (er) {
            messblock.html('Ошибка передачи формы');
            messblock.addClass('ajax-form-mess--error');
            messblock.show();
        },      
        complete: function(){       
            hidePreloader();        
        }
    });
}

function getFormValues(form) {
    var form_arr = {};
    form_arr.text = [];
    form_arr.select = [];
    form_arr.radio = [];
    form_arr.checkbox = [];
    form_arr.hidden = [];

    $.each(form[0], function () {
        var tag = this.tagName.toLowerCase(),
            type = this.type,
            $this = $(this);

        if (type == undefined) {
            type = tag;
        }
        if (type == 'text' || type == 'email' || type == 'date' || type == 'number' || type == 'tel' || type == 'textarea') {
            if($this.hasClass('select2-focusser') || $(this).hasClass('select2-input')) {
            } else {
                form_arr.text.push({
                    "name": $this.attr('name'),
                    "value": $this.val()
                });
            }
        } else if (type == 'select-one') {
            form_arr.select.push({
                "name": $this.attr('name'),
                "value": $this.val()
            });
        } else if (type == 'checkbox') {
            if ($this.prop("checked") == true) {
                var a1 = $this.attr('name');
                if (form_arr.checkbox[a1] == undefined) {
                    form_arr.checkbox[a1] = [];
                }
                form_arr.checkbox[a1].push($this.val());
            }
        } else if (type == "radio") {
            if ($this.prop("checked") == true) {
                form_arr.radio.push({
                    "name": $this.attr('name'),
                    "value": $this.val()
                });
            }
        } else if (type == "hidden"){
            form_arr.hidden.push({
                "name": $this.attr('name'),
                "value": $this.val()
            });
        }
    });


    return form_arr;
}

function getCallbackScript(){
    $.getScript( 'http://smartcallback.ru/api/SmartCallBack.js', function() {

        function CallTouchPhoneReplace(){
            jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 1000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 2000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 3000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 4000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 5000);
            setTimeout(function () {jQuery('.ya-phone').html(jQuery('#ya-phone-1').html());}, 6000);
        }
        jQuery('.apartment_link').on('click', function(){
            CallTouchPhoneReplace();
        });
        jQuery('.t_r div').on('click', function(){
            CallTouchPhoneReplace();
        });

        $('.scb_but_img').on('click', function(){
            ga('send', 'event', 'zayavka', 'scb');
            yaCounter18350230.reachGoal('SCB_CALLBACK');
        });

        SCBinit("uLWNsem2hlZO3ZONIiOC");

        $('.c-phone-tile').on('click', function(e){
            e.preventDefault();
            SCBchange('YES');
            SCBopen(1);
        });
    });
}

function formValidate($form){
    var validatorLanguage = {
        errorTitle: 'Ошибка отправки формы!',
        requiredFields: 'Необходимо заполнить обязательные поля',
        badTime: 'Введите корректное время',
        badEmail: 'Введите корректный e-mail адрес',
        badTelephone: 'Введите корректный номер телефона',
        badSecurityAnswer: 'Ответ на секретный вопрос не верен',
        badDate: 'Введите корректную дату',
        lengthBadStart: 'Значение должно быть между ',
        lengthBadEnd: ' знаками',
        lengthTooLongStart: 'Значение поля больше, чем ',
        lengthTooShortStart: 'Значение поля меньше, чем ',
        notConfirmed: 'Input values could not be confirmed',
        badDomain: 'Введите корректное доменное имя',
        badUrl: 'Значение поля не является корректным url-адресом',
        badCustomVal: 'Зачение поля не верно',
        andSpaces: ' и пробелов ',
        badInt: 'Поле может содержать только цифры',
        badSecurityNumber: 'Your social security number was incorrect',
        badUKVatAnswer: 'Incorrect UK VAT Number',
        badStrength: 'The password isn\'t strong enough',
        badNumberOfSelectedOptionsStart: 'You have to choose at least ',
        badNumberOfSelectedOptionsEnd: ' answers',
        badAlphaNumeric: 'The input value can only contain alphanumeric characters ',
        badAlphaNumericExtra: ' и ',
        wrongFileSize: 'Максимальный размер файла %s',
        wrongFileType: 'Разрешениы следующие форматы файлов: %s',
        groupCheckedRangeStart: 'Выберите между ',
        groupCheckedTooFewStart: 'Выберите не меньше, чем ',
        groupCheckedTooManyStart: 'Выберите не больше, чем ',
        groupCheckedEnd: ' item(s)',
        badCreditCard: 'The credit card number is not correct',
        badCVV: 'The CVV number was not correct',
        wrongFileDim : 'Incorrect image dimensions,',
        imageTooTall : 'the image can not be taller than',
        imageTooWide : 'the image can not be wider than',
        imageTooSmall : 'the image was too small',
        min : 'мин.',
        max : 'макс.',
        imageRatioNotAccepted : 'Image ratio is not accepted'
    };

    $.validate({
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : false,
        language: validatorLanguage,
        borderColorOnError : '#ED5050',
        onSuccess : function($form){
            // send form by ajax
            if($form.hasClass('c-ajaxForm')){

                sendFormbyAjax($form);

                return false;
            }

        }
    });
}

function getCurrPercent(elem, textBlock){
    var fullSumm = parseInt($('#summ').val()),
        currVal = parseInt(elem.val());

    if(currVal){
        var p = (100 * currVal / fullSumm).toFixed(2);

        textBlock.text('руб. - '+p+'% от стоимости').removeClass('hidden');
        $('.c-first-summ-percent').val(p);
    }
}

function creditSendAjax(form, dataForm){
    showPreloader();

    $.ajax({
        url: form.data('ajax-url'),
        data: dataForm,
        type : 'POST',
        success: function (data) {
            if(dataForm.tableStep > 1){
                $('.show-more').remove();
                $(".objects-table").last().after(data);
            }
            else{
                $('.c-filter-result').html(data);
            }
        },
        error: function (er) {
            console.log(er)
        },      
        complete: function(){       
            hidePreloader();        
        }
    });
}


function accordionValue(current) {
    var link = current.prev('.c-accordion-link'),
        data_text = link.data('link-text'),
        data_type = link.data('type-accordion'),
        link_text = link.find('.accordion__link-text');

    if (data_type == 'radio') {
        current.find('.checkbox').change(function() {
            var a1 = $(this).next('.checkbox-label'),
                a2 = a1.find('.checkbox-text').html();
            link_text.html(data_text.replace('%s',a2));
        });
    } else if (data_type == 'slider') {
        var slider = current.find(".c-slider_range__input");
        slider.on("change", function () {
            var $this = $(this),
                from = $this.data("from");
            link_text.html(data_text.replace('%s',from));
        });
    } else if (data_type == 'text') {
        current.on('blur','.slider_range__textbox', function(){
            link_text.html(data_text.replace('%s',$(this).val()));
        });
    } else {
        console.log('err');
    }
}



function sliderIn(slider_el) {
    if (slider_el != undefined) {
		var slider = slider_el.Swipe({
			startSlide: 0,
			continuous: false,
			transitionEnd: function(index, element) {
				$('.c-fullscreen').attr('data-slide',index);
			}
		}).data('Swipe');
    }
}

function check_tags(elem, type, bool) {
    var wrap;
    if (type == 'all') {
        wrap = elem.parents('.filter_params_b');
        wrap.find('input[type="checkbox"]').prop('checked', bool);
        if (wrap.hasClass('c-check_tags')) {
            if(bool == true) {
                var wrap_elem = wrap.find('.c-checkbox_wrap'),
                    id_elem, text_elem;
                $.each(wrap_elem,function(i){
                    id_elem = $(wrap_elem[i]).find('input[type="checkbox"]').attr('id');
                });
            }
        }
    } else if (type == 'single') {
        var swrap_elem = elem.parent('.c-checkbox_wrap'),
            stext_elem,
            checkbox = elem,
            sid_elem = checkbox.attr('id');
        wrap = elem.parents('.c-check_tags');
    } else {
        console.log('err');
    }
}

function sliderInit(slider_el) {
    if (slider_el != undefined) {
        setTimeout(function(){
            var slider = slider_el.Swipe({
                startSlide: 0,
                continuous: false,
                transitionEnd: function(index, element) {
                    $('.c-fullscreen').attr('data-slide',index);
                }
            }).data('Swipe');
        },1000);
    }
}

function selectsMainSearch(selectsList, autocompleteInput){
	var searchData = {},
		inputs = $('.c-search-input');

	searchData.selects = [],
		searchData.type = 'realty';
	searchData.inputs = [];
	searchData.checkboxes = [];
	
	if($('.c-text_ser').hasClass('id')) {
		searchData.selects = [];
		searchData.type = 'id';
		searchData.inputs = [];
		searchData.checkboxes = [];
	} else {
		for(var i=0; i < selectsList.length; i++){
			var elem = $(selectsList[i]);

			if(elem.val() != null){
				searchData.selects.push( [elem.attr('name') , elem.val()] );
			}
		}

		selectsList.SumoSelect({
			placeholder: 'Выбрать пункты',
			csvDispCount: 3,
			captionFormat: '{0} Selected',
			floatWidth: 100,
			forceCustomRendering: false,
			nativeOnDevice: [ 'Opera Mini', 'Silk'],
			outputAsCSV : false,
			csvSepChar : ',',
			okCancelInMulti: false,
			triggerChangeCombined : true,
			selectAll: false,
			selectAlltext: 'Select All'

		})
		.on("change",function(){
			var elem = $(this),
				chooseSelect = $('.c-choose-type'),
				customSelects = $(".c-second-step .c-custom-select");

			if(elem.hasClass('c-choose-type')){
				var wrap = $('#'+elem.find(':selected').data('type'));

				wrap.addClass('active');
				wrap.siblings().removeClass('active');

				customSelects.each(function(){
					$(this)[0].sumo.unSelectAll();
				});
			} 

			searchData.selects = []

			for(var i=0; i < selectsList.length; i++){
				var elem = $(selectsList[i]);

				if(elem.val() != null){
					searchData.selects.push( [elem.attr('name') , elem.val()] );
				}
			}

			if(autocompleteInput.length){
				searchAutocomplete(autocompleteInput, searchData);
			}
		});
		$('.s_check input[type="checkbox"]').on('change',function(){
			setTimeout(function(){
				var sCheck = $('.s_check input[type="checkbox"]:checked');
				searchData.checkboxes = []
				for(var q=0; q < $(sCheck).length; q++){
					var elem = sCheck[q];
					searchData.checkboxes.push( [$(elem).attr('name') , 'Y'] );
				}
			},300);
		});

		$('body').on('click', '.c-search-switch', function(){
			var searchType = $(this).data('search'),
				customSearch = $('.c-custom-search'),
				selectList = customSearch.find("select.c-custom-select");

			if(searchType == 'show'){
				customSearch.removeClass('disable');

				searchData.type = 'realty';

				if(autocompleteInput.length){
					searchAutocomplete(autocompleteInput, searchData);
				}
			}
			else{
				customSearch.addClass('disable');

				selectList[0].sumo.unSelectAll();


				searchData.type = 'id';
				searchData.selects = [];

				if(autocompleteInput.length){
					searchAutocomplete(autocompleteInput, searchData);
				}
			}
		});
	}
	
	inputs.on('blur', function(){
		if(this.value != ''){
			searchData.inputs.push( [this.name , this.value ]);
		}

		if(autocompleteInput.length){
			searchAutocomplete(autocompleteInput, searchData);
		}
	})

	if(autocompleteInput.length){
		searchAutocomplete(autocompleteInput, searchData);
	}
}
function searchAutocomplete(input, formData){
    formData.searchText = '';

    input.autocomplete({
        serviceUrl: input.data('ajaxurl'),
        ajaxSettings: { dataType: 'JSON', data: formData },
        width : 400,
        deferRequestBy: 150,
        onSearchStart : function(){
            formData.searchText = this.value;
            input.autocomplete('setOptions', {ajaxSettings : { dataType: 'JSON', data: formData }});
        },
        transformResult: function(response) {
            return {
                suggestions: $.map(response.list, function(dataItem) {
                    if(dataItem.description != null){
                        return { value: dataItem.description, data: dataItem.url };
                    }
                    else{
                        return { value: dataItem.value, data: dataItem.url };
                    }
                })
            };
        },
        onSelect: function (suggestion) {
            if(suggestion.data && suggestion.data != ''){
                window.open(suggestion.data, "_blank");
            }
        }
    });
}

function _getPopupFilterInputs(elem, min, max, FilterValues){
	if(elem.data('filtervalue') == 'area'){
		FilterValues.area.min = min;
		FilterValues.area.max = max;
	}
	else{
		FilterValues.price.min = min;
		FilterValues.price.max = max;
	}

	siteYandexMap.getFilteredCollection(FilterValues);
}

function popupFilter(){
	
	var FilterValues = {},
		links = $('.c-switch-filter .c-filter-link'),
		dropfilters = $('.c-switch-filter .c-filter-drop'),
		checkOptions = $('.c-switch-filter .checkbox-label'),
		inputOptions = $('.c-switch-filter .c-input-option');

	FilterValues.checkValues = [];
	FilterValues.price = {};
	FilterValues.area = {};
	FilterValues.links = [];

	links.each(function(){
		if($(this).hasClass('active')){
			FilterValues.links.push($(this).data("filtervalue"));
		}
	});

	$.each(checkOptions, function(){
		var label = this;

		if($(label).data('checked') == "Y"){
			FilterValues.checkValues.push($(this).prev().val());
		}
	});

	$.each(inputOptions, function(){
		var elem = $(this),
			inputs = elem.find('input[type="text"]');

		if(elem.parents('.c-filter-drop').data('filtervalue') == 'area'){
			FilterValues.area.min = inputs.eq(0).val();
			FilterValues.area.max = inputs.eq(1).val();
		}
		else{
			FilterValues.price.min = inputs.eq(0).val();
			FilterValues.price.max = inputs.eq(1).val();
		}

	});

	siteYandexMap.getFilteredCollection(FilterValues);

	$('.c-filter-link').on('click', function(e){
		e.preventDefault();

		$(this).toggleClass('active');

		FilterValues.links = [];

		links.each(function(){
			if($(this).hasClass('active')){
				FilterValues.links.push($(this).data("filtervalue"));
			}
		});

		siteYandexMap.getFilteredCollection(FilterValues);

	});

	dropfilters.find('a').on('click', function(e){
		e.preventDefault();
	});

	$.each(dropfilters, function(){
		var elem = $(this),
			checkOptions = elem.find('.checkbox-label'),
			inputOption = elem.find('.c-input-option');

		if(inputOption.length){

			var inputs = elem.find('input[type="text"]'),
				clear = elem.find('.c-clear-inputs'),
				select = elem.find('.c-priceval');

			inputs.on('blur', function(){
				var min = parseInt(inputs.eq(0).val()),
					max = parseInt(inputs.eq(1).val());
				_getPopupFilterInputs(elem, min, max, FilterValues);
			});

			inputs.on('keypress', function(e){
				var min = parseInt(inputs.eq(0).val()),
					max = parseInt(inputs.eq(1).val());
				_getPopupFilterInputs(elem, min, max, FilterValues);
			});

			function editInp() {
				var minInput = inputs.eq(0),
					maxInput = inputs.eq(1);
				if(select.find('option:selected').val() == 'USD') {
					minInput.val(parseInt(minInput.attr('data-defaultUSD')));
					maxInput.val(parseInt(maxInput.attr('data-defaultUSD')));
				} else {
					minInput.val(parseInt(minInput.attr('data-default')));
					maxInput.val(parseInt(maxInput.attr('data-default')));
				}
				_getPopupFilterInputs(elem, minInput.data('default'), maxInput.data('default'), FilterValues);
			}

			clear.on('click', function(e){
				e.preventDefault();
				editInp();
			});

			select.on('change', function(e){
				e.preventDefault();
				editInp();
			});
		}

		if(checkOptions.length){
			checkOptions.off('click').on( "click", function() {
				var label = this;

				FilterValues.checkValues = [];
				label.dataset.checked == "Y" ? label.dataset.checked = "N" : label.dataset.checked = "Y";

				$.each(checkOptions, function(){
					var label = $(this)[0];

					if($(label).data('checked') == "Y"){
						FilterValues.checkValues.push($(this).prev().val());
					}
				});

				siteYandexMap.getFilteredCollection(FilterValues);
			});
		}

	});
	
}

function getPriceFormat(input){
	if(!input.val()){
		return;
	}
	var val = input.val().replace(/\s+/g, '');

	val = parseInt(val).toFixed(0);

	input.val(val);

	input.priceFormat({
	    prefix: '',
	    centsSeparator: '.',
	    thousandsSeparator: ' ',
	    centsLimit: 0
	});		
}

function drumValues(btn) {
	var d_dobule = btn.data('double') || false,
		select_o = btn.data('select'),
		select_t = btn.data('select2'),
		select_p = btn.data('select3'),
		input_o = btn.data('input'),
		input_t = btn.data('input2'),
		price = btn.data('price') || false;
	if(d_dobule == true) {
		$('input[name="'+input_o+'"]').val($('select[name="'+select_o+'"]').val());
		$('input[name="'+input_t+'"]').val($('select[name="'+select_t+'"]').val());
	} else if (price == true) {
		var price = parseInt($('select[name="'+select_o+'"]').val()),
			input_o = $('input[name="'+input_o+'"]');
		input_o.val(price);
		getPriceFormat(input_o);
		input_o.val(input_o.val()+' млн. $');
		
	} else {
		$('input[name="'+input_o+'"]').val($('select[name="'+select_o+'"]').val());
	}
	$('.modal_drum').hide();
	$('.elite_filter, .c-subit_params').show();
}

$(function(){

    var body = $('body'),
        scroll_p;


    $('.c-mobile-scroll').mCustomScrollbar({
        contentTouchScroll:25,
        scrollButtons:{enable:true}
        // autoHideScrollbar:true

    });

    getMessInLandscape();
    //reviews
    $('.c-review-author').each(function (){
        var authorLenght = $(this).width();
        $(this).parent().find('.c-review-text-ar').css('margin-left', authorLenght);
    });

    window.addEventListener('orientationchange', function ()
    {
        getMessInLandscape();
        $('.c-popup').css('width', '100%');
    });

    var accordionsLinks = body.find('.c-accordion-link');
    if (accordionsLinks.length) {
        $.each(accordionsLinks, function () {
            var link = $(this),
                accordion = link.closest('.c-accordion'),
                drops = accordion.find('.c-accordion-drop'),
                links = accordion.find('.c-accordion-link');

            link.on('click', function (e) {
                e.preventDefault();
                var elem = $(this),
                    currDrop = elem.next('.c-accordion-drop');

                links.not(elem).removeClass('active');
                elem.toggleClass('active');
                if (slider_el.length) {
                    slider.kill();
                    sliderInit(slider_el);
                }

                currDrop.slideToggle(function () {
                    $('html, body').stop().animate({ scrollTop: elem.offset().top - 200 });
                });

                drops.not(currDrop).slideUp();

                if (currDrop.find('.c-direction-list').length) {
                    var list = currDrop.find('.c-direction-list'),
                        checkboxes = list.find('.checkbox-wrap'),
                        n = 0, _height = 0;

                    while ((n < 5) && (n < checkboxes.length)) {
                        _height = _height + checkboxes.eq(n).height() + 16;
                        n++;
                    }

                    list.height(_height + 5);
                }
                if(accordion.hasClass('c-accordion_filter')) {
                  accordionValue(currDrop);
                }
            })
        });
    }

    var slider_el = $('.swipe'),
		slider = [],
		next_buttons = $('.c-slide_next'),
		prev_buttons = $('.c-slide_prev');
	slider_el.each(function (i){
		slider[i] = $(this).Swipe({
			startSlide: 0,
			continuous: false,
			transitionEnd: function(index, element) {
				$('.c-fullscreen').attr('data-slide',index);
			}
		}).data('Swipe');
		
		$(next_buttons[i]).on('click',function(){
			slider[i].next();
		});
		$(prev_buttons[i]).on('click',function(){
			slider[i].prev();
		});
	});
	
	$('.btn_id').on('click',function(e){
		e.preventDefault();
		if($(this).hasClass('back')){
			$(this).val('Поиск по ID');
			$(this).removeClass('back');
			$('.s_check').find('input[type="checkbox"]').prop('disabled',false);
			$('.search-price').find('select, input[type="text"]').prop('disabled',false);
			$('.search-price .SumoSelect').removeClass('disable');
			$('.c-text_ser').removeClass('id').text('Ключевые слова');
			$('input[name="type"]').val('realty');
		} else {
			$(this).val('Вернуться назад');
			$(this).addClass('back');
			$('.s_check').find('input[type="checkbox"]').prop('disabled',true).prop('checked', false);
			$('.search-price').find('select, input[type="text"]').prop('disabled',true);
			$('.search-price .SumoSelect').addClass('disable');
			$('.c-text_ser').addClass('id').text('Введите ID объекта');
			$('.fliter_check__item').hide();
			$('.fliter_check__item input[type="text"]').val('');
			$('input[name="type"]').val('id');
			$.each($(".search_select"),function(i){
				$(".search_select")[i].sumo.unSelectAll();
			});
			
		}
		selectsMainSearch($('.c-custom-select'), $('#autocomplete'));
	});
	
	$('.с-show_count').on('click',function(e){
		e.preventDefault();
		var show = $(this).data('show');
		$('input[name="show"]').val(show);
		$('input[name="pagen"]').val('1');
		$('.search_f').submit();
	});
	
	
	$('.c-page_pag').on('click',function(e){
		e.preventDefault();
		var page = $(this).data('page');
		$('input[name="pagen"]').val(page);
		$('.search_f').submit();
	});
	
	$('#btn_search').on('click',function(){
		$('input[name="pagen"]').val('1');
	});
	
	
	function searchBlocks (num) {
		
		$.ajax({
		  url: $('.search_param').data('url'),
		  dataType: 'json',
		  type:'GET',
		  success:function(data){
			$('.fliter_check__item').hide();
			$.each(data[num],function(i){
				$('#'+data[num][i]).show();
			})
		  },
		  error: function (err) {
			console.log(err);
		}
		});
		
		$('.fliter_check__item input[type="text"]').val('');
		$.each($(".search_select"),function(i){
			$(".search_select")[i].sumo.unSelectAll();
		});
	}
	
	$('.c-search_check').on('change',function(e){
		e.preventDefault();
		var new_c = $('.c-search_check[data-check="new"]'),
			second = $('.c-search_check[data-check="second"]'),
			cottage = $('.c-search_check[data-check="cottage"]'),
			plots = $('.c-search_check[data-check="plots"]'),
			elem = '.c-search_reg',
			num = 0;

		if(new_c.prop('checked') == true){
			$(elem).prop('disabled',false).prop('checked',true);
        } else if(second.prop('checked') == true) {
            $(elem).prop('disabled',false).prop('checked',true);
		} else if(cottage.prop('checked') == true || plots.prop('checked') == true) {
			$(elem).prop('disabled',true).prop('checked',false);
			$(elem+'[data-city="region"]').prop('disabled',false).prop('checked',true);
		} else {
			$(elem).prop('disabled',false).prop('checked',false);
		}
		
		if (new_c.prop('checked') == true && second.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true) {
			num = 1;
		} else if(second.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true) {
			num = 2;
		} else if (new_c.prop('checked') == true && second.prop('checked') == true && plots.prop('checked') == true || new_c.prop('checked') == true && cottage.prop('checked') == true && plots.prop('checked') == true || new_c.prop('checked') == true && plots.prop('checked') == true) {
			num = 3;
		} else if (new_c.prop('checked') == true && cottage.prop('checked') == true || new_c.prop('checked') == true && second.prop('checked') == true && cottage.prop('checked') == true) {
			num = 4;
		} else if (plots.prop('checked') == true && cottage.prop('checked') == true) {
			num = 5;
		} else if (second.prop('checked') == true && cottage.prop('checked') == true) {
			num = 6;
		} else if (second.prop('checked') == true && plots.prop('checked') == true) {
			num = 7;
        } else if (new_c.prop('checked') == true && second.prop('checked') == true) {
			num = 8;
        } else if (new_c.prop('checked') == true) {
            num = 12;
		} else if (plots.prop('checked') == true) {
			num = 9;
		} else if (cottage.prop('checked') == true) {
			num = 10;
		} else if (second.prop('checked') == true) {
			num = 11;
		} else {
			num = 0;
		}
		
		searchBlocks(num);
		
	});

    body.on('click', '.c-tabs-link', function(e){
        e.preventDefault();

        var link = $(this),
            contents = link.closest('.c-tabs').children('.tabs__content').children('.c-tabs-item');
			if (contents.length == 0) {
				contents = link.closest('.page-content').find('.tabs__content').children('.c-tabs-item');
			}

        link.addClass('active').siblings().removeClass('active');
        contents.removeClass('active');
        contents.eq(link.index()).addClass('active');
    }).on('click', '.c-map-link', function(e){
        e.preventDefault();

        // ok for yandex.maps , doesn't work on google
        siteYandexMap.init($(this));

    }).on('click', '.delete-manager', function(){
        var elem = $(this).parents('.managers__item');
        elem.slideUp(200, function(){
            showPreloader();

            $.ajax({
                url: elem.data('ajax-url'),
                data: { managerId : elem.data('manager-id') },
                success: function(){
                    console.log('Менеджер удален')
                },
                error: function(er){
                    console.log(er)
                },      
                complete: function(){       
                    hidePreloader();        
                }
            });
        });
    }).on('click','.c-enable_input', function(){
        var elem = $(this).parents('.c-container_input').find('.c-form_input');
        elem.removeAttr('disabled').focus();
    }).on('click', '.accordion_trigger__link', function(){
        $('.accordion_trigger').trigger('click');
    }).on('click', '.c-table-elite', function(){
        $('.c-elite_desc').hide();
        $('.c-table-sale').show();
    }).on('click', '.c-back-in-desc', function(){
        $('.c-table-sale').hide();
        $('.c-elite_desc').show();
    }).on('click', '.c-button-elite_pr', function(){

        var hide_blocks = $(this).data('hide'),
            show_block = $(this).data('show'),
            fscreen = $(this).data('fscreen'),
            fcreen_dom = $('.c-elite-fullscreen');

        $(hide_blocks).hide();
        $(show_block).show();
        $('.buttons-elite__item').show();
        $(this).parent('.buttons-elite__item').hide();

        if (fscreen == 'map') {
            $('.c-fscreen_el').hide();
            $('.c-popup_map').show();
        } else if(fscreen == 'sat') {
            $('.c-fscreen_el').hide();
            $('.c-popup_sat').show();
        } else {
            $('.c-fscreen_el').hide();
            $('.c-popup_slider').show();
        }
    }).on('click', '.c-nmarket_dropdown', function(){
        var elem = $(this);
        elem.toggleClass('active');
        setTimeout(function(){
            elem.find('.c-nmarket_drp').slideToggle(300);
        },200);

    }).on('click', '.dropdown-menu--active, .dropdown-section--active', function(){
        var elem = $(this),
            list = elem.siblings('.c-dropdown__list');
        list.toggleClass('active');
        list.slideToggle(300);
        if(list.hasClass('active')) {
            list.width($(document).width());
        }
    }).on('click', '.c-dropdown__list .close-popup', function(){
        var elem = $(this),
            list = elem.parent('.c-dropdown__list');
        list.removeClass('active');
        list.slideUp(300);
    }).on('click','.c-params_op',function(){
        var open_elem = $(this).data('number');
        $('.c-parametrs').hide();
        $('.blocks_param-h[data-number="'+open_elem+'"]').show();
    }).on('click','.c-back-params',function(){
        var wrap = $(this).parents('.blocks_param-h'),
            numb = wrap.data('number'),
            button = $('.c-params_op[data-number="'+numb+'"]');
        wrap.hide();
        $('.c-parametrs').show();
        if (wrap.hasClass('c-checkbox_f')) {
            var checkbox = wrap.find('input[type="checkbox"], input[type="radio"]'),
                length_t = 0,
                name_dom = button.find('.c-name_params'),
                name = name_dom.data('default-t');

            $.each(checkbox,function(i) {
                if ($(checkbox[i]).prop('checked') == true) {
                    length_t++;
                }
            });
            if (length_t > 1) {
                button.addClass('active');
                name_dom.text(name+' ('+length_t+')');
            } else if(length_t > 0) {
                button.addClass('active');
                name_dom.text(name);
            } else {
                button.removeClass('active');
                name_dom.text(name);
            }
        } else if (wrap.hasClass('c-input_f')) {
            var input = wrap.find('input[type="text"]'),
                length_i = 0;
            $.each(input,function(i) {
                if($(input[i]).val() != $(input[i]).data('default_val')) {
                    length_i++;
                }
            });
            if (length_i > 0) {
                button.addClass('active');
            } else {
                button.removeClass('active');
            }
        } else {
            console.log('err');
        }
    }).on('click','.clear_input',function(){
       $(this).prev().val('');
    }).on('click','.c-uncheck__link',function(){
        check_tags($(this),'all', false);
    }).on('click','.c-check__link',function(){
        check_tags($(this),'all', true);
    }).on('change','.c-checkbox_i',function(){
        check_tags($(this),'single');
    }).on('click','.c-del_tag',function(){
        var id = $(this).data('del');
        $(this).remove();
        $('#'+id).prop('checked',false);
    }).on('click','.c-reset_inputs',function(){
        var wrap = $(this).parents('.popup-filter_m__bottom').prev('.c-popup_inputs'),
            elem = wrap.find('input[type="text"]');

        $.each(elem,function(i){
            var d_val = $(elem[i]).data('default_val');
            $(elem[i]).val(d_val);
        });
    }).on('click', '.c-desc_view_ajax', function(){
        var elem = $(this),
            link = elem.data('desc_href'),
            from = elem.data('desc_from');

        showPreloader();

        $.ajax({
            url: link,
            dataType: "html",
            success: function(data){
                $(from).slideDown(300);
                $(from).append(data);
                elem.slideUp(300);
            },
            error: function(er){
                console.log(er)
            },      
            complete: function(){       
                hidePreloader();        
            }
        });
    }).on('click', '.c-desc_view', function(){
        var elem = $(this),
            from = elem.data('desc_from');
            $(from).slideDown(300);
            elem.slideUp(300);
    })
    .on('click', '.c-remove-compare', function (e) {
        e.preventDefault();

        var elem = $(this);

        $.ajax({
            url: elem.data('ajax-url'),
            data: { OBJ_ID: elem.data('obj-id') },
            success: function (data) {
                if (data['status'] == 'success'){
                    elem.closest('.c-compare-obj').slideUp(100, function(){
                        elem.closest('tr').remove();
                    });
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    })

    .on('click', '.c-like', function () {
        var elem = this,
            src_url = elem.dataset.ajax_url;

        showPreloader();

        $.ajax({
            url: src_url,
            data: { OBJ_ID: elem.dataset.obj_id, ACTION: elem.dataset.action, LIKE_ID: elem.dataset.like_id },
            dataType: 'JSON',
            success: function (data) {
                if (data['status'] == 'success' || data['status'] == true) {
                    $(elem).toggleClass('active');

                    if (elem.dataset.action == 'like') {
                        //меняем action
                        elem.dataset.action = 'dislike';
                        //и ставим ID лайка
                        elem.dataset.like_id = data['likeId'];
                    }
                    else {
                        if ($(elem).closest('.c-my-choice').length) {
                            $(elem).closest('.new-building').remove();
                        }
                        else {
                            elem.dataset.action = 'like';
                            //удаляем ID лайка
                            delete elem.dataset.like_id;
                        }
                    }

                    $(elem).focusout();
                }
            },
            error: function (er) {
                console.log(er)
            },      
            complete: function(){       
                hidePreloader();        
            }
        });
    })
    .on('click', '.c-main-popup-link', function(e){
        e.preventDefault();

        var popup = $('#'+this.dataset.popup);

        popup.fadeIn(100, function(){
            popup.siblings('.c-main-popup').hide();
        });
    })
    .on('click', '.c-close', function(e){
        e.preventDefault();
        $(this).closest('.c-main-popup').fadeOut(100);
    }).on('click', '.c-open_sale', function(){
        popupModule.close();
        $('.c-elite_desc').hide();
        $('.c-table-sale').show();
    }).on('click', '.c-redirect-link', function(){
		var id = $(this).data('url'),
			btn = $('.c-link-apartment');
		btn.attr('data-url',id);
		if(!btn.hasClass('active')) {
			btn.addClass('active');
			btn.slideDown(300);
		}
	}).on('click','.c-link-apartment',function(e){
		e.preventDefault();
		window.location.href = $(this).data('url');
	}).on('click', '.c-accordion-search', function(){
		if($(this).hasClass('active')) {
			$(this).find('.c-accordion-text').text('Свернуть поисковую панель');
		} else {
			$(this).find('.c-accordion-text').text('Развернуть поисковую панель');
		}
	}).on('click','.c-toggle-drum', function(e){
		e.preventDefault();
		scroll_p = $('.b-ajax-wrapper').scrollTop();
		$('.elite_filter, .c-subit_params').hide();
		$('.'+$(this).data('drum')).show();
	}).on('click','.c-close_drum',function(e){
		e.preventDefault();
		$(this).parent().hide();
		$('.elite_filter, .c-subit_params').show();
		$('.b-ajax-wrapper').scrollTop(scroll_p);
	}).on('click', '.search_inp__btn', function(){
		$(this).parents('form').submit();
	}).on('click', '.c-apply_drum', function(){
		$('.b-ajax-wrapper').scrollTop(scroll_p);
	});
	

    // выводим карту с маршрутом на первый активный таб
    var roadTabs = $('.c-road-tabs');
    if($('.c-road-tabs').length){
		setTimeout(function(){
			getRouteToActiveTab();
		},3000);

        roadTabs.find('.c-tabs-link').on('click', function () {
            var link = $(this);

            getCurrentRoadText(link.index());
        });
    }

    var geolocationLinks = $('.c-geolocationLink');
    if (geolocationLinks.length) {
        siteYandexMap.getCurrentUserPosition();
    }

    // Карта сабагентам
    var subagents_map = $('#subagents_map');
    if(subagents_map.length) {
        siteYandexMap.init(subagents_map);
    }
	setTimeout(function(){
		$('.c-dev-map-btns').find('.c-map-link.active').trigger('click');
	},3000);
    var showSaleTable = $('.c-load-table.active');
    if(showSaleTable.length){
        getTableRows(showSaleTable);
    }

    $('.c-dev-map-btns').on('click', '.c-map-link', function () {
        $(this).addClass('active').siblings().removeClass('active');
    });

    // validation
    var validatorLanguage = {
        errorTitle: 'Ошибка отправки формы!',
        requiredFields: 'Необходимо заполнить обязательные поля',
        badTime: 'Введите корректное время',
        badEmail: 'Введите корректный e-mail адрес',
        badTelephone: 'Введите корректный номер телефона',
        badSecurityAnswer: 'Ответ на секретный вопрос не верен',
        badDate: 'Введите корректную дату',
        lengthBadStart: 'Значение должно быть между ',
        lengthBadEnd: ' знаками',
        lengthTooLongStart: 'Значение поля больше, чем ',
        lengthTooShortStart: 'Значение поля меньше, чем ',
        notConfirmed: 'Input values could not be confirmed',
        badDomain: 'Введите корректное доменное имя',
        badUrl: 'Значение поля не является корректным url-адресом',
        badCustomVal: 'Зачение поля не верно',
        andSpaces: ' и пробелов ',
        badInt: 'Поле может содержать только цифры',
        badSecurityNumber: 'Your social security number was incorrect',
        badUKVatAnswer: 'Incorrect UK VAT Number',
        badStrength: 'The password isn\'t strong enough',
        badNumberOfSelectedOptionsStart: 'You have to choose at least ',
        badNumberOfSelectedOptionsEnd: ' answers',
        badAlphaNumeric: 'The input value can only contain alphanumeric characters ',
        badAlphaNumericExtra: ' и ',
        wrongFileSize: 'Максимальный размер файла %s',
        wrongFileType: 'Разрешениы следующие форматы файлов: %s',
        groupCheckedRangeStart: 'Выберите между ',
        groupCheckedTooFewStart: 'Выберите не меньше, чем ',
        groupCheckedTooManyStart: 'Выберите не больше, чем ',
        groupCheckedEnd: ' item(s)',
        badCreditCard: 'The credit card number is not correct',
        badCVV: 'The CVV number was not correct',
        wrongFileDim: 'Incorrect image dimensions,',
        imageTooTall: 'the image can not be taller than',
        imageTooWide: 'the image can not be wider than',
        imageTooSmall: 'the image was too small',
        min: 'мин.',
        max: 'макс.',
        imageRatioNotAccepted: 'Image ratio is not accepted'
    };

    $('body').on('click', '.c-send-pdf-custom', function (e) {
        $.validate({
            validateOnBlur: false,
            errorMessagePosition: 'top',
            scrollToTopOnError: false,
            language: validatorLanguage,
            borderColorOnError: '#e41919',
            onSuccess : function($form){
                // send form by ajax
                if($form.hasClass('c-ajaxForm')){
                    sendFormbyAjax($form);

                    return false;
                }

            }
        });
    });
    
    $('body').on('click', '.c-send-emailNewBuildingOfferPrice', function (e) {
        $.validate({
            validateOnBlur: false,
            errorMessagePosition: 'top',
            scrollToTopOnError: false,
            language: validatorLanguage,
            borderColorOnError: '#e41919',
            onSuccess : function($form){
                // send form by ajax
                if($form.hasClass('c-ajaxForm')){
                    sendFormbyAjax($form);

                    return false;
                }

            }
        });
    });

    var dataSlider = $('#date-slider');
    if(dataSlider.length){
        var buttons = $('.c-date-buttons'),
            ajax_url= buttons.data('ajax-url');

        body.on('click', '.c-change-month, .c-change-year', function(e){
            e.preventDefault();
            var elem = $(e.target);

            if(!elem.hasClass('disable')){
                elem.addClass('active');
                elem.siblings().removeClass('active');

                var _year = buttons.find('.c-change-year.active'),
                    _month = buttons.find('.c-change-month.active');

                showPreloader();

                $.ajax({
                    url: ajax_url,
                    data: { year : _year.data('year'), month: _month.data('month') },
                    dataType: "JSON",
                    success: function(data){
                        var template = '',
                            months = buttons.find('.c-change-month');
                        slider.kill();

                        if (data.images !== undefined) {
                            $.each(data.images, function () {
                                var links = $(this);
                                template += '<div><img src="' + links[0].thumb + '" alt=""></div>';
                            });

                            dataSlider.find('.swipe-wrap').html(template);
                            
                        }

                        if (elem.hasClass('c-change-year')) {
                            if (data.dis_months !== undefined) {
                                months.removeClass('disable');
                                for (i = 0; i < data.dis_months.length; i++) {
                                    months.eq(data.dis_months[i]).addClass('disable');
                                }
                            }
                            months.removeClass('active');
                            months.eq(data.active_month).addClass('active');
                        }
                    },
                    error: function(er){
                        console.log(er)
                    },      
                    complete: function(){       
                        hidePreloader();
    					sliderIn(slider_el);					
                    }
                });
            }
        });
    }
    var rangeSlider = $('.c-slider_range');
    if(rangeSlider.length) {
        $.each(rangeSlider,function(i){
            var elem = $(rangeSlider[i]),
                slider = elem.find(".c-slider_range__input"),
                slider_container = slider.parents('.c-slider_range'),
                elem_from = slider_container.find('.c-slider_range__from'),
                elem_to = slider_container.find('.c-slider_range__to');
            slider.ionRangeSlider({
                type: slider.data('slider_type') || "double",
                min: slider.data('slider_min'),
                max: slider.data('slider_max'),
                from: slider.data('slider_from'),
                to: slider.data('slider_to'),
                step: slider.data('slider_step'),
                hide_from_to: true,
                onStart: function () {
                    elem_from.val(slider.data("slider_from"));
                    elem_to.val(slider.data("slider_to"));
                }
            });

            slider.on("change", function () {
                var $this = $(this),
                    from = $this.data("from"),
                    to = $this.data("to");
                elem_from.val(from);
                elem_to.val(to);
                if ($('#summ').length) {
                    getCurrPercent($('.c-first-summ'), $('.c-summ-describe'));
                }
            });

            slider_container.find('.c-slider_range__value').focusout(function(){
                var item = slider.data("ionRangeSlider");
                item.update({
                    from: elem_from.val(),
                    to: elem_to.val()
                });
                elem_to.val(item.old_to);
                elem_from.val(item.old_from);
            });
        });
    }

    initCustomSelect($(".custom-select"));

    // ипотечный калькулятор
    var creditForm = $('#credit-form');
    if (creditForm.length > 0) {
        var fistSummInput = creditForm.find('.c-first-summ'),
            decsribeBlock = creditForm.find('.c-summ-describe');

        fistSummInput.on('keypress', function (e) {
            if (e.keyCode < 48 || e.keyCode > 57) {
                return false;
            }
            $(this).on('blur', function () {
                getCurrPercent($(this), decsribeBlock);
            });
        });

        $.validate({
            form: '#credit-form',
            validateOnBlur: false,
            errorMessagePosition: 'top',
            scrollToTopOnError: false,
            language: validatorLanguage,
            borderColorOnError: '#e41919',
            onSuccess: function ($form) {
                var formData = {};

                formData.summ = creditForm.find('[name="summ"]').val();
                formData.firstSumm = creditForm.find('[name="firstSumm"]').val();
                formData.age = creditForm.find('[name="age"]').val();
                formData.sex = creditForm.find('[name="sex"]:checked').val();
                formData.type = creditForm.find('[name="type"]:checked').val();
                formData.ready = creditForm.find('[name="ready"]:checked').val();
                formData.certificate = creditForm.find('[name="certificate"]:checked').val();
                formData.experience = creditForm.find('[name="experience"]:checked').val();
                formData.workType = creditForm.find('[name="work-type"]:checked').val();
                formData.period = creditForm.find('[name="period"]').val();
                formData.monthSumm = creditForm.find('[name="monthSumm"]').val();
                formData.firstSummPercent = $('.c-first-summ-percent').val();
                formData.tableStep = $('#show_more').data('page') || 1;


                creditSendAjax(creditForm, formData);

                body.on('click', '#show_more',function(e){
                    e.preventDefault();

                    formData.tableStep = $(e.target).data('page');
                    creditSendAjax(creditForm, formData);
                });

                return false;
            }
        });
    }

    var dropzone = $('#myAwesomeDropzone');
    if(dropzone.length) {
        Dropzone.options.myAwesomeDropzone = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            previewTemplate: '<div class="preview-imgq dz-preview dz-file-preview"><div class="img"><img data-dz-thumbnail /></div><div data-dz-remove class="close"><div class="sprite-icon close_manager"></div></div></div>',
            clickable: ".fileinput-button",
            previewsContainer: ".preview_img"
        };
    }


    // зум карты
    body.on('click', '.c-zoom-map', function (e) {
        e.preventDefault();

        var link = $(this);

        $('.c-popup .popup__content').empty();
        $('.c-popup').removeAttr('style');

        popupModule.callOnLoadFunction = function () {
            setTimeout(function () {
                siteYandexMap.init(link);
            }, 300);
        }

        popupModule.callOnCloseFunction = function () {
            if(subagents_map.length) {
                subagents_map.prev('.c-map-link').trigger('click');
            }
            popupModule.resetCallBacksFunction();
        }

        popupModule.init($('.c-popup'), link);
    });

    body.on('click', '.c-popup__link', function (e) {
        e.preventDefault();
        popupModule.init($('.c-popup'), $(this));
		if($(this).data('preloader') == true) {
			$('.c-preloader').show();
			$('body').addClass('preloader_q');
		}
		
		popupModule.callOnLoadFunction = function () {
			$('body').css('overflow','hidden');
			if(body.hasClass('preloader_q')) {
				$('.c-preloader').hide();
			}
		};

		popupModule.callOnCloseFunction = function () {
			$('body').removeAttr('style');
		};
    })
    .on('click', '.c-jc-objects-link', function (e) {
        e.preventDefault();

        var map = $('#big-map');

        $(this).data('placemarks-json', 'ajax/elit-placemarks-jc.back.json');
        siteYandexMap.init($(this));

        setTimeout(function(){
            map.append('<a href="" title="" class="c-back-link c-map-link back-map-link" data-map-id="big-map" data-placemarks-json="ajax/elit-placemarks-jc.back.json" data-elit="Y" data-has-child="Y">Вернуться к ЖК</a>');
        }, 300);

    }).on('click', '.c-load-table', function(e){
        e.preventDefault();
        getTableRows($(this));
    });

    if($('#newYearMess').length){
        $('#newYearMess').trigger('click');

        popupModule.callOnLoadFunction = function () {
            $('.c-popup').addClass('new-image');
            $('.b-modal').addClass('transparented');
        };
        popupModule.callOnCloseFunction = function () {
            $('.c-popup').removeClass('new-image');
            $('.b-modal').removeClass('transparented');
        };
    }
	
	var lrmenu;
	
	if($('body').hasClass('elite')) {
		lrmenu = 'left';
	} else {
		lrmenu = 'right';
	}

	$('#mobile_menu').mmenu({
		extensions	: [ 'effect-slide-menu', 'pagedim'],
		offCanvas: {
		   position  : lrmenu
		},
		navbar 		: {
			title		: 'Главное меню'
		},
	});
	
	
	body.append('<div class="c-preloader preloader"><img src="/images/preloader.gif" alt=""></div>');
	
	if($('#open_modal').length){
		$('#open_modal').trigger('click');
	}
	
});




var popupModule = (function () {
    var module = {},
        bPopup,
        scrollTop = 0;

    module.init = function (elem, link) {

        this.close();

        $('.c-popup .popup__content').empty();

        module.type = link.data('type-popup') || 'ajax';
        module.url = link.data('url-popup') || '/';


        bPopup = elem.bPopup({
            content: module.type,
            closeClass: 'c-popup-close',
            contentContainer: '.popup__content',
            loadUrl: module.url,
            modalColor: '#71737a',
            opacity: 1,
            scrollBar: false,
            follow: [true, false],
            loadCallback: function () {
                scrollTop = $(window).scrollTop();
                $(window).scrollTop(0);
                $('.c-popup-close').show();
                module.callOnLoadFunction();
            },
            onClose: function () {
                $(self.content).empty();
                $('.c-popup-close').hide();
                $(window).scrollTop(scrollTop);
                module.callOnCloseFunction();
                popupModule.resetCallBacksFunction();
            }
        });
    }

    module.callOnLoadFunction = function () {

    };

    module.callOnCloseFunction = function () {

    };

    module.close = function(){
        if (bPopup) {
            bPopup.close();
        }
    }

    module.reposition = function(){
        if (bPopup) {
            bPopup.reposition(100);
        }
    }

    module.resetCallBacksFunction = function () {

        module.callOnLoadFunction = function () {
            console.log('Loaded');
        };

        module.callOnCloseFunction = function () {

        };
    }

    return module;
})();