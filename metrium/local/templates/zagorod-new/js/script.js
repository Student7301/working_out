var myMap;
var bigMap = false;
var showBy = 20;
var currPage = 1;
var sortDir = 'DESC';

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

$(function () {
    loadFromHash();


    // $('body').on('click', '.c-play-video', function (e) {
    //    e.preventDefault();
    //     $(this).toggleClass('paused');
    //     if ($('.c-play-video').hasClass('paused')) {
    //         var Player = $(this).closest('.c-video-item').find('.c-video')[0];
    //         Player.pause();
    //     } else {
    //         var Player = $(this).closest('.c-video-item').find('.c-video')[0];
    //         Player.play();
    //     }
    // });


    if ($(window).width() >= 768) {
        playMainVideo();
    }
    openPopup();
    closePopup();
    scrollHeader();
    scrollToTop();
    scrollToList();
    createRangeFilter();
    changeDropdown();
    progressUpdateVideo();
    if ($('#filter-map').length) {
        ymaps.ready(initMapFilter);
    }

    $('body').on('click', 'div:not(.c-popup-content)', function () {
        $(this).closest('.c-popup-menu').removeClass('open');
        $('body, html').removeClass('no-scroll');
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.c-arrow-top').fadeIn();
        } else {
            $('.c-arrow-top').fadeOut();
        }
    });

    $('.c-custom-select').SumoSelect();
    $('.c-custom-scroll').mCustomScrollbar();

    $('body').on('input', '.c-input-filter', function (e) {
        e.preventDefault();
        currPage = 1;
        sendDataFilter();
    });
    // $('body').on('change', '.c-input-filter', function (e) {
    //     e.preventDefault();
    //     currPage = 1;
    //     console.log(2);
    //     sendDataFilter();
    // });
    $('body').on('change', '.c-custom-select', function (e) {
        e.preventDefault();
        currPage = 1;
        sendDataFilter();
    });
    $('body').on('change', '.c-custom-checkbox', function (e) {
        e.preventDefault();
        currPage = 1;
        sendDataFilter();
    });
    $('body').on('click', '.c-send-form', function (e) {
        e.preventDefault();
        sendDataSubscr();
    });
    $('body').on('click', '.c-pagination-item', function (e) {
        e.preventDefault();
        showBy = $(this).data('content');
        currPage = 1;
        sendDataFilter();
        scrollToUp();
    });
    $('body').on('click', '.c-pagination-link', function (e) {
        e.preventDefault();
        if (!$(this).data('content')) return;
        currPage = $(this).data('content');
        sendDataFilter();
        scrollToUp();
    });
    $('body').on('click', '.c-sort', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        if (!($(this).hasClass('active'))) {
            $(this).attr('data-direction', 'DESC');
            sortDir = 'DESC';
        } else {
            $(this).attr('data-direction', 'ASC');
            sortDir = 'ASC';
        }
        currPage = 1;
        sendDataFilter();
    });

    $('body').on('click', '.pagination__text.pagination__text--clear', function (e) {
        e.preventDefault();
        window.location.hash = '';
        window.location.reload();
    })

    sendDataFilter();

    // Лайк объекта
    $('body').on('click', '.c-like', function (e) {
        e.preventDefault();
        var elem = this,
            src_url = elem.dataset.ajax_url;
        $.ajax({
            url: src_url,
            data: {OBJ_ID: elem.dataset.obj_id, ACTION: elem.dataset.action},
            dataType: 'JSON',
            success: function (data) {
                if (data['status']) {
                    $(elem).toggleClass('active');
                    if (elem.dataset.action == 'like') {
                        //меняем action
                        elem.dataset.action = 'dislike';
                    }
                    else {
                        if ($(elem).closest('.c-my-choice').length) {
                            $(elem).closest('.new-building').remove();
                        }
                        else {
                            elem.dataset.action = 'like';
                        }
                    }
                }
            },
            error: function (er) {
                console.log(er)
            },
            complete: function () {

            }
        });
    });
    $('body').on('click', '.c-like', function () {
        var count_elem = $('.header-panel__left-info .choose .choose__count'),
            count = parseInt(count_elem.html());
        if ($(this).hasClass('active')) {
            count_elem.html(count - 1);
        } else {
            count_elem.html(count + 1);
        }
    });


    $('body').on('click', '.c-popup__link', function (e) {
        e.preventDefault();
        popupModule.init($('.c-popup'), $(this));
        if ($(this).data('preloader') == true) {
            $('.c-preloader').show();
            $('body').addClass('preloader_q');
        }

        popupModule.callOnLoadFunction = function () {
            $('body').css('overflow', 'hidden');
            if ($('body').hasClass('preloader_q')) {
                $('.c-preloader').hide();
            }
        };

        popupModule.callOnCloseFunction = function () {
            $('body').removeAttr('style');
        };
    });

    $('body').on('click', '.c-send-emailNewBuildingOfferPrice', function (e) {
        $.validate({
            validateOnBlur: false,
            errorMessagePosition: 'top',
            scrollToTopOnError: false,
            language: validatorLanguage,
            borderColorOnError: '#e41919',
            onSuccess: function ($form) {
                // send form by ajax
                if ($form.hasClass('c-ajaxForm')) {
                    sendFormbyAjax($form);

                    return false;
                }

            }
        });
    });

    $('body').on('keypress', '.js-only-number', function (e) {
        var char = getChar(e);

        if (e.ctrlKey || e.altKey || e.metaKey || char == null || char < '0' || char > '9') {
            e.preventDefault();
            e.stopPropagation();
            return;
        }
    });


    // validation
    var validatorLanguage = {
        errorTitle: 'Ошибка отправки формы!',
        requiredFields: 'Необходимо заполнить обязательные поля',
        badTime: 'Введите корректное время',
        badEmail: 'Введите корректный e-mail адрес',
        badTelephone: 'Введите корректный номер телефона',
        badSecurityAnswer: 'Ответ на секретный вопрос не верен',
        badDate: 'Введите корректную дату',
        lengthBadStart: 'Значение должно быть между ',
        lengthBadEnd: ' знаками',
        lengthTooLongStart: 'Значение поля больше, чем ',
        lengthTooShortStart: 'Значение поля меньше, чем ',
        notConfirmed: 'Input values could not be confirmed',
        badDomain: 'Введите корректное доменное имя',
        badUrl: 'Значение поля не является корректным url-адресом',
        badCustomVal: 'Зачение поля не верно',
        andSpaces: ' и пробелов ',
        badInt: 'Поле может содержать только цифры',
        badSecurityNumber: 'Your social security number was incorrect',
        badUKVatAnswer: 'Incorrect UK VAT Number',
        badStrength: 'The password isn\'t strong enough',
        badNumberOfSelectedOptionsStart: 'You have to choose at least ',
        badNumberOfSelectedOptionsEnd: ' answers',
        badAlphaNumeric: 'The input value can only contain alphanumeric characters ',
        badAlphaNumericExtra: ' и ',
        wrongFileSize: 'Максимальный размер файла %s',
        wrongFileType: 'Разрешениы следующие форматы файлов: %s',
        groupCheckedRangeStart: 'Выберите между ',
        groupCheckedTooFewStart: 'Выберите не меньше, чем ',
        groupCheckedTooManyStart: 'Выберите не больше, чем ',
        groupCheckedEnd: ' item(s)',
        badCreditCard: 'The credit card number is not correct',
        badCVV: 'The CVV number was not correct',
        wrongFileDim: 'Incorrect image dimensions,',
        imageTooTall: 'the image can not be taller than',
        imageTooWide: 'the image can not be wider than',
        imageTooSmall: 'the image was too small',
        min: 'мин.',
        max: 'макс.',
        imageRatioNotAccepted: 'Image ratio is not accepted'
    };
    $.validate({
        validateOnBlur: false,
        errorMessagePosition: 'top',
        scrollToTopOnError: false,
        language: validatorLanguage,
        borderColorOnError: '#e41919',
        onSuccess: function ($form) {
            // send form by ajax
            if ($form.hasClass('c-ajaxForm')) {

                sendFormbyAjax($form);

                return false;
            }

        }
    });
});

function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
    }

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // остальные
    }

    return null; // специальная клавиша
}

function showMarkMap() {
    var objectManager = new ymaps.ObjectManager({
        clusterize: true
    });

    jQuery.getJSON('/ajax/data.json', function (json) {
        objectManager.add(json);
        myMap.setBounds(objectManager.getBounds());
    });

    myMap.geoObjects.add(objectManager);
}

function progressUpdateVideo() {
    setInterval(function () {
        $('.c-video').bind('timeupdate', function () {
            var currentTime = this.currentTime;
            var positionBar = $(this).parent('.c-video-item').find('.positionBar');
            var durationBar = $(this).parent('.c-video-item').find('.durationBar');
            var duration = this.duration;
            $(positionBar).css({width: (currentTime / duration * 100) + "%"});
            $(durationBar).find('.c-progress-percent').html(Math.ceil(currentTime / duration * 100) + "%")
        });
    }, 1000);
}

function sendFormbyAjax(form) {
    var url = form.attr('action'),
        messblock = form.find('.c-ajax-form-mess');
    messblock.hide();
    messblock.removeClass('ajax-form-mess--error');
    messblock.html('');
    showPreloader();
    var emailSubscriber = $(form).find('input[name=email]').val();
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: getFormValues(form),
        success: function (json) {
            messblock.html(json.mess);
            if (json.status == 'error') {
                messblock.addClass('ajax-form-mess--error');
            } else {
                (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function () {
                    rrApi.setEmail(emailSubscriber);
                });

            }

            messblock.show();

        },
        error: function (er) {
            console.log(er);
            messblock.html('Ошибка передачи формы');
            messblock.addClass('ajax-form-mess--error');
            messblock.show();
        },
        complete: function () {
            hidePreloader();
        }
    });
}
function showPreloader() {
    $('.c-preloader').addClass('active').show();
}
function hidePreloader() {
    $('.c-preloader').removeClass('active').hide();
    if ($('.c-aside-popup').length && !$('.select2-container').hasClass('select2-dropdown-open')) {
        /*$('.c-elit-catalog').on('mouseover', function(){
         filterTimer = setTimeout(function(){  $('.sprite-icon.close-elit').trigger('click');   }, 2000);
         $('.c-elit-catalog').off('mouseover');
         });
         $('#aside-filter').on('mouseover', function(){
         clearTimeout(filterTimer);
         });*/
    }
}
function getFormValues(form) {
    var form_arr = {};
    form_arr.text = [];
    form_arr.select = [];
    form_arr.radio = [];
    form_arr.checkbox = [];
    form_arr.hidden = [];

    $.each(form[0], function () {
        var tag = this.tagName.toLowerCase(),
            type = this.type,
            $this = $(this);

        if (type == undefined) {
            type = tag;
        }
        if (type == 'text' || type == 'email' || type == 'date' || type == 'number' || type == 'tel' || type == 'textarea') {
            if ($this.hasClass('select2-focusser') || $(this).hasClass('select2-input')) {
            } else {
                form_arr.text.push({
                    "name": $this.attr('name'),
                    "value": $this.val()
                });
            }
        } else if (type == 'select-one') {
            form_arr.select.push({
                "name": $this.attr('name'),
                "value": $this.val()
            });
        } else if (type == 'checkbox') {
            if ($this.prop("checked") == true) {
                var a1 = $this.attr('name');
                form_arr.checkbox.push({
                    "name": $this.attr('name'),
                    "value": $this.val()
                });
            }
        } else if (type == "radio") {
            if ($this.prop("checked") == true) {
                form_arr.radio.push({
                    "name": $this.attr('name'),
                    "value": $this.val()
                });
            }
        } else if (type == "hidden") {
            form_arr.hidden.push({
                "name": $this.attr('name'),
                "value": $this.val()
            });
        }
    });

    return form_arr;
}


function playMainVideo() {
    $('.c-video-item').mouseover(function () {
        var Player = $(this).find('.c-video')[0];
        var interval;
        var hideImg = $(this).find('.c-hide-img');
        hideImg.hide();
        Player.play();
        interval = setInterval(function () {
            if (Player.ended == true) {
                hideImg.show();
                clearInterval(interval);
                $(this).find('.c-play-video').removeClass('paused');
            }
        }, 2000);
        $(this).find('.c-play-video').addClass('paused');
    });
    $('.c-video-item').mouseout(function () {
        var Player = $(this).find('.c-video')[0];
        $(this).find('.c-hide-img').show();
        Player.pause();
        $(this).find('.c-play-video').removeClass('paused');
    });
}

function openPopup() {
    $('body').on('click', '.c-popup-open', function () {
        $(this).siblings('.c-popup-menu').addClass('open');
        $('body, html').addClass('no-scroll');
    });
}


function closePopup() {
    $('body').on('click', '.c-popup-close-menu', function () {
        $(this).closest('.c-popup-menu').removeClass('open');
        $('body, html').removeClass('no-scroll');
    });
}

function scrollHeader() {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 65) {
            $('.c-top-panel').addClass('fixed');
            $('.c-top-panel-alternate').show();
        } else if ($(window).scrollTop() < 65) {
            $('.c-top-panel').removeClass('fixed');
            $('.c-top-panel-alternate').hide();
        }
    });
}

function scrollToTop() {
    $('.c-arrow-top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 1000);
    });
}

function scrollToList() {
    $('.c-sea-list').click(function () {
        $('html, body').animate({
            scrollTop: $('.tile').offset().top - 70
        }, 1000);
    });
}

function createRangeFilter() {
    var priceTo = $("#priceTo").data('value');
    var priceFrom = $("#priceFrom").data('value');
    $("#price").slider({
        orientation: "horizontal",
        range: true,
        values: [$("#priceFrom").val(), $("#priceTo").val()],
        max: priceTo,
        min: priceFrom,
        slide: function (event, ui) {
            $("#priceFrom").val(ui.values[0]);
            $("#priceTo").val(ui.values[1]);
            $(ui.handle).text(ui.value);
        },
        stop: function (event, ui) {
            $("#priceFrom").trigger('change');
            $("#priceTo").trigger('change');
            sendDataFilter();
            $(ui.handle).text(ui.value);
        },
        create: function (event, ui) {
            var $target = $(event.target);
            $target.find('.ui-slider-handle').eq(0).text($("#priceFrom").val())
            $target.find('.ui-slider-handle').eq(1).text($("#priceTo").val())
        },
    });
    $("#priceFrom").val($("#price").slider("values", 0));
    $("#priceTo").val($("#price").slider("values", 1));
}

function changeDropdown() {
    var linkHeight = 0;
    var linkFilter = '';
    var textDropdown = '';
    $('body').on('click', '.c-dropdown-link', function (e) {
        e.preventDefault();
        linkFilter = $(this).siblings('.c-dropdown-content');
        $(linkFilter).toggleClass('open');
        $(this).toggleClass('open-link');
        // $(this).outerWidth(linkFilter.find('.filter__field--dropdown').width());
    });

    $('body').on('change', '.c-custom-checkbox', function () {
        textDropdown = $(this).siblings('.c-custom-label').text();
        linkDropdown = $(this).closest('.c-dropdown-content').siblings('.c-dropdown-link').find('.c-input-filter-name');
        $(linkDropdown).html(textDropdown);
    });
}
function initMapFilter() {
    myMap = new ymaps.Map("filter-map", {
        center: [55.734550, 37.571796],
        zoom: 9,
        controls: ['zoomControl']
    });
    myMap.behaviors.disable('scrollZoom');

    $('.c-show-on-map').click(toggle);
}

function toggle(e) {
    e.preventDefault();
    if (bigMap) {
        $('.c-filter-map').removeClass('open-map');
        $('.c-detected-map').val('close');
        myMap && myMap.container.fitToViewport();
        myMap && myMap.geoObjects.removeAll();
    } else {
        $('.c-filter-map').addClass('open-map');
        $('.c-detected-map').val('open');
        myMap && myMap.container.fitToViewport();
        updateMarkersMap();
    }
    bigMap = !bigMap;
}

function updateMarkersMap() {
    sendDataFilter(true).then(function (data) {
        ymaps.ready(function () {
            initMapMarkers(data.items);
        });
    });
}

var isFirstApiLoad = true;
function generateFilterResult(data) {
    var $container = $('.js-filter-content');
    $container.empty();

    $('.js-count-filter').text(data.itemsCount || 0)
    /*Генерация списка элементов*/
    data.items.forEach(function (item) {
        var $mainItemContainer = $('<a></a>').attr('href', item.url).addClass('tile__item');

        var $content = $('<div class="tile__item-content"></div>');

        var $imgDiv = $('<div></div>').addClass('tile__item-img');
        if (item.image) {
            $imgDiv.css({
                'background-image': 'url(' + item.image + ')', 'background-size': 'cover'
            });
        }

        var $infoContent = $('<div class="tile__info-content"></div>');
        var $info = $('<div class="tile__item-info"></div>').append($infoContent);

        var $infoName = $('<div class="tile__info-name"></div>').text(item.name);
        var $infoAddress = $('<div class="tile__info-address"></div>').text(item.direction);
        var $infoParams = $('<div class="tile__info-params"></div>');
        item.propsList.forEach(function (param) {
            $infoParams.append($('<div class="tile__info-param">' + param + '</div>'))
        });

        // Формирование блока с ценой
        var $infoPrice = $('<div class="tile__info-price"></div>');
        Object.keys(item.price).forEach(function (key, index) {
            var mapClasses = ['tile__info-price-one', 'tile__info-price-two', 'tile__info-price-three'];
            if (item.sectionCode == 'poselki') {
                if (item.price[key] != 'Цена по запросу' && item.price[key] != '') {
                    var $price = 'от ' + item.price[key];
                } else {
                    var $price = item.price[key];
                }
            } else {
                var $price = item.price[key];
            }
            var $item = $('<div>' + $price + '</div>').addClass(mapClasses[index]);
            $infoPrice.append($item);
            if (index === 0 && (item.priceUpdate == 'up' || item.priceUpdate == 'down') && item.price[key] != 'Цена по запросу') {
                var $classColor = '';
                if (item.priceUpdate == 'up') {
                    $classColor = 'up-red';
                }
                $item.append(' <i class="fa fa-arrow-' + item.priceUpdate + ' ' + $classColor + '" aria-hidden="true"></i>');
            }
        });

        var $infoId = $('<div class="tile__info-id"></div>').text('ID объекта: ' + item.crm_id);

        $infoContent.append($infoName).append($infoAddress).append($infoParams).append($infoPrice).append($infoId);

        if (item.likeBlock.action == 'like') {
            var dataActionClass = ''
        } else {
            var dataActionClass = 'active'
        }

        var $like = $('<div class="' + dataActionClass + ' tile__special-mark c-like" data-obj_id="' + item.objId + '" data-action="' + item.likeBlock.action + '" data-ajax_url="/local/templates/.default/ajax/like.php"> <span class="tile__icon icon-heart"></span></div>');
        var $new = $('<div class="tile__special-mark tile__special-mark--new-offer"><span class="tile__special-text">Новое предложение</span> </div>');

        $content.append($imgDiv).append($info);

        if (item.new) {
            $content.append($new);
        }

        $content.append($like);


        $mainItemContainer.append($content);
        $container.append($mainItemContainer);
        $container.append('<div class="tile__item-clear"></div>');
    });

    /*Если ничего не пришло*/
    if (!data.items.length) {
        $container.append($('<p style="text-align: center; color: #fff; font-size: 24px; text-transform: uppercase">' +
            'Ничего не найдено по вашему запросу</p>'));
    }

    /*Выставляем тип сортировки*/
    if (data.sortBy === 'DESC') {
        $('.js-sort-type').text('дороже → дешевле');
    } else if (data.sortBy === 'ASC') {
        $('.js-sort-type').text('дешевле → дороже');
    }

    /*Выставляем количество показываемых элементов*/
    $('.c-pagination-item').removeClass('active').filter('[data-content=' + data.showBy + ']').addClass('active');

    /*Генирируем пагинацию*/
    $('.pagination__pages').empty();
    data.pager.pages.forEach(function (page) {
        if (page !== 'separator') {
            var $p = $('<a href="#" data-content="' + page + '" class="pagination__item c-pagination-link">' + page + '</a>');
            $('.pagination__pages').append($p);
            if (data.pager.active == page) {
                $p.addClass('active');
            }
        } else {
            $('.pagination__pages')
                .append($('<a href="#" data-content class="pagination__item c-pagination-link other">...</a>'));
        }
    });

    var dataForm = $('.c-filter-form').serializeArray();
    dataForm.push({name: 'currPage', value: currPage});
    dataForm.push({name: 'showBy', value: showBy});
    dataForm.push({name: 'sortDir', value: sortDir});

    if (!isFirstApiLoad) {
        SaveToHash(dataForm);
    }

    isFirstApiLoad = false;


    if ($('.c-detected-map').val() === 'open') {
        updateMarkersMap();
    }
}

function initMapMarkers(items) {
    myMap.geoObjects.removeAll();

    var icon = [
        {
            // href: window.basePath + '/images/cluster_circle.png',
            size: [36, 36],
            offset: [-18, -18],
            border: [3]
        }]

    var layout = ymaps.templateLayoutFactory
        .createClass('<div style="color: #000000; font-family: Circe; font-size: 15px; border: 3px solid #3a3d46; border-radius: 100%; width: 36px; height: 36px">{{ properties.geoObjects.length }}</div>');

    var clusterer = new ymaps.Clusterer({
        clusterIconContentLayout: layout,
        clusterIcons: icon,
        gridSize: Math.pow(2, 8)
    });

    var balloon = ymaps.templateLayoutFactory.createClass(
            '<link rel="stylesheet" href="http://' + window.location.host + '/' + window.basePath + '/css/yandex.css">' +
            '<div class="b-container" style="width: 400px; height: 180px;">' +
            '{% if properties.image %}<img src="{{properties.image}}" alt="">{% endif %}' +
            '<div class="b-inner">' +
            '<a href="http://' + window.location.host + '{{properties.url}}" target="_blank">{{properties.name}}</a>' +
            '<div>Цена: {{properties.price.usd || properties.price.eur || properties.price.rub}}</div>' +
            '{% for item in properties.propsList %} <div>{{ item }}</div>{% endfor %}' +
            '<div>{{properties.direction}}</div>' +
            '<div>ID объекта: {{properties.id}}</div>' +
            '</div>' +
            '</div>'
        )
    ;

    items.forEach(function (item) {
        if (item.yandex_map) {
            var coords = item.yandex_map.split(', ').map(function (coords) {
                return parseFloat(coords);
            });
            if (!coords[0] || !coords[1]) return;

            // item.image = item.image || 'http://www.metrium.ru.opt-images.1c-bitrix-cdn.ru/upload/medialibrary/77d/prodazha_zagorodnoj_nedvizhimosti_v_podmoskovie_jelitnaja_zagorodnaja_nedvizhimost_podmoskovie.jpg?1410290749119803';
            item.clusterCaption = item.name;
            item.balloonContentHeader = '<div></div>';
            item.balloonContentBody = '<link rel="stylesheet" href="http://' + window.location.host + '/' + window.basePath + '/css/yandex.css">' +
                '<div class="b-container" style="width: 340px; height: 180px;">' +
                (item.image ? '<img src="' + item.image + '" alt="">' : '') +
                '<div class="b-inner">' +
                '<a href="http://' + window.location.host + '{{properties.url}}" target="_blank">' + item.name + '</a>' +
                '<div>Цена: ' + (item.price.usd || item.price.eur || item.price.rub) + '</div>' +
                (function () {
                    return item.propsList.map(function (i) {
                        return '<div>' + i + '</div>'
                    }).join('');
                })() +
                '<div>' + item.direction + '</div>' +
                '<div>ID объекта: ' + item.id + '</div>' +
                '</div>' +
                '</div>';

            myPlacemark = new ymaps.Placemark(coords, item, {
                iconLayout: 'default#image',
                iconImageHref: window.basePath + '/images/pin.png',
                iconImageSize: [24, 34],
                iconImageOffset: [-12, -34],
                balloonContentLayout: balloon
            });
            clusterer.add(myPlacemark);
        }
    })

    myMap.geoObjects.add(clusterer);
    myMap.setBounds(myMap.geoObjects.getBounds());
}

function changeName(name) {
    var Names;
    (function (Names) {
        Names[Names["state-map"] = 'map'] = "state-map";
        Names[Names["houseAreaFrom"] = 'house_from'] = "houseAreaFrom";
        Names[Names["houseAreaTo"] = 'house_to'] = "houseAreaTo";
        Names[Names["remotnessMKADFrom"] = 'remotness_from'] = "remotnessMKADFrom";
        Names[Names["remotnessMKADTo"] = 'remotness_to'] = "remotnessMKADTo";
        Names[Names["groundAreaFrom"] = 'area_from'] = "groundAreaFrom";
        Names[Names["groundAreaTo"] = 'area_to'] = "groundAreaTo";
        Names[Names["facing"] = 'facing'] = "facing";
        Names[Names["priceFrom"] = 'price_from'] = "priceFrom";
        Names[Names["priceTo"] = 'price_to'] = "priceTo";
        Names[Names["currPage"] = 'page'] = "currPage";
        Names[Names["showBy"] = 'show_by'] = "showBy";
        Names[Names["sortDir"] = 'sort'] = "sortDir";
    })(Names || (Names = {}));

    return Names[name] || name;
}

function SaveToHash(data) {
    var result = data.map(function (item) {
        if (typeof item.value === 'string') {
            item.value = $.trim(item.value).replace('+', encodeURIComponent('+')).replace(' ', '_');
        }
        if (item.value) {
            return changeName(item.name) + ':' + item.value;
        } else return null;
    }).filter(function (s) {
        return !!s;
    }).join('&');
    if (result)
        window.location.hash = result;
}

function loadFromHash() {
    var hash = window.location.hash.substr(1);
    if (hash) {
        var result = hash.split('&').map(function (item) {
            var splited = item.split(':');
            var key = changeName(splited[0]);
            var value = decodeURIComponent(splited[1]).replaceAll('_', ' ');
            if (key === 'currPage') {
                currPage = parseInt(value);
            } else if (key === 'showBy') {
                showBy = parseInt(value);
            } else if (key === 'sortDir') {
                sortDir = value;
            } else {
                if (key === 'state-map' && value === 'open') {
                    setTimeout(function () {
                        toggle(jQuery.Event('click'));
                    }, 0);
                }
                var type = $('[name="' + key + '"]').attr('type');
                if (type == 'checkbox') {
                    var $c = $('[name="' + key + '"]').filter('[value="' + value + '"]');
                    $c.prop('checked', true);

                    textDropdown = $c.siblings('.c-custom-label').text();
                    linkDropdown = $c.closest('.c-dropdown-content').siblings('.c-dropdown-link').find('.c-input-filter-name');
                    $(linkDropdown).html(textDropdown);
                } else {
                    $('[name="' + key + '"]').val(value);
                }
            }
        });
    }

    // $('[name="direction[]"]').val(['Алтуфьевское', 'Волоколамское']);
}

function scrollToUp() {
    $('html, body').animate({
        scrollTop: $('.js-filter-content').offset().top - 200
    }, 300);
}

var preloaderCounter = 0;
function sendDataFilter(mapRequest) {
    $('.c-preloader').show();
    preloaderCounter++;
    var data = $('.c-filter-form').attr('action') || window.location.pathname + '?AJAX=Y';
    console.log(data);
    data += '&' + $('.c-filter-form').serialize();
    data = data + '&currPage=' + (mapRequest ? 1 : currPage) + '&showBy=' + (mapRequest ? 5000 : showBy) + '&sortDir=' + sortDir;
    console.log(data);
    return $.ajax({
        url: data,
        type: 'POST',
        dataType: 'JSON'
    }).then(function (data) {
        if (!mapRequest) {
            generateFilterResult(data);
        }
        return data;
    }).always(function () {
        preloaderCounter--;
        preloaderCounter === 0 && $('.c-preloader').hide();
    })
}

function sendDataSubscr() {
    $('.c-preloader').show();
    var data = $('.c-subscr-form').serialize();
    $.ajax({
        type: 'GET',
        data: data,
        success: function (data) {
            $('.c-subscr-form').html("<div class='send-data'>Данные успешно отправлены</div>");
        },
        complete: function () {
            $('.c-preloader').hide();
        },
    })
}


// скрипты на детальной странице
$(function () {
    $('body').on('click', '.c-dropdownform__link-custom', function (e) {
        e.preventDefault();
        $('div.elit-detail').css('z-index', '1');
        $('div.arrow-top').css('z-index', '9');
        //$('header.c-top-panel').css('z-index', '10');
        $('.c-dropdownform__link-custom').not($(this)).removeClass('active');
        $('.c-dropdownform__content-custom').slideUp(300);
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.c-dropdownform__content-custom').slideUp(300);
            $('header.c-top-panel').css('z-index', '10');
        } else {
            $(this).addClass('active');
            $(this).next('.c-dropdownform__content-custom').slideDown(300);
            $('header.c-top-panel').css('z-index', '1');
        }
    });
    $('body').on('click', '.c-dropdownform__content-custom .c-aside-close', function (e) {
        e.preventDefault();
        $(this).parent('.c-dropdownform__content-custom').slideUp(300);
        $(this).parent('.c-dropdownform__content-custom').siblings('.c-dropdownform__link-custom').removeClass('active');
        $('div.elit-detail').css('z-index', '1');
        $('div.arrow-top').css('z-index', '9');
        $('header.c-top-panel').css('z-index', '10');
    });
});


// скрипты для детальной мобильной
$(function () {
    function sliderIn(slider_el) {
        if (slider_el != undefined) {
            var slider = slider_el.Swipe({
                startSlide: 0,
                continuous: false,
                transitionEnd: function (index, element) {
                    $('.c-fullscreen').attr('data-slide', index);
                }
            }).data('Swipe');
        }
    }

    function sliderInit(slider_el) {
        if (slider_el != undefined) {
            setTimeout(function () {
                var slider = slider_el.Swipe({
                    startSlide: 0,
                    continuous: false,
                    transitionEnd: function (index, element) {
                        $('.c-fullscreen').attr('data-slide', index);
                    }
                }).data('Swipe');
            }, 1000);
        }
    }

    var slider_el = $('.swipe'),
        slider = [],
        next_buttons = $('.c-slide_next'),
        prev_buttons = $('.c-slide_prev');
    slider_el.each(function (i) {
        slider[i] = $(this).Swipe({
            startSlide: 0,
            continuous: false,
            transitionEnd: function (index, element) {
                $('.c-fullscreen').attr('data-slide', index);
            }
        }).data('Swipe');

        $(next_buttons[i]).on('click', function () {
            slider[i].next();
        });
        $(prev_buttons[i]).on('click', function () {
            slider[i].prev();
        });
    });
    var dataSlider = $('#date-slider');
    if (dataSlider.length) {
        var buttons = $('.c-date-buttons'),
            ajax_url = buttons.data('ajax-url');

        body.on('click', '.c-change-month, .c-change-year', function (e) {
            e.preventDefault();
            var elem = $(e.target);

            if (!elem.hasClass('disable')) {
                elem.addClass('active');
                elem.siblings().removeClass('active');

                var _year = buttons.find('.c-change-year.active'),
                    _month = buttons.find('.c-change-month.active');

                showPreloader();

                $.ajax({
                    url: ajax_url,
                    data: {year: _year.data('year'), month: _month.data('month')},
                    dataType: "JSON",
                    success: function (data) {
                        var template = '',
                            months = buttons.find('.c-change-month');
                        slider.kill();

                        if (data.images !== undefined) {
                            $.each(data.images, function () {
                                var links = $(this);
                                template += '<div><img src="' + links[0].thumb + '" alt=""></div>';
                            });

                            dataSlider.find('.swipe-wrap').html(template);

                        }

                        if (elem.hasClass('c-change-year')) {
                            if (data.dis_months !== undefined) {
                                months.removeClass('disable');
                                for (i = 0; i < data.dis_months.length; i++) {
                                    months.eq(data.dis_months[i]).addClass('disable');
                                }
                            }
                            months.removeClass('active');
                            months.eq(data.active_month).addClass('active');
                        }
                    },
                    error: function (er) {
                        console.log(er)
                    },
                    complete: function () {
                        hidePreloader();
                        sliderIn(slider_el);
                    }
                });
            }
        });
    }
    var rangeSlider = $('.c-slider_range');
    if (rangeSlider.length) {
        $.each(rangeSlider, function (i) {
            var elem = $(rangeSlider[i]),
                slider = elem.find(".c-slider_range__input"),
                slider_container = slider.parents('.c-slider_range'),
                elem_from = slider_container.find('.c-slider_range__from'),
                elem_to = slider_container.find('.c-slider_range__to');
            slider.ionRangeSlider({
                type: slider.data('slider_type') || "double",
                min: slider.data('slider_min'),
                max: slider.data('slider_max'),
                from: slider.data('slider_from'),
                to: slider.data('slider_to'),
                step: slider.data('slider_step'),
                hide_from_to: true,
                onStart: function () {
                    elem_from.val(slider.data("slider_from"));
                    elem_to.val(slider.data("slider_to"));
                }
            });

            slider.on("change", function () {
                var $this = $(this),
                    from = $this.data("from"),
                    to = $this.data("to");
                elem_from.val(from);
                elem_to.val(to);
                if ($('#summ').length) {
                    getCurrPercent($('.c-first-summ'), $('.c-summ-describe'));
                }
            });

            slider_container.find('.c-slider_range__value').focusout(function () {
                var item = slider.data("ionRangeSlider");
                item.update({
                    from: elem_from.val(),
                    to: elem_to.val()
                });
                elem_to.val(item.old_to);
                elem_from.val(item.old_from);
            });
        });
    }

    setTimeout(function () {
        var elem = $('.swipe_popup'),
            contain = $('.page-content')
        contain.animate({opacity: 1}, 300);
        var slider = elem.Swipe({
            startSlide: $('.c-fullscreen').attr('data-slide'),
            continuous: false
        }).data('Swipe');

        $('.c-slide_next_p').on('click', function () {
            slider.next();
        });

        $('.c-slide_prev_p').on('click', function () {
            slider.prev();
        });
    }, 1000);

    $('.c-dropdownform__link-custom').click(function () {
        $('div.elit-detail').css('z-index', '2');
        $('div.arrow-top').css('z-index', '1');
        $('header.c-top-panel').css('z-index', '1');
        $(this).parent().find('*').filter(':input:visible:first').focus();
    });
});


var popupModule = (function () {
    var module = {},
        bPopup,
        scrollTop = 0;

    module.init = function (elem, link) {

        this.close();

        $('.c-popup .popup__content').empty();

        module.type = link.data('type-popup') || 'ajax';
        module.url = link.data('url-popup') || '/';


        bPopup = elem.bPopup({
            content: module.type,
            closeClass: 'c-popup-close',
            contentContainer: '.popup__content',
            loadUrl: module.url,
            modalColor: '#71737a',
            opacity: 1,
            scrollBar: false,
            follow: [true, false],
            loadCallback: function () {
                scrollTop = $(window).scrollTop();
                $(window).scrollTop(0);
                $('.c-popup-close').show();
                module.callOnLoadFunction();
            },
            onClose: function () {
                $(self.content).empty();
                $('.c-popup-close').hide();
                $(window).scrollTop(scrollTop);
                module.callOnCloseFunction();
                popupModule.resetCallBacksFunction();
            }
        });
    }

    module.callOnLoadFunction = function () {

    };

    module.callOnCloseFunction = function () {

    };

    module.close = function () {
        if (bPopup) {
            bPopup.close();
        }
    }

    module.reposition = function () {
        if (bPopup) {
            bPopup.reposition(100);
        }
    }

    module.resetCallBacksFunction = function () {

        module.callOnLoadFunction = function () {
            console.log('Loaded');
        };

        module.callOnCloseFunction = function () {

        };
    }

    return module;
})();

