$(function () {
    $('body').on('change', '.c-selected-check', function() {
        var selectedElem = '';

        var selectedDrop = $(this).closest('.c-selected-drop');
        if ($(this).is(':checked')) {
            selectedElem = $(this).next().find('.c-selected-text').html();
        } else {
            selectedDrop.find('.c-selected-check').each(function () {
                if ($(this).is(':checked') && !selectedElem.length) {
                    selectedElem = $(this).next().find('.c-selected-text').html();
                    return;

                }
            });

        }

        if (!selectedElem.length) {
            selectedElem = $('.c-selected-check').next().find('.c-selected-text').html();
            return;
        }
        selectedDrop.find('.c-selected-el').html(selectedElem);
    });
    // slick-slider
    $('.js-slick-banner').slick({
        arrows: true,
        dots: false,
        infinite: true,
        slidesToScroll: 1,
        slidesToShow: 1,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 5000
    });
});