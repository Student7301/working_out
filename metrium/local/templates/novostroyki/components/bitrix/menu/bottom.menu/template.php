<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (empty($arResult))
    return;
?>
<div class="navigation-wrap">
    <div class="container">
        <menu class="navigation">
            <?foreach($arResult as $key=>$menuItem):?>
                <a href="<?=$menuItem['LINK']?>" title="" class="navigation__link"><?=$menuItem['TEXT']?></a>
            <?endforeach;?>
        </menu>
    </div>
</div>
