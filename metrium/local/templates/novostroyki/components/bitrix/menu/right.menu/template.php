<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (empty($arResult))
    return;
?>

<div id="aside-menu" class="aside-slide-popup c-aside-popup c-mCustomScrollbar">
    <div class="close-popup c-aside-close"><i class="sprite-icon close-elit"></i></div>
    <div class="popup-menu">
        <?foreach($arResult as $key=>$menuItem):?>
            <a href="<?= ($menuItem['LINK'] == '/news/') ? $menuItem['LINK'] . \metrium\helpers\IBlockHelper::getDataLastNews() : $menuItem['LINK'] ?>" title="" class="popup-menu__item <?if(isset($menuItem['PARAMS']['class'])) echo $menuItem['PARAMS']['class']?>"><?=$menuItem['TEXT']?></a>
            <?if ($menuItem['PARAMS']['hr']):?>
                <hr>
            <?endif;?>
        <?endforeach;?>
        <div class="socials">
            <a href="https://www.facebook.com/metrium/?pnref=lhc"  target="_blank" rel="nofollow" title="" class="socials__item">
                <i class="sprite-icon fb"></i>
            </a>
        </div>
    </div>
</div>