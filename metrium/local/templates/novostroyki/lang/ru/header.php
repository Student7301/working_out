<?
$MESS['FOR_CALLS_FROM_MOSCOW'] = "для звонков из Москвы";
$MESS['FREE_CALL_RUSSIA'] = "бесплатный звонок по России";
$MESS['FULL_ADDRESS'] = "Москва, ул. Беговая, 3, БЦ &quot;Нордстар Тауэр&quot;, этаж 35";
$MESS['SHOW_MAP'] = "На карте";
$MESS['REQUEST_CALL'] = "Заказать звонок";
$MESS['MY_CHOICE'] = "Мой выбор";
$MESS['TOTAL_OFFERS'] = "Всего предложений";
$MESS['PRICE'] = "Цена";
$MESS['FROM'] = "от";
$MESS['TO'] = "до";
$MESS['FIND_BUTTON'] = "Найти";
$MESS['SEARCH_TIP'] = "Ключевые слова или ID";
$MESS['ROOMS_COUNT'] = "Количество комнат";
$MESS['STUDIO'] = "Студия";
$MESS['FOUR_OR_MORE'] = "4 и более";
$MESS['REGION'] = "Регион";
$MESS['MOSCOW'] = "Москва";
$MESS['MOSCOW_REGION'] = "Московская область";
$MESS['NEW_MOSCOW'] = "Новая Москва";
$MESS['TYPE_FINISHING'] = 'Тип отделки';
$MESS['TYPE_FINISHING_WITHOUT'] = 'Без отделки';
?>