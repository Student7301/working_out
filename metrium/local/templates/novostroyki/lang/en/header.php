<?
$MESS['FOR_CALLS_FROM_MOSCOW'] = "for calls from Moscow";
$MESS['FREE_CALL_RUSSIA'] = "free calls within Russia";
$MESS['FULL_ADDRESS'] = "Moscow, street Begovaya, 3, BC &quot;Nordstar Tower&quot;, floor 35";
$MESS['SHOW_MAP'] = "Show map";
$MESS['REQUEST_CALL'] = "Request call";
$MESS['MY_CHOICE'] = "My choice";
$MESS['TOTAL_OFFERS'] = "Total offers";
$MESS['PRICE'] = "Price";
$MESS['FROM'] = "from";
$MESS['TO'] = "to";
$MESS['FIND_BUTTON'] = "Find";
$MESS['SEARCH_TIP'] = "Keyword or ID";
$MESS['ROOMS_COUNT'] = "Rooms count";
$MESS['STUDIO'] = "Studio";
$MESS['FOUR_OR_MORE'] = "4 or more";
$MESS['REGION'] = "Region";
$MESS['MOSCOW'] = "Moscow";
$MESS['MOSCOW_REGION'] = "Moscow region";
$MESS['NEW_MOSCOW'] = "New Moscow";
$MESS['TYPE_FINISHING'] = 'Finish type';
$MESS['TYPE_FINISHING_WITHOUT'] = 'Without finishing';
?>