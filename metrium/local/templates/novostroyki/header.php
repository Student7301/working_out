<?
use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadLanguageFile(__FILE__);?>
<!DOCTYPE html>
<html>
<head>
    <? $url_array = explode('/', $APPLICATION->GetCurPage());
    $nov = $url_array[count($url_array) - 2];
    if($nov == 'v-severnom-butovo') { ?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5NGBK9L');</script>
        <!-- End Google Tag Manager -->
    <?
        }
    ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="1B-rSwspHvZQ14S-q3mGt_lKedE72rDXTfVqWGURpEU" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title><?$APPLICATION->ShowTitle()?></title>
    <?CJSCore::Init();?>
    <?CJSCore::Init(array("jquery"))?>
    <script src="//api-maps.yandex.ru/2.1/?load=package.full&lang=ru_RU"></script>
    <?$APPLICATION->SetAdditionalCSS( "/local/templates/.default/css/all.css")?>
    <?Asset::getInstance()->addJs('/local/templates/.default/js/map-yandex.js')?>
    <?Asset::getInstance()->addJs('/local/templates/.default/js/plugins.js')?>
    <?Asset::getInstance()->addJs('/local/templates/.default/js/jquery.maskedinput.js')?>
    <?Asset::getInstance()->addJs('/local/templates/.default/js/modules.js')?>
    <?Asset::getInstance()->addJs('/local/templates/.default/js/script.js')?>
    <?Asset::getInstance()->addJs('/local/templates/.default/js/autocomplete.min.js')?>
    <?Asset::getInstance()->addJs('/local/templates/.default/js/ofi.browser.js')?>
    <?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.js')?>
    <?$APPLICATION->ShowHead()?>

    <!--[if lte IE 11]>
    <link rel='stylesheet' href='/local/templates/.default/css/ie9.css'>
    <script src='/local/templates/.default/js/html5-dataset.js'></script>
    <script src='/local/templates/.default/js/ie9.js'></script>
    <![endif]-->

    <meta content="IE=edge" http-equiv="X-UA-Compatible">
</head>
<body>
<? if($nov == 'v-severnom-butovo') { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NGBK9L"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?
}
?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<!-- fb share -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- fb share end -->
<!-- BEGIN pagewrap -->
<div class="pagewrap pagewrap--novostroyki">
    <!--background block-->
    <?global $USER;
    if (!($USER->IsAdmin() && $_SERVER['REMOTE_ADDR'] == '109.195.194.85')):?>
        <div class="page-background page-background--light c-bg-elit">
            <img src="/local/templates/.default/images/content/novostroyki-bg.jpg" alt="" class="active">
        </div>
    <?endif;?>
    <div class="content elit fix-head-new">
        <?$APPLICATION->IncludeComponent("bitrix:menu", 'right.menu', Array(
                "ROOT_MENU_TYPE" => "right",
                "MAX_LEVEL" => "1",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => ""
            )
        );?>
        <div class="fixed-header elit--novostroyki">
            <div class="container container--elit">
                <!-- BEGIN header -->
                <header class="header header--elit clearfix">
                    <div class="logo left">
                        <a href="<?=(LANGUAGE_ID == LANG_EN) ? '/en/' : '/'?>" title="" class="absolute-link c-clearSession"></a>
                        <img src="/local/templates/.default/images/logo<?=(LANGUAGE_ID == LANG_EN) ? '_en' : ''?>.svg" alt="">
                    </div>
                    <div class="content-line clearfix">
                        <div class="left left_inf">
                            <?$APPLICATION->ShowViewContent('LIST_INFO')?>
                        </div>
                        <div class="left left_lnk" style="display: none">
                            <a href="/o-kompanii/" class="novostroiki-navigation-header__link">О компании</a>
                            <a href="/o-kompanii/contacts/" class="novostroiki-navigation-header__link">Контакты</a>
                            <a href="/novostroyki/novostroyki-moskvy/" class="novostroiki-navigation-header__link">Новостройки</a><br>
                            <a href="/zagorodnaya-nedvizhimost/" class="novostroiki-navigation-header__link">Загородная недвижимость</a>
                            <a href="/elitnaya-gorodskaya-nedvijimost/" class="novostroiki-navigation-header__link">Городская недвижимость</a>
                        </div>
                        <div data-aside-id="#aside-menu" class="menu menu--elit right c-aside-popup-link">
                            <i class="sprite-icon menu-elit"></i>
                        </div>

                        <div class="search right">
                            <a href="" title="" data-url-popup="/local/templates/.default/ajax/search-on-main.php" class="absolute-link c-popup-link"></a>
                            <i class="sprite-icon search-light"></i>
                        </div>
                        <div class="choose right">
                            <a href="/kabinet/vybor/" title="" class="choose__text"><?=Loc::getMessage('MY_CHOICE')?></a>
                            <div class="choose__icon">
                                <i class="like"></i>
                                <span class="choose__count"><?=count(\metrium\Likes::getLikes())?></span>
                            </div>
                        </div>

                        <div data-map-id="big-map" data-url-popup="/local/templates/.default/ajax/zoom-map-header.php?TYPE=new" data-placemarks-json="/local/templates/.default/ajax/elit-placemarks-jc.php?SECTION=<?=$arResult['SECTION']?>" data-elit="Y" data-has-child="Y" data-custom-search="Y" data-has-balloon="Y" class="elit-map-block c-zoom-map c-dbl-map right">
                            <div><i class="ico"></i></div><?=Loc::getMessage('SHOW_MAP')?>
                        </div>
                        <div class="request-call right">
                            <a href="" onclick="ga('send', 'pageview','/gaclickzvonok'); yaCounter18350230.reachGoal('clickzvonok'); SCBopen(30);return false;" title="" class="absolute-link"></a>
                            <a href="" title="" class="request-call__link"><?=Loc::getMessage('REQUEST_CALL')?></a>
                        </div>
                        <div class="phone right"><span class="phone_moscow" id="phone_moscow"><?=preg_match("/silart|Donskoj-olimp|pokolenie/", $_SERVER['REQUEST_URI']) ? '<a href="tel:84992700027/" id="comagic_moscow">8 (499) 270 00 27</a>' : '<a href="tel:84997552020/" id="comagic_moscow">8 (499) 755-20-20</a>'?></span></div>
                    </div>
                </header>
                <div class="elit-container elit-container--first  elit-container--filter clearfix">
                <!-- правый блок активности -->

                <!-- правый блок активности END-->
                    <?
                    if (strpos($_SERVER['REQUEST_URI'], '/akcii/') !== false) {
                        $urlPath = '/akcii/filtered/';
                    } else {
                        $urlPath = '/novostroyki/filtered/';
                    }
                    $urlPath = (LANGUAGE_ID == LANG_EN) ? "/en" . $urlPath : $urlPath;
                    ?>
                    <div class="page-head page-head--with-sort c-additionFilter">
                        <form action="<?=$urlPath?>" method="get">
                            <div class="filter-bl c-selected-drop">
                                <div class="filter-bl__label c-selectDropdownLink"><span class="c-selected-el"><?=Loc::getMessage('MOSCOW')?></span></div>
                                <div class="filter-bl__dropdown c-selectDropdown">
                                    <div class="column__inner c-regenOption">
                                        <div class="filter-checkbox-list c-check-options n-regen">
                                            <div class="checkbox-wrap">
                                                <input type="checkbox" name="moscow" id="region1" value="Y" class="checkbox checkbox--green c-selected-check" <?if(!empty($_GET['moscow'])):?> checked<?endif;?>>
                                                <label for="region1" data-checked="" class="checkbox-label">
                                                    <span class="checkbox-icon"></span>
                                                    <span class="checkbox-text c-selected-text"><?=Loc::getMessage('MOSCOW')?></span>
                                                </label>
                                            </div>

                                            <div class="checkbox-wrap">
                                                <input type="checkbox" name="region" id="region2" value="Y" class="checkbox checkbox--green c-selected-check"<?if(!empty($_GET['region'])):?> checked<?endif;?>>
                                                <label for="region2" data-checked="" class="checkbox-label">
                                                    <span class="checkbox-icon"></span>
                                                    <span class="checkbox-text c-selected-text"><?=Loc::getMessage('MOSCOW_REGION')?></span>
                                                </label>
                                            </div>

                                            <div class="checkbox-wrap">
                                                <input type="checkbox" name="new_moscow" id="region3" value="Y" class="checkbox checkbox--green c-selected-check"<?if(!empty($_GET['new_moscow'])):?> checked<?endif;?>>
                                                <label for="region3" data-checked="" class="checkbox-label">
                                                    <span class="checkbox-icon"></span>
                                                    <span class="checkbox-text c-selected-text"><?=Loc::getMessage('NEW_MOSCOW')?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-bl c-selected-drop">
                                <div class="filter-bl__label c-selectDropdownLink"><span class="c-selected-el"><?=Loc::getMessage('STUDIO')?></span></div>
                                <div class="filter-bl__dropdown c-selectDropdown">
                                    <div class="column__inner c-regenOption">
                                        <div class="filter-checkbox-list c-check-options n-regen">
                                            <?$rooms = [
                                                0 => Loc::getMessage('STUDIO'),
                                                1 => 1,
                                                2 => 2,
                                                3 => 3,
                                                4 => Loc::getMessage('FOUR_OR_MORE')
                                            ];?>
                                            <?foreach ($rooms as $key => $value) :?>
                                                <div class="checkbox-wrap">
                                                    <input type="checkbox" name="roomsCount[]" id="countroom<?=$key?>" value="<?=$key?>" class="checkbox checkbox--green c-selected-check"<?if(isset($_GET['roomsCount']) && in_array($key, $_GET['roomsCount'])):?> checked<?endif;?>>
                                                    <label for="countroom<?=$key?>" data-checked="" class="checkbox-label">
                                                        <span class="checkbox-icon"></span>
                                                        <span class="checkbox-text c-selected-text"><?=$value?></span>
                                                    </label>
                                                </div>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="filter-bl c-selected-drop">
                                <div class="filter-bl__label c-selectDropdownLink"><span class="c-selected-el"><?=Loc::getMessage('TYPE_FINISHING_WITHOUT')?></span></div>
                                <div class="filter-bl__dropdown c-selectDropdown">
                                    <div class="column__inner c-regenOption">
                                        <div class="filter-checkbox-list c-check-options n-regen">
                                            <?$typeFinishing = \metrium\Search::getFinishingNovostroyki();?>
                                            <?foreach ($typeFinishing as $key => $value) :?>
                                                <div class="checkbox-wrap">
                                                    <input type="checkbox" name="typeFinishing[]" id="finishingtype<?=$key?>" value="<?=$value?>" class="checkbox checkbox--green c-selected-check"<?if(isset($_GET['typeFinishing']) && in_array($value, $_GET['typeFinishing'])):?> checked<?endif;?>>
                                                    <label for="finishingtype<?=$key?>" data-checked="" class="checkbox-label">
                                                        <span class="checkbox-icon"></span>
                                                        <span class="checkbox-text c-selected-text"><?=$value?></span>
                                                    </label>
                                                </div>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <? $price = metrium\Search::getMinAndMaxApPrice();?>
                            <div class="filter-bl filter-bl">
                                <div class="filter-bl__label c-selectDropdownLink"><?=Loc::getMessage('PRICE')?></div>
                                <div class="filter-bl__dropdown wide c-selectDropdown">
                                    <div class="position-block c-regenOption">
                                        <span class="filter-text--price"><?=Loc::getMessage('FROM')?></span>
                                        <input type="text" name="price_from" class="filter-input filter-input--price c-format-price" placeholder="<?=\metrium\Price::getFormattedPrice($price['min_price'])?>" value="<?if(!empty($_GET['price_from'])):?><?=$_GET['price_from']?><?endif;?>">

                                        <span class="filter-text--price"><?=Loc::getMessage('TO')?></span>
                                        <input type="text" name="price_to" class="filter-input filter-input--price c-format-price" placeholder="<?=\metrium\Price::getFormattedPrice($price['max_price'])?>" value="<?if(!empty($_GET['price_to'])):?><?=$_GET['price_to']?><?endif;?>">

                                        <span class="filter-price">₽</span>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-bl filter-bl--big">
                                <input type="text" placeholder="<?=Loc::getMessage('SEARCH_TIP')?>" class="input" name="searchText" value="<?=(!empty($_GET['searchText'])) ? $_GET['searchText'] : ""?>" id="autocomplete" data-ajaxurl="/local/templates/.default/ajax/autocomplete-novostroyki.php" autocomplete="on">
                                <input type="submit" value="<?=Loc::getMessage('FIND_BUTTON')?>" class="btn-search">
                            </div>
                        </form>

                        <? $APPLICATION->IncludeComponent(
                            "metrium:multi.lang.panel",
                            "",
                            Array(
                                "LANG" => (LANGUAGE_ID == LANG_EN) ? "EN" : "RU"
                            )
                        );
                        ?>
                    </div>
                </div>
<!--                <div class="accordion c-accordion" style="margin-top: 15px;display:none">-->
<!--                    <div class="accordion__item">-->
<!--                        <div class="accordion__link accordion__link--detail bottom-shadow bottom-shadow--small c-accordion-link">-->
<!--                            <div class="accordion__link-text">Подробнее</div>-->
<!--                            <div class="accordion-icon"><i class="sprite-icon bottom-arrow"></i></div>-->
<!--                        </div>-->
<!--                        <div class="accordion__dropdown c-accordion-drop" style="text-align: center">-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/gotovye/"><div class="newBuilding-button">Готовые новостройки</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/biznes-klassa/"><div class="newBuilding-button">Новостройки бизнес класса</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/ryadom-s-metro/"><div class="newBuilding-button">Новостройки рядом с метро</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-novaya-moskva/s-otdelkoj/"><div class="newBuilding-button">Новостройки с отделкой</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/sao/"><div class="newBuilding-button">Новостройки в САО</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/szao/"><div class="newBuilding-button">Новостройки в СЗАО</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/svao/"><div class="newBuilding-button">Новостройки в СВАО</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/vao/"><div class="newBuilding-button">Новостройки в ВАО</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/yuzao/"><div class="newBuilding-button">Новостройки в ЮЗАО</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/zao/"><div class="newBuilding-button">Новостройки в ЗАО</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/ramenki/"><div class="newBuilding-button">Новостройки в Раменках</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/perovo/"><div class="newBuilding-button">Новостройки в Перово</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/metro_domodedovskaya/"><div class="newBuilding-button">Новостройки у метро Домодедовское</div></a>-->
<!--                            <a href="http://--><?//=$_SERVER['HTTP_HOST']?><!--/novostroyki/novostroyki-moskvy/novogireevo/"><div class="newBuilding-button">Новостройки по улице Новогереевской</div></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            selectsMainSearch($('.c-custom-select'), $('#autocomplete'), '<?=GetMessage('SELECT_ITEMS')?>');

            // format price
            $('.c-format-price').priceFormat({
                prefix: '',
                centsSeparator: '.',
                thousandsSeparator: ' ',
                centsLimit: 0
            });
        });
    </script>

    <?if(!array_key_exists('ALL', $_GET)):?>
        <?$APPLICATION->ShowViewContent("banners");?>
    <?endif;?>