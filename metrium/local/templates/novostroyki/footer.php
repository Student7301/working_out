<?use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);?>


        <!-- BEGIN fixed_contacts -->
        <div class="fixed_contacts">
            <!--<div class="tile tile-phone">
                <div class="middle-text"></div>
            </div>-->
            <div class="tile tile-email c-dropdownform c-dropdownform-hover no-scroll-action content-drop">
                <div class="middle-text"><i class="sprite-icon email"></i></div>
                <a href="" title="" class="absolute-link c-dropdownform__link"></a>
                <div class="drop-form drop-form--adapt drop-form--top-left contact-form bottom-shadow bottom-shadow--small c-dropdownform__content">
                    <?$APPLICATION->IncludeComponent(
                        "metrium:callback.form",
                        ".default",
                        array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "POST_EVENT_ID" => "3431",
                            "MESSAGE_ID" => "80",
                            "SITE_ID" => "s1",
                            "SUCCESS_MESSAGE" => "Сообщение отправлено!"
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
        <!-- END fixed_contacts -->
<!--LiveInternet counter-->
<script type="text/javascript"><!--
    document.write("<a href='//www.liveinternet.ru/click' "+
        "target=_blank><img src='//counter.yadro.ru/hit?t44.6;r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";"+Math.random()+
        "' alt='' title='LiveInternet' "+
        "border='0' width='31' height='31'><\/a>")
    //--></script>
<!--/LiveInternet-->
    </div><!-- END pagewrap -->

    <!-- popup -->
    <div class="c-popup popup">
        <div class="sprite-icon close c-popup-close"></div>
        <div class="popup__content"></div>
    </div>
    <!-- preloader -->
    <div class="c-preloader preloader"><img src="/local/templates/.default/images/preloader.gif" alt=""></div>

    <!--  AdRiver code START. Type:counter(zeropixel) Site: metrium PZ: 0 BN: 0 -->

    <script type="text/javascript">
        var RndNum4NoCash = Math.round(Math.random() * 1000000000);
        var ar_Tail='unknown'; if (document.referrer) ar_Tail = escape(document.referrer);
        document.write('<img src="' + ('https:' == document.location.protocol ? 'https:' : 'http:') + '//ad.adriver.ru/cgi-bin/rle.cgi?' + 'sid=193229&bt=21&pz=0&rnd=' + RndNum4NoCash + '&tail256=' + ar_Tail + '" border=0 width=1 height=1>')
    </script>


    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 952495884;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <script type="text/javascript" async src="http://smartcallback.ru/api/SmartCallBack.js?t=uLWNsem2hlZO3ZONIiOC" charset="utf-8"></script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter18350230 = new Ya.Metrika({ id:18350230, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/18350230" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!--Google-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-59013509-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!--Google-->

    <script>
        $(function () {
            var ua = navigator.userAgent;
            // IE 10 or older
            var msie = ua.indexOf('MSIE ');
            // IE 11
            var trident = ua.indexOf('Trident/');
            // EDGE
            var edge = ua.indexOf('Edge/');
            if (msie > 0 || trident > 0 || edge > 0) {
                $('.center-fotorama .fotorama').on('fotorama:load', function (e, fotorama) {
                    objectFitImages();
                });
            }
        });

    </script>

    <!--[if IE 9]>
    <script type="text/javascript">
        setPlaceHolders();
    </script>
    <![endif]-->

    <!--Comagic-->
    <script type="text/javascript">
        var __cs = __cs || [];
        __cs.push(["setAccount", "YKFScihFvpgc2RO4FCYf_wrAcEL4wDSt"]);
        __cs.push(["setHost", "//server.comagic.ru/comagic"]);
        __cs.push(["setDynamicalReplacement", true]);
    </script>
    <script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
    <!--END Comagic-->
	
	<!--трекинг код -->
		<script>
			var rrPartnerId = "585932ae65bf1910d0616f6e";
			var rrApi = {};
			var rrApiOnReady = rrApiOnReady || [];
			rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
				rrApi.recomMouseDown = rrApi.recomAddToCart = function () {
				};
			(function (d) {
				var ref = d.getElementsByTagName('script')[0];
				var apiJs, apiJsId = 'rrApi-jssdk';
				if (d.getElementById(apiJsId)) return;
				apiJs = d.createElement('script');
				apiJs.id = apiJsId;
				apiJs.async = true;
				apiJs.src = "//cdn.retailrocket.ru/content/javascript/tracking.js";
				ref.parentNode.insertBefore(apiJs, ref);
			}(document));
		</script>
	<!--END трекинг код -->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '823081721188267', {
            em: 'insert_email_variable,'
        });
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1066320783433419&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    </body>
</html>