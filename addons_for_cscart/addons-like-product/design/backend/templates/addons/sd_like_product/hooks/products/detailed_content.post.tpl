{include file="common/subheader.tpl" title=__("product_likes") target="#acc_product_likes"}
<div id="acc_product_likes" class="collapse in">
    <div class="control-group">
        <label class="control-label" for="product_likes">{__("likes")}:</label>
        <div class="controls">
            <div class="text-type-value">{$product_data.likes|default:"0"}</div>
        </div>
    </div>
</div>