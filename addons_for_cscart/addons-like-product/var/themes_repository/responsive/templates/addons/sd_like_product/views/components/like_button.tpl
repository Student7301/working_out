{if !$product_id && $product.product_id}
    {assign var="product_id" value=$product.product_id}
{/if}

{if !$likes}
    {assign var="likes" value=$product.likes|default:0}
{/if}

{if !$user_id}
    {assign var="user_id" value=$auth.user_id}
{/if}

{if !$postfix}
    {assign var="postfix" value=1|rand:200}
{/if}

{assign var="likes" value=$product_id|fn_sd_like_product_get}
{assign var="liked" value=$product_id|fn_sd_like_product_check_user:$user_id}

<div {if $class} class="{$class}" {/if} id="sd_div_like_{$product_id}_{$postfix}">

    {assign var="session_like" value=$product_id|fn_sd_like_product_check_session_user:$sessions_like}

    {if !empty($auth.user_id) || !empty($likes) || $addons.sd_like_product.sd_count_likes_unregistered_users == 'Y'}
        {if $likes < 2}
            {assign var="like_lang_var" value=__("like")}
        {else}
            {assign var="like_lang_var" value=__("count_likes")}
        {/if}

        {if !empty($liked)}
           {if $addons.sd_like_product.sd_count_likes_unregistered_users == 'N' && empty($user_id)}
                {assign var="sd_link" value=""}
                {assign var="sd_no_link" value=""}
                {assign var="but_name" value=""}
            {else}
                {assign var="sd_link" value="$.fn.fn_sd_unlike(`$product_id`, `$likes`, `$user_id`, `$postfix`);"}
                {assign var="sd_no_link" value=""}
                {assign var="but_name" value="dispatch[like.add]"}
            {/if}
        {elseif !$user_id}
            {if $addons.sd_like_product.sd_count_likes_unregistered_users == 'N'}
                {assign var="sd_link" value=""}
                {assign var="sd_no_link" value=""}
            {else}
                {assign var="sd_link" value="$.fn.fn_sd_add_like(`$product_id`, `$likes`, `$user_id`, `$postfix`);"}
                {assign var="sd_no_link" value="ty-btn__secondary"}
            {/if}
        {elseif $user_id}
            {assign var="sd_link" value="$.fn.fn_sd_add_like(`$product_id`, `$likes`, `$user_id`, `$postfix`);"}
            {assign var="sd_no_link" value="ty-btn__secondary"}
            {assign var="but_name" value="dispatch[like.add]"}
        {/if}

        {assign var="but_text" value="`$like_lang_var`{if $likes}: `$likes`{/if}"}

        {include file="buttons/button.tpl"
            but_name="`$but_name`"
            but_meta="`$sd_no_link` ty-btn__big like"
            but_text="`$but_text`"
            but_role="act"
            but_onclick="`$sd_link`"
            but_icon="ty-icon-heart"
        }
    {/if}
<!--sd_div_like_{$product_id}_{$postfix}--></div>