<script type="text/javascript">
    (function(_, $) {
        function fn_sd_like_ajax_request(product_id, likes, user_id, action, postfix)
        {
            if (product_id) {
                $.ceAjax('request', fn_url("like." + action), {
                    method: 'post',
                    data: {
                        'product_id': product_id,
                        'likes': likes,
                        'user_id': user_id,
                        'postfix': postfix,
                        'result_ids': 'sd_div_like_' + product_id + '_' + postfix
                    },
                    callback: function (data, params) {
                        if (typeof data.html != "undefined") {
                            var buttonContent = data.html[Object.keys(data.html)[0]];
                            $("[id^=sd_div_like_" + product_id + "]").html(buttonContent);
                        }
                    }
                });
            }
        }

        $.fn.fn_sd_add_like = function(product_id, likes, user_id, postfix) {
            fn_sd_like_ajax_request(product_id, likes, user_id, 'add', postfix);
        }
        $.fn.fn_sd_unlike = function(product_id, likes, user_id, postfix) {
            fn_sd_like_ajax_request(product_id, likes, user_id, 'unlike', postfix);
        }
    }(Tygh, Tygh.$));
</script>