{if $auth.user_id}
    <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a" href="{"products.my_likes"|fn_url}" rel="nofollow">{__("my_likes")}{if $likes_count > 0} ({$likes_count}){/if}</a></li>
{/if}