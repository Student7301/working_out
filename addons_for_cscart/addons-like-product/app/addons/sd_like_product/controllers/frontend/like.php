<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$params = $_REQUEST;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'add') {

        if (defined('AJAX_REQUEST') && !empty($params['product_id'])) {

            $result = fn_sd_like_product_add($params['product_id'], $auth['user_id'], Registry::get('runtime.company_id'));

            if ($result == false) {
                fn_set_notification('N', __('notice'), __('sd_text_like'));
            }

            Tygh::$app['session']['sd_product_like'][$params['product_id']] = true;
            Tygh::$app['view']->assign('user_id', $params['user_id']);
            Tygh::$app['view']->assign('likes', $params['likes'] + 1);
            Tygh::$app['view']->assign('postfix', $params['postfix']);
            Tygh::$app['view']->assign('product_id', $params['product_id']);
            Tygh::$app['view']->display('addons/sd_like_product/hooks/products/buttons_block.pre.tpl');
            exit;
        }
    } elseif ($mode == 'unlike') {

        if (defined('AJAX_REQUEST') && !empty($params['product_id'])) {
            $total_likes = $params['likes'];
            $company_id = Registry::get('runtime.company_id');
            if (fn_sd_like_remove_like($params['product_id'], $auth['user_id'], $company_id)) {
                Tygh::$app['session']['sd_product_like'][$params['product_id']] = false;
                $total_likes = $params['likes'] - 1;

                $result = fn_sd_like_remove_like_from_queue($params['product_id'], $auth['user_id'], $company_id);
                if (!$result) {
                    fn_sd_like_decrease_likes($params['product_id']);
                }
            }
            Tygh::$app['view']->assign('user_id', $params['user_id']);
            Tygh::$app['view']->assign('likes', $total_likes);
            Tygh::$app['view']->assign('postfix', $params['postfix']);
            Tygh::$app['view']->assign('product_id', $params['product_id']);
            Tygh::$app['view']->display('addons/sd_like_product/hooks/products/buttons_block.pre.tpl');
            exit;
        }
    }

    return;
}
if ($mode == 'cron') {
    if (!empty($params['key']) && $params['key'] == Registry::get('addons.sd_like_product.sd_like_cron_key')) {
        fn_sd_like_product_queue_process();
    } else {
        fn_echo('Access denied');
    }

    exit;
}