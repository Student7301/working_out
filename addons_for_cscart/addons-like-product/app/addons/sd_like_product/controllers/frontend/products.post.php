<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    return;
}

if ($mode == 'my_likes') {

    if (empty($auth['user_id'])) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    $params = $_REQUEST;
    $title = __('products_liked');
    $params['extend'] = array('description');
    $params['my_likes'] = true;

    fn_add_breadcrumb($title);

    list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));

    fn_gather_additional_products_data($products, array('get_icon' => true, 'get_detailed' => true, 'get_additional' => true, 'get_options'=> true));

    $selected_layout = fn_get_products_layout($params);

    Tygh::$app['view']->assign('products', $products);
    Tygh::$app['view']->assign('search', $search);
    Tygh::$app['view']->assign('title', $title);
    Tygh::$app['view']->assign('selected_layout', $selected_layout);
    Tygh::$app['view']->assign('sessions_like', Tygh::$app['session']->getID());
}

if ($mode == 'view' || $mode == 'quick_view') {
    Tygh::$app['view']->assign('sessions_like', Tygh::$app['session']->getID());
}