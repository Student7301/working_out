<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['products']['content']['items']['fillings']['most_liked'] = array (
    'params' => array (
        'most_liked' => true,
        'sort_by' => 'likes',
        'sort_order' => 'desc',
    ),
);

$schema['products']['content']['items']['fillings']['my_likes'] = array (
    'params' => array (
        'my_likes' => true,
        'sort_by' => 'likes',
        'sort_order' => 'desc',
    ),
);

$schema['my_account']['content']['likes_count'] = array (
    'type' => 'function',
    'function' => array('fn_sd_like_product_get_by_user'),
);

return $schema;