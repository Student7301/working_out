<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_sd_like_product_add($product_id, $user_id, $company_id)
{
    $result = false;

    if ($product_id) {
        $check_like = fn_sd_like_product_check_user($product_id, $user_id);

        if (empty($check_like)) {
            $result = db_query('INSERT INTO ?:products_sd_like ?e', array(
                'product_id' => $product_id,
                'user_id' => $user_id,
                'company_id' => $company_id,
                'session_id' => Tygh::$app['session']->getID(),
            ));
            if ($result) {
                fn_sd_like_product_add_queue($product_id, $user_id, $company_id);
            }
        }
    }

    return $result;
}

function fn_sd_like_product_add_queue($product_id, $user_id, $company_id)
{
    $result = false;
    if ($product_id) {
        $result = db_query('INSERT INTO ?:products_sd_like_queue ?e', array(
            'product_id' => $product_id,
            'user_id' => $user_id,
            'company_id' => $company_id,
            'session_id' => Tygh::$app['session']->getID(),
        ));
    }

    return $result;
}

function fn_sd_like_remove_like($product_id, $user_id, $company_id)
{
    $result = false;
    if ($product_id && $user_id) {
        $result = db_query('DELETE FROM ?:products_sd_like ' .
            'WHERE product_id = ?i AND user_id = ?i AND company_id = ?i',
            $product_id, $user_id, $company_id
        );
    } else {
        $session_id = Tygh::$app['session']->getID();
        $result = db_query('DELETE FROM ?:products_sd_like ' .
           'WHERE product_id = ?i AND user_id = ?i AND company_id = ?i AND session_id = ?s',
            $product_id, $user_id, $company_id, $session_id
        );
    }

    return $result;
}

function fn_sd_like_remove_like_from_queue($product_id, $user_id, $company_id)
{
    $result = false;
    if ($product_id ) {
        $result = db_query('DELETE FROM ?:products_sd_like_queue ' .
            'WHERE product_id = ?i AND user_id = ?i AND company_id = ?i AND status = ?s',
            $product_id, $user_id, $company_id, SD_LIKE_STATUS_OPEN
        );
    }

    return $result;
}

function fn_sd_like_decrease_likes($product_id)
{
    if ($product_id) {
        db_query('UPDATE ?:products SET likes = likes - 1 WHERE product_id = ?i', $product_id);
    }
}

function fn_sd_like_product_get($product_id)
{
    $count = 0;

    if ($product_id) {
        $count = db_get_field('SELECT COUNT(*) FROM ?:products_sd_like WHERE product_id = ?i', $product_id);
    }

    return $count;
}

function fn_sd_like_product_get_by_user($user_id)
{
    $count = 0;

    $user_id = empty($user_id) ? Tygh::$app['session']['auth']['user_id'] : 0;
    if ($user_id) {
        $count = db_get_field('SELECT COUNT(*) FROM ?:products_sd_like WHERE user_id = ?i', $user_id);
    }

    return $count;
}

function fn_sd_like_product_check_user($product_id, $user_id)
{
    $result = array();

    if ($product_id) {
        if (!empty($user_id)) {
            $result = db_get_row('SELECT * FROM ?:products_sd_like WHERE user_id = ?i AND product_id = ?i ', $user_id, $product_id);
        } else {
            $session = Tygh::$app['session']->getID();
            $result = db_get_row('SELECT * FROM ?:products_sd_like WHERE user_id = ?i AND product_id = ?i AND session_id = ?s', $user_id, $product_id, $session);
        }
    }

    return $result;
}

function fn_sd_like_product_queue_process()
{
    db_query('UPDATE ?:products_sd_like_queue SET status = ?s WHERE status = ?s', SD_LIKE_STATUS_PROCESSING, SD_LIKE_STATUS_OPEN);
    $queue = db_get_hash_single_array(
        'SELECT product_id, COUNT(*) AS total FROM ?:products_sd_like_queue ' .
        'WHERE status = ?s GROUP BY product_id', array('product_id', 'total'), SD_LIKE_STATUS_PROCESSING
    );
    if ($queue) {
        foreach ($queue as $product_id => $likes) {
            db_query('UPDATE ?:products SET likes = likes + ?i WHERE product_id = ?i', $likes, $product_id);
        }
        db_query('DELETE FROM ?:products_sd_like_queue WHERE status = ?s', SD_LIKE_STATUS_PROCESSING);
    }
}

function fn_sd_like_product_get_products($params, &$fields, &$sortings, &$condition, &$join, $sorting, $group_by, $lang_code, $having)
{
    $fields['likes'] = 'products.likes';
    $sortings['likes'] = 'products.likes';
    if (!empty($params['most_liked'])) {
        $condition .= db_quote(' AND products.likes != 0');
    }
    if (!empty($params['my_likes']) && !empty(Tygh::$app['session']['auth']['user_id'])) {
        $join .= ' LEFT JOIN ?:products_sd_like as product_likes ON products.product_id = product_likes.product_id';
        $condition .= db_quote(' AND product_likes.user_id = ?i', Tygh::$app['session']['auth']['user_id']);
    }
}

function fn_sd_like_product_products_sorting(&$sorting)
{
    if ( Registry::get('runtime.mode') != 'my_likes') {
        $sorting['likes'] = array('description' => __('likes'), 'default_order' => 'asc');
    }
}

function fn_sd_like_product_check_session_user($product_id, $session_id)
{
    $result = false;

    if (!empty($product_id)) {
        $session = Tygh::$app['session']->getID();
        $result = db_get_row('SELECT * FROM ?:products_sd_like WHERE product_id = ?i AND session_id=?i', $product_id, $session_id);
    }

    return $result;
}
