{capture name="mainbox"}
<script type="text/javascript">
    Tygh.$(".cm-tab-tools").click(function(){
        Tygh.$(".cm-hide-inputs").addClass("cm-ajax cm-comet ");
    });
</script>

<form action="{""|fn_url}" method="post" name="xml_import_form" class=" cm-hide-inputs" enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />

{if $datafeeds}
    <table class="table table-middle">
    <thead>
    <tr>
        <th width="1%" class="left">
            {include file="common/check_items.tpl" class="cm-no-hide-input"}</th>
        <th>{__("name")}</th>
        <th>{__("url")}</th>
        <th width="10%">{__("cron_status")}</th>
        <th width="6%">&nbsp;</th>
        <th width="10%" class="right">{__("status")}</th>
    </tr>
    </thead>

    {foreach from=$datafeeds item=xml_import}
        <tr class="cm-row-status-{$xml_import.status|lower}">
            {assign var="allow_save" value=$xml_import}

            {if $allow_save}
                {assign var="no_hide_input" value="cm-no-hide-input"}
            {else}
                {assign var="no_hide_input" value=""}
            {/if}

            <td class="left">
                <input type="checkbox" name="import_profiles_ids[]" value="{$xml_import.import_profiles_id}" class="cm-item {$no_hide_input}" />
            </td>
            <td class="{$no_hide_input}">
                <a class="row-status" href="{"xml_import.update?import_profiles_id=`$xml_import.import_profiles_id`"|fn_url}">{$xml_import.datafeed_name}</a>
                {include file="views/companies/components/company_name.tpl" object=$xml_import}
            </td>
            <td class="{$no_hide_input}">
                {$xml_import.url}
            </td>
            <td class="{$no_hide_input}">
                <span {if $xml_import.cron_status == "xml_complete" && $xml_import.finish_time}title="{$xml_import.finish_time|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}"{/if}>{__($xml_import.cron_status)}</span>
            </td>
            <td>
                {capture name="tools_list"}
                    <li>{btn type="list" text=__("edit") href="xml_import.update?import_profiles_id=`$xml_import.import_profiles_id`"}</li>
                {if $allow_save}
                    <li>{btn type="list" class="cm-confirm" text=__("delete") href="xml_import.delete?import_profiles_id=`$xml_import.import_profiles_id`" method="POST"}</li>
                {/if}
                {/capture}
                <div class="hidden-tools">
                    {dropdown content=$smarty.capture.tools_list}
                </div>
            </td>
            <td class="right">
                {include file="common/select_popup.tpl" id=$xml_import.import_profiles_id status=$xml_import.status hidden=false object_id_name="import_profiles_id" table="import_profiles" popup_additional_class="`$no_hide_input` dropleft"}
            </td>
        </tr>
    {/foreach}
    </table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="buttons"}
    {if $datafeeds}
        {capture name="tools_list"}
            <li>{btn type="delete_selected" dispatch="dispatch[xml_import.m_delete]" form="xml_import_form"}</li>
        {/capture}
    {/if}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="xml_import.add" prefix="top" hide_tools="true" title=__("add") icon="icon-plus"}
{/capture}

</form>
{/capture}

{include file="common/mainbox.tpl" title=__("xml_importer_profiles") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}