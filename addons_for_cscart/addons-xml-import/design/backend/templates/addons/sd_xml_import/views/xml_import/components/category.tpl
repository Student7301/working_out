<tr class="category" id="tr-category-{$category.category_link_id}">
    <td class="left">
        <input type="checkbox" name="category_link_ids[]" value="{$category.category_link_id}" class="checkbox cm-item" />
    </td>
    <td class="{if $category.category_id}{if $category.is_manual == "Y"} manual{else} auto{/if}{else} not-category{/if}">{$category.category}</td>
    <td>
    {if $category.path_names}
        {foreach from=$category.path_names key="path_id" item="path_name" name="path_names"}
        <a target="_blank" class="{if !$smarty.foreach.path_names.last}ty-breadcrumbs__a{else}ty-breadcrumbs__current{/if}" href="{"categories.update&category_id={$path_id}"|fn_url}">{$path_name}</a>{if !$smarty.foreach.path_names.last} / {/if}
        {/foreach}
    {else}
        {__("no_match_is_found")}
    {/if}
     </td>
    <td>
        {if $category.category_id} 
            {if $category.is_manual == "Y"} {__("manually")}{else} {__("automatically")}{/if}
        {/if}
    </td>
    <td>
        <div class="hidden-tools">
            {capture name="tools_list"}
            {if $category.category_id} 
                <li>{include file="pickers/categories/picker.tpl" input_name="link_category_id" but_text="{__("change_category")}" view_mode="list" but_meta="icon" category_link_id=$category.category_link_id }</li>
            {else}
                <li><a class="cm-dialog-opener cm-dialog-auto-size create-category" data-ca-target-id="category_path_popup" category_link_id="{$category.category_link_id}">{__("create_category")}</a></li>
                <li>{include file="pickers/categories/picker.tpl" input_name="link_category_id" but_text="{__("link_category")}" view_mode="list" but_meta="icon" category_link_id=$category.category_link_id }</li>
            {/if}
            {/capture}
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
<!--tr-category-{$category.category_link_id}-->
</tr>