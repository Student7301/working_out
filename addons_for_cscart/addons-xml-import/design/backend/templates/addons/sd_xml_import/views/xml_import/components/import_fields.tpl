{assign var="key" value="0"}
<p>{__("product_name_obligatory")}<br/>{__("category_obligatory")}</p>
<table class="table">
<thead class="cm-first-sibling">
<tr>
    <th>{__("field_name")}</th>
    <th>{__("field_type")}</th>
    <th class="center">{__("active")}</th>
    <th>&nbsp;</th>
</tr>
</thead>

<tbody>
{if $datafeed_data.fields}
{foreach from=$datafeed_data.fields item="field" key="key"}
<tr class="cm-row-item">
    <td>
        <select name="datafeed_data[fields][{$key}][export_field_name]">
            <optgroup label="{__("fields")}">
                <option value="">{__("select_fields")}</option>
                {foreach from=$datafeed_data.fields_mapping item="params" key="_field"}
                    <option value="{$params}" {if $params == $field.export_field_name}selected="selected"{/if}>{$params}</option>
                {/foreach}
            </optgroup>
        </select>
    </td>
    <td>
        {if $export_fields}
            <select name="datafeed_data[fields][{$key}][field]" class="fields">
                {if $field.field == "product"}
                    <option value="product">{__("xml_product_name")}</option>
                {else}
                    <option value="">{__("select_fields")}</option>
                    <optgroup label="{__("fields")}">
                    {foreach from=$export_fields item="params" key="_field"}
                        <option {if $params.db_field} {if $field.field == $params.db_field}selected="selected"{/if} {else} {if $field.field == $_field}selected="selected"{/if} {/if}  value="{if $params.db_field}{$params.db_field}{else}{$_field}{/if}">{$_field}</option>
                    {/foreach}
                    </optgroup>
                    {if $feature_fields}
                        <optgroup label="{__("features")}">
                        {foreach from=$feature_fields item="params" key="_field"}
                            <option {if $field.field == $_field}selected="selected"{/if} value="{$_field}">{$_field}</option>
                        {/foreach}
                        </optgroup>
                    {/if}
                    {if $options_fields}
                        <optgroup label="{__("options")}">
                        {foreach from=$options_fields item="_field"}
                            <option {if $field.field == $_field}selected="selected"{/if} value="{$_field}">{$_field}</option>
                        {/foreach}
                        </optgroup>
                    {/if}
                {/if}
            </select>
        {/if}
    </td>

    <td class="center">
        <input type="hidden" name="datafeed_data[fields][{$key}][avail]" value="{if $field.field == "product"}Y{else}N{/if}" />
        <input type="checkbox" name="datafeed_data[fields][{$key}][avail]" value="Y" {if $field.avail == "Y"}checked="checked"{/if} {if $field.field == "product"}disabled="disabled"{/if}/></td>
    <td>
        {if $field.field != "product"}
            {include file="buttons/clone_delete.tpl" microformats="cm-delete-row" no_confirm=true}
        {/if}
    </td>
</tr>
{/foreach}

{else}
{math equation="x + 1" x=$key assign="key"}
<tr>
    <td>
        <select name="datafeed_data[fields][{$key}][export_field_name]">
            <optgroup label="{__("fields")}">
                <option value="">{__("select_fields")}</option>
                {foreach from=$datafeed_data.fields_mapping item="params" key="_field"}
                    <option value="{$params}">{$params}</option>
                {/foreach}
            </optgroup>
        </select>
    </td>
    <td>
        {if $export_fields}
            <select name="datafeed_data[fields][{$key}][field]" class="fields">
                <option value="product">{__("xml_product_name")}</option>
            </select>
        {/if}
    </td>
    <td class="center">
        <input type="hidden" name="datafeed_data[fields][{$key}][avail]" value="Y" />
        <input type="checkbox" name="datafeed_data[fields][{$key}][avail]"  value="Y" checked="checked" disabled="disabled" />
    </td>
    <td>
        
    </td>
</tr>

{/if}

{math equation="x + 1" x=$key assign="key"}

<tr id="box_add_datafeed_fields">
    <td>
        <select name="datafeed_data[fields][{$key}][export_field_name]">
            <optgroup label="{__("fields")}">
                <option value="">{__("select_fields")}</option>
                {foreach from=$datafeed_data.fields_mapping item="params" key="_field"}
                    <option value="{$params}">{$params}</option>
                {/foreach}
            </optgroup>
        </select>
    </td>
    <td>
        {if $export_fields}
            <select name="datafeed_data[fields][{$key}][field]" class="fields">
                <optgroup label="{__("fields")}">
                <option value="">{__("select_fields")}</option>
                {foreach from=$export_fields item="params" key="_field"}
                    <option value="{if $params.db_field}{$params.db_field}{else}{$_field}{/if}">{$_field}</option>
                {/foreach}
                </optgroup>
                {if $feature_fields}
                    <optgroup label="{__("features")}">
                    {foreach from=$feature_fields item="params" key="_field"}
                        <option value="{$_field}">{$_field}</option>
                    {/foreach}
                    </optgroup>
                {/if}
                {if $options_fields}
                    <optgroup label="{__("options")}">
                    {foreach from=$options_fields item="_field"}
                        <option value="{$_field}">{$_field}</option>
                    {/foreach}
                    </optgroup>
                {/if}
            </select>
        {/if}
    </td>
    <td class="center">
        <input type="hidden" name="datafeed_data[fields][{$key}][avail]" value="N" />
        <input type="checkbox" name="datafeed_data[fields][{$key}][avail]"  value="Y" checked="checked" />
    </td>
    <td>
        {include file="buttons/multiple_buttons.tpl" item_id="add_datafeed_fields"}
    </td>
</tr>
</tbody>
</table>