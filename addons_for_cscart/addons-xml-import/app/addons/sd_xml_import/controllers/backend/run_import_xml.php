<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'import') {
    if (!empty($_REQUEST['cron_password']) && $_REQUEST['cron_password'] == Registry::get('addons.sd_xml_import.cron_password')) {
        if (!empty($_REQUEST['import_id'])) {
            $params['import_id'] = $_REQUEST['import_id'];  
        } else {
            $params = array();
        }

        list($datafeeds) = fn_sd_xml_import_list($params, DESCR_SL);
        
        if (!empty($datafeeds)) {
            $import_profiles_ids = fn_array_column($datafeeds, 'import_profiles_id');
            if (!empty($import_profiles_ids)) {
                db_query('UPDATE ?:import_profiles SET cron_status = ?s WHERE import_profiles_id IN (?n)', 'xml_pending', $import_profiles_ids);
            }

            foreach ($datafeeds as $key => $date) {
                $date['fields'] = unserialize($date['fields']);
                $date['fields_mapping'] = unserialize($date['fields_mapping']);
                $date['export_options'] = unserialize($date['export_options']);
                $date['cron'] = 'Y';

                if (!empty($date['import_profiles_id']) && $date['status'] == 'A') {
                    $import_id = fn_exim_xml_imports($date, $date['import_profiles_id']);
                }
            }
        }
        fn_print_die(__('sd_import_completed'));
    } else {
        die(__('access_denied'));
    }
}